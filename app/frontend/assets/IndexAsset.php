<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class IndexAsset extends AssetBundle
{
    public $sourcePath = '@frontend/assets';

    public $css = [
        'css/index.css',
        'css/slick.css',
    ];
    public $js = [
        'js/slick.min.js',
        'js/index.js',
    ];
    public $depends = [
        'frontend\assets\AppAsset',
    ];
}