<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class VueJsAsset extends AssetBundle
{
    public $sourcePath = '@frontend/assets';

    public $js = [];

    public function init()
    {
        $isDev = getenv('YII_ENV') == 'dev';

        $this->publishOptions['forceCopy'] = $isDev;

        $this->js = $isDev
            ? ['vuejs/vue.js', 'vuejs/vue-router.min.js', 'vuejs/vue-resource.min.js']
            : ['vuejs/vue.min.js', 'vuejs/vue-router.min.js', 'vuejs/vue-resource.min.js'];

        parent::init();
    }
}