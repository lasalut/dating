$(document).ready(function() {
	// подсказки всплывающие
	$('[data-toggle="tooltip"]').tooltip();
	var onRegister = false;

	// выравнивание попап заголовков по центру
	$('.box-header, .auto-center').each(function() {
		var $this = $(this);
		if ($this.css('margin-left') == null || $this.css('margin-left') == '0px') {
			$this.css('margin-left', 0 - $this.outerWidth() / 2);
		}
	});

	// в мобильной версии при модальном окне прокрутка наверх
	$('.modal.fade').on('shown.bs.modal', function (e) {
		$(window).scrollTop(0);
	});

	// восстановления пароля
	$('#remind-email').val('');
	$('#remind-phone').val('');

	$('#nav-remind a[href="#tab-remind-phone"]').click(function() {
		$('#remind-email').val('');
		setTimeout(function() { $('#remind-phone').focus(); }, 500);
	});

	$('#nav-remind a[href="#tab-remind-email"], #remindForm-btn').click(function() {
		$('#loginForm').modal('hide');
		$('#remind-phone').val('');
		setTimeout(function() { $('#remind-email').focus(); }, 500);
	});

	$('#remind-form').submit(function(e) {
		e.preventDefault();
		var data = $('#remind-form').serialize();

		$.ajax({
			type:    'POST',
			url:     '/profiles/auth/remind',
			data:    data,
			success: function(data) {
				$("#remind-errors li").remove();
				$("#remind-info").html(data.msg).show();
			},
			error:   function(xhr) {
				$("#remind-info").html('').hide();
				var data = xhr.responseJSON;
				if ("errors" in data) {
					$("#remind-errors li").remove();
					for (var i = 0; i < data.errors.length; i++) {
						$("#remind-errors").append("<li>" + data.errors[i] + "</li>");
					}
				}
			}
		});
	});

	// регистрация
	$('#register-form').submit(function(e) {
		e.preventDefault();

		if (onRegister) {
			return false;
		}

		onRegister = true;
		var data = $('#register-form').serialize();

		$.ajax({
			type:    'POST',
			url:     '/profiles/auth/register',
			data:    data,
			success: function(data) {
				onRegister = false;
				window.location = data.redirect;
			},
			error:   function(xhr) {
				onRegister = false;
				var data = xhr.responseJSON;
				if ("errors" in data) {
					$("#reg-errors li").remove();
					for (var i = 0; i < data.errors.length; i++) {
						$("#reg-errors").append("<li>" + data.errors[i] + "</li>");
					}
				}
			}
		});
	});

	// вход на сайт
	$('#login-email').val('');
	$('#login-phone').val('');

	$('#nav-login a[href="#tab-login-phone"]').click(function() {
		$('#login-email').val('');
		setTimeout(function() { $('#login-phone').focus(); }, 500);
	});

	$('#nav-login a[href="#tab-login-email"], #login-btn').click(function() {
		$('#login-phone').val('');
		setTimeout(function() { $('#login-email').focus(); }, 500);
	});

	$('#login-form').submit(function(e) {
		e.preventDefault();
		var data = $('#login-form').serialize();

		$.ajax({
			type:    'POST',
			url:     '/profiles/auth/login',
			data:    data,
			success: function(data) {
				window.location = data.redirect;
			},
			error:   function(xhr) {
				var data = xhr.responseJSON;
				if ("errors" in data) {
					$("#login-errors li").remove();
					for (var i = 0; i < data.errors.length; i++) {
						$("#login-errors").append("<li>" + data.errors[i] + "</li>");
					}
				}
			}
		});
	});

	// слайдер выбор возраста
	$(".slide-years input").slider({
		focus: true
	}).on('change', function(event) {
		$('.search-from').val(event.value.newValue[0]);
		$('.search-to').val(event.value.newValue[1]);
	});

	$("#search-city").autocomplete({
		source:    function(request, response) {
			var url = '/profiles/auth/city?term=' + request.term.trim();
			$.getJSON(url, function(data) {
				if (!data.length) {
					var result = [{
						label: '* Населенный пункт не найден',
						value: response.term
					}];
					response(result);
				}
				else {
					response($.map(data, function(item) {
						return {
							label: item,
							value: item
						}
					}));
				}
			});
		},
		select:    function(event, ui) {
			if (ui.item) {
				$(this).val(ui.item.value);
			}
		},
		minLength: 2,
		autoFocus: true,
		limit:     15
	}).data("ui-autocomplete")._renderItem = function(ul, item) {
		return $('<li class="aut"></li>')
			.data("item.autocomplete", item)
			.append("<a>" + item.label + "</a>")
			.appendTo(ul);
	};

	if (typeof connOnlines !== 'undefined') {
		updateOnlines();

		connOnlines = new ab.Session(webSocket + ':8080',
			function() {
				connOnlines.subscribe('all', function(subscriber, data) {
					if (data.type === 'onlines') {
						var ids = data.object.ids;
						$(".ua").each(function() {
							var ua = this.getAttribute('data-ua');
							ids.indexOf(ua) !== -1 ? $(this).addClass("ua-online") : $(this).removeClass("ua-online");
						});
					}
				});
			},
			function() {
				console.warn('WebSocket connection closed');
			},
			{'skipSubprotocolCheck': true}
		);
	}
});

// обновление статуса он-лайн участников
function updateOnlines() {
	$.getJSON('/profiles/auth/onlines', function(data) {
		$(".ua").each(function() {
			var ua = this.getAttribute('data-ua');
			data.indexOf(ua) !== -1 ? $(this).addClass("ua-online") : $(this).removeClass("ua-online");
		});
	});
}