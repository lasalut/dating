$(document).ready(function() {

	$("#register-btn").click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $("#registerForm").offset().top
		}, 1000, function() { $('#register-form input[type="email"]').focus() });
	});

	// BLOGS @ index page
	var $blogs = $('#blogs');

	$blogs.slick({
		dots:           false,
		infinite:       true,
		speed:          300,
		slidesToShow:   1,
		adaptiveHeight: true,
		arrows:         false
	});

	$('.slick-prev').click(function() {
		$blogs.slick('slickPrev');
	});

	$('.slick-next').click(function() {
		$blogs.slick('slickNext');
	});

	$blogs.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
		$('.slick-curr').html(nextSlide + 1);
	})
});