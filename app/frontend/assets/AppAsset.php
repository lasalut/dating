<?php

namespace frontend\assets;

use modules\profiles\common\models\Profile;
use yii\web\AssetBundle;
use yii\web\View;

class AppAsset extends AssetBundle
{
    public $sourcePath = '@frontend/assets';

    public $css = [
        'https://fonts.googleapis.com/css?family=Open+Sans|Playfair+Display',
        'bootstrap-slider/css/bootstrap-slider.min.css',
        'jquery-ui/jquery-ui.min.css',
        'css/main.css',
    ];

    public $js = [
        'bootstrap-slider/bootstrap-slider.min.js',
        'jquery-ui/jquery-ui.min.js',
        'js/autobahn.js',
        'js/main.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yz\assets\YzAsset',
        'yii\web\JqueryAsset',
        'yii\widgets\MaskedInputAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
        'frontend\assets\VueJsAsset',
    ];

    public function registerAssetFiles($view)
    {
        /** @var Profile $profile */
        $profile = \Yii::$app->user->identity;
        $profile_id = $profile ? $profile->id : '';
        $ws = getenv('WS');

        $js = <<<JS
            const myProfileId = '{$profile_id}';
            const webSocket = '{$ws}';
JS;
        $view->registerJs($js, View::POS_HEAD, __CLASS__);

        return parent::registerAssetFiles($view);
    }
}