<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class NicescrollAsset extends AssetBundle
{
    public $sourcePath = '@frontend/assets';

    public $js = [
        'nicescroll/jquery.nicescroll.min.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}