<form id="login-form">
    <div class="form-group form-icon">
        <input type="text" class="form-control" placeholder="Номер телефона или email"
			   name="login" required id="login-login"/>
        <span class="fa fa-envelope input-icon"></span>
    </div>
    <div class="form-group form-icon">
        <input type="password" class="form-control" placeholder="Пароль"
			   name="password" required id="login-password"/>
        <span class="fa fa-lock input-icon"></span>
    </div>
	<ul class="errors" id="login-errors"></ul>
    <div class="login-restore">
        <a href="/profiles/auth/restore">Восстановить пароль</a>
    </div>
    <div class="form-submit">
        <button type="submit" class="btn btn-primary">Войти на сайт</button>
    </div>
</form>