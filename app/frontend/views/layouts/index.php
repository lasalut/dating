<?php

use frontend\assets\IndexAsset;
use modules\blog\common\models\Blog;
use modules\profiles\common\models\Profile;
use yii\bootstrap\Nav;
use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var string $content
 */

IndexAsset::register($this);
?>

<?php $this->beginPage() ?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<title><?= empty($this->title) ? 'La-Salut - знакомства онлайн' : $this->title ?></title>
		<link href="/favicon3.ico" rel="shortcut icon" type="image/x-icon">
		<meta name="viewport" content="width=device-width">
        <?= Html::csrfMetaTags() ?>
        <?php $this->head() ?>
	</head>

	<body id="body">
    <?php $this->beginBody() ?>

	<div id="header">
		<div class="container header-line">
			<a href="/" class="link-home pull-left">
				<img src="/images/index/logo_index.jpg"/>
			</a>
            <?= Nav::widget([
                'encodeLabels' => false,
                'items' => [
                    ['label' => 'О ПРОЕКТЕ', 'url' => ['/about']],
                ],
                'options' => ['class' => 'navbar-nav'],
            ]);
            ?>
			<div class="pull-right trans-buttons">
				<a href="#loginForm" role="button" class="btn btn-default" id="login-btn" data-toggle="modal">ВОЙТИ</a>
				<a href="#" class="btn btn-default" id="register-btn">РЕГИСТРАЦИЯ</a>
			</div>
		</div>
        <?= $this->render('_index_search') ?>
	</div>

    <?= $this->render('_index_login') ?>
    <?= $this->render('_remind') ?>

	<div class="wrapper">
		<div class="container content">
            <?= $this->render('_alerts') ?>
            <?= $content ?>
		</div>
        <?= $this->render('_blogs') ?>
	</div>

	<div id="pre-footer">
        <?= $this->render('_index_register') ?>
	</div>

    <?= $this->render('_footer') ?>

    <?php $this->endBody() ?>
	</body>
	</html>
<?php $this->endPage() ?>