<?php
/** @var Profile $profile */

use modules\profiles\common\models\Premium;
use modules\profiles\common\models\Profile;

/** @var Premium[] $premiums */
$premiums = Premium::find()->where(['like', 'code', 'premium-'])->orderBy(['code' => SORT_ASC])->all();

/** @var Premium[] $premiumsPlus */
$premiumsPlus = Premium::find()->where(['like', 'code', 'plus-'])->orderBy(['code' => SORT_ASC])->all();

?>

<div id="premiumForm" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content" style="width:800px">
			<button type="button" class="close" data-dismiss="modal">×</button>

			<div class="box-wrap auto-center" id="auth-block">
				<div class="box">
					<div class="box-header" style="margin-left:-120px">
						Купить премиум
					</div>
					<div class="box-content">
						<div class="premium-buy">
							<form id="premium-form">
                                <?php if (count($premiums)): ?>
                                    <?php foreach ($premiums as $i => $premium): ?>
										<?php if ($i == 0): ?>
											<div class="premium-header center">
												<?= $premium->package ?>
											</div>
											<div class="row premium-bonuses">
												<div class="col-md-4 col-premium center">
													<div class="premium-icon">
														<i class="fa fa-comments"></i>
													</div>
													Общение без ограничений – пишите понравившимся девушкам.
												</div>
												<div class="col-md-4 col-premium center">
													<div class="premium-icon">
														<i class="fa fa-sliders"></i>
													</div>
													Расширенные критерии поиска – найдите любовницу по нужным параметрам.
												</div>
												<div class="col-md-4 col-premium center">
													<div class="premium-icon">
														<i class="fa fa-random"></i>
													</div>
													Обмен контактными данными – получайте телефоны и e-mail.
												</div>
											</div>
										<?php endif; ?>
										<div class="form-row premium-radio">
											<label for="<?= $premium->code ?>">
												<input type="radio" name="premium" required="required"
													   id="<?= $premium->code ?>" value="<?= $premium->code ?>"/>
												<span class="premium-title"><?= $premium->title ?></span><br/>
												<span class="premium-price"><?= $premium->price ?> р.</span>
											</label>
										</div>
                                    <?php endforeach ?>
                                <?php endif; ?>

                                <?php if (count($premiumsPlus)): ?>
                                    <?php foreach ($premiumsPlus as $i => $premium): ?>
                                        <?php if ($i == 0): ?>
											<div class="premium-header plus center">
                                                <?= $premium->package ?>
											</div>
											<div class="row premium-bonuses">
												<div class="col-md-4 col-premium center">
													<div class="premium-icon">
														<i class="fa fa-comments"></i>
													</div>
													Общение без ограничений – пишите понравившимся девушкам.
												</div>
												<div class="col-md-4 col-premium center">
													<div class="premium-icon">
														<i class="fa fa-sliders"></i>
													</div>
													Расширенные критерии поиска – найдите любовницу по нужным параметрам.
												</div>
												<div class="col-md-4 col-premium center">
													<div class="premium-icon">
														<i class="fa fa-random"></i>
													</div>
													Обмен контактными данными – получайте телефоны и e-mail.
												</div>
											</div>
											<div class="row premium-bonuses">
												<div class="col-md-4 col-premium center">
													<div class="premium-icon">
														<i class="fa fa-user-secret"></i>
													</div>
													Режим «Инкогнито»
												</div>
												<div class="col-md-4 col-premium center">
													<div class="premium-icon">
														<i class="fa fa-globe"></i>
													</div>
													Функция «Кто онлайн?»
												</div>
												<div class="col-md-4 col-premium center">
													<div class="premium-icon">
														<i class="fa fa-diamond"></i>
													</div>
													Возможность просматривать фото 18+
												</div>
											</div>
                                        <?php endif; ?>
										<div class="form-row premium-radio">
											<label for="<?= $premium->code ?>">
												<input type="radio" name="premium" required="required"
													   id="<?= $premium->code ?>" value="<?= $premium->code ?>"/>
												<span class="premium-title"><?= $premium->title ?></span><br/>
												<span class="premium-price">
													<?= $premium->price ?> р.
												</span>
											</label>
										</div>
                                    <?php endforeach ?>
                                <?php endif; ?>

								<div class="form-row form-submit">
									<button type="submit" class="btn btn-primary">Купить</button>
								</div>
							</form>

							<div class="premium-note center">
								Мы не используем подписки с автоплатежами – оплачивайте
								премиум-доступ только тогда, когда вам это нужно!
							</div>
						</div>

						<div id="premium-info"></div>
						<ul class="errors" id="premium-errors"></ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>