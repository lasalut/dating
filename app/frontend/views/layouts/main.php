<?php

use frontend\assets\AppAsset;
use frontend\assets\IndexAsset;
use modules\profiles\common\models\Profile;
use modules\profiles\frontend\assets\ProfileAsset;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Nav;
use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var string $content
 */

/** @var Profile $profile */
$profile = Yii::$app->user->isGuest ? null : Yii::$app->user->identity;
$pathInfo = Yii::$app->request->pathInfo;

AppAsset::register($this);
if ($profile) {
    ProfileAsset::register($this);
}
?>

<?php $this->beginPage() ?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<title><?= empty($this->title) ? 'La-Salut - знакомства онлайн' : $this->title ?></title>
		<link href="/favicon3.ico" rel="shortcut icon" type="image/x-icon">
		<meta name="viewport" content="width=device-width">

        <?= Html::csrfMetaTags() ?>
        <?php $this->head() ?>
	</head>

	<body id="body" class="<?= $profile ? 'body-profile' : 'body-guest' ?> page-<?= $pathInfo ?>">
    <?php $this->beginBody() ?>

    <?= $this->render('_main_header', ['profile' => $profile]) ?>
    <?= $this->render('_main_search', ['profile' => $profile]) ?>

	<?php if ($profile): ?>
        <?= $this->render('_premium', ['profile' => $profile]) ?>
	<?php endif; ?>

	<div class="wrapper main">
		<div class="container">
			<div class="content">
                <?= $this->render('_alerts') ?>
                <?= $content ?>
			</div>
		</div>
	</div>

    <?php if (isset($this->blocks['after-content'])): ?>
		<div class="after-content">
            <?= $this->blocks['after-content']; ?>
		</div>
    <?php endif; ?>

    <?= $this->render('_footer') ?>

    <?php if ($profile): ?>
        <?= $this->render('_mobile_menu', compact('profile')) ?>
    <?php endif; ?>

    <?php $this->endBody() ?>
	</body>
	</html>
<?php $this->endPage() ?>