<?php
# года поиска и город
$from = Yii::$app->request->get('from', 22);
$to = Yii::$app->request->get('to', 35);
$city = Yii::$app->request->get('c');

$maleSelected = false;

if ($type = Yii::$app->request->get('t')) {
    $maleSelected = $type == 'm';
}
?>

<div class="container" id="auth-block">
	<div class="box-wrap index" id="registerForm">
		<div class="box">
			<div class="box-header" style="margin-left: -95px;">
				Регистрация
			</div>
			<div class="box-content">
				<form id="register-form">
					<ul class="errors" id="reg-errors"></ul>
					<div class="form-row">
						<input type="text" class="full" placeholder="Эл. почта или Тел." name="emailPhone" required/>
					</div>
					<div class="row form-row">
						<div class="col-md-8">
							<input type="text" class="full" placeholder="Ваше имя" name="name" required/>
						</div>
						<div class="col-md-4 genders">
							<div class="btn-group" data-toggle="buttons">
								<label class="btn btn-default genders-male">
									<input type="radio" name="my_gender" value="male" autocomplete="off" required>М
								</label>
								<label class="btn btn-default genders-female">
									<input type="radio" name="my_gender" value="female" autocomplete="off">Ж
								</label>
							</div>
						</div>
					</div>

					<div class="form-group form-row">
						<div class="box-title">
							<div>Дата рождения:</div>
						</div>
						<div class="birthdate row">
							<div class="col-md-4">
								<select class="form-control full" name="day" required>
                                    <?php $options = ['prompt' => 'день']; ?>
                                    <?= \yii\helpers\Html::renderSelectOptions(null, \modules\profiles\common\models\Profile::dayOptions(), $options) ?>
								</select>
							</div>
							<div class="col-md-4">
								<select class="form-control full" name="month" required>
                                    <?php $options = ['prompt' => 'месяц']; ?>
                                    <?= \yii\helpers\Html::renderSelectOptions(null, \modules\profiles\common\models\Profile::monthOptions(), $options) ?>
								</select>
							</div>
							<div class="col-md-4" name="year">
								<select class="form-control full" name="year" required>
                                    <?php $options = ['prompt' => 'год']; ?>
                                    <?= \yii\helpers\Html::renderSelectOptions(null, \modules\profiles\common\models\Profile::yearOptions(), $options) ?>
								</select>
							</div>
						</div>
					</div>
					<div class="form-submit form-row">
						<button type="submit" class="btn btn-primary">Зарегистрироваться</button>
					</div>
					<div class="form-row">
						<a href="#loginForm" role="button" class="link-restore" data-toggle="modal">Войти</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
