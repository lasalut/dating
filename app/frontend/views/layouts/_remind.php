<div id="remindForm" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content" style="max-width:500px">
			<button type="button" class="close" data-dismiss="modal">×</button>

			<div class="box-wrap auto-center" id="auth-block">
				<div class="box">
					<div class="box-header" style="margin-left:-110px">
						Забыли пароль
					</div>
					<div class="box-content">
						<form id="remind-form">
							<div class="form-row">
								<ul class="nav nav-tabs" id="nav-remind">
									<li class="active">
										<a data-toggle="tab" href="#tab-remind-email">E-mail</a>
									</li>
									<li><a data-toggle="tab" href="#tab-remind-phone">Номер телефона</a></li>
								</ul>

								<div class="tab-content">
									<div id="tab-remind-email" class="tab-pane fade in active">
										<input type="text" name="email" id="remind-email" class="full"
											   placeholder="E-mail адрес"/>
									</div>
									<div id="tab-remind-phone" class="tab-pane fade">
                                        <?= \yii\widgets\MaskedInput::widget([
                                            'id' => 'remind-phone',
                                            'name' => 'phone',
                                            'mask' => '+7 999 999-99-99',
                                            'options' => ['class' => 'full'],
                                        ]); ?>
									</div>
								</div>
							</div>
							<div class="form-row form-submit">
								<button type="submit" class="btn btn-primary">Восстановить</button>
							</div>
							<div class="info-msg" id="remind-info"></div>
							<ul class="errors" id="remind-errors"></ul>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>