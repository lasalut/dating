<?php
/** @var Profile $profile */

use modules\chat\common\models\ChatMessage;
use modules\profiles\common\models\Profile;
use modules\profiles\common\models\ProfileLike;
use modules\profiles\common\models\ProfileVisit;
use modules\profiles\frontend\controllers\VisitsController;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Nav;
use yii\helpers\Url;

$msgCounter = $profile ? ChatMessage::countByProfile($profile) : null;
$visitsCounter = $profile ? ProfileVisit::countByProfile($profile) : null;
$likesCounter = $profile ? ProfileLike::countByProfile($profile) : null;
?>

<div id="header" class="clearfix">
	<div class="container">
		<div class="header-menu">
            <?php if ($profile): ?>
				<a href="#" id="mobile-menu-icon">
					<div class="bar1"></div>
					<div class="bar2"></div>
					<div class="bar3"></div>
				</a>
				<a href="#" id="mobile-menu-hide">
					<i class="fa fa-times"></i>
				</a>
            <?php endif; ?>

			<a href="/" class="link-home pull-left">
				<img src="/images/logo.jpg"/>
			</a>

            <?= Nav::widget([
                'encodeLabels' => false,
                'items' => [
                    ['label' => 'О ПРОЕКТЕ', 'url' => ['/about']],
                    ['label' => 'ПОИСК', 'url' => ['/search']],
                ],
                'options' => ['class' => 'navbar-nav', 'id' => 'nav-top'],
            ]);
            ?>

			<div id="header-controls" class="pull-right">
                <?php if ($profile): ?>
					<a href="/finance" id="btn-wallet">
						<img src="/images/fin/wallet.png"/>
						<span>Счет: <span class="my-balance"><?= $profile->purse->balance ?></span> р.</span>
					</a>

					<a class="btn btn-default beige-border btn-icon" id="btn-views"
					   title="Просмотры анкеты" data-toggle="tooltip" data-placement="bottom"
					   href="<?= Url::to(['/visits', 's' => VisitsController::SECTION_VISITS]) ?>">
						<i class="counter <?= $visitsCounter ? 'display-inline' : '' ?>"
						   id="visits-counter"><?= $visitsCounter ? $visitsCounter : '' ?></i>

						<img src="/images/icons/icon_eye.png"/>
					</a>
					<a class="btn btn-default beige-border btn-icon" id="btn-likes"
					   title="Симпатии" data-toggle="tooltip" data-placement="bottom"
					   href="<?= Url::to(['/visits', 's' => VisitsController::SECTION_LIKES]) ?>">
						<i class="counter <?= $likesCounter ? 'display-inline' : '' ?>"
						   id="likes-counter"><?= $likesCounter ? $likesCounter : '' ?></i>

						<img src="/images/icons/icon_heart.png"/>
					</a>
					<a class="btn btn-default beige-border btn-icon last"
					   title="Мои сообщения" data-toggle="tooltip" data-placement="bottom"
					   id="btn-messages" href="/chat">
						<i class="counter <?= $msgCounter ? 'display-inline' : '' ?>"
						   id="messages-counter"><?= $msgCounter ? $msgCounter : '' ?></i>

						<img src="/images/icons/icon_envelope.png"/>
					</a>

                    <?= ButtonDropdown::widget([
                        'label' => $profile->name,
                        'dropdown' => [
                            'encodeLabels' => false,
                            'items' => [
                                ['label' => '<i class="fa fa-user" style="width:20px"></i> Анкета', 'url' => ['/profile/' . $profile->id]],
                                ['label' => '<i class="fa fa-photo" style="width:20px"></i> Фотоальбом', 'url' => ['/photo/albums']],
                                ['label' => '<i class="fa fa-rub" style="width:20px"></i> Мой кошелек', 'url' => ['/finance']],
                                ['label' => '<i class="fa fa-cog" style="width:20px"></i> Настройки', 'url' => ['/settings']],
                                ['label' => '<i class="fa fa-sign-out" style="width:20px"></i> Выход', 'url' => ['/logout'], 'options' => ['class' => 'last']],
                            ],
                        ],
                        'options' => ['class' => 'btn-default profile-menu'],
                    ]) ?>
                <?php else: ?>
                    <?= $this->render('_main_auth') ?>
                <?php endif; ?>
			</div>
		</div>
	</div>
</div>