<?php
/** @var Blog[] $blogs */

use modules\blog\common\models\Blog;

$blogs = Blog::find()->where([
    'enabled' => true,
    'category' => Blog::CATEGORY_INDEX
])->orderBy(['id' => SORT_ASC])->all();
?>

<?php if (!empty($blogs)): ?>
	<div class="after-content blogs noselect">
		<div class="container" id="blogs">
            <?php foreach ($blogs as $i => $blog): ?>
				<div class="row blog <?= $i == 0 ? 'active' : '' ?>">
					<div class="col-md-5 blog-image">
						<img src="<?= $blog->square_url ?>"/>
					</div>
					<div class="col-md-7 blog-info">
						<div class="blog-title serif"><?= $blog->title ?></div>
						<div class="blog-separator"><i></i></div>
						<div class="blog-content">
                            <?= $blog->content ?>
						</div>
					</div>
				</div>
            <?php endforeach; ?>
		</div>

		<div class="blogs-paginator">
			<span class="slick-prev">
				<img src="/images/index/arr-left.png"/>
			</span>
			<span class="slick-curr serif">1</span>
			<span class="slick-total serif">/<?= count($blogs) ?></span>
			<span class="slick-next">
				<img src="/images/index/arr-right.png"/>
			</span>
		</div>
	</div>
<?php endif; ?>
