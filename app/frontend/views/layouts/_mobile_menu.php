<?php
/** @var \modules\profiles\common\models\Profile $profile */

use modules\chat\common\models\ChatMessage;
use modules\profiles\common\models\ProfileVisit;

/** @var integer $visitsCounter */
/** @var integer $likesCounter */
/** @var integer $msgCounter */

$pathInfo = Yii::$app->request->pathInfo;

$msgCounter = $profile ? ChatMessage::countByProfile($profile) : null;
$visitsCounter = $profile ? ProfileVisit::countByProfile($profile) : null;
?>

<nav id="mobile-menu">
	<ul>
		<li class="with-avatar">
			<a href="/profile/<?= $profile->id ?>" class="row">
				<div class="col-xs-3 col-avatar">
					<img src="<?= $profile->avatar_url ?>"/>
				</div>
				<div class="com-xs-9">
					<div class="mobile-username">
                        <?= $profile->name ?>, <?= $profile->years ?>
					</div>
                    <?php if ($city = $profile->city): ?>
						<div class="mobile-city">
                            <?= $city->title ?>, <?= $city->country->title ?>
						</div>
                    <?php endif; ?>
				</div>
			</a>
		</li>
		<li class="<?= $pathInfo == 'search' ? 'active' : '' ?>">
			<a href="/search"><i class="fa fa-search"></i>Поиск</a>
		</li>
		<li class="<?= $pathInfo == 'chat' ? 'active' : '' ?>">
			<a href="/chat"><i class="fa fa-comment-o"></i>Сообщения
				<i class="counter <?= $msgCounter ? 'display-inline' : '' ?>"
				   id="messages-counter"><?= $msgCounter ? $msgCounter : '' ?></i>
			</a>
		</li>
		<li class="<?= $pathInfo == 'visits' ? 'active' : '' ?>">
			<a href="/visits?s=visits"><i class="fa fa-eye"></i>Кто смотрел
				<i class="counter <?= $visitsCounter ? 'display-inline' : '' ?>"
				   id="visits-counter"><?= $visitsCounter ? $visitsCounter : '' ?></i>
			</a>
		</li>
		<li class="<?= $pathInfo == 'broadcast' ? 'active' : '' ?>">
			<a href="/broadcast"><i class="fa fa-send-o"></i>Рассылка</a>
		</li>
		<li class="<?= $pathInfo == 'calls' ? 'active' : '' ?>">
			<a href="/calls"><i class="fa fa-phone"></i>Звонки</a>
		</li>
		<li class="<?= $pathInfo == 'finance' ? 'active' : '' ?>">
			<a href="/finance"><i class="fa fa-rub"></i>Кошелек
				<span class="mobile-purse"><?= $profile->purse->balance ?> р.</span>
			</a>
		</li>
		<li class="mobile-gray <?= $pathInfo == 'photo/albums' ? 'active' : '' ?>">
			<a href="/photo/albums"><i class="fa fa-photo"></i>Фотоальбом</a>
		</li>
		<li class="mobile-gray <?= $pathInfo == 'settings' ? 'active' : '' ?>">
			<a href="/settings"><i class="fa fa-cog"></i>Настройки</a>
		</li>
		<li class="mobile-gray <?= $pathInfo == 'about' ? 'active' : '' ?>">
			<a href="/about"><i class="fa fa-heart-o"></i>О проекте</a>
		</li>
		<li class="mobile-gray <?= $pathInfo == 'blogs' || strpos($pathInfo, 'blog/') !== false ? 'active' : '' ?>">
			<a href="/blogs"><i class="fa fa-newspaper-o"></i>Блог</a>
		</li>
		<li class="mobile-gray <?= $pathInfo == 'feedback' || strpos($pathInfo, 'feedback/') !== false ? 'active' : '' ?>">
			<a href="/feedback"><i class="fa fa-commenting-o"></i>Обратная связь</a>
		</li>
		<li class="mobile-gray <?= $pathInfo == 'confidential' ? 'active' : '' ?>">
			<a href="/confidential"><i class="fa fa-user-secret"></i>Конфиденциальность</a>
		</li>
		<li class="mobile-gray">
			<a href="/logout"><i class="fa fa-sign-out"></i> Выход</a>
		</li>
	</ul>
</nav>