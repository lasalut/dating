<?php
# года поиска и город
use modules\profiles\frontend\controllers\SearchController;

$from = Yii::$app->request->get('from', 22);
$to = Yii::$app->request->get('to', 35);
$city = Yii::$app->request->get('c');

$maleSelected = false;

if ($type = Yii::$app->request->get('t')) {
    $maleSelected = $type == 'm';
}

$section = Yii::$app->request->get('s', SearchController::SECTION_ALL);
?>

<div class="container box-wrap" id="header-search">
	<div class="box">
		<div class="box-header">
			<div>Я ищу:</div>
		</div>
		<div class="box-content">
			<form method="GET" action="/search">
				<select class="form-control" id="search-for" name="t">
					<option value="f"<?= $maleSelected ? '' : ' selected' ?>>Девушку</option>
					<option value="m"<?= $maleSelected ? ' selected' : '' ?>>Мужчину</option>
				</select>

				<input id="search-city" type="text" class="form-control" placeholder="из города"
					   name="c" value="<?= $city ?>"/>

				<span id="age-label">Возраст</span>
				<div class="search-years">
					<input type="text" name="from" value="<?= $from ?>" class="search-from"/> -
					<input type="text" name="to" value="<?= $to ?>" class="search-to"/>

					<div class="slide-years">
						<input type="text" data-slider-tooltip="hide"
							   class="span2" value="" data-slider-min="18" data-slider-handle="custom"
							   data-slider-max="70" data-slider-step="1"
							   data-slider-value="[<?= $from ?>, <?= $to ?>]"/>
					</div>
					<div class="search-min">18</div>
					<div class="search-max">70</div>
				</div>
				<input type="hidden" name="s" value="<?= $section ?>"/>
				<button id="search-btn" type="submit" class="btn btn-beige pull-right">Искать</button>
			</form>
		</div>
	</div>
</div>
