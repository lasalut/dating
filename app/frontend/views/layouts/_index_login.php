<div id="loginForm" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content" style="max-width:500px">
			<button type="button" class="close" data-dismiss="modal">×</button>

			<div class="box-wrap auto-center" id="auth-block">
				<div class="box">
					<div class="box-header" style="margin-left:-36px">
						Вход
					</div>
					<div class="box-content">
						<form id="login-form">
							<div class="form-row">
								<ul class="nav nav-tabs" id="nav-login">
									<li class="active">
										<a data-toggle="tab" href="#tab-login-email">E-mail</a>
									</li>
									<li><a data-toggle="tab" href="#tab-login-phone">Номер телефона</a></li>
								</ul>

								<div class="tab-content">
									<div id="tab-login-email" class="tab-pane fade in active">
										<input id="login-email-fake" style="display:none" type="text" name="fakeusernameremembered">
										<input type="text" name="email" id="login-email" class="full"
											   placeholder="E-mail адрес" autocomplete="nope"/>
									</div>
									<div id="tab-login-phone" class="tab-pane fade">
                                        <?= \yii\widgets\MaskedInput::widget([
                                            'id' => 'login-phone',
                                            'name' => 'phone',
                                            'mask' => '+7 999 999-99-99',
                                            'options' => ['class' => 'full'],
                                        ]); ?>
									</div>
								</div>
							</div>
							<div class="form-row">
								<input type="password" placeholder="Пароль" name="password"
									   id="login-password" class="full" autocomplete="new-password"/>
							</div>
							<div class="form-row form-submit">
								<button type="submit" class="btn btn-primary">Войти на сайт</button>
							</div>
							<div style="text-align: right">
								<a href="#remindForm" role="button" class="link-restore" id="remindForm-btn"
								   data-toggle="modal">
									Забыли пароль?
								</a>
							</div>
							<ul class="errors" id="login-errors"></ul>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>