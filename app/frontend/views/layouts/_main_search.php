<?php
/** @var Profile $profile */

use modules\profiles\common\models\Profile;

# года поиска и город
$from = Yii::$app->request->get('from', 22);
$to = Yii::$app->request->get('to', 35);
$city = Yii::$app->request->get('c');

$maleSelected = false;

if ($type = Yii::$app->request->get('t')) {
    $maleSelected = $type == 'm';
}
elseif ($profile) {
    $maleSelected = $profile->lookingForMale();
}
?>

<div class="container header-search" id="main-search">
	<form method="GET" action="/search">
		<label for="search-for">Я ищу</label>
		<select class="form-control" id="search-for" name="t">
			<option value="f"<?= $maleSelected ? '' : ' selected' ?>>Девушку</option>
			<option value="m"<?= $maleSelected ? ' selected' : '' ?>>Мужчину</option>
		</select>

		<div class="search-years">
			<input type="text" name="from" value="<?= $from ?>" class="search-from"/> -
			<input type="text" name="to" value="<?= $to ?>" class="search-to"/> лет

			<div class="slide-years">
				<input type="text" data-slider-tooltip="hide"
					   class="span2" value="" data-slider-min="18" data-slider-handle="custom"
					   data-slider-max="80" data-slider-step="1"
					   data-slider-value="[<?= $from ?>, <?= $to ?>]"/>
			</div>
		</div>

		<input id="search-city" type="text" class="form-control" name="c"
			   value="<?= $city ?>" placeholder="из города"/>

		<button type="submit" class="btn btn-primary search-btn">Подобрать</button>

        <?php if ($profile): ?>
			<a class="btn btn-default beige-border" id="btn-any" href="/any">
				Играть в Salut
			</a>
			<a class="btn btn-default beige-border" id="btn-random" href="/broadcast">
				Рассылка
			</a>
        <?php endif; ?>

		<!-- <button class="btn btn-default pull-right" id="btn-extended-search">Параметры</button> -->
	</form>
</div>
