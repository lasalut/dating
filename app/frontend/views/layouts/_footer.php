<?php
use yii\bootstrap\Nav;
?>

<div id="footer">
    <div class="container">
        <div class="shares">
            <img src="/images/index/shares.png"/>
        </div>
        <div class="row menues">
            <div class="col-md-5">
                <?= Nav::widget([
                    'encodeLabels' => false,
                    'items' => [
                        ['label' => 'КОНФИДЕНЦИАЛЬНОСТЬ', 'url' => ['/confidential']],
                        ['label' => 'ПРАВИЛА', 'url' => ['/rules']],
                        ['label' => 'БЛОГ', 'url' => ['/blogs']],
                    ],
                    'options' => ['class' => 'navbar-nav pull-left'],
                ]);
                ?>
            </div>
            <div class="col-md-2 footer-text">
                &#0169; LA-SALUT, 2018
            </div>
            <div class="col-md-5">
                <?= Nav::widget([
                    'encodeLabels' => false,
                    'items' => [
                        ['label' => 'ПОЛЬЗОВАТЕЛЬСКОЕ СОГЛАШЕНИЕ', 'url' => ['/terms-of-use']],
                        ['label' => 'ПОДДЕРЖКА', 'url' => ['/feedback']],
                    ],
                    'options' => ['class' => 'navbar-nav pull-right'],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>