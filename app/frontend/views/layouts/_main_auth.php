<a href="#loginForm" role="button" class="btn btn-trans beige-border btn-guest" id="login-btn" data-toggle="modal">
	Войти
</a>
<a href="#registerForm" role="button" class="btn btn-trans beige-border btn-guest" id="register-btn" data-toggle="modal">
	Регистрация
</a>

<div id="loginForm" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content" style="max-width:500px">
			<button type="button" class="close" data-dismiss="modal">×</button>

			<div class="box-wrap auto-center" id="auth-block">
				<div class="box">
					<div class="box-header" style="margin-left:-36px">
						Вход
					</div>
					<div class="box-content">
						<form id="login-form">
							<div class="form-row">
								<ul class="nav nav-tabs" id="nav-login">
									<li class="active">
										<a data-toggle="tab" href="#tab-login-email">E-mail</a>
									</li>
									<li><a data-toggle="tab" href="#tab-login-phone">Номер телефона</a></li>
								</ul>

								<div class="tab-content">
									<div id="tab-login-email" class="tab-pane fade in active">
										<input id="login-email-fake" style="display:none" type="text" name="fakeusernameremembered">
										<input type="text" name="email" id="login-email" class="full"
											   placeholder="E-mail адрес" autocomplete="nope"/>
									</div>
									<div id="tab-login-phone" class="tab-pane fade">
                                        <?= \yii\widgets\MaskedInput::widget([
                                            'id' => 'login-phone',
                                            'name' => 'phone',
                                            'mask' => '+7 999 999-99-99',
                                            'options' => ['class' => 'full'],
                                        ]); ?>
									</div>
								</div>
							</div>
							<div class="form-row">
								<input type="password" placeholder="Пароль" name="password" required
									   id="login-password" class="full" autocomplete="new-password"/>
							</div>
							<div class="form-row form-submit">
								<button type="submit" class="btn btn-primary">Войти на сайт</button>
							</div>
							<div style="text-align: right">
								<a href="#remindForm" role="button" class="link-restore" id="remindForm-btn"
								   data-toggle="modal">
									Забыли пароль?
								</a>
							</div>
							<ul class="errors" id="login-errors"></ul>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="registerForm" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content" style="max-width:500px">
			<button type="button" class="close" data-dismiss="modal">×</button>

			<div class="box-wrap auto-center" id="auth-block">
				<div class="box">
					<div class="box-header" style="margin-left:-85px">
						Регистрация
					</div>
					<div class="box-content">
						<form id="register-form">
							<ul class="errors" id="reg-errors"></ul>
							<div class="form-row">
								<input type="text" class="full" placeholder="Эл. почта или Тел." name="emailPhone" required/>
							</div>
							<div class="row form-row">
								<div class="col-md-8">
									<input type="text" class="full" placeholder="Ваше имя" name="name" required/>
								</div>
								<div class="col-md-4 genders">
									<div class="btn-group" data-toggle="buttons">
										<label class="btn btn-default genders-male">
											<input type="radio" name="my_gender" value="male" autocomplete="off" required>М
										</label>
										<label class="btn btn-default genders-female">
											<input type="radio" name="my_gender" value="female" autocomplete="off">Ж
										</label>
									</div>
								</div>
							</div>

							<div class="form-group form-row">
								<div class="box-title">
									<div>Дата рождения:</div>
								</div>
								<div class="birthdate row">
									<div class="col-md-4">
										<select class="form-control full" name="day" required>
                                            <?php $options = ['prompt' => 'день']; ?>
                                            <?= \yii\helpers\Html::renderSelectOptions(null, \modules\profiles\common\models\Profile::dayOptions(), $options) ?>
										</select>
									</div>
									<div class="col-md-4">
										<select class="form-control full" name="month" required>
                                            <?php $options = ['prompt' => 'месяц']; ?>
                                            <?= \yii\helpers\Html::renderSelectOptions(null, \modules\profiles\common\models\Profile::monthOptions(), $options) ?>
										</select>
									</div>
									<div class="col-md-4" name="year">
										<select class="form-control full" name="year" required>
                                            <?php $options = ['prompt' => 'год']; ?>
                                            <?= \yii\helpers\Html::renderSelectOptions(null, \modules\profiles\common\models\Profile::yearOptions(), $options) ?>
										</select>
									</div>
								</div>
							</div>
							<div class="form-submit form-row">
								<button type="submit" class="btn btn-primary">Зарегистрироваться</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?= $this->render('_remind') ?>