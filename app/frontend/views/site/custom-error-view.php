<?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \yii\base\ErrorException $exception
 */

// $this->context->layout = 'custom-error-layout';

$code_404 = 0;
$code_500 = 8;
$code = $exception->getCode();

$css = <<<CSS
	.error-container {
		margin: 40px auto;
		max-width: 600px;
	}
	.error404 {
		width: 100%;
		font-weight: bold;
		font-size: 13px;
	}
	.error404 td {
		padding: 0 20px;
		vertical-align: top;
	}
	.error404 td.first {
		width: 40%;
	}
	.error404 td.first img {
		width: 100%;
	}
	.error404 li {
		margin-bottom: 13px;
	}
CSS;
$this->registerCss($css);
?>

<div class="error-container">
    <?php if ($code == $code_404): ?>
		<table class="error404">
			<tr>
				<td class="first">
					<img src="/images/not_found.jpeg">
				</td>
				<td class="second">
					<ul class="ul-style">
						<li>Возможно, неправильно набран адрес страницы</li>
						<li>Или старая ссылка более не существует<br>
							(страница была удалена или поменяла адрес)
						</li>
					</ul>
				</td>
			</tr>
		</table>
    <?php elseif ($code == $code_500): ?>
		<h1>К сожалению, возникли трудности:</h1>
		<h4>Ваш запрос сейчас не может быть обработан. Мы в курсе этой ошибки и работаем над её устранением.
			Попробуйте заглянуть чуть позже</h4>
    <?php else: ?>
		<h1>К сожалению, возникли трудности:</h1>
		<h4>Ваш запрос сейчас не может быть обработан. Мы в курсе этой ошибки и работаем над её устранением.
			Попробуйте заглянуть чуть позже</h4>
    <?php endif; ?>
</div>
