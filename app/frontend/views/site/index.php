<?php

use modules\profiles\common\models\Profile;
use modules\profiles\frontend\assets\SearchAsset;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;

/**
 * @var \yii\web\View $this
 * @var Profile $profile
 * @var ActiveDataProvider $dataProvider
 */
?>

<h1 class="center">Новые участники</h1>
<div class="row row-5 profiles">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_profile',
        'viewParams' => [
            'profile' => $profile,
        ],
    ]); ?>
</div>