<?php

use modules\profiles\common\models\Profile;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var Profile $profile */
?>
<div class="col-md-5ths col-xs-6 avatar-preview">
	<a data-ua="<?= $model->id ?>" href="<?= Url::to(['/profile/' . $model->id]) ?>">
		<img src="<?= $model->avatar_path ?>">
	</a>
</div>