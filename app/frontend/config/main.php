<?php

return [
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'frontend\controllers',
    'bootstrap' => getenv('YII_ENV') == 'prod'
        ? ['GlobalComponent', 'modules\pages\Bootstrap', 'assetsAutoCompress']
        : ['GlobalComponent', 'modules\pages\Bootstrap'],
    'components' => [
        'session' => [
            'name' => 'PHPFRONTSESSID',
        ],
        'assetsAutoCompress' => [
            'class' => '\skeeks\yii2\assetsAuto\AssetsAutoCompressComponent',
            'enabled' => true,

            'readFileTimeout' => 3,           //Time in seconds for reading each asset file

            'jsCompress' => true,        //Enable minification js in html code
            'jsCompressFlaggedComments' => true,        //Cut comments during processing js

            'cssCompress' => true,        //Enable minification css in html code

            'cssFileCompile' => true,        //Turning association css files
            'cssFileRemouteCompile' => false,       //Trying to get css files to which the specified path as the remote file, skchat him to her.
            'cssFileCompress' => true,        //Enable compression and processing before being stored in the css file
            'cssFileBottom' => false,       //Moving down the page css files
            'cssFileBottomLoadOnJs' => false,       //Transfer css file down the page and uploading them using js

            'jsFileCompile' => true,        //Turning association js files
            'jsFileRemouteCompile' => false,       //Trying to get a js files to which the specified path as the remote file, skchat him to her.
            'jsFileCompress' => true,        //Enable compression and processing js before saving a file
            'jsFileCompressFlaggedComments' => true,        //Cut comments during processing js

            'htmlCompress' => true,        //Enable compression html
            'noIncludeJsFilesOnPjax' => true,        //Do not connect the js files when all pjax requests
            'htmlCompressOptions' =>              //options for compressing output result
                [
                    'extra' => false,        //use more compact algorithm
                    'no-comments' => true   //cut all the html comments
                ],
        ],
        'request' => [
            'cookieValidationKey' => getenv('FRONTEND_COOKIE_VALIDATION_KEY'),
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'site/index',
                'logout' => 'site/logout',
                'profile/<id:\d+>' => 'profiles/profile/view',
                'edit' => 'profiles/profile/edit',
                'settings' => 'profiles/profile/settings',
                'broadcast' => 'profiles/broadcast',
                'calls' => 'profiles/premium/calls',
                'incognito' => 'profiles/premium/incognito',
                'online' => 'profiles/premium/online',
                'top' => 'profiles/premium/top',
                'contacts' => 'profiles/premium/contacts',
                'sticked' => 'profiles/premium/sticked',
                'visits' => 'profiles/visits',
                'search' => 'profiles/search',
                'any' => 'profiles/any',
                'finance' => 'profiles/finance',
                'balance' => 'profiles/finance/balance',
                'chat' => 'chat/chat',
                'web' => 'webrtc/room',
                'feedback' => 'feedback/messages/add',
                'blogs' => 'blog/blogs',
                'blog/<url:.+>' => 'blog/blogs/article',
            ],
        ],
        'user' => [
            'identityClass' => \modules\profiles\common\models\Profile::class,
            'loginUrl' => ['/'],
        ],
        'assetManager' => [
            'appendTimestamp' => true,
        ],
    ],
    'modules' => [
        'profiles' => [
            'class' => modules\profiles\frontend\Module::class,
        ],
        'chat' => [
            'class' => modules\chat\frontend\Module::class,
        ],
        'photo' => [
            'class' => modules\photo\frontend\Module::class,
        ],
        'webrtc' => [
            'class' => modules\webrtc\frontend\Module::class,
        ],
        'pages' => [
            'class' => modules\pages\frontend\Module::class,
        ],
        'blog' => [
            'class' => modules\blog\frontend\Module::class,
        ],
        'feedback' => [
            'class' => ms\loyalty\feedback\frontend\Module::class,
        ],
        'files-attachments' => [
            'class' => \ms\files\attachments\frontend\Module::class,
        ],
    ],
    'params' => [
        'defaultTitle' => 'Сайт знакомств',
    ],
];
