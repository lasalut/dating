<?php

namespace frontend\controllers;

use frontend\base\Controller;
use marketingsolutions\captcha\algorithms\Numbers;
use marketingsolutions\captcha\CaptchaAction;
use modules\photo\common\models\Photo;
use modules\profiles\common\models\Profile;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\ErrorAction;
use yii\web\NotFoundHttpException;
use yii\web\ViewAction;

/**
 * Class SiteController
 *
 * @package \frontend\controllers
 */
class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::className(),
                'view' => '@app/views/site/custom-error-view.php'
            ],
            'page' => [
                'class' => ViewAction::className(),
            ],
            'captcha' => [
                'class' => CaptchaAction::className(),
                'algorithm' => Numbers::className(),
            ],
        ];
    }

    public function actionIndex()
    {
        /** @var Profile $profile */
        $profile = Yii::$app->user->isGuest ? null : Yii::$app->user->identity;

        if ($profile === null) {
            $query = Profile::find()
                ->where(Profile::activatedSql())
                ->andWhere(['avatar_approved' => true])
                ->orderBy(['created_at' => SORT_DESC]);

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => ['pageSize' => 20],
            ]);

            $this->layout = '@app/views/layouts/index';
            return $this->render('index', compact('profile', 'dataProvider'));
        }

        /** @var Profile $model */
        $model = $profile;
        $photos = Photo::findApprovedByProfile($model);

        return $this->render('@modules/profiles/frontend/views/profile/view', compact('profile', 'model', 'photos'));
    }

    public function actionLogout()
    {
        \Yii::$app->user->logout();

        return $this->redirect('/');
    }

    public function actionPhpinfo()
    {
        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;

        if ($profile == null || $profile->id != 1) {
            throw new NotFoundHttpException();
        }

        echo phpinfo();
        Yii::$app->end();
    }

    public function actionLogin($id, $hash)
    {
        /** @var Profile $profile */
        $profile = Profile::findOne($id);

        if ($profile == null || md5($id) != $hash) {
            throw new NotFoundHttpException();
        }

        @Yii::$app->user->logout();
        Yii::$app->user->login($profile);

        return $this->redirect(["/profile/{$id}"]);
    }
} 