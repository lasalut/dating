<?php

namespace modules\photo\console\controllers;

use console\base\Controller;
use modules\chat\console\models\Chat;
use modules\photo\common\models\Photo;
use modules\profiles\common\models\Profile;
use yii\db\Query;
use yii\mutex\FileMutex;

class NotifyController extends Controller
{
    public function actionIndex()
    {
        $grouped = [];
        $photoIds = [];
        $profileIds = [];

        # Собираем фотографии
        $photos = (new Query())
            ->select('photo.id, photo.file_url, photo.crop_url, photo.profile_id, profile.name')
            ->from(['photo' => Photo::tableName()])
            ->innerJoin(['profile' => Profile::tableName()], 'photo.profile_id = profile.id')
            ->where(['photo.sent' => false])
            ->all();

        if (!empty($photos)) {
            foreach ($photos as $photo) {
                $photoIds[] = $photo['id'];
                $key = $photo['profile_id'];
                if (empty($grouped[$key])) {
                    $grouped[$key] = ['photos' => [], 'name' => $photo['name'], 'avatar' => null];
                }
                $grouped[$key]['photos'][] = getenv('FRONTEND_WEB') . $photo['crop_url'];
            }
        }

        # Собираем аватарки
        /** @var Profile[] $profiles */
        $profiles = Profile::find()->where('avatar IS NOT NULL')->andWhere(['avatar_sent' => false])->all();

        if (!empty($profiles)) {
            foreach ($profiles as $profile) {
                $profileIds[] = $profile->id;
                $key = $profile->id;
                if (empty($grouped[$key])) {
                    $grouped[$key] = ['photos' => [], 'name' => $profile->name, 'avatar' => null];
                }
                $grouped[$key]['avatar'] = $profile->avatar_url;
            }
        }

        $emails = \Yii::$app->params['adminEmails'];

        if (!empty($grouped) && !empty($emails)) {
            foreach ($emails as $email) {
                try {
                    \Yii::$app->mailer
                        ->compose('@modules/photo/common/email/notify', compact('grouped'))
                        ->setTo($email)
                        ->send();
                }
                catch (\Exception $e) {
                    continue;
                }
            }

            if (!empty($photoIds)) {
                Photo::updateAll(['sent' => true], ['id' => $photoIds]);
            }
            if (!empty($profileIds)) {
                Profile::updateAll(['avatar_sent' => true], ['id' => $profileIds]);
            }
        }
    }
}