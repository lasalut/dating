<?php

namespace modules\photo\common\models;

use modules\profiles\common\models\Profile;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_photos".
 *
 * @property integer $id
 * @property integer $album_id
 * @property integer $profile_id
 * @property integer $position
 * @property string $created_at
 * @property string $updated_at
 * @property string $comment
 * @property boolean $sent
 * @property string $file_exif_data
 * @property string $file_path
 * @property string $file_url
 * @property string $file_name
 * @property string $file_orig_name
 * @property string $file_type
 * @property integer $file_size
 * @property integer $approved
 * @property integer $approved_by
 * @property string $approved_at
 * @property integer $banned
 * @property integer $banned_by
 * @property string $banned_at
 * @property string $banned_comment
 * @property boolean $nudes
 *
 * @property string $crop_url
 * @property string $crop_name
 * @property string $crop_path
 *
 * @property string $blur_url
 * @property string $blur_name
 * @property string $blur_path
 *
 * @property string $blur_crop_url
 * @property string $blur_crop_name
 * @property string $blur_crop_path
 *
 * @property string $url
 * @property string $crop
 * @property string $blur
 * @property string $blur_crop
 */
class Photo extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%photos}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Фотография';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Фотографии';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['album_id', 'integer'],
            ['profile_id', 'integer'],
            ['position', 'integer'],
            ['created_at', 'safe'],
            ['updated_at', 'safe'],
            ['comment', 'string'],
            ['sent', 'safe'],
            ['file_exif_data', 'string'],
            ['file_path', 'string', 'max' => 255],
            ['file_name', 'string', 'max' => 255],
            ['file_orig_name', 'string', 'max' => 255],
            ['file_type', 'string', 'max' => 255],
            ['file_url', 'string', 'max' => 255],
            ['file_size', 'integer'],
            ['approved', 'integer'],
            ['approved_by', 'integer'],
            ['approved_at', 'safe'],
            ['banned', 'integer'],
            ['banned_by', 'integer'],
            ['banned_at', 'safe'],
            ['banned_comment', 'string'],
            ['nudes', 'safe'],
            ['crop_path', 'string', 'max' => 255],
            ['crop_name', 'string', 'max' => 255],
            ['crop_url', 'string', 'max' => 255],
            ['blur_path', 'string', 'max' => 255],
            ['blur_name', 'string', 'max' => 255],
            ['blur_url', 'string', 'max' => 255],
            ['blur_crop_path', 'string', 'max' => 255],
            ['blur_crop_name', 'string', 'max' => 255],
            ['blur_crop_url', 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'album_id' => 'Альбом',
            'profile_id' => 'Участник',
            'position' => 'Позиция',
            'created_at' => 'Создана',
            'updated_at' => 'Обновлена',
            'comment' => 'Комментарий',
            'sent' => 'Уведомление отправлено',
            'file_exif_data' => 'File Exif Data',
            'file_path' => 'File Path',
            'file_name' => 'File Name',
            'file_orig_name' => 'File Orig Name',
            'file_type' => 'File Type',
            'file_size' => 'File Size',
            'file_url' => 'File Url',
            'approved' => 'Подтверждена',
            'approved_by' => 'Подтверждена кем',
            'approved_at' => 'Подтверждена когда',
            'banned' => 'Отклонена',
            'banned_by' => 'Отклонена кем',
            'banned_at' => 'Отклонена когда',
            'nudes' => '18+',
            'crop_name' => 'Crop Name',
            'crop_path' => 'Crop Path',
            'crop_url' => 'Crop Url',
            'blur_name' => 'Blur Name',
            'blur_path' => 'Blur Path',
            'blur_url' => 'Blur Url',
            'blur_crop_name' => 'Blur Crop Name',
            'blur_crop_path' => 'Blur Crop Path',
            'blur_crop_url' => 'Blur Crop Url',
        ];
    }

    public function beforeSave($insert)
    {
        if (empty($this->position)) {
            /** @var Photo $maxPosition */
            $maxPosition = Photo::find()->orderBy(['position' => SORT_DESC])->one();
            $this->position = $maxPosition ? $maxPosition->position + 1 : 1;
        }

        return parent::beforeSave($insert);
    }

    public function fields()
    {
        return [
            'id',
            'name' => 'file_orig_name',
            'size' => 'file_size',
            'type' => 'file_type',
            'url' => function (Photo $model) {
                return $model->getUrl();
            },
            'thumbnailUrl' => function (Photo $model) {
                return $model->getUrl();
            },
        ];
    }

    public function getUrl()
    {
        return getenv('FRONTEND_WEB') . $this->file_url;
    }

    public function getCrop()
    {
        return getenv('FRONTEND_WEB') . $this->crop_url;
    }

    public function getBlur()
    {
        return getenv('FRONTEND_WEB') . $this->blur_url;
    }

    public function getBlur_crop()
    {
        return getenv('FRONTEND_WEB') . $this->blur_crop_url;
    }

    public function beforeDelete()
    {
        if (!empty($this->file_path)) {
            @unlink($this->file_path);
        }
        if (!empty($this->crop_path)) {
            @unlink($this->crop_path);
        }
        if (!empty($this->blur_path)) {
            @unlink($this->blur_path);
        }
        if (!empty($this->blur_crop_path)) {
            @unlink($this->blur_crop_path);
        }

        return parent::beforeDelete();
    }

    public function printClasses()
    {
        $classes = [];
        if ($this->approved) {
            $classes[] = 'approved';
        }
        if ($this->banned) {
            $classes[] = 'banned';
        }
        if ($this->nudes) {
            $classes[] = 'nudes';
        }
        return implode(' ', $classes);
    }

    public function printTitle()
    {
        $titles = [];
        if ($this->approved) {
            $titles[] = 'Фотография подтверждена';
        }
        if ($this->banned) {
            $titles[] = 'Фотография отклонена';
        }
        if ($this->nudes) {
            $titles[] = 'Помечена как 18+';
        }
        return empty($titles) ? 'Фотография ожидает проверки модератором' : implode('. ', $titles);
    }

    /**
     * @param Profile $profile
     * @return Photo[]
     */
    public static function findApprovedByProfile(Profile $profile)
    {
        return Photo::find()
            ->where(['profile_id' => $profile->id])
            ->andWhere(['approved' => true])
            ->andWhere(['!=', 'banned', true])
            ->orderBy(['position' => SORT_ASC, 'id' => SORT_ASC])
            ->all();
    }

    /**
     * @param Profile $profile
     * @return Photo[]
     */
    public static function findAllByProfile(Profile $profile)
    {
        return Photo::find()
            ->where(['profile_id' => $profile->id])
            ->orderBy(['position' => SORT_ASC])
            ->all();
    }
}
