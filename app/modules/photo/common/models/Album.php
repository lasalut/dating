<?php

namespace modules\photo\common\models;

use modules\profiles\common\models\Profile;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_photo_albums".
 *
 * @property integer $id
 * @property string $title
 * @property integer $profile_id
 * @property integer $position
 * @property boolean $main
 * @property string $created_at
 * @property string $updated_at
 */
class Album extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%photo_albums}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Альбом';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Альбомы';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'required'],
            ['title', 'string', 'max' => 255],
            ['profile_id', 'required'],
            ['profile_id', 'integer'],
            ['position', 'integer'],
            ['created_at', 'safe'],
            ['updated_at', 'safe'],
            ['main', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Альбом',
            'profile_id' => 'Участник',
            'position' => 'Позиция',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
            'main' => 'Это основной альбом',
        ];
    }

    public function isNudes()
    {
        return $this->main == false;
    }

    /**
     * @param Profile $profile
     * @return Album[]
     */
    public static function byProfile(Profile $profile)
    {
        $mainAlbum = Album::findOne(['main' => true, 'profile_id' => $profile->id]);

        if ($mainAlbum === null) {
            $mainAlbum = new Album();
            $mainAlbum->position = 1;
            $mainAlbum->profile_id = $profile->id;
            $mainAlbum->title = 'Это я';
            $mainAlbum->main = true;
            $mainAlbum->save(false);
        }

        /** @var Album[] $albums */
        $albums = Album::find()
            ->where(['profile_id' => $profile->id])
            ->orderBy(['position' => SORT_ASC])
            ->all();

        return $albums;
    }

    /**
     * @param Profile $profile
     * @return Album
     */
    public static function getMain(Profile $profile)
    {
        $album = Album::findOne(['main' => true, 'profile_id' => $profile->id]);

        if ($album === null) {
            $album = new Album();
            $album->position = 1;
            $album->profile_id = $profile->id;
            $album->title = 'Это я';
            $album->main = true;
            $album->save(false);
        }

        return $album;
    }

    /**
     * @param Profile $profile
     * @return Album
     */
    public static function getNudes(Profile $profile)
    {
        $album = Album::findOne(['main' => false, 'profile_id' => $profile->id]);

        if ($album === null) {
            $album = new Album();
            $album->position = 1;
            $album->profile_id = $profile->id;
            $album->title = '18+';
            $album->main = false;
            $album->save(false);
        }

        return $album;
    }

    /**
     * @return Photo[]
     */
    public function getPhotos()
    {
        return Photo::find()
            ->where(['album_id' => $this->id])
            ->orderBy(['position' => SORT_ASC])
            ->all();
    }
}
