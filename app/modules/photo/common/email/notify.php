<?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var array $grouped
 * @var \yii\mail\BaseMessage $message instance of newly created mail message
 */

$message->setSubject('Уведомление о новых фотографиях');
?>

<?php foreach ($grouped as $profile_id => $data): ?>
	<p>Участник <b><?= $data['name'] ?></b> (#<?= $profile_id ?>) загрузил(а)</p>

    <?php if (!empty($data['photos'])): ?>
		<p><b>ФОТОГРАФИИ:</b></p>
        <?php foreach ($data['photos'] as $photo): ?>
			<a href="<?= getenv('BACKEND_WEB') . '/photo/photos/index?PhotoSearch[profile_id]=' . $profile_id ?>"
				style="width:220px; height:220px; padding:10px; margin-right: 20px; margin-bottom: 20px; text-decoration:none;">
				<img src="<?= $photo ?>" style="width:200px;height:200px;"/>
			</a>
        <?php endforeach; ?>
    <?php endif; ?>

    <?php if (!empty($data['avatar'])): ?>
		<p><b>АВАТАРКУ:</b></p>
		<a href="<?= getenv('BACKEND_WEB') . '/profiles/profiles/index?ProfileSearch[id]=' . $profile_id ?>"
		   style="width:220px; height:220px; padding:10px; text-decoration:none;">
			<img src="<?= $data['avatar'] ?>" style="width:200px;height:200px;"/>
		</a>
    <?php endif; ?>

	<hr style="margin: 25px 0"/>
<?php endforeach; ?>

