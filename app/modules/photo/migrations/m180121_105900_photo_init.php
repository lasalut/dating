<?php

use yii\db\Migration;

class m180121_105900_photo_init extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8 DEFAULT COLLATE utf8_general_ci';
        }

        # сообщения чата
        $this->createTable('{{%photo_albums}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'main' => $this->boolean()->defaultValue(false),
            'profile_id' => $this->integer()->notNull(),
            'position' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);

        $this->createIndex('ix_profile_id', '{{%photo_albums}}', 'profile_id');

        # счетчики новых сообщений
        $this->createTable('{{%photos}}', [
            'id' => $this->primaryKey(),
            'album_id' => $this->integer(),
            'profile_id' => $this->integer(),
            'position' => $this->integer(),
            'comment' => $this->text(),
            'sent' => $this->boolean()->defaultValue(false),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),

            'file_exif_data' => $this->text(),
            'file_path' => $this->string(),
            'file_name' => $this->string(),
            'file_orig_name' => $this->string(),
            'file_type' => $this->string(),
            'file_size' => $this->integer(),
            'file_url' => $this->string(),

            'approved' => $this->boolean()->defaultValue(false),
            'approved_by' => $this->integer(),
            'approved_at' => $this->dateTime(),

            'banned' => $this->boolean()->defaultValue(false),
            'banned_by' => $this->integer(),
            'banned_at' => $this->dateTime(),
            'banned_comment' => $this->text(),

            'nudes' => $this->boolean()->defaultValue(false),

            'crop_path' => $this->string(),
            'crop_name' => $this->string(),
            'crop_url' => $this->string(),

            'blur_path' => $this->string(),
            'blur_name' => $this->string(),
            'blur_url' => $this->string(),

            'blur_crop_path' => $this->string(),
            'blur_crop_name' => $this->string(),
            'blur_crop_url' => $this->string(),
        ], $tableOptions);

        $this->createIndex('ix_profile_id', '{{%photos}}', 'profile_id');
        $this->createIndex('ix_album_id', '{{%photos}}', 'album_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%photos}}');
        $this->dropTable('{{%photo_albums}}');
    }
}
