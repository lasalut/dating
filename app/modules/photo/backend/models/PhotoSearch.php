<?php

namespace modules\photo\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;

/**
 * PhotoSearch represents the model behind the search form about `modules\photo\common\models\Photo`.
 */
class PhotoSearch extends PhotoWithData implements SearchModelInterface

{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['profile__name', 'profile__phone_mobile', 'profile__email'], 'safe'],
            [['id', 'album_id', 'profile_id', 'position', 'file_size', 'sent', 'approved', 'approved_by', 'banned', 'banned_by', 'nudes'], 'integer'],
            [['comment', 'banned_comment', 'created_at', 'updated_at', 'file_exif_data', 'file_path', 'file_name', 'file_orig_name', 'file_type', 'file_url', 'approved_at', 'banned_at', 'crop_path', 'crop_name', 'crop_url', 'blur_path', 'blur_name', 'blur_url', 'blur_crop_path', 'blur_crop_name', 'blur_crop_url'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        return PhotoWithData::find();
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();
        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'photo.id' => $this->id,
            'photo.album_id' => $this->album_id,
            'photo.profile_id' => $this->profile_id,
            'photo.position' => $this->position,
            'photo.file_size' => $this->file_size,
            'photo.approved' => $this->approved,
            'photo.approved_by' => $this->approved_by,
            'photo.banned' => $this->banned,
            'photo.banned_by' => $this->banned_by,
            'photo.nudes' => $this->nudes,
            'photo.sent' => $this->sent,
        ]);

        $query
            ->andFilterWhere(['like', 'photo.file_exif_data', $this->file_exif_data])
            ->andFilterWhere(['like', 'photo.file_path', $this->file_path])
            ->andFilterWhere(['like', 'photo.file_name', $this->file_name])
            ->andFilterWhere(['like', 'photo.file_orig_name', $this->file_orig_name])
            ->andFilterWhere(['like', 'photo.file_type', $this->file_type])
            ->andFilterWhere(['like', 'photo.file_url', $this->file_url])
            ->andFilterWhere(['like', 'photo.crop_path', $this->crop_path])
            ->andFilterWhere(['like', 'photo.crop_name', $this->crop_name])
            ->andFilterWhere(['like', 'photo.crop_url', $this->crop_url])
            ->andFilterWhere(['like', 'photo.blur_path', $this->blur_path])
            ->andFilterWhere(['like', 'photo.blur_name', $this->blur_name])
            ->andFilterWhere(['like', 'photo.blur_url', $this->blur_url])
            ->andFilterWhere(['like', 'photo.comment', $this->comment])
            ->andFilterWhere(['like', 'photo.banned_comment', $this->banned_comment])
            ->andFilterWhere(['like', 'photo.blur_crop_path', $this->blur_crop_path])
            ->andFilterWhere(['like', 'photo.blur_crop_name', $this->blur_crop_name])
            ->andFilterWhere(['like', 'photo.blur_crop_url', $this->blur_crop_url]);

        static::filtersForExtraColumns($query);
    }
}
