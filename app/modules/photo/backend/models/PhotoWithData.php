<?php

namespace modules\photo\backend\models;

use modules\photo\common\models\Photo;
use modules\profiles\common\models\Profile;
use yii\db\ActiveQuery;
use yz\admin\search\WithExtraColumns;

class PhotoWithData extends Photo
{
    use WithExtraColumns;

    protected static function extraColumns()
    {
        return [
            'profile__name',
            'profile__phone_mobile',
            'profile__email',
            'profile__id',
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), static::extraColumns());
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'profile__name' => 'Участник',
            'profile__phone_mobile' => 'Номер телефона',
            'profile__email' => 'E-mail',
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()
            ->select(static::selectWithExtraColumns(['photo.*']))
            ->from(['photo' => self::tableName()])
            ->leftJoin(['profile' => Profile::tableName()], 'photo.profile_id = profile.id');
    }
}