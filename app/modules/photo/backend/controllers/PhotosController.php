<?php

namespace modules\photo\backend\controllers;

use Yii;
use modules\photo\common\models\Photo;
use modules\photo\backend\models\PhotoSearch;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yz\admin\actions\ExportAction;
use yz\admin\actions\ToggleAction;
use yz\admin\grid\filters\BooleanFilter;
use yz\admin\models\User;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;
use yz\Yz;

/**
 * PhotosController implements the CRUD actions for Photo model.
 */
class PhotosController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function ($params) {
                    /** @var PhotoSearch $searchModel */
                    return Yii::createObject(PhotoSearch::className());
                },
                'dataProvider' => function ($params, PhotoSearch $searchModel) {
                    $dataProvider = $searchModel->search($params);
                    return $dataProvider;
                },
            ],
            'toggle' => [
                'class' => ToggleAction::className(),
                'modelClass' => Photo::className(),
                'attributes' => ['approved', 'banned', 'nudes'],
            ]
        ]);
    }

    /**
     * Lists all Photo models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var PhotoSearch $searchModel */
        $searchModel = Yii::createObject(PhotoSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $dataProvider->setSort(['defaultOrder' => ['id' => SORT_DESC]]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(PhotoSearch $searchModel)
    {
        return [
            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width:60px'],
            ],
            [
                'label' => 'Фотография',
                'format' => 'raw',
                'contentOptions' => ['style' => 'width:170px'],
                'value' => function (Photo $model) {
                    $file = getenv('FRONTEND_WEB') . $model->file_url;
                    $crop = getenv('FRONTEND_WEB') . $model->crop_url;
                    return Html::a(Html::img($crop, ['style' => 'max-width:150px']), $file, [
                        'target' => '_blank',
                        'class' => 'fancybox',
                    ]);
                }
            ],
            [
                'attribute' => 'approved',
                'contentOptions' => ['style' => 'width:180px'],
                'filter' => BooleanFilter::instance(),
                'format' => 'raw',
                'value' => function (Photo $model) {
                    return $model->approved
                        ? Html::a('<i class="fa fa-toggle-on fa-lg"></i>', ['/photo/photos/approved', 'id' => $model->id], ['class' => 'toggler'])
                        : Html::a('<i class="fa fa-toggle-off fa-lg"></i>', ['/photo/photos/approved', 'id' => $model->id], ['class' => 'toggler']);
                }
            ],
            [
                'attribute' => 'banned',
                'contentOptions' => ['style' => 'width:180px'],
                'filter' => BooleanFilter::instance(),
                'format' => 'raw',
                'value' => function (Photo $model) {
                    return $model->banned
                        ? Html::a('<i class="fa fa-toggle-on fa-lg"></i>', ['/photo/photos/banned', 'id' => $model->id], ['class' => 'toggler'])
                        : Html::a('<i class="fa fa-toggle-off fa-lg"></i>', ['/photo/photos/banned', 'id' => $model->id], ['class' => 'toggler']);
                }
            ],
            [
                'attribute' => 'nudes',
                'contentOptions' => ['style' => 'width:180px'],
                'filter' => BooleanFilter::instance(),
                'format' => 'raw',
                'value' => function (Photo $model) {
                    return $model->nudes
                        ? Html::a('<i class="fa fa-toggle-on fa-lg"></i>', ['/photo/photos/nudes', 'id' => $model->id], ['class' => 'toggler'])
                        : Html::a('<i class="fa fa-toggle-off fa-lg"></i>', ['/photo/photos/nudes', 'id' => $model->id], ['class' => 'toggler']);
                }
            ],
            'sent:boolean',
            'created_at:datetime',
            'profile__name',
            'profile__phone_mobile',
            'profile__email',
            [
                'attribute' => 'profile_id',
                'label' => 'ID_уч',
                'contentOptions' => ['style' => 'width:60px'],
            ],
        ];
    }

    /**
     * Creates a new Photo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Photo;

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
            return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Photo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
            return $this->getCreateUpdateResponse($model);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Photo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array) $id;

        foreach ($id as $id_) {
            $this->findModel($id_)->delete();
        }

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Photo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Photo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Photo::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionApproved($id)
    {
        $model = $this->findModel($id);
        /** @var User $admin */
        $admin = Yii::$app->user->identity;

        if ($model->approved) {
            $model->updateAttributes([
                'approved_at' => null,
                'approved_by' => null,
                'approved' => false,
            ]);
        }
        else {
            $model->updateAttributes([
                'approved_at' => (new \DateTime())->format('Y-m-d H:i:s'),
                'approved_by' => $admin->id,
                'approved' => true,
                'banned_at' => null,
                'banned_by' => null,
                'banned' => false,
            ]);
        }

        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function actionBanned($id)
    {
        $model = $this->findModel($id);
        /** @var User $admin */
        $admin = Yii::$app->user->identity;

        if ($model->banned) {
            $model->updateAttributes([
                'banned_at' => null,
                'banned_by' => null,
                'banned' => false,

            ]);
        }
        else {
            $model->updateAttributes([
                'banned_at' => (new \DateTime())->format('Y-m-d H:i:s'),
                'banned_by' => $admin->id,
                'banned' => true,
                'approved_at' => null,
                'approved_by' => null,
                'approved' => false,
            ]);
        }

        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function actionNudes($id)
    {
        $model = $this->findModel($id);
        $model->updateAttributes(['nudes' => $model->nudes ? false : true]);

        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function actionApprove(array $ids = [])
    {
        if (empty($ids)) {
            return $this->redirect(\Yii::$app->request->referrer ?: Url::home());
        }
        /** @var User $admin */
        $admin = Yii::$app->user->identity;

        foreach ($ids as $id) {
            $model = $this->findModel($id);
            if ($model->approved) {
                continue;
            }
            $model->updateAttributes([
                'approved_at' => (new \DateTime())->format('Y-m-d H:i:s'),
                'approved_by' => $admin->id,
                'approved' => true,
                'banned_at' => null,
                'banned_by' => null,
                'banned' => false,
            ]);
        }

        Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Подтверждено фотографий: ' . count($ids));

        return $this->redirect(\Yii::$app->request->referrer ?: Url::home());
    }

    public function actionBan(array $ids = [])
    {
        if (empty($ids)) {
            return $this->redirect(\Yii::$app->request->referrer ?: Url::home());
        }
        /** @var User $admin */
        $admin = Yii::$app->user->identity;

        foreach ($ids as $id) {
            $model = $this->findModel($id);
            if ($model->banned) {
                continue;
            }
            $model->updateAttributes([
                'banned_at' => (new \DateTime())->format('Y-m-d H:i:s'),
                'banned_by' => $admin->id,
                'banned' => true,
                'approved_at' => null,
                'approved_by' => null,
                'approved' => false,
            ]);
        }

        Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Отклонено фотографий: ' . count($ids));

        return $this->redirect(\Yii::$app->request->referrer ?: Url::home());
    }
}
