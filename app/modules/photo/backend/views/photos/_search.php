<?php

use yii\helpers\Html;
use yz\admin\widgets\ActiveForm;
use yz\admin\widgets\FormBox;

/**
 * @var yii\web\View $this
 * @var modules\photo\backend\models\PhotoSearch $model
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<div class="photo-search hidden" id="filter-search">
    <?php $box = FormBox::begin() ?>
    <?php $box->beginBody() ?>
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'fieldConfig' => [
            'horizontalCssClasses' => ['label' => 'col-sm-3', 'input' => 'col-sm-5', 'offset' => 'col-sm-offset-3 col-sm-5'],
        ],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'album_id') ?>

    <?= $form->field($model, 'profile_id') ?>

    <?= $form->field($model, 'position') ?>

    <?= $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'file_exif_data') ?>

    <?php // echo $form->field($model, 'file_path') ?>

    <?php // echo $form->field($model, 'file_name') ?>

    <?php // echo $form->field($model, 'file_orig_name') ?>

    <?php // echo $form->field($model, 'file_type') ?>

    <?php // echo $form->field($model, 'file_size') ?>

    <?php // echo $form->field($model, 'file_url') ?>

    <?php // echo $form->field($model, 'approved') ?>

    <?php // echo $form->field($model, 'approved_by') ?>

    <?php // echo $form->field($model, 'approved_at') ?>

    <?php // echo $form->field($model, 'banned') ?>

    <?php // echo $form->field($model, 'banned_by') ?>

    <?php // echo $form->field($model, 'banned_at') ?>

    <?php // echo $form->field($model, 'nudes') ?>

    <?php // echo $form->field($model, 'crop_path') ?>

    <?php // echo $form->field($model, 'crop_name') ?>

    <?php // echo $form->field($model, 'crop_url') ?>

    <?php // echo $form->field($model, 'blur_path') ?>

    <?php // echo $form->field($model, 'blur_name') ?>

    <?php // echo $form->field($model, 'blur_url') ?>

    <?php // echo $form->field($model, 'blur_crop_path') ?>

    <?php // echo $form->field($model, 'blur_crop_name') ?>

    <?php // echo $form->field($model, 'blur_crop_url') ?>

        <?php  $box->endBody() ?>
        <?php  $box->beginFooter() ?>
            <?= Html::submitButton(\Yii::t('admin/t','Search'), ['class' => 'btn btn-primary']) ?>
        <?php  $box->endFooter() ?>

    <?php ActiveForm::end(); ?>
    <?php  FormBox::end() ?>
</div>
