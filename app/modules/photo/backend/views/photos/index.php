<?php

use yii\helpers\Html;
use yz\admin\widgets\Box;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;
use yz\icons\Icons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\photo\backend\models\PhotoSearch $searchModel
 * @var array $columns
 */

$this->title = modules\photo\common\models\Photo::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

\modules\photo\frontend\assets\Fancybox2Asset::register($this);
$js = <<<JS
	$(document).ready(function() {
		$('.fancybox').fancybox();
	});
JS;
$this->registerJs($js);

$css = <<<CSS
	.toggler {
		font-size: 34px;
		display: block;
		margin: 28px auto 0;
		width: 100px;
		text-align: center;
		padding: 20px 20px;
	}
CSS;
$this->registerCss($css);
?>

<?php $box = Box::begin(['cssClass' => 'photo-index box-primary']) ?>
<div class="text-right">
    <?php echo ActionButtons::widget([
        'order' => [/*['search'],*/
            ['export',  'approve', 'ban', 'delete', 'return']],
        'gridId' => 'photo-grid',
        'searchModel' => $searchModel,
        'modelClass' => 'modules\photo\common\models\Photo',
        'buttons' => [
            'approve' => function () {
                return Html::a(Icons::p('check') . 'Подтвердить выбранные', ['approve'], [
                    'class' => 'btn btn-info selection',
                    'title' => 'Подтвердить фотографии',
                ]);
            },
            'ban' => function () {
                return Html::a(Icons::p('ban') . 'Отклонить выбранные', ['ban'], [
                    'class' => 'btn btn-warning selection',
                    'title' => 'Отклонить фотографии',
                ]);
            }
        ]
    ]) ?>
</div>

<?php //echo $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget([
    'id' => 'photo-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => array_merge([
        ['class' => 'yz\admin\grid\columns\CheckboxColumn'],
    ], $columns),
]); ?>
<?php Box::end() ?>
