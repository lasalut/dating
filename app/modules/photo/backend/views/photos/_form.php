<?php

use yii\helpers\Html;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use yz\admin\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var modules\photo\common\models\Photo $model
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<?php  $box = FormBox::begin(['cssClass' => 'photo-form box-primary', 'title' => '']) ?>
    <?php $form = ActiveForm::begin(); ?>

    <?php $box->beginBody() ?>
    <?= $form->field($model, 'album_id')->textInput() ?>

    <?= $form->field($model, 'profile_id')->textInput() ?>

    <?= $form->field($model, 'position')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'file_exif_data')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'file_path')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'file_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'file_orig_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'file_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'file_size')->textInput() ?>

    <?= $form->field($model, 'file_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'approved')->textInput() ?>

    <?= $form->field($model, 'approved_by')->textInput() ?>

    <?= $form->field($model, 'approved_at')->textInput() ?>

    <?= $form->field($model, 'banned')->textInput() ?>

    <?= $form->field($model, 'banned_by')->textInput() ?>

    <?= $form->field($model, 'banned_at')->textInput() ?>

    <?= $form->field($model, 'nudes')->textInput() ?>

    <?= $form->field($model, 'crop_path')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'crop_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'crop_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'blur_path')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'blur_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'blur_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'blur_crop_path')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'blur_crop_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'blur_crop_url')->textInput(['maxlength' => true]) ?>

    <?php $box->endBody() ?>

    <?php $box->actions([
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
        AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
    ]) ?>
    <?php ActiveForm::end(); ?>

<?php  FormBox::end() ?>
