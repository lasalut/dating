<?php

namespace modules\photo\frontend\models;

use modules\photo\common\models\Album;
use modules\photo\common\models\Photo;
use modules\profiles\common\models\Profile;
use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $files;

    public function rules()
    {
        return [
            [['files'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg, gif',
                'maxFiles' => 12, 'maxSize' => 1024 * 1024 * 10],
        ];
    }

    public function upload(Album $album)
    {
        $photos = [];

        $dir = Yii::getAlias("@data/photos");
        FileHelper::createDirectory($dir);

        $cropDir = Yii::getAlias("@data/crop");
        FileHelper::createDirectory($cropDir);

        $blurDir = Yii::getAlias("@data/blur");
        FileHelper::createDirectory($blurDir);

        $blurCropDir = Yii::getAlias("@data/blur_crop");
        FileHelper::createDirectory($blurCropDir);

        foreach ($this->files as $file) {
            /** @var \yii\web\UploadedFile $file */
            if ($file->error == false) {
                $uniqid = uniqid();
                $name = "profile_{$album->profile_id}_album_{$album->id}_{$uniqid}.{$file->extension}";
                $path = $dir . DIRECTORY_SEPARATOR . $name;
                $file->saveAs($path);

                # сохраняем обычную фотку
                $photo = new Photo();
                $photo->album_id = $album->id;
                $photo->profile_id = $album->profile_id;
                $photo->file_orig_name = $file->name;
                $photo->file_size = $file->size;
                $photo->file_type = $file->extension;
                $photo->file_name = $name;
                $photo->file_path = $path;
                $photo->nudes = $album->isNudes();
                $photo->file_url = '/data/photos/' . $name;

                # создание кропнутой квадратной картинки
                $cropPath = $cropDir . DIRECTORY_SEPARATOR . $name;
                $im = new \Imagick($path);
                $im->cropThumbnailImage(370, 370);
                $im->writeImage($cropPath);
                $photo->crop_name = $name;
                $photo->crop_path = $cropPath;
                $photo->crop_url = '/data/crop/' . $name;

                # создание кропнутой размытой
                $blurCropPath = $blurCropDir . DIRECTORY_SEPARATOR . $name;
                $im = new \Imagick($path);
                $im->cropThumbnailImage(370, 370);
                $im->blurImage(0, 10);
                $im->writeImage($blurCropPath);
                $photo->blur_crop_name = $name;
                $photo->blur_crop_path =  $blurCropPath;
                $photo->blur_crop_url = '/data/blur_crop/' . $name;

                # создание размытой
                $blurPath = $blurDir . DIRECTORY_SEPARATOR . $name;
                $im = new \Imagick($path);
                $im->blurImage(0, 10);
                $im->writeImage($blurPath);
                $photo->blur_name = $name;
                $photo->blur_path = $blurPath;
                $photo->blur_url = '/data/blur/' . $name;

                $photo->save(false);
                $photos[] = $photo;
            }
        }

        if (getenv('YII_ENV') == 'dev') {
            FileHelper::copyDirectory($dir, Yii::getAlias("@frontendWebroot/data/photos"));
            FileHelper::copyDirectory($cropDir, Yii::getAlias("@frontendWebroot/data/crop"));
            FileHelper::copyDirectory($blurDir, Yii::getAlias("@frontendWebroot/data/blur"));
            FileHelper::copyDirectory($blurCropDir, Yii::getAlias("@frontendWebroot/data/blur_crop"));
        }

        return $photos;
    }
}