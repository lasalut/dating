<?php

namespace modules\photo\frontend\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class PhotoAsset extends AssetBundle
{
    public $sourcePath = '@modules/photo/frontend/assets';

    public $css = [
        'css/photo.css'
    ];
    public $js = [
        'js/photo.js',
    ];

    public $depends = [
        JqueryAsset::class,
        JqueryFileUploadAsset::class,
        Fancybox2Asset::class,
    ];
}