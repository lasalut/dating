$(document).ready(function() {
	if (window.location.hash.length) {
		$('a[href="' + window.location.hash + '"]').tab('show');
	}

	$('#nav-albums a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
		history.pushState(null, null, this.href);
		$('html,body').animate({scrollTop: 0}, 'slow');
	});

	$('#upload-all-btn').click(function() {
		$('.upload-btn').each(function() {
			$(this).trigger('click');
		})
	});

	$(".fancybox-thumb").fancybox({
		prevEffect: 'none',
		nextEffect: 'none',
		helpers:    {
			thumbs: {
				width:  50,
				height: 50
			}
		}
	});
});


$(function() {
	var url = '/photo/upload?album_id=' + $('#nav-albums li.active').attr('data-album');

	var uploadButton = $('<button/>')
		.addClass('btn btn-primary upload-btn')
		.prop('disabled', true)
		.text('Загрузка...')
		.on('click', function() {
			var $this = $(this);
			var data = $this.data();
			$this.off('click')
				.text('Abort')
				.on('click', function() {
					$this.remove();
					data.abort();
				});
			data.submit().always(function() {
				$this.remove();
			});
		});

	$('#fileupload')
		.fileupload({
			url:                url,
			dataType:           'json',
			autoUpload:         false,
			acceptFileTypes:    /(\.|\/)(gif|jpe?g|png)$/i,
			maxFileSize:        10 * 1024 * 1024,
			// Enable image resizing, except for Android and Opera,
			// which actually support image resizing, but fail to
			// send Blob objects via XHR requests:
			disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
			previewMaxWidth:    100,
			previewMaxHeight:   100,
			previewCrop:        true
		})
		.on('fileuploadadd', function(e, data) {
			data.url = '/photo/upload?album_id=' + $('#nav-albums li.active').attr('data-album');
			data.context = $('<div/>').addClass('uploaded-photo col-md-5ths col-xs-6').appendTo('#files');
			$.each(data.files, function(index, file) {
				var node = $('<p/>')
					.append($('<span/>').text(file.name))
					.append($('<i/>').addClass('fa fa-check-circle'));
				if (!index) {
					node.append('<br>').append(uploadButton.clone(true).data(data));
				}
				node.appendTo(data.context);
			});
			$('#progress').show();
		})
		.on('fileuploadprocessalways', function(e, data) {
			var index = data.index,
				file = data.files[index],
				node = $(data.context.children()[index]);
			if (file.preview) {
				node
					.prepend('<br>')
					.prepend(file.preview);
			}
			if (file.error) {
				node.append('<br>').append($('<span class="text-danger"/>').text(file.error));
			}
			if (index + 1 === data.files.length) {
				data.context.find('button')
					.text('Загрузить')
					.prop('disabled', !!data.files.error);
			}
		})
		.on('fileuploadprogressall', function(e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('#progress .progress-bar').css(
				'width',
				progress + '%'
			);
		})
		.on('fileuploaddone', function(e, data) {
			$.each(data.result.files, function(index, file) {
				if (file.url) {
					var link = $('<a>')
						.attr('target', '_blank')
						.prop('href', file.url);
					$(data.context.children()[index])
						.wrap(link);
				} else if (file.error) {
					var error = $('<span class="text-danger"/>').text(file.error);
					$(data.context.children()[index])
						.append('<br>')
						.append(error);
				}
			});
			window.location.reload(true);
		})
		.on('fileuploadfail', function(e, data) {
			$.each(data.files, function(index) {
				var error = $('<span class="text-danger"/>').text('Ошибка при загрузке файла');
				$(data.context.children()[index])
					.append('<br>')
					.append(error);
			});
		})
		.prop('disabled', !$.support.fileInput)
		.parent().addClass($.support.fileInput ? undefined : 'disabled');
});