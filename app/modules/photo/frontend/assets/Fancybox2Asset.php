<?php

namespace modules\photo\frontend\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class Fancybox2Asset extends AssetBundle
{
    public $sourcePath = '@modules/photo/frontend/assets/fancybox2';

    public $css = [
        'source/jquery.fancybox.css?v=2.1.5',
        'source/helpers/jquery.fancybox-buttons.css?v=1.0.5',
        'source/helpers/jquery.fancybox-thumbs.css?v=1.0.7',
    ];

    public $js = [
        'lib/jquery.mousewheel.pack.js?v=3.1.3',
        'source/jquery.fancybox.pack.js?v=2.1.5',
        'source/helpers/jquery.fancybox-buttons.js?v=1.0.5',
        'source/helpers/jquery.fancybox-thumbs.js?v=1.0.7',
        'source/helpers/jquery.fancybox-media.js?v=1.0.6'
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}