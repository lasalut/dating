<?php

namespace modules\photo\frontend\assets;

use yii\web\AssetBundle;

class JqueryFileUploadAsset extends AssetBundle
{
    public $sourcePath = '@modules/photo/frontend/assets';

    public $css = [
        'jqueryFileUpload/css/jquery.fileupload.css',
    ];

    public $js = [
        'jqueryFileUpload/vendor/jquery.ui.widget.js',
        'jqueryFileUpload/load-image.all.min.js',
        'jqueryFileUpload/canvas-to-blob.min.js',
        'jqueryFileUpload/jquery.iframe-transport.js',
        'jqueryFileUpload/jquery.fileupload.js',
        'jqueryFileUpload/jquery.fileupload-process.js',
        'jqueryFileUpload/jquery.fileupload-image.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}