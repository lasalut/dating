<?php

/**
 * @var \yii\web\View $this
 * @var \modules\profiles\common\models\Profile $profile
 * @var \modules\photo\common\models\Album $album
 * @var \modules\photo\common\models\Album $albumNudes
 * @var Album $album
 * @var Photo[] $photos
 * @var Photo[] $photosNudes
 */

use modules\photo\common\models\Album;
use modules\photo\common\models\Photo;
use modules\photo\frontend\assets\PhotoAsset;

$this->title = 'Мой фотоальбом';
PhotoAsset::register($this);
?>

<div class="row photo-header">
	<div class="col-md-4">
		<h1>Мой фотоальбом</h1>
	</div>
	<div class="col-md-8">
		<span class="btn btn-primary fileinput-button">
			<i class="glyphicon glyphicon-plus"></i>
			<span>Выберите фотографии</span>
			<input id="fileupload" type="file" name="files[]" multiple>
    	</span>
		<button class="btn btn-default" id="upload-all-btn">Загрузить файлы</button>
	</div>
</div>

<ul class="nav nav-tabs" id="nav-albums">
	<li data-album="<?= $album->id ?>" class="active">
		<a data-toggle="tab" href="#photos">Основной альбом</a>
	</li>
    <?php if ($profile->isMale() == false): ?>
		<li data-album="<?= $albumNudes->id ?>">
			<a data-toggle="tab" href="#photosNudes">Фото 18+</a>
		</li>
    <?php endif; ?>
</ul>

<div id="progress" class="progress">
	<div class="progress-bar progress-bar-success"></div>
</div>
<div id="files" class="files row row-5"></div>

<div class="tab-content">
	<div id="photos" class="tab-pane fade in active">
		<div class="row row-5">
            <?php for ($i = 0; $i < count($photos); $i++): ?>
				<div class="photo col-md-5ths col-xs-6">
					<a class="fancybox-thumb <?= $photos[$i]->printClasses() ?>" rel="fancybox-thumb"
					   href="<?= $photos[$i]->url ?>" target="_blank" title="<?= $photos[$i]->printTitle() ?>">
						<img src="<?= $photos[$i]->crop ?>"/>
					</a>
				</div>
            <?php endfor; ?>
		</div>
	</div>
    <?php if ($profile->isMale() == false): ?>
		<div id="photosNudes" class="tab-pane fade">
			<div class="row row-5">
                <?php for ($i = 0; $i < count($photosNudes); $i++): ?>
					<div class="photo col-md-5ths col-xs-6">
						<a class="fancybox-thumb <?= $photosNudes[$i]->printClasses() ?>" rel="fancybox-thumb"
						   href="<?= $photosNudes[$i]->url ?>" target="_blank"
						   title="<?= $photosNudes[$i]->printTitle() ?>">
							<img src="<?= $photosNudes[$i]->crop ?>"/>
						</a>
					</div>
                <?php endfor; ?>
			</div>

			<div class="row photo-warning">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					Вы можете добавить сюда свои интимные и откровенные фотографии, доступ к которым будет разрешен
					лишь пользователям сайта, купившим аккаунт «Премиум Plus». Все остальные пользователи не будут иметь
					возможность их увидеть, потому что картинка будет размыта
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
    <?php endif; ?>
</div>

<div class="row photo-warning">
	<div class="col-md-2"></div>
	<div class="col-md-8">
		<h3>Правила размещения фотографий</h3>
		<p>Фотографии тщательно проверяются модераторами, поэтому чтобы избежать
			блокировки или отклонения фотографий, тщательно подходите к их выбору.</p>
		<p><b>Запрещено использовать:</b></p>
		<ul class="list">
			<li>фото животных, местности</li>
			<li>скаченные из интернета</li>
			<li>фото других людей</li>
			<li>групповые фото, с количеством людей на них больше 1</li>
			<li>фото, содержащие нецензурную лексику, религиозные или политические лозунги, юмористические скетчи</li>
		</ul>
		<p>Подходите в выбору фото серьезно, ведь именно от них зависит ваш успех.</p>
	</div>
	<div class="col-md-2"></div>
</div>