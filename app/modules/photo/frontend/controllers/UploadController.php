<?php

namespace modules\photo\frontend\controllers;

use modules\photo\common\models\Album;
use modules\photo\frontend\models\UploadForm;
use modules\profiles\common\models\Profile;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class UploadController extends Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        Yii::$app->response->statusCode = 200;
        Yii::$app->response->format = Response::FORMAT_JSON;

        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'accessControl' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                    ]
                ]
            ]
        ];
    }

    public function actionIndex($album_id)
    {
        $model = new UploadForm();
        $model->files = UploadedFile::getInstancesByName('files');

        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;
        /** @var Album $album */
        $album = Album::findOne($album_id);

        if (!$profile || !$album || $album->profile_id != $profile->id) {
            throw new ForbiddenHttpException();
        }

        if ($model->validate()) {
            $photos = $model->upload($album);

            return ['files' => $photos];
        }
        else {
            Yii::$app->response->statusCode = 400;
            return ['result' => 'FAIL', 'errors' => $model->getFirstErrors()];
        }
    }
}