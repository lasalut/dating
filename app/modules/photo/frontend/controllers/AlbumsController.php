<?php

namespace modules\photo\frontend\controllers;

use modules\photo\common\models\Album;
use modules\photo\common\models\Photo;
use modules\profiles\common\models\Profile;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class AlbumsController extends Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'accessControl' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                    ]
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;

        $album = Album::getMain($profile);
        $albumNudes = Album::getNudes($profile);

        $photos = $album->getPhotos();
        $photosNudes = $albumNudes->getPhotos();

        return $this->render('index', compact('profile', 'album', 'photos', 'albumNudes', 'photosNudes'));
    }

    public function actionClear()
    {
        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;

        $query = Photo::find()->where(['profile_id' => $profile->id]);

        foreach ($query->each() as $photo) {
            /** @var Photo $photo */
            $photo->delete();
        }

        return $this->redirect('/photo/albums');
    }
}