<?php

namespace modules\profiles\backend\models;

use modules\profiles\common\models\City;
use modules\profiles\common\models\Country;
use modules\profiles\common\models\Dealer;
use modules\profiles\common\models\Profile;
use modules\profiles\common\models\Region;
use marketingsolutions\finance\models\Purse;
use yii\db\ActiveQuery;
use yz\admin\search\WithExtraColumns;

/**
 * Class ProfileWithData
 */
class ProfileWithData extends Profile
{
    use WithExtraColumns;

    protected static function extraColumns()
    {
        return [
            'country__title',
            'city__title',
            'region__title',
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), static::extraColumns());
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'country__title' => 'Страна',
            'city__title' => 'Город',
            'region__title' => 'Регион',
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()
            ->select(static::selectWithExtraColumns(['profile.*']))
            ->from(['profile' => self::tableName()])
            ->leftJoin(['city' => City::tableName()], 'city.id = profile.city_id')
            ->leftJoin(['region' => Region::tableName()], 'region.id = city.region_id')
            ->leftJoin(['country' => Country::tableName()], 'country.id = region.country_id');
    }
}