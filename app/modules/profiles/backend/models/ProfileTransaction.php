<?php

namespace modules\profiles\backend\models;
use modules\profiles\common\models\City;
use modules\profiles\common\models\Dealer;
use modules\profiles\common\models\Profile;
use marketingsolutions\finance\models\Purse;
use marketingsolutions\finance\models\Transaction;
use modules\profiles\common\models\Region;
use yii\db\ActiveQuery;
use yz\admin\search\WithExtraColumns;


/**
 * Class ProfileTransaction
 * @property Profile $profile
 */
class ProfileTransaction extends Transaction
{
    use WithExtraColumns;

    /**
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()
            ->select(static::selectWithExtraColumns(['transaction.*']))
            ->from(['transaction' => ProfileTransaction::tableName()])
			->leftJoin(['purse' => Purse::tableName()], 'purse.id = transaction.purse_id')
			->leftJoin(['profile' => Profile::tableName()], 'profile.id = purse.owner_id')
			->leftJoin(['city' => City::tableName()], 'city.id = profile.city_id')
            ->leftJoin(['region' => Region::tableName()], 'region.id = city.region_id')
            ->where(['purse.owner_type' => Profile::class]);
    }

    protected static function extraColumns()
    {
        return [
            'purse__balance',
            'purse__owner_id',
            'profile__name',
            'profile__phone_mobile',
			'city__title',
            'region__title',
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), static::extraColumns());
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'purse__balance' => 'Баланс',
            'purse__owner_id' => 'ID участника',
            'profile__name' => 'Участник',
            'profile__phone_mobile' => 'Номер телефона',
			'city__title' => 'Город',
			'region__title' => 'Регион',
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['id' => 'owner_id'])
            ->via('purse');
    }

}