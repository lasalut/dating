<?php

namespace modules\profiles\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;
use modules\profiles\common\models\PremiumHistory;

/**
 * PremiumHistorySearch represents the model behind the search form about `modules\profiles\common\models\PremiumHistory`.
 */
class PremiumHistorySearch extends PremiumHistoryWithData implements SearchModelInterface

{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'profile_id', 'hours', 'days', 'price'], 'integer'],
            [['premium_id', 'expires_at', 'created_at', 'updated_at'], 'safe'],
            [['broadcasts', 'contacts', 'minutes', 'days_incognito', 'days_online', 'days_nudes'], 'integer'],
            [static::extraColumns(), 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function getQuery()
    {
        return PremiumHistoryWithData::find();
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = $this->getQuery();
        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            'history.id' => $this->id,
            'history.profile_id' => $this->profile_id,
            'history.premium_id' => $this->premium_id,
            'history.hours' => $this->hours,
            'history.days' => $this->days,
            'history.price' => $this->price,
            'history.broadcasts' => $this->broadcasts,
            'history.contacts' => $this->contacts,
            'history.minutes' => $this->minutes,
            'history.days_incognito' => $this->days_incognito,
            'history.days_online' => $this->days_online,
            'history.days_nudes' => $this->days_nudes,
        ]);

        self::filtersForExtraColumns($query);
    }
}
