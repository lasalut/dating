<?php

namespace modules\profiles\backend\models;

use modules\profiles\common\models\Premium;
use modules\profiles\common\models\PremiumHistory;
use modules\profiles\common\models\Profile;
use yii\db\ActiveQuery;
use yz\admin\search\WithExtraColumns;


class PremiumHistoryWithData extends PremiumHistory
{
    use WithExtraColumns;

    protected static function extraColumns()
    {
        return [
            'profile__name',
            'premium__title',
            'premium__package',
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), static::extraColumns());
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'profile__name' => 'Участник',
            'premium__title' => 'Премиум, название',
            'premium__package' => 'Премиум, пакет',
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()
            ->select(static::selectWithExtraColumns(['history.*']))
            ->from(['history' => self::tableName()])
            ->leftJoin(['premium' => Premium::tableName()], 'history.premium_id = premium.id')
            ->leftJoin(['profile' => Profile::tableName()], 'history.profile_id = profile.id');
    }
}