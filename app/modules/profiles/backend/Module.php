<?php

namespace modules\profiles\backend;
use modules\profiles\backend\rbac\Rbac;
use yz\icons\Icons;


/**
 * Class Module
 */
class Module extends \modules\profiles\common\Module
{
    public function getName()
    {
        return 'Профили участников';
    }

    public function getAdminMenu()
    {
        return [
            [
                'label' => 'Участники программы',
                'icon' => Icons::o('users'),
                'items' => [
                    [
                        'route' => ['/profiles/profiles/index'],
                        'label' => 'Профили участников',
                        'icon' => Icons::o('user'),
                    ],
                    [
                        'route' => ['/photo/photos/index'],
                        'label' => 'Фотографии',
                        'icon' => Icons::o('photo'),
                    ],
                    [
                        'route' => ['/profiles/profile-complaints/index'],
                        'label' => 'Жалобы на участников',
                        'icon' => Icons::o('ban'),
                    ],
                    [
                        'route' => ['/profiles/broadcasts/index'],
                        'label' => 'Рассылки',
                        'icon' => Icons::o('retweet'),
                    ],
                    [
                        'route' => ['/profiles/profile-transactions/index'],
                        'label' => 'Движение баллов',
                        'icon' => Icons::o('list'),
                    ],
                    [
                        'route' => ['/profiles/countries/index'],
                        'label' => 'Страны',
                        'icon' => Icons::o('list'),
                    ],
                    [
                        'route' => ['/profiles/regions/index'],
                        'label' => 'Регионы',
                        'icon' => Icons::o('list'),
                    ],
                    [
                        'route' => ['/profiles/cities/index'],
                        'label' => 'Города',
                        'icon' => Icons::o('list'),
                    ],
                    [
                        'route' => ['/profiles/subways/index'],
                        'label' => 'Метро',
                        'icon' => Icons::o('list'),
                    ],
                ],
            ],
            [
                'label' => 'Премиум',
                'icon' => Icons::o('gift'),
                'items' => [
                    [
                        'route' => ['/profiles/premiums/index'],
                        'label' => 'Премиум',
                        'icon' => Icons::o('gift'),
                    ],
                    [
                        'route' => ['/profiles/premium-history/index'],
                        'label' => 'Премиум история',
                        'icon' => Icons::o('list'),
                    ],
                    [
                        'route' => ['/profiles/broadcasts/index'],
                        'label' => 'Рассылки',
                        'icon' => Icons::o('list'),
                    ],
                ],
            ],
        ];
    }

    public function getAuthItems()
    {
        return array_merge(parent::getAuthItems(), Rbac::dependencies());
    }
}