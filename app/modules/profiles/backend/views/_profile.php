<?php
/**
 * @var yii\web\View $this
 * @var \modules\profiles\common\models\Profile $profile
 * @var integer $profile_id
 */

$profile = \modules\profiles\common\models\Profile::findOne($profile_id);
?>

<?php if ($profile): ?>
	<div class="row">
		<div class="col-md-4">
			<img src="<?= $profile->avatar_path ?>" style="max-width:100%"/>
		</div>
		<div class="col-md-8">
			<div>
				<a href="/profiles/profiles/index?ProfileSearch[id]=<?= $profile->id ?>">
					<b><?= $profile->name ?></b>
				</a>
			</div>
			<div>
				<?php if ($city = $profile->city): ?>
					<?= $profile->years ?>, <?= $city->title ?>
				<?php else: ?>
                    <?= $profile->years ?>
				<?php endif; ?>
			</div>
			<div style="margin-top:10px">
				<a href="/profiles/profiles/login?id=<?= $profile->id ?>"
				   class="btn btn-xs btn-default"><i class="fa fa-key"></i> войти</a>
			</div>
		</div>
	</div>
<?php endif; ?>