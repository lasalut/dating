<?php

use common\utils\Notifier;
use modules\photo\common\models\Photo;
use modules\profiles\common\models\Profile;
use yii\helpers\Html;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;
use yz\icons\Icons;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\profiles\backend\models\ProfileSearch $searchModel
 * @var array $columns
 */

$this->title = modules\profiles\common\models\Profile::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

\modules\photo\frontend\assets\Fancybox2Asset::register($this);
$js = <<<JS
	$(document).ready(function() {
		$('.fancybox').fancybox();
		
		$('.btn-block').click(function(e) {
			var reason = prompt('Укажите причину блокировки участника');
			this.href = this.href + '&reason=' + reason;
		})
	});
JS;
$this->registerJs($js);

$css = <<<CSS
	.toggler {
		font-size: 26px;
		display: block;
		margin: 10px auto 0;
		width: 100px;
		text-align: center;
		padding: 20px 20px;
	}
	.btn-controls {
		display: block !important;
		margin-bottom: 3px;
	}
CSS;
$this->registerCss($css);
?>

<?php $box = Box::begin(['cssClass' => 'profile-index box-primary']) ?>
<div class="text-right">
    <?php echo ActionButtons::widget([
        'order' => [['export', 'approve', 'delete', 'return']],
        'gridId' => 'profile-grid',
        'searchModel' => $searchModel,
        'modelClass' => \modules\profiles\common\models\Profile::class,
        'buttons' => [
            'approve' => function () {
                return Html::a(Icons::p('check') . 'Подтвердить аватарки', ['approve'], [
                    'class' => 'btn btn-info selection',
                    'title' => 'Подтвердить фотографии',
                ]);
            },
            'ban' => function () {
                return Html::a(Icons::p('ban') . 'Отклонить аватарки', ['ban'], [
                    'class' => 'btn btn-warning selection',
                    'title' => 'Отклонить фотографии',
                ]);
            }
        ]
    ]) ?>
</div>

<?= GridView::widget([
    'id' => 'profile-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => array_merge([
        ['class' => 'yii\grid\CheckboxColumn'],
    ], [
        [
            'class' => 'yz\admin\widgets\ActionColumn',
            'template' => '{login} {photos} {view}',
            'contentOptions' => ['style' => 'width: 50px'],
            'header' => 'Управление',
            'buttons' => [
                'login' => function ($url, Profile $model) {
                    return Html::a(Icons::i('key'), '/profiles/profiles/login?id=' . $model->id, [
                        'target' => '_blank',
                        'title' => 'Войти под участником',
                        'data-method' => 'post',
                        'class' => 'btn btn-warning btn-sm btn-controls',
                        'data-pjax' => '0',
                    ]);
                },
                'photos' => function ($url, Profile $model) {
                    $count = Photo::find()->where(['profile_id' => $model->id])->count();
                    return Html::a(
                        ($count ? $count . ' ' : '') . Icons::i('photo'),
                        '/photo/photos/index?PhotoSearch[profile_id]=' . $model->id,
                        [
                            'title' => 'Фотогаллерея',
                            'data-method' => 'post',
                            'class' => 'btn btn-default btn-sm btn-controls',
                            'data-pjax' => '0',
                        ]
                    );
                },
                'view' => function ($url, Profile $model) {
                    return Html::a(Icons::i('user'), '/profiles/profiles/update?id=' . $model->id, [
                        'target' => '_blank',
                        'title' => 'Просмотреть',
                        'data-method' => 'post',
                        'class' => 'btn btn-info btn-sm btn-controls',
                        'data-pjax' => '0',
                    ]);
                },
            ],
        ],
    ], $columns),
]); ?>
<?php Box::end() ?>
