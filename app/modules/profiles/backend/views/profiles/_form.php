<?php

use modules\photo\frontend\assets\PhotoAsset;
use modules\profiles\common\models\Profile;
use yz\admin\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var Profile $model
 * @var yz\admin\widgets\ActiveForm $form
 */

$profile = $model;

# CSS
$css = <<<CSS
    .error-summary {
        color: maroon;
        background: beige;
        border: 1px solid #ddd;
        padding: 7px 12px;
    }
	/*********************************************************/
	.photogallery {
		margin-bottom: 20px;
	}
	.photo {
		text-align:    center;
		margin-bottom: 14px;
	}
	.photo a {
		height:             auto;
		display:            block;
		cursor:             pointer;
		position:           relative;
		width:              100%;
		max-width:          200px;
		padding:            2px;
		border-radius:      5px;
		-webkit-box-shadow: 0 1px 4px 0 rgba(0, 0, 0, .3);
		-moz-box-shadow:    0 1px 4px 0 rgba(0, 0, 0, .3);
		box-shadow:         0 1px 4px 0 rgba(0, 0, 0, .3);
		color:              inherit;
	}
	.photo img {
		border:     1px solid #ccc;
		max-width:  100%;
		max-height: 100%;
	}
	.btn-block {
		text-align: center;
		margin: 10px 0 20px;
	}
	.avatar-block {
		margin: 0 auto 10px;
		width:              100%;
		max-width:          270px;
		height:             270px;
		padding:            10px;
		border-radius:      5px;
		-webkit-box-shadow: 0 1px 4px 0 rgba(0, 0, 0, .3);
		-moz-box-shadow:    0 1px 4px 0 rgba(0, 0, 0, .3);
		box-shadow:         0 1px 4px 0 rgba(0, 0, 0, .3);
	}
	.avatar-block img {
		width: 100%;
	}
	.center {
		text-align: center;
	}
	.col-photos {
		padding-right: 35px;
	}
	#profile-avatar_local {
    	display: block;
    	margin: 0 auto;
	}
CSS;
$this->registerCss($css);

# JS
$js = <<<JS
	let album_id = 0;
JS;
$this->registerJs($js, \yii\web\View::POS_HEAD);

$photos = \modules\photo\common\models\Photo::findAllByProfile($model);
PhotoAsset::register($this);
?>

<div class="container" style="width:100%">
    <?php $form = ActiveForm::begin(); ?>

	<div class="row">
		<div class="col-md-6 center col-photos">
			<div class="avatar-block">
				<img src="<?= getenv('FRONTEND_WEB') . $model->avatar_path ?>">
			</div>
            <?= $form->field($model, 'avatar_local')->fileInput()->label('') ?>

			<div class="btn-block">
				<button type="submit" class="btn btn-primary">
					Сохранить
				</button>
			</div>

			<!-- ФОТОГАЛЛЕРЕЯ -->
			<div class="row">
                <?php foreach ($photos as $photo): ?>
					<div class="col-md-3 photo">
						<a class="fancybox-thumb <?= $photo->printClasses() ?>" rel="fancybox-thumb"
						   href="<?= $photo->url ?>" target="_blank" title="<?= $photo->printTitle() ?>">
							<img src="<?= $photo->crop ?>"/>
						</a>
					</div>
                <?php endforeach; ?>
			</div>
		</div>
		<div class="col-md-6">
            <?= $form->errorSummary($model); ?>

			<div class="panel panel-default">
				<div class="panel-heading">
					Персональные данные
				</div>
				<div class="panel-body">
                    <?= $form->field($profile, 'name') ?>
                    <?= $form->field($model, 'phone_mobile_local')->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '+7 999 999-99-99',
                    ]) ?>
                    <?= $form->field($profile, 'email')->input('email') ?>
                    <?= $form->field($model, 'birthday_on_local')->widget(\marketingsolutions\widgets\DatePicker::class,
                        \marketingsolutions\datetime\DatePickerConfig::get($model, 'birthday_on_local', [
                            'options' => [
                                'class' => 'form-control b-form-control'
                            ],
                            'clientOptions' => [
                                'startView' => 'decade',
                                'autoclose' => true,
                            ],
                        ], \marketingsolutions\widgets\DatePicker::class)) ?>

                    <?= $form->field($profile, 'premium_date')->textInput() ?>
                    <?= $form->field($profile, 'premium_minutes')->input('number') ?>
                    <?= $form->field($profile, 'premium_contacts')->input('number') ?>
                    <?= $form->field($profile, 'premium_ups')->input('number') ?>
                    <?= $form->field($profile, 'premium_broadcasts')->input('number') ?>

					<div class="btn-block">
						<button type="submit" class="btn btn-primary">
							Сохранить
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

    <?php ActiveForm::end(); ?>
</div>
