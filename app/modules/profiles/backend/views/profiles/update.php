<div class="text-right">
    <?php use yz\admin\widgets\ActionButtons;
    use yz\admin\widgets\Box;

    Box::begin() ?>
    <?=  ActionButtons::widget([
        'order' => [['index', 'return']],
        'addReturnUrl' => false,
    ]) ?>
    <?php  Box::end() ?>
</div>

<?php echo $this->render('_form', [
    'model' => $model,
]); ?>