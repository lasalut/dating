<?php

use yii\helpers\Html;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use yz\admin\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var modules\profiles\common\models\Premium $model
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<?php $box = FormBox::begin(['cssClass' => 'premium-form box-primary', 'title' => '']) ?>
<?php $form = ActiveForm::begin(); ?>

<?php $box->beginBody() ?>
<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'package')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'price')->input('number') ?>
<?= $form->field($model, 'days')->input('number') ?>

<?= $form->field($model, 'contacts')->input('number') ?>
<?= $form->field($model, 'minutes')->input('number') ?>
<?= $form->field($model, 'broadcasts')->input('number') ?>
<?= $form->field($model, 'days_incognito')->input('number') ?>
<?= $form->field($model, 'days_online')->input('number') ?>
<?= $form->field($model, 'days_nudes')->input('number') ?>

<?php $box->endBody() ?>

<?php $box->actions([
    AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
    AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
    AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
]) ?>
<?php ActiveForm::end(); ?>

<?php FormBox::end() ?>
