<?php

namespace modules\profiles\backend\controllers;

use Yii;
use modules\profiles\common\models\ProfileComplaint;
use modules\profiles\backend\models\ProfileComplaintSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;

/**
 * ProfileComplaintsController implements the CRUD actions for ProfileComplaint model.
 */
class ProfileComplaintsController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function ($params) {
                    /** @var ProfileComplaintSearch $searchModel */
                    return Yii::createObject(ProfileComplaintSearch::className());
                },
                'dataProvider' => function ($params, ProfileComplaintSearch $searchModel) {
                    $dataProvider = $searchModel->search($params);
                    return $dataProvider;
                },
            ]
        ]);
    }

    /**
     * Lists all ProfileComplaint models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var ProfileComplaintSearch $searchModel */
        $searchModel = Yii::createObject(ProfileComplaintSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(ProfileComplaintSearch $searchModel)
    {
        return [
            'id',
            [
                'label' => 'Кто пожаловался',
                'format' => 'raw',
                'contentOptions' => ['style' => 'width:20%'],
                'value' => function (ProfileComplaint $model) {
                    return $this->renderFile('@modules/profiles/backend/views/_profile.php', [
                        'profile_id' => $model->profile_id,
                    ]);
                }
            ],
            [
                'label' => 'На кого жалоба',
                'format' => 'raw',
                'contentOptions' => ['style' => 'width:20%'],
                'value' => function (ProfileComplaint $model) {
                    return $this->renderFile('@modules/profiles/backend/views/_profile.php', [
                        'profile_id' => $model->with_id,
                    ]);
                }
            ],
            [
                'attribute' => 'source',
                'filter' => ProfileComplaint::getSourceOptions(),
                'titles' => ProfileComplaint::getSourceOptions(),
            ],
            'comment:ntext',
            'created_at:datetime',
        ];
    }

    /**
     * Creates a new ProfileComplaint model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProfileComplaint;

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
            return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ProfileComplaint model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
            return $this->getCreateUpdateResponse($model);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ProfileComplaint model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array) $id;

        foreach ($id as $id_) {
            $this->findModel($id_)->delete();
        }

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProfileComplaint model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return ProfileComplaint the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProfileComplaint::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
