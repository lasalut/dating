<?php

namespace modules\profiles\backend\controllers;

use Yii;
use modules\profiles\common\models\Premium;
use modules\profiles\backend\models\PremiumSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;

/**
 * PremiumsController implements the CRUD actions for Premium model.
 */
class PremiumsController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function ($params) {
                    /** @var PremiumSearch $searchModel */
                    return Yii::createObject(PremiumSearch::className());
                },
                'dataProvider' => function ($params, PremiumSearch $searchModel) {
                    $dataProvider = $searchModel->search($params);
                    return $dataProvider;
                },
            ]
        ]);
    }

    /**
     * Lists all Premium models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var PremiumSearch $searchModel */
        $searchModel = Yii::createObject(PremiumSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        $dataProvider->setSort([
            'defaultOrder' => ['id' => SORT_DESC],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(PremiumSearch $searchModel)
    {
        return [
            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width:60px'],
            ],
            [
                'attribute' => 'title',
                'contentOptions' => ['style' => 'width:500px'],
            ],
            [
                'attribute' => 'package',
                'contentOptions' => ['style' => 'width:160px'],
            ],
            [
                'attribute' => 'days',
                'value' => function (Premium $model) {
                    return empty($model->days) ? '' : $model->days;
                },
            ],
            [
                'attribute' => 'price',
                'value' => function (Premium $model) {
                    return empty($model->price) ? '' : $model->price;
                },
            ],
            [
                'attribute' => 'hours',
                'value' => function (Premium $model) {
                    return empty($model->hours) ? '' : $model->hours;
                },
            ],
            [
                'attribute' => 'broadcasts',
                'value' => function (Premium $model) {
                    return empty($model->broadcasts) ? '' : $model->broadcasts;
                },
            ],
            [
                'attribute' => 'contacts',
                'value' => function (Premium $model) {
                    return empty($model->contacts) ? '' : $model->contacts;
                },
            ],
            [
                'attribute' => 'minutes',
                'value' => function (Premium $model) {
                    return empty($model->minutes) ? '' : $model->minutes;
                },
            ],
            [
                'attribute' => 'days_incognito',
                'value' => function (Premium $model) {
                    return empty($model->days_incognito) ? '' : $model->days_incognito;
                },
            ],
            [
                'attribute' => 'days_online',
                'value' => function (Premium $model) {
                    return empty($model->days_online) ? '' : $model->days_online;
                },
            ],
            [
                'attribute' => 'days_nudes',
                'value' => function (Premium $model) {
                    return empty($model->days_nudes) ? '' : $model->days_nudes;
                },
            ],
        ];
    }

    /**
     * Creates a new Premium model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Premium;

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
            return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Premium model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
            return $this->getCreateUpdateResponse($model);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Premium model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array) $id;

        foreach ($id as $id_) {
            $this->findModel($id_)->delete();
        }

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Premium model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Premium the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Premium::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
