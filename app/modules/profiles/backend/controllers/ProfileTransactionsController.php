<?php

namespace modules\profiles\backend\controllers;

use backend\base\Controller;
use modules\profiles\backend\models\ProfileTransactionSearch;
use marketingsolutions\finance\models\Transaction;
use Yii;
use yz\admin\actions\ExportAction;
use yz\admin\grid\columns\DataColumn;
use marketingsolutions\widgets\DatePicker;

/**
 * Class ProfileTransactionsController
 */
class ProfileTransactionsController extends Controller
{
    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'dataProvider' => function ($params) {
                    $searchModel = Yii::createObject(ProfileTransactionSearch::class);
                    $dataProvider = $searchModel->search($params);
                    return $dataProvider;
                },
            ]
        ]);
    }

    public function actionIndex()
    {
        /** @var ProfileTransactionSearch $searchModel */
        $searchModel = Yii::createObject(ProfileTransactionSearch::class);
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns($searchModel)
    {
        if(!empty($_GET['ProfileTransactionSearch']['created_at'])) {
            $date = $_GET['ProfileTransactionSearch']['created_at'];
        }
        else {
            $date = date('Y-m');
        }

        return [
            ['attribute' => 'id', 'label' => 'ID транзакции'],
            [
                'attribute' => 'type',
                'filter' => Transaction::getTypeValues(),
                'titles' => Transaction::getTypeValues(),
                'labels' => [
                    Transaction::INCOMING => DataColumn::LABEL_SUCCESS,
                    Transaction::OUTBOUND => DataColumn::LABEL_DANGER,
                ]
            ],
            'profile__name',
            'profile__phone_mobile',
			'city__title',
            ['attribute' => 'amount', 'label' => 'Сумма'],
            ['attribute' => 'purse.balance', 'label' => 'Текущий баланс'],
            'title',
            'comment',
            [
                'attribute' => 'created_at',
                'format' => 'date',
                'filter' => DatePicker::widget([
                    'name' => 'ProfileTransactionSearch[created_at]',
                    'model' => $searchModel,
                    'value' => $date,
                    'options' => ['class' => 'form-control input-sm',],
                    'dateFormat' => 'yyyy-MM',
                    'clientOptions' => ['minViewMode'=>1,]
                ])
            ],
        ];
    }
}