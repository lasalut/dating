<?php

namespace modules\profiles\backend\controllers;

use common\utils\Notifier;
use modules\profiles\backend\models\ProfileSearch;
use modules\profiles\backend\models\RegisterForm;
use modules\profiles\backend\models\RegistrationForm;
use modules\profiles\common\models\Bonus;
use modules\profiles\common\models\Profile;
use ms\loyalty\identity\phonesEmails\common\models\Identity;
use ms\loyalty\taxes\common\forms\AccountProfileForm;
use ms\loyalty\taxes\common\models\Account;
use ms\loyalty\taxes\common\models\AccountProfile;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yz\admin\actions\ExportAction;
use yz\admin\contracts\AccessControlInterface;
use yz\admin\grid\filters\BooleanFilter;
use yz\admin\grid\filters\Select2Filter;
use yz\admin\models\User;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\icons\Icons;
use yz\Yz;

/**
 * ProfilesController implements the CRUD actions for Profile model.
 */
class ProfilesController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'dataProvider' => function ($params) {
                    $searchModel = Yii::createObject(ProfileSearch::class);
                    $dataProvider = $searchModel->search($params);

                    return $dataProvider;
                },
            ]
        ]);
    }

    /**
     * Lists all Profile models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = Yii::createObject(ProfileSearch::class);
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $dataProvider->setSort([
            'defaultOrder' => ['id' => SORT_DESC],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns(),
        ]);
    }

    public function getGridColumns()
    {
        return [
            [
                'attribute' => 'avatar_approved',
                'contentOptions' => ['style' => 'width:100px'],
                'filter' => BooleanFilter::instance(),
                'format' => 'raw',
                'value' => function (Profile $model) {
                    return $model->avatar_approved
                        ? Html::a('<i class="fa fa-toggle-on fa-lg"></i>', ['/profiles/profiles/avatar-approved', 'id' => $model->id], ['class' => 'toggler'])
                        : Html::a('<i class="fa fa-toggle-off fa-lg"></i>', ['/profiles/profiles/avatar-approved', 'id' => $model->id], ['class' => 'toggler']);
                }
            ],
            [
                'attribute' => 'avatar_banned',
                'contentOptions' => ['style' => 'width:100px'],
                'filter' => BooleanFilter::instance(),
                'format' => 'raw',
                'value' => function (Profile $model) {
                    return $model->avatar_banned
                        ? Html::a('<i class="fa fa-toggle-on fa-lg"></i>', ['/profiles/profiles/avatar-banned', 'id' => $model->id], ['class' => 'toggler'])
                        : Html::a('<i class="fa fa-toggle-off fa-lg"></i>', ['/profiles/profiles/avatar-banned', 'id' => $model->id], ['class' => 'toggler']);
                }
            ],
            [
                'label' => 'Аватарка',
                'format' => 'raw',
                'value' => function (Profile $model) {
                    $url = getenv('FRONTEND_WEB') . $model->avatar_path;
                    return Html::a(Html::img($url, ['style' => 'max-width:100px']), $url, [
                        'target' => '_blank',
                        'class' => 'fancybox',
                    ]);
                }
            ],
            'name',
            'phone_mobile',
            'email:email',
            'city__title',
            ['attribute' => 'purse.balance', 'label' => 'Текущий баланс'],
            [
                'label' => 'Ручное начисление баллов',
                'format' => 'raw',
                'value' => function (Profile $model) {
                    return Html::a(Icons::i('rub'), ['/manual/manage-bonuses/index', 'id' => $model->id], [
                        'class' => 'btn btn-default btn-sm',
                        'title' => 'Изменить баланс участника',
                    ]);
                }
            ],
            [
                'label' => 'Премиум',
                'format' => 'raw',
                'value' => function (Profile $model) {
                    if (empty($model->premium_date)) {
                        return Html::tag('span', 'нет', ['class' => 'badge badge-default']);
                    }

                    $now = new \DateTime('now');
                    $premium = new \DateTime($model->premium_date);
                    if ($premium > $now) {
                        return Html::tag('span', 'да', ['class' => 'badge badge-success']);
                    }

                    $html = [];
                    $html[] = Html::tag('span', 'нет', ['class' => 'badge badge-default']);
                    $html[] = Html::tag('small', 'был до: ' . $model->premium_date);

                    return implode('<br>', $html);
                }
            ],
            'created_at:datetime',
            [
                'attribute' => 'blocked',
                'filter' => BooleanFilter::instance(),
                'format' => 'raw',
                'value' => function (Profile $profile) {
                    if ($profile->blocked === null) {
                        return '';
                    }

                    $html = $profile->blocked
                        ? 'Заблокирован ' . Html::a('Разблокировать', ['/profiles/profiles/unblock', 'id' => $profile->id], ['class' => 'btn btn-unblock btn-danger'])
                        : 'Активен ' . Html::a('Заблокировать', ['/profiles/profiles/block', 'id' => $profile->id], ['class' => 'btn btn-block btn-success']);

                    return $html;
                }
            ],
            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width:60px'],
            ],
        ];
    }

    /**
     * Updates an existing Profile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));

            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', compact('model'));
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->redirect(getenv('FRONTEND_WEB') . '/profile/' . $model->id);
    }

    /**
     * Finds the Profile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Profile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Profile::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Deletes an existing Profile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array) $id;

        foreach ($id as $id_) {
            $this->findModel($id_)->delete();
        }

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    public function actionBlock($id, $reason = null)
    {
        $model = $this->findModel($id);
        $model->block($reason);

        return $this->goPreviousUrl();
    }

    public function actionUnblock($id)
    {
        $model = $this->findModel($id);
        $model->unblock();

        return $this->goPreviousUrl();
    }

    public function actionLogin($id)
    {
        $profile = $this->findModel($id);
        $hash = md5($profile->id);
        $url = getenv('FRONTEND_WEB') . "/site/login?id={$profile->id}&hash={$hash}";

        return $this->redirect($url);
    }

    public function actionAvatarApproved($id)
    {
        $model = $this->findModel($id);
        /** @var User $admin */
        $admin = Yii::$app->user->identity;

        if ($model->avatar_approved) {
            $model->updateAttributes([
                'avatar_approved_at' => null,
                'avatar_approved_by' => null,
                'avatar_approved' => false,
            ]);
        }
        else {
            $model->updateAttributes([
                'avatar_approved_at' => (new \DateTime())->format('Y-m-d H:i:s'),
                'avatar_approved_by' => $admin->id,
                'avatar_approved' => true,
                'avatar_banned' => false,
            ]);
        }

        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function actionAvatarBanned($id)
    {
        $model = $this->findModel($id);

        if ($model->avatar_banned) {
            $model->updateAttributes([
                'avatar_banned' => false,
            ]);
        }
        else {
            $model->updateAttributes([
                'avatar_banned' => true,
                'avatar_approved_at' => null,
                'avatar_approved_by' => null,
                'avatar_approved' => false,
            ]);
        }

        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    public function actionApprove(array $ids = [])
    {
        if (empty($ids)) {
            return $this->redirect(\Yii::$app->request->referrer ?: Url::home());
        }
        /** @var User $admin */
        $admin = Yii::$app->user->identity;

        foreach ($ids as $id) {
            $model = $this->findModel($id);
            if ($model->avatar_approved) {
                continue;
            }
            $model->updateAttributes([
                'avatar_approved_at' => (new \DateTime())->format('Y-m-d H:i:s'),
                'avatar_approved_by' => $admin->id,
                'avatar_approved' => true,
                'avatar_banned' => false,
            ]);
        }

        Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Подтверждено аватарок: ' . count($ids));

        return $this->redirect(\Yii::$app->request->referrer ?: Url::home());
    }
}
