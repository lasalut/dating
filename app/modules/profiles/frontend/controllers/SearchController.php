<?php

namespace modules\profiles\frontend\controllers;

use frontend\base\Controller;
use Minishlink\WebPush\Subscription;
use Minishlink\WebPush\WebPush;
use modules\profiles\common\models\City;
use modules\profiles\common\models\Country;
use modules\profiles\common\models\Profile;
use modules\profiles\common\models\Region;
use Yii;
use yii\data\ActiveDataProvider;

class SearchController extends Controller
{
    const FROM = 22;
    const TO = 35;

    const SECTION_ALL = 'all';
    const SECTION_ONLINE = 'online';
    const SECTION_TOP = 'top';
    const SECTION_CLOSE = 'close';

    public function actionTest()
    {
        /** @var Profile $model */
        $model = Profile::findOne(['phone_mobile' => '+79299004002']);

        $pushSubscription = Subscription::create([
            "endpoint" => $model->webpush_endpoint,
            'publicKey' => $model->webpush_publicKey,
            'authToken' => $model->webpush_authToken,
            'contentEncoding' => 'aesgcm',
        ]);

        $auth = array(
            'VAPID' => array(
                'subject' => $model->email,
                'publicKey' => getenv('WEBPUSH_PUBLIC_KEY'),
                'privateKey' => getenv('WEBPUSH_PRIVATE_KEY'),
            ),
        );

        $webPush = new WebPush($auth);

        $info = $webPush->sendNotification(
            $pushSubscription,
            'Hello there!',
            true
        );

        var_dump($info);
        exit;
    }

    public function actionIndex()
    {
        /** @var Profile $profile */
        $profile = Yii::$app->user->isGuest ? null : Yii::$app->user->identity;
        $query = Profile::find()->where(Profile::activatedSql());

        $type = Yii::$app->request->get('t', null);
        $section = Yii::$app->request->get('s', self::SECTION_ALL);

        if ($profile && $type === null) {
            $profile->my_gender == Profile::GENDER_MALE
                ? $query->andWhere(['my_gender' => Profile::GENDER_FEMALE])
                : $query->andWhere(['my_gender' => Profile::GENDER_MALE]);
        }
        elseif ($type == 'm') {
            $query->andWhere(['my_gender' => Profile::GENDER_MALE]);
        }
        else {
            $query->andWhere(['my_gender' => Profile::GENDER_FEMALE]);
        }

        if ($from = Yii::$app->request->get('from')) {
            if ($to = Yii::$app->request->get('to')) {
                $fromDate = new \DateTime("-$from years");
                $toDate = new \DateTime("-$to years");
                $query->andWhere(['>=', 'birthday_on', $toDate->format('Y-m-d')]);
                $query->andWhere(['<=', 'birthday_on', $fromDate->format('Y-m-d')]);
            }
        }

        if ($city = Yii::$app->request->get('c')) {
            $titles = explode(',', $city);
            $city = trim($titles[0]);
            $region = null;
            $country = null;

            if (isset($titles[2])) {
                $region = trim($titles[1]);
                $country = trim($titles[2]);
            }

            $query
                ->leftJoin(['c' => City::tableName()], 'c.id = {{%profiles}}.city_id')
                ->leftJoin(['r' => Region::tableName()], 'r.id = c.region_id')
                ->leftJoin(['co' => Country::tableName()], 'co.id = c.country_id')
                ->andWhere("c.title LIKE :cityName", [':cityName' => $city . '%']);

            if ($country) {
                $query->andWhere("co.title LIKE :countryName", [':countryName' => $country . '%']);
            }
        }

        if ($profile) {
            $query->andWhere(['!=', '{{%profiles}}.id', $profile->id]);
        }

        if ($section == self::SECTION_ONLINE) {
            $now = (new \DateTime('-5 minutes'))->format('Y-m-d H:i:s');
            $query->andWhere(['>=', '{{%profiles}}.online_at', $now]);
        }

        $query->orderBy(['premium_priority' => SORT_DESC, 'created_at' => SORT_DESC]);

        $models = $query->all();

        $photos = Profile::countPhotos();
        $likedAvatars = $profile ? $profile->findLikedAvatars() : null;

        return $this->render('index', compact('profile', 'models', 'photos', 'likedAvatars'));
    }
}