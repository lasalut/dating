<?php

namespace modules\profiles\frontend\controllers;

use modules\profiles\common\models\Broadcast;
use modules\profiles\common\models\Premium;
use modules\profiles\common\models\Profile;
use modules\profiles\frontend\models\BroadcastForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class BroadcastController
 */
class BroadcastController extends Controller
{
    public function behaviors()
    {
        return [
            'accessControl' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                    ]
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;
        $myCityId = $profile->city_id;

        $broadcast = new BroadcastForm();
        $broadcast->profile_id = $profile->id;

        if ($broadcast->load(Yii::$app->request->post()) && $broadcast->process()) {
            Yii::$app->session->setFlash('info', 'Ваша рассылка добавлена на ' . $broadcast->premium->hoursText());
            return $this->redirect(['/broadcast']);
        }

        $premiums = Premium::find()->where(['like', 'code', 'broadcast-'])->orderBy(['hours' => SORT_ASC])->all();
        $myContacts = $profile->findMyContacts();

        $now = (new \DateTime())->format('Y-m-d H:i:s');
        /** @var Broadcast[] $broadcasts */
        $broadcasts = Broadcast::find()
            ->where(['not like', 'excluded', '-' . $profile->id . '-'])
            ->andWhere(['>=', 'expires_at', $now])
            ->andWhere('(city_id IS NULL OR city_id = :cityId)')
            ->addParams([':cityId' => $myCityId])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();

        $photos = Profile::countPhotos();
        $likedAvatars = $profile ? $profile->findLikedAvatars() : null;

        return $this->render('index', compact('broadcast', 'broadcasts', 'premiums', 'profile',
            'myContacts', 'likedAvatars', 'photos'));
    }
}