<?php

namespace modules\profiles\frontend\controllers;

use marketingsolutions\finance\models\Transaction;
use modules\profiles\common\finance\PremiumHistoryPartner;
use modules\profiles\common\models\Premium;
use modules\profiles\common\models\PremiumHistory;
use modules\profiles\common\models\Profile;
use modules\profiles\frontend\models\CallsForm;
use modules\profiles\frontend\models\ContactsForm;
use modules\profiles\frontend\models\IncognitoForm;
use modules\profiles\frontend\models\OnlineForm;
use modules\profiles\frontend\models\StickedForm;
use modules\profiles\frontend\models\TopForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class PremiumController
 */
class PremiumController extends Controller
{
    public function behaviors()
    {
        return [
            'accessControl' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                    ]
                ]
            ]
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionBuy()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $premiumCode = Yii::$app->request->post('premium');
        $premium = Premium::findOne(['code' => $premiumCode]);
        $profile = Yii::$app->user->identity;

        /** @var Profile $profile */
        if ($profile === null) {
            return $this->error('Вы не авторизованы на сайте');
        }

        if ($premium->price > $profile->purse->balance) {
            return $this->error("На вашем счету недостаточно средств ({$profile->purse->balance})"
                . '<a class="badge badge-default" href="/finance" style="margin-left:25px; font-size:16px;">Пополнить счет</a>');
        }

        $trans = Yii::$app->db->beginTransaction();

        try {
            if ($profile->isPremium()) {
                $premiumDate = new \DateTime($profile->premium_date);
                $premiumDate->modify("+ {$premium->days} days");
            }
            else {
                $premiumDate = new \DateTime("+ {$premium->days} days");
            }

            $premiumDateStr = $premiumDate->format('Y-m-d H:i:s');
            $profile->premium_date = $premiumDateStr;

            $h = new PremiumHistory();
            $h->profile_id = $profile->id;
            $h->premium_id = $premium->id;
            $h->price = $premium->price;
            $h->days = $premium->days;
            $h->expires_at = $premiumDateStr;

            $h->days_nudes = $premium->days_nudes;
            $h->days_incognito = $premium->days_incognito;
            $h->days_online = $premium->days_online;
            $h->broadcasts = $premium->broadcasts;
            $h->contacts = $premium->contacts;
            $h->minutes = $premium->minutes;

            if ($premium->days_online) {
                if ($profile->hasPremiumOnline()) {
                    $premiumOnline = new \DateTime($profile->premium_online);
                    $premiumOnline->modify("+ {$premium->days_online} days");
                }
                else {
                    $premiumOnline = new \DateTime("+ {$premium->days_online} days");
                }
                $profile->premium_online = $premiumOnline->format('Y-m-d H:i:s');
            }

            if ($premium->days_nudes) {
                if ($profile->hasPremiumNudes()) {
                    $premiumNudes = new \DateTime($profile->premium_nudes);
                    $premiumNudes->modify("+ {$premium->days_nudes} days");
                }
                else {
                    $premiumNudes = new \DateTime("+ {$premium->days_nudes} days");
                }
                $profile->premium_nudes = $premiumNudes->format('Y-m-d H:i:s');
            }

            if ($premium->days_incognito) {
                if ($profile->hasPremiumIncognito()) {
                    $premiumIncognito = new \DateTime($profile->premium_incognito);
                    $premiumIncognito->modify("+ {$premium->days_incognito} days");
                }
                else {
                    $premiumIncognito = new \DateTime("+ {$premium->days_incognito} days");
                }
                $profile->premium_incognito = $premiumIncognito->format('Y-m-d H:i:s');
            }

            if ($premium->broadcasts) {
                $profile->premium_broadcasts = empty($profile->premium_broadcasts)
                    ? $premium->broadcasts
                    : $premium->broadcasts + $profile->premium_broadcasts;
            }

            if ($premium->contacts) {
                $profile->premium_contacts = empty($profile->premium_contacts)
                    ? $premium->contacts
                    : $premium->contacts + $profile->premium_contacts;
            }

            if ($premium->minutes) {
                $profile->premium_minutes = empty($profile->premium_minutes)
                    ? $premium->minutes
                    : $premium->minutes + $profile->premium_minutes;
            }

            $h->save(false);
            $profile->save(false);

            $profile->purse->addTransaction(Transaction::factory(
                Transaction::OUTBOUND,
                $premium->price,
                new PremiumHistoryPartner(['id' => $h->id]),
                'Покупка премиум-доступа: ' . $premium->title
            ));
            $trans->commit();
        }
        catch (\Exception $e) {
            $trans->rollBack();
            throw $e;
        }

        $profile->purse->refresh();
        $premiumDays = $profile->getPremiumDays();
        $premiumDateFormatted = $premiumDate->format('d.m.Y H:i');
        $days = $premium->days < 5 ? $premium->days . ' дня' : $premium->days . ' дней';

        return [
            'result' => 'OK',
            'msg' => "Премиум-доступ приобретен до {$premiumDateFormatted}, на {$days}.<br><b>{$premium->title}</b>",
            'balance' => $profile->purse->balance,
            'premiumDays' => $premiumDays,
        ];
    }

    public function actionCalls()
    {
        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;

        $model = new CallsForm();
        $model->profile = $profile;

        if ($model->load(Yii::$app->request->post()) && $model->process()) {
            Yii::$app->session->setFlash('info', "Вы приобрели {$model->premium->minutes} минут для звонков");
            return $this->redirect(['/calls']);
        }

        return $this->render('calls', compact('profile', 'model'));
    }

    public function actionIncognito()
    {
        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;

        $model = new IncognitoForm();
        $model->profile = $profile;

        if ($model->load(Yii::$app->request->post()) && $model->process()) {
            $days = Premium::daysText($model->premium->days_incognito);
            Yii::$app->session->setFlash('info', "Вы приобрели $days для услуги \"Инкогнито\"");
            return $this->redirect(['/incognito']);
        }

        return $this->render('incognito', compact('profile', 'model'));
    }

    public function actionOnline()
    {
        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;

        $model = new OnlineForm();
        $model->profile = $profile;

        if ($model->load(Yii::$app->request->post()) && $model->process()) {
            $days = Premium::daysText($model->premium->days_online);
            Yii::$app->session->setFlash('info', "Вы приобрели $days для услуги \"Кто онлайн\"");
            return $this->redirect(['/online']);
        }

        return $this->render('online', compact('profile', 'model'));
    }

    public function actionTop()
    {
        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;

        $model = new TopForm();
        $model->profile = $profile;

        if ($model->load(Yii::$app->request->post()) && $model->process()) {
            Yii::$app->session->setFlash('info', "Ваша анкета в топе!");
            return $this->redirect(['/top']);
        }

        return $this->render('top', compact('profile', 'model'));
    }

    public function actionContacts()
    {
        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;

        $model = new ContactsForm();
        $model->profile = $profile;

        if ($model->load(Yii::$app->request->post()) && $model->process()) {
            Yii::$app->session->setFlash('info', "Приобретено {$model->premium->contacts} контактов");
            return $this->redirect(['/contacts']);
        }

        return $this->render('contacts', compact('profile', 'model'));
    }

    public function actionSticked()
    {
        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;

        $model = new StickedForm();
        $model->profile = $profile;

        if ($model->load(Yii::$app->request->post()) && $model->process()) {
            $days = Premium::daysText($model->premium->days);
            Yii::$app->session->setFlash('info', "Ваша анкета закреплена в ТОПе на {$days}");
            return $this->redirect(['/sticked']);
        }

        return $this->render('sticked', compact('profile', 'model'));
    }

    private function error($msg)
    {
        Yii::$app->response->statusCode = 400;
        return ['result' => 'FAIL', 'error' => $msg];
    }
}