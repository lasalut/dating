<?php

namespace modules\profiles\frontend\controllers;

use modules\profiles\common\models\Profile;
use modules\profiles\common\models\ProfileLike;
use modules\profiles\common\models\ProfileVisit;
use Yii;
use yii\web\Controller;

class VisitsController extends Controller
{
    const SECTION_ALL = 'all';
    const SECTION_TODAY = 'today';
    const SECTION_VISITS = 'visits';
    const SECTION_LIKES = 'likes';

    public function actionIndex()
    {
        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;
        $section = Yii::$app->request->get('s', self::SECTION_ALL);
        $visits = ProfileVisit::findByProfile($profile, $section);

        if ($section == self::SECTION_ALL || $section == self::SECTION_TODAY) {
            ProfileVisit::updateAll(['seen' => true], ['with_id' => $profile->id]);
            ProfileLike::updateAll(['seen' => true], ['with_id' => $profile->id]);
        }
        elseif ($section == self::SECTION_VISITS) {
            ProfileVisit::updateAll(['seen' => true], ['with_id' => $profile->id]);
        }
        elseif ($section == self::SECTION_LIKES) {
            ProfileLike::updateAll(['seen' => true], ['with_id' => $profile->id]);
        }

        return $this->render('visits', compact('profile', 'visits'));
    }
}