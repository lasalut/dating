<?php

namespace modules\profiles\frontend\controllers;

use frontend\base\Controller;
use modules\profiles\common\models\Profile;
use Yii;

class AnyController extends Controller
{
    const TYPE_PREV = 'prev';
    const TYPE_NEXT = 'next';
    const TYPE_CLEAR = 'clear';

    public function actionIndex($type = null)
    {
        /** @var Profile $profile */
        $profile = Yii::$app->user->isGuest ? null : Yii::$app->user->identity;
        $query = Profile::find()->where(Profile::activatedSql());

        if ($profile) {
            $query->andWhere(['my_gender' => $profile->isMale() ? Profile::GENDER_FEMALE : Profile::GENDER_MALE]);
            $query->andWhere(['!=', 'id', $profile->id]);
        }

        if ($type == self::TYPE_CLEAR) {
            Yii::$app->session->remove('any_ids');
            Yii::$app->session->remove('any_id');
        }

        $any_ids = Yii::$app->session->get('any_ids', null);
        $any_id = Yii::$app->session->get('any_id', null);

        if (empty($any_ids) || empty($any_id)) {
            $ids = $query->select('id')->column();
            shuffle($ids);
            $last = count($ids) - 1;
            $prevId = $ids[$last];
            $nextId = $ids[1];
            $any_id = $ids[0];
            Yii::$app->session->set('any_ids', implode(',', $ids));
            Yii::$app->session->set('any_id', $any_id);
        }
        else {
            $any_ids = explode(',', $any_ids);
            $prev = [];
            $next = [];
            $idFound = false;

            foreach ($any_ids as $id) {
                if ($id == $any_id) {
                    $idFound = true;
                    continue;
                }

                if (!$idFound) {
                    $prev[] = $id;
                }
                else {
                    $next[] = $id;
                }
            }

            if ($type == self::TYPE_NEXT) {
                if (!empty($next)) {
                    $any_id = $next[0];
                }
                else {
                    $any_id = $prev[0];
                }
            }
            elseif ($type == self::TYPE_PREV) {
                if (!empty($prev)) {
                    $last = count($prev) - 1;
                    $any_id = $prev[$last];
                }
                else {
                    $last = count($next) - 1;
                    $any_id = $next[$last];
                }
            }

            # находим modelPrev / modelNext
            $idFound = false;
            $prevId = $nextId = null;
            foreach ($any_ids as $id) {
                if ($id == $any_id) {
                    $idFound = true;
                    continue;
                }

                if (!$idFound) {
                    $prevId = $id;
                }
                else {
                    $nextId = $id;
                    break;
                }
            }
            if ($prevId == null) {
                $last = count($any_ids) - 1;
                $prevId = $any_ids[$last];
            }
            if ($nextId == null) {
                $nextId = $any_ids[0];
            }
        }

        /** @var Profile $modelPrev */
        $modelPrev = Profile::findOne($prevId);
        /** @var Profile $nextModel */
        $modelNext = Profile::findOne($nextId);

        /** @var Profile $model */
        $model = Profile::findOne($any_id);
        Yii::$app->session->set('any_id', $any_id);

        $photos = Profile::countPhotos();
        $likedAvatars = $profile ? $profile->findLikedAvatars() : null;

        return $this->render('index', compact('profile', 'modelNext', 'modelPrev', 'model', 'photos', 'likedAvatars'));
    }
}