<?php

namespace modules\profiles\frontend\controllers;

use modules\profiles\common\finance\PaymentsPartner;
use modules\profiles\common\finance\PremiumHistoryPartner;
use modules\profiles\common\models\Premium;
use modules\profiles\common\models\PremiumHistory;
use modules\profiles\common\models\Profile;
use ms\loyalty\finances\common\models\Transaction;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

class FinanceController extends Controller
{
    const SECTION_ALL = 'all';
    const SECTION_TODAY = 'today';
    const SECTION_VISITS = 'visits';
    const SECTION_LIKES = 'likes';

    public function behaviors()
    {
        return [
            'accessControl' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                    ]
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;

        return $this->render('index', compact('profile'));
    }

    public function actionBalance()
    {
        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;

        return $this->render('balance', compact('profile'));
    }

    public function actionPay($amount, $payment_uniq)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->statusCode = 200;

        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;

        if ($purse = $profile->purse) {
            $purse->addTransaction(Transaction::factory(
                Transaction::INCOMING,
                $amount,
                new PaymentsPartner(['id' => $profile->id]),
                'Зачисление на счет #' . $profile->id,
                $payment_uniq
            ));
        }

        $purse->refresh();

        return ['result' => 'OK', 'balance' => $purse->balance];
    }
}