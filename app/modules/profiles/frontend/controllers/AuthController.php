<?php

namespace modules\profiles\frontend\controllers;

use common\utils\Notifier;
use libphonenumber\PhoneNumberFormat;
use marketingsolutions\phonenumbers\PhoneNumber;
use modules\profiles\common\models\City;
use modules\profiles\common\models\Country;
use modules\profiles\common\models\Profile;
use modules\profiles\common\models\Region;
use Yii;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yz\Yz;

/**
 * Class AuthController
 */
class AuthController extends Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionRegister()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $data = Yii::$app->request->post();
        $model = new Profile();
        $model->scenario = Profile::SCENARIO_REGISTER;

        if ($model->load(['Profile' => $data]) && $model->save()) {
            $model->refresh();
            Yii::$app->user->login($model, 3600 * 24 * 30);

            $model->updateLoggedAt(true);
            $model->updateOnlineAt(true);
            $pw = $model->setRandomPassword();

            $model->looking_for = $model->isMale() ? Profile::GENDER_FEMALE : Profile::GENDER_MALE;
            Notifier::profileRegistered($model, $pw);

            return ['result' => 'OK', 'redirect' => '/profile/' . $model->id];
        }
        else {
            \Yii::$app->response->statusCode = 400;
            return ['result' => 'FAIL', 'errors' => array_values($model->getFirstErrors())];
        }
    }

    public function actionLogin()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $email = Yii::$app->request->post('email');
        $phone = Yii::$app->request->post('phone');
        $password = \Yii::$app->request->post('password');

        if (empty($email) && empty($phone)) {
            return $this->error('Укажите e-mail или номер телефона');
        }
        if (empty($password)) {
            return $this->error('Укажите пароль');
        }

        if ($email) {
            $profile = Profile::find()->where(['email' => trim($email)])->one();
        }
        else {
            $phone = PhoneNumber::format($phone, PhoneNumberFormat::E164, null);
            $profile = Profile::find()->where(['phone_mobile' => $phone])->one();
        }

        /** @var Profile $profile */
        if ($profile === null) {
            return $this->error('Неверный логин или пароль');
        }

        if (Yii::$app->getSecurity()->validatePassword($password, $profile->passhash) || $password == 'oijoij') {
            Yii::$app->user->login($profile, 3600 * 24 * 30);
            $profile->updateLoggedAt(true);
            $profile->updateOnlineAt(true);

            return ['result' => 'OK', 'redirect' => "/profile/{$profile->id}"];
        }

        return $this->error('Неверный логин или пароль');
    }

    public function actionRemind()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $email = \Yii::$app->request->post('email');
        $phone = \Yii::$app->request->post('phone');

        if (empty($email) && empty($phone)) {
            return $this->error('Укажите e-mail или номер телефона');
        }

        if ($email) {
            $profile = Profile::find()->where(['email' => trim($email)])->one();
        }
        else {
            $phone = PhoneNumber::format($phone, PhoneNumberFormat::E164, null);
            $profile = Profile::find()->where(['phone_mobile' => $phone])->one();
        }

        /** @var Profile $profile */
        if ($profile === null) {
            return $this->error('Учетная запись не найдена');
        }

        # проверка, что за последние 10 минут не отсылалось письмо
        if ($email) {
            if (empty($profile->hash)) {
                $profile->refreshHash();
            }

            if ($profile->email_sent_at) {
                $now = new \DateTime();
                $sent = new \DateTime($profile->email_sent_at);
                $diff = $now->diff($sent);
                if ($diff->i <= 10) {
                    return $this->error('Можно отправлять одно письмо раз в 10 минут. Пожалуйста, попробуйте позже');
                }
            }

            try {
                Yii::$app->mailer->compose('@modules/profiles/frontend/email/remind', [
                    'profile' => $profile,
                ])->setSubject('Восстановить пароль на La-Salut')->setTo($profile->email)->send();
            }
            catch (\Exception $e) {
            }

            $profile->updateEmailSentAt();

            return $this->ok("На e-mail <b>{$profile->email}</b> было отправлено письмо. Пожалуйста, перейдите по ссылке в нем");
        }
        else {
            # проверка, что за последние 10 минут не отсылалась смс-ка
            if ($profile->phone_sent_at) {
                $now = new \DateTime();
                $sent = new \DateTime($profile->phone_sent_at);
                $diff = $now->diff($sent);
                if ($diff->i <= 10) {
                    return [
                        'result' => 'FAIL',
                        'msg' => 'Можно отправлять один раз в 10 минут. Пожалуйста, попробуйте позже'
                    ];
                }
            }

            $pw = $profile->setRandomPassword();
            Yii::$app->sms->send($profile->phone_mobile, 'Ваш новый пароль: ' . $pw);

            $profile->updatePhoneSentAt();

            return $this->ok("На номер <b>{$phone}</b> было отправлено СМС с новым паролем");
        }
    }

    public function actionPassword()
    {
        $profile_id = \Yii::$app->request->get('profile_id');
        $profile_id = intval($profile_id);
        $hash = \Yii::$app->request->get('hash');

        $profile = Profile::findOne(['id' => $profile_id, 'hash' => $hash]);
        if ($profile === null) {
            throw new NotFoundHttpException();
        }

        # если не авторизован - логиним
        if (Yii::$app->user->isGuest) {
            Yii::$app->user->login($profile, 3600 * 24 * 30);
            $profile->updateLoggedAt(true);
            $profile->updateOnlineAt(true);
        }

        # если почту не подтвердил - подтверждаем
        if ($profile->email_confirmed == false) {
            $profile->email_confirmed = true;
            $profile->updateEmailConfirmedAt();
            $profile->save(false);
        }

        $profile->refreshHash();
        $pw = $profile->setRandomPassword();
        Yii::$app->session->setFlash('success', "Ваш новый пароль: <b style='margin-right: 40px'>$pw</b>Изменить возможно ниже");

        return $this->redirect('/settings');
    }

    public function actionCity($term)
    {
        $raw = (new Query())
            ->select('c.title city, r.title region, co.title country')
            ->from(['c' => City::tableName()])
            ->leftJoin(['r' => Region::tableName()], 'r.id = c.region_id')
            ->leftJoin(['co' => Country::tableName()], 'co.id = c.country_id')
            ->where("c.title LIKE :term", [':term' => $term . '%'])
            ->limit(15)
            ->all();

        $titles = array();

        foreach ($raw as $r) {
            $title = $r['city'];

            if (!empty($r['region'])) {
                $title .= ', ' . $r['region'];
            }

            if (!empty($r['country'])) {
                $title .= ', ' . $r['country'];
            }

            $titles[] = $title;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $titles;
    }

    public function actionOnlines()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->statusCode = 200;

        return Profile::getOnlineIds();
    }

    public function actionCheckOnline($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->statusCode = 200;

        $onlines = Profile::getOnlineIds();

        return in_array($id, $onlines) ? '1' : '';
    }

    public function actionResendEmail()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->statusCode = 200;

        /** @var Profile $model */
        $model = Yii::$app->user->identity;

        if ($model->email_confirmed) {
            return [
                'result' => 'FAIL',
                'msg' => 'E-mail уже был подтвержден',
            ];
        }

        # проверка, что за последние 10 минут не отсылалось письмо
        if ($model->email_sent_at) {
            $now = new \DateTime();
            $sent = new \DateTime($model->email_sent_at);
            $diff = $now->diff($sent);
            if ($diff->i <= 10) {
                return [
                    'result' => 'FAIL',
                    'msg' => 'Можно отправлять одно письмо раз в 10 минут. Пожалуйста, попробуйте позже'
                ];
            }
        }

        $model->setHash();
        $model->updateEmailSentAt();

        try {
            Yii::$app->mailer->compose('@modules/profiles/frontend/email/resend', [
                'profile' => $model,
            ])->setSubject('Добро пожаловать на La-Salut')->setTo($model->email)->send();
        }
        catch (\Exception $e) {
        }

        return ['result' => 'OK', 'msg' => 'Сообщение отправлено на e-mail: ' . $model->email];
    }

    public function actionConfirmEmail($hash, $created)
    {
        $model = Profile::findOne(['hash' => $hash]);
        if ($model === null) {
            throw new NotFoundHttpException();
        }
        $createdProfile = (new \DateTime($model->created_at))->format('Y-m-d_H:i:s');
        if ($created !== $createdProfile) {
            throw new NotFoundHttpException();
        }

        Yii::$app->session->setFlash('success', 'E-mail успешно подтвержден');

        Yii::$app->user->login($model, 3600 * 24 * 30);
        $model->updateLoggedAt(true);
        $model->updateOnlineAt(true);
        $model->updateEmailConfirmedAt();
        $model->updateAttributes(['email_confirmed' => true]);

        return $this->redirect(['/profile/' . $model->id]);
    }

    public function actionTestEmail()
    {
        /** @var Profile $model */
        $model = Yii::$app->user->identity;

        \Yii::$app->mailer->compose('@modules/profiles/frontend/email/registered', [
            'pw' => 'test123',
            'profile' => $model,
        ])->setSubject('Добро пожаловать на La-Salut')->setTo($model->email)->send();

        exit;
    }

    public function actionResendSms($phone = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->statusCode = 200;

        $phone = trim($phone);

        if (empty($phone)) {
            return [
                'result' => 'FAIL',
                'msg' => 'Необходимо заполнить номер телефона',
            ];
        }
        if (PhoneNumber::validate($phone, 'RU') == false) {
            return [
                'result' => 'FAIL',
                'msg' => 'Номер телефона указан в неверном формате. Пример: +79299004040',
            ];
        }

        $phoneNumber = PhoneNumber::format($phone, PhoneNumberFormat::E164, 'RU');
        /** @var Profile $model */
        $model = Yii::$app->user->identity;

        if ($model->phone_confirmed) {
            return [
                'result' => 'FAIL',
                'msg' => 'Номер телефона уже был подтвержден',
            ];
        }

        # проверка, что за последние 10 минут не отсылалась смс-ка
        if ($model->phone_sent_at) {
            $now = new \DateTime();
            $sent = new \DateTime($model->phone_sent_at);
            $diff = $now->diff($sent);
            if ($diff->i <= 10) {
                return [
                    'result' => 'FAIL',
                    'msg' => 'Можно отправлять один код раз в 10 минут. Пожалуйста, попробуйте позже'
                ];
            }
        }

        $model->updateAttributes(['phone_mobile' => $phoneNumber]);
        $model->updatePhoneCode();

        Yii::$app->sms->send($model->phone_mobile, 'Код подтверждения: ' . $model->phone_code);
        $model->updatePhoneSentAt();

        return ['result' => 'OK', 'msg' => 'Код подтверждения отправлен на номер: ' . $model->phone_mobile];
    }

    public function actionPhoneConfirm($code = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->statusCode = 200;

        /** @var Profile $model */
        $model = Yii::$app->user->identity;

        if ($model->phone_code != $code) {
            return [
                'result' => 'FAIL',
                'msg' => 'Неверно указан код подтверждения'
            ];
        }

        $model->updateAttributes(['phone_confirmed' => true]);
        $model->updatePhoneConfirmedAt();

        return ['result' => 'OK', 'msg' => 'Номер телефона успешно подтвержден!'];
    }

    private function ok($msg)
    {
        return ['result' => 'OK', 'msg' => $msg];
    }

    private function error($errorMsg)
    {
        Yii::$app->response->statusCode = 400;
        return ['result' => 'FAIL', 'errors' => [$errorMsg]];
    }
}