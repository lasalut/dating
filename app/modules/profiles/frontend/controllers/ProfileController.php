<?php

namespace modules\profiles\frontend\controllers;

use modules\chat\console\models\ZmqService;
use modules\photo\common\models\Photo;
use modules\profiles\common\models\Profile;
use modules\profiles\common\models\ProfileComplaint;
use modules\profiles\common\models\ProfileLike;
use modules\profiles\common\models\ProfileList;
use modules\profiles\common\models\ProfileVisit;
use modules\profiles\frontend\models\ProfileForm;
use Yii;
use yii\base\NotSupportedException;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\validators\FileValidator;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yz\Yz;

/**
 * Class ProfileController
 */
class ProfileController extends Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionView($id)
    {
        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;
        /** @var Profile $model */
        $model = Profile::findOne($id);

        if ($model == null) {
            throw new NotFoundHttpException();
        }
        if ($model->removed) {
            return $this->render('profile_removed', ['model' => $model]);
        }
        if ($model->blocked) {
            return $this->render('profile_blocked', ['model' => $model]);
        }

        if ($model->isActivated() == false) {
            if (!$profile || $profile->id != $model->id) {
                throw new NotFoundHttpException(); # Анкета не активирована и не моя - ее нельзя просматривать
            }
        }

        if ($profile && $profile->id != $model->id) {
            ProfileVisit::add($profile->id, $model->id);
            ZmqService::publish($model->id, ZmqService::TYPE_VISIT, []);
        }

        $photos = Photo::findApprovedByProfile($model);

        return $this->render('view', compact('profile', 'model', 'photos'));
    }

    public function actionEdit()
    {
        /** @var Profile $model */
        $model = \Yii::$app->user->identity;

        if ($model == null) {
            throw new NotFoundHttpException();
        }
        if ($model->removed) {
            return $this->render('profile_removed', ['model' => $model]);
        }
        if ($model->blocked) {
            return $this->render('profile_blocked', ['model' => $model]);
        }

        $model->scenario = Profile::SCENARIO_EDIT;

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', 'Анкета обновлена');
            return $this->redirect(['edit']);
        }

        return $this->render('edit', ['model' => $model]);
    }

    public function actionRemove()
    {
        /** @var Profile $profile */
        $profile = \Yii::$app->user->identity;

        if ($profile->load(\Yii::$app->request->post())) {
            \Yii::$app->session->setFlash('success', 'Ваша анкета удалена');
            $profile->removed = true;
            $profile->save(false);

            return $this->redirect('/profile/' . $profile->id);
        }

        return $this->render('remove', ['profile' => $profile]);
    }

    public function actionRestore()
    {
        /** @var Profile $profile */
        $profile = \Yii::$app->user->identity;

        if ($profile === null) {
            throw new NotFoundHttpException();
        }

        $profile->removed = false;
        $profile->removed_comment = null;
        $profile->save(false);

        \Yii::$app->session->setFlash('success', 'Ваша анкета восстановлена');

        return $this->redirect('/profile/' . $profile->id);
    }

    public function actionSettings()
    {
        /** @var Profile $model */
        $model = ProfileForm::findOne(\Yii::$app->user->identity->id);
        $model->scenario = Profile::SCENARIO_EDIT;

        if ($model->premium_incognito_on && $model->hasPremiumIncognito() == false) {
            $model->premium_incognito_on = false;
        }

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', 'Изменения сохранены');
            return $this->redirect(['settings', $model->id]);
        }

        return $this->render('settings', ['model' => $model]);
    }

    public function actionWebpushRegister()
    {
        /** @var Profile $model */
        $model = Yii::$app->user->identity;

        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->statusCode = 200;

        $data = Yii::$app->request->post();

        if (!empty($data['stop_webpush'])) {
            $model->updateAttributes([
                'notify_webpush' => false,
                'webpush_endpoint' => null,
                'webpush_publicKey' => null,
                'webpush_authToken' => null,
            ]);
            return ['result' => 'OK'];
        }

        $data = json_decode($data['json'], true);
        # Chrome
        $model->updateAttributes([
            'notify_webpush' => true,
            'webpush_endpoint' => $data['endpoint'],
            'webpush_publicKey' => $data['keys']['p256dh'],
            'webpush_authToken' => $data['keys']['auth'],
        ]);

        return ['result' => 'OK'];
    }

    public function actionList($id, $type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->statusCode = 200;

        /** @var Profile $profile */
        $profile = \Yii::$app->user->identity;
        /** @var Profile $model */
        $model = Profile::findOne($id);

        if (null === $model || null === $profile) {
            throw new NotFoundHttpException();
        }

        if ($list = ProfileList::findOne(['profile_id' => $profile->id, 'with_id' => $model->id, 'type' => $type])) {
            $list->delete();
            return '';
        }
        else {
            $list = new ProfileList();
            $list->type = $type;
            $list->profile_id = $profile->id;
            $list->with_id = $model->id;
            $list->save(false);
            return '1';
        }
    }

    public function actionComplain()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->statusCode = 200;

        $model_id = Yii::$app->request->post('model_id');
        $model_id = intval($model_id);
        $source = Yii::$app->request->post('source');
        $reasons = Yii::$app->request->post('reasons');
        $reason = empty($reasons) ? Yii::$app->request->post('reason') : $reasons;

        /** @var Profile $profile */
        $profile = \Yii::$app->user->identity;
        /** @var Profile $model */
        $model = Profile::findOne($model_id);

        if (!$profile || !$model) {
            throw new NotFoundHttpException();
        }

        $complaint = new ProfileComplaint();
        $complaint->profile_id = $profile->id;
        $complaint->with_id = $model_id;
        $complaint->comment = $reason;
        $complaint->source = $source;
        $complaint->save();

        return 'Ваше обращение принято!';
    }

    public function actionAvatar()
    {
        /** @var Profile $profile */
        $profile = \Yii::$app->user->identity;

        $file = UploadedFile::getInstanceByName('avatar');

        if ($file instanceof UploadedFile && $file->hasError == false) {
            $extension = $this->getImageExtension($file->type);
            $uniqid = uniqid();
            $name = "profile_{$profile->id}_avatar_{$uniqid}.{$extension}";
            $dir = Yii::getAlias("@data/photos");
            FileHelper::createDirectory($dir);
            $path = $dir . DIRECTORY_SEPARATOR . $name;
            $file->saveAs($path);

            $cropName = "profile_{$profile->id}_avatar_crop_{$uniqid}.{$extension}";
            $cropPath = $dir . DIRECTORY_SEPARATOR . $cropName;
            $im = new \Imagick($path);
            $im->cropThumbnailImage(370, 370);
            $im->writeImage($cropPath);

            if (!empty($profile->avatar_path)) {
                $oldFile = Yii::getAlias("@data/photos/{$profile->avatar_path}");
                @unlink($oldFile);
            }
            @unlink($path);

            $profile->avatar = $cropName;
            $profile->avatar_sent = false;
            $profile->avatar_approved = true;
            $profile->avatar_approved_at = (new \DateTime('now'))->format('Y-m-d H:i:s');
            $profile->avatar_approved_by = null;
            $profile->save(false);

            if (getenv('YII_ENV') == 'dev') {
                $frontendDir = Yii::getAlias("@frontendWebroot/data/photos");
                FileHelper::copyDirectory($dir, $frontendDir);
            }
        }

        return new Response();
    }

    public function actionGetAvatarUrl()
    {
        /** @var Profile $profile */
        $profile = \Yii::$app->user->identity;

        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->statusCode = 200;

        return ['avatar_url' => $profile->avatar_url];
    }

    public function actionFabricAvatar()
    {
        $data = \Yii::$app->request->post('base64');
        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data));

        /** @var Profile $profile */
        $profile = \Yii::$app->user->identity;
        $uniqid = uniqid();

        if (!empty($profile->avatar_path)) {
            $oldFile = Yii::getAlias("@data/photos/{$profile->avatar_path}");
            @unlink($oldFile);
        }

        $cropName = "profile_{$profile->id}_avatar_crop_{$uniqid}.jpeg";
        $dir = Yii::getAlias("@data/photos");
        FileHelper::createDirectory($dir);
        $filePath = $dir . DIRECTORY_SEPARATOR . $cropName;

        $profile->avatar = $cropName;
        $profile->avatar_sent = false;
        $profile->avatar_approved = false;
        $profile->avatar_approved_at = null;
        $profile->avatar_approved_by = null;
        $profile->save(false);

        file_put_contents($filePath, $data);
        \Yii::$app->session->setFlash('success', 'Маска успешно добавлена');

        if (getenv('YII_ENV') == 'dev') {
            $frontendDir = Yii::getAlias("@frontendWebroot/data/photos");
            FileHelper::copyDirectory($dir, $frontendDir);
        }

        return new Response();
    }

    private function getImageExtension($type)
    {
        switch ($type) {
            case 'image/png':
                return 'png';
            case 'image/jpg':
                return 'jpg';
            case 'image/jpeg':
                return 'jpeg';
            case 'image/gif':
                return 'gif';
            default:
                throw new NotSupportedException();
        }
    }

    public function actionRotate($to = null, $fileRotated = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->statusCode = 200;

        $file = UploadedFile::getInstanceByName('rotate');
        $degrees = $to == 'right' ? 90 : 270;

        if (!empty($fileRotated)) {
            $dir = Yii::getAlias("@data/tmp");
            $extension = pathinfo($fileRotated, PATHINFO_EXTENSION);
            $rotatedName = uniqid() . '.' . $extension;
            $rotatedPath = $dir . DIRECTORY_SEPARATOR . $fileRotated;
            $rotatedNewPath = Yii::getAlias("@data/tmp/$rotatedName");
            $im = new \Imagick($rotatedPath);
            $im->rotateImage(new \ImagickPixel('#00000000'), $degrees);
            $im->writeImage($rotatedNewPath);
            @unlink($rotatedPath);

            if (getenv('YII_ENV') == 'dev') {
                $frontendDir = Yii::getAlias("@frontendWebroot/data/tmp");
                FileHelper::copyDirectory($dir, $frontendDir);
            }

            return [
                'url' => Yii::getAlias("@frontendWeb/data/tmp/$rotatedName"),
                'fileRotated' => $rotatedName,
            ];
        }
        elseif ($file instanceof UploadedFile && $file->hasError == false) {
            $extension = $this->getImageExtension($file->type);
            $uniqid = uniqid();
            $name = "{$uniqid}.{$extension}";
            $dir = Yii::getAlias("@data/tmp");
            FileHelper::createDirectory($dir);
            $path = $dir . DIRECTORY_SEPARATOR . $name;
            $file->saveAs($path);

            $rotatedName = "{$uniqid}_rotated.{$extension}";
            $rotatedPath = $dir . DIRECTORY_SEPARATOR . $rotatedName;
            $im = new \Imagick($path);
            $im->rotateImage(new \ImagickPixel('#00000000'), $degrees);
            $im->writeImage($rotatedPath);
            @unlink($path);

            if (getenv('YII_ENV') == 'dev') {
                $frontendDir = Yii::getAlias("@frontendWebroot/data/tmp");
                FileHelper::copyDirectory($dir, $frontendDir);
            }

            return [
                'url' => Yii::getAlias("@frontendWeb/data/tmp/$rotatedName"),
                'fileRotated' => $rotatedName,
            ];
        }

        throw new NotSupportedException();
    }

    public function actionLike($id, $type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->statusCode = 200;

        /** @var Profile $profile */
        $profile = \Yii::$app->user->isGuest ? null : \Yii::$app->user->identity;
        /** @var Profile $model */
        $model = Profile::findOne($id);

        if (null === $model || null === $profile) {
            throw new NotFoundHttpException();
        }

        /** @var ProfileLike $like */
        $like = ProfileLike::findOne(['profile_id' => $profile->id, 'with_id' => $model->id, 'type' => $type]);

        if ($like) {
            $like->delete();

            return ['like' => false];
        }
        else {
            $like = new ProfileLike();
            $like->profile_id = $profile->id;
            $like->type = $type;
            $like->with_id = $model->id;
            $like->save();

            ZmqService::publish($model->id, ZmqService::TYPE_LIKE, $like->toArray());

            return ['like' => true];
        }
    }

    public function actionBlocked($id)
    {
        $model = Profile::findOne($id);

        if (null == $model || $model->blocked == false) {
            throw new NotFoundHttpException();
        }

        return $this->render('blocked', compact('model'));
    }
}