<?php

namespace modules\profiles\frontend\widgets;
use ms\loyalty\contracts\identities\IdentityInterface as LoyaltyIdentityInterface;
use yii\base\Widget;
use yii\web\IdentityInterface;


/**
 * Class ProfileTopBar
 */
class ProfileTopBar extends Widget
{
    public function run()
    {
        $user = \Yii::$app->user;
        if ($user->isGuest) {
            return '';
        }
        /** @var IdentityInterface|LoyaltyIdentityInterface $identity */
        $identity = $user->identity;
        $profile = $identity->profile;
        return $this->render('profile-top-bar', compact('user', 'identity', 'profile'));
    }
}