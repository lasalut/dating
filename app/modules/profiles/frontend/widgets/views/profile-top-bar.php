<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \yii\web\User $user
 * @var \yii\web\IdentityInterface $identity
 * @var \modules\profiles\common\models\Profile $profile
 */
?>

<li class="top-bar__text-welcome">
    Добро пожаловать, <span><a href="<?= Url::to(['/profiles/profile/index']) ?>" class="link-primary"><strong><?= Html::encode($profile->name) ?></strong></a></span>!
</li>
<li class="top-bar__balance">
    Бонусный баланс:
    <strong>
        <a href="<?= Url::to(['/profiles/bonuses-transactions/index']) ?>">
            <?= Yii::$app->formatter->asDecimal($profile->purse->balance) ?>
        </a>
        <?= Yii::t('loyalty', 'баллов') ?>
    </strong>
</li>
