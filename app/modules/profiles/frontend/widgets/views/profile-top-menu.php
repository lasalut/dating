<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 */
?>

<?= \yii\widgets\Menu::widget([
    'options' => [
        'tag' => false
    ],
    'items' => [
        ['label' => 'Главная', 'url' => ['/dashboard/index'], 'visible' => ! Yii::$app->user->isGuest],
    ]
]) ?>