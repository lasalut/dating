<?php

namespace modules\profiles\frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class CropperAsset extends AssetBundle
{
    public $sourcePath = '@modules/profiles/frontend/assets';

    public $css = [
        'cropper/cropper.min.css'
    ];

    public $js = [
        'cropper/cropper.min.js'
    ];

    public $depends = [
        'frontend\assets\AppAsset',
    ];
}