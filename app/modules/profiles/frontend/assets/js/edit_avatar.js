$(document).ready(function() {
	var avatar = document.getElementById('avatar');
	var image = document.getElementById('image');
	var input = document.getElementById('input');
	var $alert = $('.alert');
	var $modal = $('#modal');
	var cropper;
	var file;
	var fileRotated = '';

	input.addEventListener('change', function(e) {
		var files = e.target.files;
		var done = function(url) {
			input.value = '';
			image.src = url;
			$alert.hide();
			$modal.modal({
				backdrop: 'static'
			});
		};
		var reader;

		if (files && files.length > 0) {
			file = files[0];

			if (URL) {
				done(URL.createObjectURL(file));
			} else if (FileReader) {
				reader = new FileReader();
				reader.onload = function(e) {
					done(reader.result);
				};
				reader.readAsDataURL(file);
			}
		}
	});

	$modal.on('shown.bs.modal', function() {
		if (cropper) {
			cropper.destroy();
			cropper = null;
		}
		cropper = new Cropper(image, {
			aspectRatio: 1,
			viewMode:    1,
		});
	}).on('hidden.bs.modal', function() {
		cropper.destroy();
		cropper = null;
	});

	$('#rotate-right').click(function() {
		if (cropper) {
			var formData = new FormData();
			formData.append('rotate', file);
			$.ajax('/profiles/profile/rotate?to=right&fileRotated=' + fileRotated, {
				method:      'POST',
				data:        formData,
				processData: false,
				contentType: false,
				success:     function(data) {
					fileRotated = data.fileRotated;
					cropper.replace(data.url);
				}
			});
		}
	});

	$('#rotate-left').click(function() {
		if (cropper) {
			var formData = new FormData();
			formData.append('rotate', file);
			$.ajax('/profiles/profile/rotate?to=left&fileRotated=' + fileRotated, {
				method:      'POST',
				data:        formData,
				processData: false,
				contentType: false,
				success:     function(data) {
					fileRotated = data.fileRotated;
					cropper.replace(data.url);
				}
			});
		}
	});

	document.getElementById('crop').addEventListener('click', function() {
		var initialAvatarURL;
		var canvas;

		$modal.modal('hide');

		if (cropper) {
			canvas = cropper.getCroppedCanvas({
				width:  600,
				height: 600
			});

			initialAvatarURL = avatar.src;
			avatar.src = canvas.toDataURL();
			$alert.removeClass('alert-success alert-warning');
			canvas.toBlob(function(blob) {
				var formData = new FormData();
				formData.append('avatar', blob);

				$.ajax('/profiles/profile/avatar', {
					method:      'POST',
					data:        formData,
					processData: false,
					contentType: false,
					success:     function() {
					},
					error:       function() {
						avatar.src = initialAvatarURL;
						$alert.show().addClass('alert-warning').text('Произошла ошибка при загрузке аватара');
					}
				});
			});
		}
	});
});