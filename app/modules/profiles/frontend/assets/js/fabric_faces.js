$(document).ready(function() {
	var canvas,
		mask2 = new Image(),
		mask3 = new Image(),
		mask4 = new Image(),
		mask5 = new Image(),
		face2,
		face3,
		face4,
		face5
	;

	mask2.src = '/images/masks/mask-2.png';
	mask3.src = '/images/masks/mask-3.png';
	mask4.src = '/images/masks/mask-4.png';
	mask5.src = '/images/masks/mask-5.png';

	canvas = new fabric.Canvas('c');
	canvas.setBackgroundImage(avatar_url, canvas.renderAll.bind(canvas));

	$("body")
		.tooltip({
			selector:  "[data-tooltip=tooltip]",
			container: "body"
		})
		.on("click", "#btn-fabric", function(e) {
			$(".tooltip").tooltip("hide");
			$.getJSON('/profiles/profile/get-avatar-url', function(data) {
				if (avatar_url !== data.avatar_url) {
					canvas.setBackgroundImage(data.avatar_url, canvas.renderAll.bind(canvas));
				}
			});
		})
		.on("click", "#fabric-save", function(e) {
			$('#fabric-save').hide();
			$('.loadersmall').show();

			var base64 = canvas.toDataURL({
				format:  'jpeg',
				quality: 0.8
			});
			// $(".save").html(canvas.toSVG());

			$.ajax({
				type:     "POST",
				url:      '/profiles/profile/fabric-avatar',
				dataType: 'text',
				data:     {
					base64: base64
				},
				success:  function() {
					document.location.reload();
				}
			});
		});

	$('.faces .mask-1').click(function() {
		removeFaces();
	});

	$('.faces .mask-2').click(function() {
		removeFaces();
		face2 = new fabric.Image(mask2, {left: 50, top: 70, scaleX: .20, scaleY: .20});
		canvas.add(face2);
	});

	$('.faces .mask-3').click(function() {
		removeFaces();
		face3 = new fabric.Image(mask3, {left: 50, top: 70, scaleX: .20, scaleY: .20});
		canvas.add(face3);
	});

	$('.faces .mask-4').click(function() {
		removeFaces();
		face4 = new fabric.Image(mask4, {left: 50, top: 70, scaleX: .20, scaleY: .20});
		canvas.add(face4);
	});

	$('.faces .mask-5').click(function() {
		removeFaces();
		face5 = new fabric.Image(mask5, {left: 50, top: 70, scaleX: .20, scaleY: .20});
		canvas.add(face5);
	});

	function removeFaces() {
		canvas.remove(face2);
		canvas.remove(face3);
		canvas.remove(face4);
		canvas.remove(face5);
	}
});