$(document).ready(function() {
	$(document).on('click', '.col-contacts .contact', function() {
		$(this).appendTo('.col-excluded');
	});

	$(document).on('click', '.col-excluded .contact', function() {
		$(this).appendTo('.col-contacts');
	});

	$('#btn-contacts-add').click(function(e) {
		e.preventDefault();
		$('.col-contacts .contact').each(function() {
			$(this).appendTo('.col-excluded');
		});
		return false;
	});

	$('#btn-contacts-exclude').click(function(e) {
		e.preventDefault();
		$('.col-excluded .contact').each(function() {
			$(this).appendTo('.col-contacts');
		});
		return false;
	});

	// перед отправкой формы собираем, кого исключать
	$('#formBroadcast').submit(function() {
		var ids = [];
		$('.col-excluded .contact').each(function() {
			ids.push($(this).attr('data-id'));
		});
		if (ids.length) {
			$('#excluded_ids').val('-' + ids.join('-') + '-');
		}
	});

	// мобильные вкладки
	$('#tab-add').click(function() {
		$('#tab-broadcasts').removeClass('active');
		$('#tab-add').addClass('active');
		$('.broadcasts').hide();
		$('.broadcast-form').show();
	});

	$('#tab-broadcasts').click(function() {
		$('#tab-add').removeClass('active');
		$('#tab-broadcasts').addClass('active');
		$('.broadcast-form').hide();
		$('.broadcasts').show();
	});

	// рассылка - автодополнение города
	$("#broadcast-city").autocomplete({
		source:    function(request, response) {
			var url = '/profiles/auth/city?term=' + request.term.trim();
			$.getJSON(url, function(data) {
				if (!data.length) {
					var result = [{
						label: '* Населенный пункт не найден',
						value: response.term
					}];
					response(result);
				}
				else {
					response($.map(data, function(item) {
						return {
							label: item,
							value: item
						}
					}));
				}
			});
		},
		select:    function(event, ui) {
			if (ui.item) {
				$(this).val(ui.item.value);
			}
		},
		minLength: 2,
		autoFocus: true,
		limit:     15
	}).data("ui-autocomplete")._renderItem = function(ul, item) {
		return $('<li class="aut"></li>')
			.data("item.autocomplete", item)
			.append("<a>" + item.label + "</a>")
			.appendTo(ul);
	};
});