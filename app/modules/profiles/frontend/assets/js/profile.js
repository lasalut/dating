$(document).ready(function() {

	// мобильное меню
	$('#mobile-menu-icon').click(function(e) {
		e.preventDefault();
		$('.wrapper, .after-content').hide();
		$('.page-search #main-search').hide();
		$('#mobile-menu-icon').hide();
		$('#mobile-menu-hide').show();
		$('#mobile-menu').show();
	});

	$('#mobile-menu-hide').click(function(e) {
		e.preventDefault();
		$('.wrapper, .after-content').show();
		$('.page-search #main-search').show();
		$('#mobile-menu-hide').hide();
		$('#mobile-menu-icon').show();
		$('#mobile-menu').hide();
	});

	// лайк
	$('.preview-like').click(function(e) {
		e.preventDefault();
		var $this = $(this);
		$.getJSON($this.attr('data-href'), function(data) {
			if (data.like) {
				$this.removeClass('empty').addClass('filled');
			}
			else {
				$this.removeClass('filled').addClass('empty');
			}
			console.log(data);
		});
		return false;
	});

	$('.link-favorite').click(function(e) {
		e.preventDefault();
		var $this = $(this);
		$.getJSON(this.href, function(data) {
			data.length
				? $this.addClass('has').attr('title', 'Убрать из избранных')
				: $this.removeClass('has').attr('title', 'Добавить в избранные');

			$(this).attr('data-original-title', 'changed tooltip');
			$this.attr('data-original-title', $this.attr('title')).parent().find('.tooltip-inner').html($this.attr('title'));
		});
		return false;
	});

	$('.link-blacklist').click(function(e) {
		e.preventDefault();
		var $this = $(this);
		$.getJSON(this.href, function(data) {
			data.length
				? $this.addClass('has').attr('title', 'Убрать из игнора')
				: $this.removeClass('has').attr('title', 'Добавить в игнор');
			$this.attr('data-original-title', $this.attr('title')).parent().find('.tooltip-inner').html($this.attr('title'));
		});
		return false;
	});

	$.notify.defaults({autoHideDelay: 5000, globalPosition: 'bottom center'});

	// веб-конференция, проверка на онлайн
	$('.btn-web').click(function(e) {
		e.preventDefault();
		var $this = $(this);
		$.getJSON('/profiles/auth/check-online?id=' + $this.attr('data-id'), function(data) {
			if (data.length) {
				location.href = $this.attr('data-href');
			}
			else {
				$.notify('Участник не в сети', {className: 'info', autoHideDelay: 5000});
			}
		});
		return false;
	});

	// премиум
	$('#premium-form').submit(function(e) {
		e.preventDefault();
		var data = $('#premium-form').serialize();

		$.ajax({
			type:    'POST',
			url:     '/profiles/premium/buy',
			data:    data,
			success: function(data) {
				$("#premium-errors li").remove();
				$("#premium-info").html(data.msg).show();
				$('.premium-buy, .premium-radio').hide();
				$('.my-balance').html(data.balance);
				$('.premium-days').html(data.premiumDays);
			},
			error:   function(xhr) {
				$("#premium-info").html('').hide();
				var data = xhr.responseJSON;
				if ("errors" in data) {
					$("#premium-errors li").remove();
					for (var i = 0; i < data.errors.length; i++) {
						$("#premium-errors").append("<li>" + data.errors[i] + "</li>");
					}
				}
			}
		});
	});

	$('.premium-btn').click(function(e) {
		e.preventDefault();
		$('#premiumForm').modal('show');
		return false;
	});
});

///////////////////////////////////////////////

var webSocketConn;
var onlineInterval = null;

function startSocket() {
	if (onlineInterval) {
		clearInterval(onlineInterval);
	}

	webSocketConn = new ab.Session(webSocket + ':8080',
		function() {
			webSocketConn.setMaxIdleTime = 12000000;
			webSocketConn.subscribe(myProfileId, function(subscriber, data) {
				var newTitle, i, oldTitle;

				// приняли входящее сообщение от другого участника
				if (data.type === 'message' && window.location.href.indexOf('/chat') === -1) {
					addCounter('#messages-counter');
					i = 0;
					oldTitle = document.title;

					if (newCounter == 1) {
						newTitle = '1 новое сообщение';
					}
					else if (newCounter > 1 && newCounter < 5) {
						newTitle = newCounter + ' новых сообщения';
					}
					else {
						newTitle = newCounter + ' новых сообщений';
					}

					setInterval(function() {
						i++;
						document.title = i % 2 ? newTitle : oldTitle;
					}, 1500);
				}

				// получили лайк от другого участника
				if (data.type === 'like') {
					addCounter('#likes-counter');
				}

				// другой участник просмотрел анкету
				if (data.type === 'visit') {
					addCounter('#visits-counter');
				}

				// другой участник инициировал общение через WebRTC
				if (data.type === 'webrtc'
					&& $('a[data-room="' + data.object.webrtcRoom + '"]').length == 0
					&& window.location.href.indexOf('/web') === -1
				) {
					var u = data.object;
					oldTitle = document.title;
					newTitle = u.userName + ' (' + u.userYears + ') хочет начать видео-конференцию';
					i = 0;

					$('#topmenu').append(
						$('<li>').append(
							$('<a>').addClass('link-room')
								.attr('data-room', u.webrtcRoom)
								.attr('href', '/web?with=' + u.userId)
								.attr('title', newTitle).append(
								'<i class="fa fa-video-camera"></i> ' + u.userName + ' (' + u.userYears + ')'
							)));

					var $lr = $('a[data-room="' + data.object.webrtcRoom + '"]');

					setInterval(function() {
						i++;
						document.title = i % 2 ? newTitle : oldTitle;
						i % 2 ? $lr.addClass('active') : $lr.removeClass('active');
					}, 1500);
				}
			});

			// раз в 20 секунд обновляем свой статус онлайна
			onlineInterval = setInterval(function() {
				webSocketConn.publish('dating.chat', {'type': 'online', 'userId': myProfileId});
			}, 20 * 1000);
		},
		function() {
			startSocket();
		},
		{'skipSubprotocolCheck': true}
	);
}

startSocket();

function addCounter(selector) {
	var $counter = $(selector);
	var msgCounter = $counter.html().length ? parseInt($counter.html()) : 0;
	var newCounter = msgCounter + 1;
	$counter.html(newCounter);
	$counter.addClass('display-inline');
}