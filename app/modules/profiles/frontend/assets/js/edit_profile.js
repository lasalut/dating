$(document).ready(function() {
	var hash = window.location.hash.substr(1);

	if (hash == 'photo') {
		$('label[for="profile-avatar_local"]').click();
	}
	else if (hash == 'about') {
		$('#profile-social_about').focus().select();
	}
	else if (hash == 'interests') {
		$('#profile-social_interests').focus().select();
	}

	$(".field-profile-city_local .btn-close").click(function() {
		$('#profile-city_local').val("");
	});

	$("#profile-city_local").autocomplete({
		source:    function(request, response) {
			var url = '/profiles/auth/city?term=' + request.term.trim();
			$.getJSON(url, function(data) {
				if (!data.length) {
					var result = [{
						label: '* Населенный пункт не найден',
						value: response.term
					}];
					response(result);
				}
				else {
					response($.map(data, function(item) {
						return {
							label: item,
							value: item
						}
					}));
				}
			});
		},
		select:    function(event, ui) {
			if (ui.item) {
				$(this).val(ui.item.value);
			}
		},
		minLength: 2,
		autoFocus: true,
		limit:     15
	}).data("ui-autocomplete")._renderItem = function(ul, item) {
		return $('<li class="aut"></li>')
			.data("item.autocomplete", item)
			.append("<a>" + item.label + "</a>")
			.appendTo(ul);
	};
});