$(document).ready(function() {
	$(".fancybox-thumb").fancybox({
		prevEffect: 'none',
		nextEffect: 'none',
		helpers:    {
			thumbs: {
				width:  50,
				height: 50
			}
		}
	});

	$('#resend_email').click(function (e) {
		e.preventDefault();
		$.getJSON('/profiles/auth/resend-email', function(data) {
			$('#resend_msg').show().text(data.msg);
		});
		return false;
	});

	$('#resend_sms').click(function (e) {
		e.preventDefault();
		$(this).remove();
		$('.line-phone').show();
		return false;
	});

	$('#phone_send').click(function (e) {
		e.preventDefault();
		var url = '/profiles/auth/resend-sms?phone=' + $('#phone_number').val();
		$.getJSON(url, function(data) {
			$('#resend_sms_msg').show().text(data.msg);
			if (data.result == 'OK') {
				$('#phone_code, #phone_confirm').css('display', 'inline-block');
			}
		});
		return false;
	});

	$('#phone_confirm').click(function(e) {
		e.preventDefault();
		var url = '/profiles/auth/phone-confirm?code=' + $('#phone_code').val();
		$.getJSON(url, function(data) {
			$('#resend_sms_msg').show().text(data.msg);
			if (data.result == 'OK') {
				$('#phone_indicator').removeClass('fa-ban').addClass('fa-check-circle');
			}
		});
		return false;
	})
});
