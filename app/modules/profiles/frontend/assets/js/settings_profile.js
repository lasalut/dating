$(document).ready(function() {
	if (!('serviceWorker' in navigator)) {
		$('.field-notify_webpush').hide();
	}
	if (!('PushManager' in window)) {
		$('.field-notify_webpush').hide();
	}

	$('#notify_webpush').click(function() {
		if ($(this).is(':checked')) {
			requestPermission();
		} else {
			resetPermissions();
			$.post("/profiles/profile/webpush-register", {stop_webpush: 1});
		}
	});

	function requestPermission() {
		return new Promise(function(resolve, reject) {
			const permissionResult = Notification.requestPermission(function(result) {
				resolve(result);
			});

			if (permissionResult) {
				permissionResult.then(resolve, reject);
			}
		}).then(function(permissionResult) {
			if (permissionResult === 'denied') {
				$('#notify_webpush').prop('checked', false);
				alert("Уведомления заблокированы браузером. Пожалуйста, включите их в настройках браузера.");
			}
			else {
				subscribeUserToPush();
			}
		});
	}

	const publicKey = base64UrlToUint8Array(webpush_public_key);

	function subscribeUserToPush() {
		return navigator.serviceWorker
			.register('service-worker.js', {
				useCache:  false,
				cache:     'no-cache',
				"max-age": "0"
			})
			.then(function(registration) {
				return registration.pushManager.subscribe({
					userVisibleOnly:      true,
					applicationServerKey: publicKey
				});
			})
			.then(function(pushSubscription) {
				$.post("/profiles/profile/webpush-register", {json: JSON.stringify(pushSubscription)});
				return pushSubscription;
			});
	}

	function base64UrlToUint8Array(base64UrlData) {
		const padding = '='.repeat((4 - base64UrlData.length % 4) % 4);
		const base64 = (base64UrlData + padding)
			.replace(/\-/g, '+')
			.replace(/_/g, '/');

		const rawData = atob(base64);
		const buffer = new Uint8Array(rawData.length);

		for (let i = 0; i < rawData.length; ++i) {
			buffer[i] = rawData.charCodeAt(i);
		}

		return buffer;
	}

	function resetPermissions() {
		navigator.serviceWorker.getRegistrations().then(function(registrations) {
				for (var i = 0; i < registrations.length; i++) {
					registrations[i].unregister();
				}
			}
		);
	}
});
