$(document).ready(function() {
	var pay = function(amount) {
		var widget = new cp.CloudPayments();
		widget.charge({ // options
				publicId:    'test_api_00000000000000000000001',  //id из личного кабинета
				description: 'Пример оплаты (деньги сниматься не будут)', //назначение
				amount:      amount, //сумма
				currency:    'RUB', //валюта
				invoiceId:   payment_uniq, //номер заказа  (необязательно)
				accountId:   profile_id, //идентификатор плательщика (необязательно)
				email:       profile_email,
				data:        {
					myProp: 'myProp value' //произвольный набор параметров
				}
			},
			function(options) { // success
				var url = '/profiles/finance/pay?amount=' + amount + '&payment_uniq=' + payment_uniq;
				$.getJSON(url, function(data) {
					$('.my-balance').html(data.balance);
					$.notify('Ваш счет пополнен на ' + amount + ' рублей', {className: 'success'});
					$('#checkout-amount').val('');
				});
			},
			function(reason, options) { // fail
				$.notify('Произошла ошибка при зачислении: ' + reason, {className: 'warning'});
			});
	};

	$('#checkout-btn').click(function(e) {
		e.preventDefault();
		var amount = $('#checkout-amount').val();
		if (!amount.length) {
			$.notify('Пожалуйста, укажите сумму платежа', {className: 'info'});
			return false;
		}
		amount = parseInt(amount);
		if (amount < 10) {
			$.notify('Минимальная сумма платежа - 10 рублей', {className: 'info'});
			return false;
		}
		pay(amount);
	});
});