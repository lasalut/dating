<?php

namespace modules\profiles\frontend\assets;

use modules\profiles\common\models\Profile;
use Yii;
use yii\web\AssetBundle;
use yii\web\View;

class CallsAsset extends AssetBundle
{
    public $sourcePath = '@modules/profiles/frontend/assets';

    public $css = [
        'css/calls.css'
    ];

    public $depends = [
        'frontend\assets\AppAsset',
    ];
}