<?php

namespace modules\profiles\frontend\assets;

use modules\profiles\common\models\Profile;
use Yii;
use yii\web\AssetBundle;
use yii\web\View;

class FinanceAsset extends AssetBundle
{
    public $sourcePath = '@modules/profiles/frontend/assets';

    public $css = [
        'css/finance.css'
    ];

    public $js = [
        'https://widget.cloudpayments.ru/bundles/cloudpayments',
        'js/finance.js',
    ];

    public $depends = [
        'frontend\assets\AppAsset',
    ];

    public function registerAssetFiles($view)
    {
        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;
        $uniq = uniqid();
        $view->registerJs("
            var profile_email = '{$profile->email}', 
            profile_id='{$profile->id}', 
            payment_uniq = '{$uniq}'
        ;", View::POS_HEAD, __CLASS__);
        return parent::registerAssetFiles($view);
    }
}