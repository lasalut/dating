<?php

namespace modules\profiles\frontend\assets;

use frontend\assets\AppAsset;
use modules\photo\frontend\assets\Fancybox2Asset;
use yii\web\AssetBundle;
use yii\web\View;

class ViewProfileAsset extends AssetBundle
{
    public $sourcePath = '@modules/profiles/frontend/assets';

    public $css = [
        'css/view_profile.css'
    ];
    public $js = [
        'js/view_profile.js',
    ];
    public $depends = [
        AppAsset::class,
        Fancybox2Asset::class,
    ];

    public function registerAssetFiles($view)
    {
        $view->registerJs("var connOnlines = true;", View::POS_HEAD, __CLASS__);

        return parent::registerAssetFiles($view);
    }
}