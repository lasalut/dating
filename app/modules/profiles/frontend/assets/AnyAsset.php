<?php

namespace modules\profiles\frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class AnyAsset extends AssetBundle
{
    public $sourcePath = '@modules/profiles/frontend/assets';

    public $css = [
        'css/any.css'
    ];

    public $depends = [
        'frontend\assets\AppAsset',
    ];
}