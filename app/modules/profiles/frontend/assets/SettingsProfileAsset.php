<?php

namespace modules\profiles\frontend\assets;

use frontend\assets\AppAsset;
use yii\web\AssetBundle;
use yii\web\View;

class SettingsProfileAsset extends AssetBundle
{
    public $sourcePath = '@modules/profiles/frontend/assets';

    public $css = [
        'css/settings_profile.css'
    ];
    public $js = [
        'js/settings_profile.js',
    ];
    public $depends = [
        AppAsset::class,
    ];

    public function registerAssetFiles($view)
    {
        $pk = getenv('WEBPUSH_PUBLIC_KEY');
        $view->registerJs("var webpush_public_key = \"{$pk}\";", View::POS_HEAD, __CLASS__);
        return parent::registerAssetFiles($view);
    }
}