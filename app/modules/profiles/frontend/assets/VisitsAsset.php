<?php

namespace modules\profiles\frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class VisitsAsset extends AssetBundle
{
    public $sourcePath = '@modules/profiles/frontend/assets';

    public $css = [
        'css/visits.css'
    ];

    public $js = [
        'js/visits.js'
    ];

    public $depends = [
        'frontend\assets\AppAsset',
    ];

    public function registerAssetFiles($view)
    {
        $view->registerJs("var connOnlines = true;", View::POS_HEAD, __CLASS__);
        return parent::registerAssetFiles($view);
    }
}