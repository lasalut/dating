<?php

namespace modules\profiles\frontend\assets;

use frontend\assets\AppAsset;
use yii\web\AssetBundle;
use yii\web\View;

class EditAvatarAsset extends AssetBundle
{
    public $sourcePath = '@modules/profiles/frontend/assets';

    public $css = [
        'css/edit_avatar.css'
    ];
    public $js = [
        'js/edit_avatar.js',
    ];
    public $depends = [
        AppAsset::class,
        CropperAsset::class,
    ];
}