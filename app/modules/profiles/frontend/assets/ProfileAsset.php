<?php

namespace modules\profiles\frontend\assets;

use Yii;
use yii\web\AssetBundle;

class ProfileAsset extends AssetBundle
{
    public $sourcePath = '@modules/profiles/frontend/assets';

    public $js = [
        'js/notify.min.js',
        'js/profile.js',
    ];

    public $css = [
        'css/profile.css'
    ];

    public $depends = [
        'frontend\assets\AppAsset',
    ];
}