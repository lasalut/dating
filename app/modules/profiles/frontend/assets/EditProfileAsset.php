<?php

namespace modules\profiles\frontend\assets;

use frontend\assets\AppAsset;
use yii\web\AssetBundle;
use yii\web\View;

class EditProfileAsset extends AssetBundle
{
    public $sourcePath = '@modules/profiles/frontend/assets';

    public $css = [
        'css/edit_profile.css'
    ];
    public $js = [
        'js/edit_profile.js',
    ];
    public $depends = [
        AppAsset::class,
    ];
}