<?php

namespace modules\profiles\frontend\assets;

use modules\profiles\common\models\Profile;
use Yii;
use yii\web\AssetBundle;
use yii\web\View;

class BroadcastAsset extends AssetBundle
{
    public $sourcePath = '@modules/profiles/frontend/assets';

    public $css = [
        'css/broadcast.css'
    ];

    public $js = [
        'js/broadcast.js',
    ];

    public $depends = [
        'frontend\assets\AppAsset',
    ];

    public function registerAssetFiles($view)
    {
        $view->registerJs("var connOnlines = true;", View::POS_HEAD, __CLASS__);
        return parent::registerAssetFiles($view);
    }
}