<?php

namespace modules\profiles\frontend\assets;

use frontend\assets\AppAsset;
use modules\profiles\common\models\Profile;
use yii\web\AssetBundle;
use yii\web\View;

class FabricAsset extends AssetBundle
{
    public $sourcePath = '@modules/profiles/frontend/assets';

    public $css = [
        'css/fabric.css',
    ];
    public $js = [
        'js/fabric.min.js',
        'js/fabric_faces.js',
    ];
    public $depends = [
        AppAsset::class,
    ];

    public function registerAssetFiles($view)
    {
        /** @var Profile $profile */
        $profile = \Yii::$app->user->identity;
        $avatar_url = $profile->avatar_url;

        $view->registerJs("var avatar_url = '{$avatar_url}';", View::POS_HEAD, __CLASS__);

        return parent::registerAssetFiles($view);
    }
}