<?php

namespace modules\profiles\frontend\models;

use marketingsolutions\finance\models\Transaction;
use modules\profiles\common\finance\PremiumHistoryPartner;
use modules\profiles\common\models\Premium;
use modules\profiles\common\models\PremiumHistory;
use modules\profiles\common\models\Profile;
use Yii;

class TopForm extends PremiumForm
{
    public function process()
    {
        if ($this->validate() == false) {
            return false;
        }

        $trans = Yii::$app->db->beginTransaction();

        try {
            $h = new PremiumHistory();
            $h->profile_id = $this->profile->id;
            $h->premium_id = $this->premium->id;
            $h->price = $this->premium->price;
            $h->save(false);

            if ($this->premium->code == 'top-0') {
                if ($this->profile->premium_ups > 0) {
                    $this->profile->premium_ups =  $this->profile->premium_ups - 1;
                }
                else {
                    $trans->rollBack();
                    $this->addError('premium_id', 'Данная услуга недоступна');
                    return false;
                }
            }
            else {
                $this->profile->purse->addTransaction(Transaction::factory(
                    Transaction::OUTBOUND,
                    $this->premium->price,
                    new PremiumHistoryPartner(['id' => $h->id]),
                    "Покупка услуги \"Подняться в топ\""
                ), true, false);
            }

            $maxPriority = Profile::find()->select('max(premium_priority)')->scalar();

            $this->profile->premium_priority = $maxPriority + 1;
            $this->profile->save(false);

            $trans->commit();
        }
        catch (\Exception $e) {
            $trans->rollBack();
            throw $e;
        }

        return true;
    }
}
