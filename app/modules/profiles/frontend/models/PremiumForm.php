<?php

namespace modules\profiles\frontend\models;

use modules\profiles\common\models\Premium;
use modules\profiles\common\models\Profile;
use Yii;
use yii\base\Model;

abstract class PremiumForm extends Model
{
    public $premium_id;

    /** @var Profile */
    public $profile;

    /** @var Premium */
    public $premium;

    public function rules()
    {
        return [
            ['premium_id', 'integer'],
            ['premium_id', 'required'],
            ['premium_id', 'checkPremium'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'premium_id' => 'Выберите из списка',
        ];
    }

    public function checkPremium()
    {
        if ($premium = Premium::findOne($this->premium_id)) {
            $this->premium = $premium;
            if ($this->profile->purse->balance < $premium->price) {
                $this->addError('premium_id', 'На вашем счету недостаточно средств. <a href="/balance">Пополнить</a>');
            }
        }
        else {
            $this->addError('premium_id', 'Данная услуга недоступна');
        }
    }

    abstract public function process();
}
