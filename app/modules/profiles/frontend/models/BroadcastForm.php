<?php

namespace modules\profiles\frontend\models;

use libphonenumber\PhoneNumberFormat;
use marketingsolutions\finance\models\Transaction;
use marketingsolutions\phonenumbers\PhoneNumber;
use modules\profiles\common\finance\PremiumHistoryPartner;
use modules\profiles\common\models\Broadcast;
use modules\profiles\common\models\City;
use modules\profiles\common\models\Country;
use modules\profiles\common\models\Premium;
use modules\profiles\common\models\PremiumHistory;
use modules\profiles\common\models\Profile;
use modules\profiles\common\models\Region;
use Yii;
use yii\db\Query;

class BroadcastForm extends Broadcast
{
    public $city_local = null;

    /** @var Premium */
    public $premium = null;

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['city_local', 'safe'],
            ['city_local', 'checkCity'],
            ['hours', 'checkHours'],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [

        ]);
    }

    public function process()
    {
        if ($this->validate() == false) {
            return false;
        }

        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;

        $hours = $this->premium->hours;
        $expires = (new \DateTime("+$hours hours"))->format('Y-m-d H:i:s');

        $trans = Yii::$app->db->beginTransaction();

        try {
            $h = new PremiumHistory();
            $h->profile_id = $profile->id;
            $h->premium_id = $this->premium->id;
            $h->expires_at = $expires;
            $h->hours = $this->premium->hours;

            if ($this->hours == '0') {
                $h->price = 0;
                $h->save(false);
                $profile->premium_broadcasts = $profile->premium_broadcasts - 1;
                $profile->save(false);
            }
            else {
                $h->price = $this->premium->price;
                $h->save(false);

                $profile->purse->addTransaction(Transaction::factory(
                    Transaction::OUTBOUND,
                    $this->premium->price,
                    new PremiumHistoryPartner(['id' => $h->id]),
                    'Покупка рассылки на ' . $this->premium->hoursText()
                ));
            }

            $this->created_at = (new \DateTime())->format('Y-m-d H:i:s');
            $this->expires_at = $expires;
            $this->save(false);

            $trans->commit();
        }
        catch (\Exception $e) {
            $trans->rollBack();
            throw $e;
        }

        return true;
    }

    public function checkHours()
    {
        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;

        if ($this->hours == '0') {
            if ($profile->premium_broadcasts < 1) {
                $this->addError('hours', 'Отсутствуют рассылки для использования. Пожалуйста, выберите другой вариант');
            }

            /** @var Premium $premium */
            $premium = Premium::find()
                ->where(['like', 'code', 'broadcast'])
                ->orderBy(['hours' => SORT_ASC])
                ->limit(1)
                ->one();

            $this->premium = $premium;
        }
        else {
            /** @var Premium $premium */
            $premium = Premium::find()
                ->where(['like', 'code', 'broadcast'])
                ->andWhere(['hours' => $this->hours])
                ->one();

            if ($profile->purse->balance < $premium->price) {
                $this->addError('hours', 'На вашем счету недостаточно средств. Пожалуйста, пополните в разделе "Мой кошелек"');
            }

            $this->premium = $premium;
        }

        if ($this->premium == null) {
            $this->addError('hours', 'Такая рассылка отсутствует');
        }
    }

    public function checkCity()
    {
        if (!empty($this->city_local)) {
            $titles = explode(',', $this->city_local);
            $city = trim($titles[0]);
            $region = null;
            $country = null;

            if (isset($titles[2])) {
                $region = trim($titles[1]);
                $country = trim($titles[2]);
            }

            $query = (new Query())
                ->select('c.id')
                ->from(['c' => City::tableName()])
                ->leftJoin(['r' => Region::tableName()], 'r.id = c.region_id')
                ->leftJoin(['co' => Country::tableName()], 'co.id = c.country_id')
                ->where("c.title LIKE :cityName", [':cityName' => $city . '%'])
                ->limit(1);

            if ($country) {
                $query->andWhere("co.title LIKE :countryName", [':countryName' => $country . '%']);
            }

            $result = $query->one();

            if (empty($result)) {
                $this->addError('city_local', 'Город не найден');
            }
            else {
                $this->city_id = $result['id'];
            }
        }
    }
}
