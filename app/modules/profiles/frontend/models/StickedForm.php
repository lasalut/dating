<?php

namespace modules\profiles\frontend\models;

use marketingsolutions\finance\models\Transaction;
use modules\profiles\common\finance\PremiumHistoryPartner;
use modules\profiles\common\models\Premium;
use modules\profiles\common\models\PremiumHistory;
use Yii;

class StickedForm extends PremiumForm
{
    public function process()
    {
        if ($this->validate() == false) {
            return false;
        }

        $trans = Yii::$app->db->beginTransaction();

        try {
            $date = $this->profile->hasPremiumSticked()
                ? new \DateTime($this->profile->premium_sticked_to)
                : new \DateTime('now');
            $date->modify("+ {$this->premium->days} days");

            $h = new PremiumHistory();
            $h->profile_id = $this->profile->id;
            $h->premium_id = $this->premium->id;
            $h->days = $this->premium->days;
            $h->price = $this->premium->price;
            $h->expires_at = $date->format('Y-m-d H:i:s');
            $h->save(false);

            $days = Premium::daysText($this->premium->days);
            $this->profile->purse->addTransaction(Transaction::factory(
                Transaction::OUTBOUND,
                $this->premium->price,
                new PremiumHistoryPartner(['id' => $h->id]),
                "Закрепление в ТОПе на $days"
            ), true, false);

            $this->profile->premium_sticked_to = $date->format('Y-m-d H:i:s');
            $this->profile->save(false);

            $trans->commit();
        }
        catch (\Exception $e) {
            $trans->rollBack();
            throw $e;
        }

        return true;
    }
}
