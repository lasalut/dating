<?php

namespace modules\profiles\frontend\models;

use marketingsolutions\finance\models\Transaction;
use modules\profiles\common\finance\PremiumHistoryPartner;
use modules\profiles\common\models\PremiumHistory;
use Yii;

class ContactsForm extends PremiumForm
{
    public function process()
    {
        if ($this->validate() == false) {
            return false;
        }

        $trans = Yii::$app->db->beginTransaction();

        try {
            $h = new PremiumHistory();
            $h->profile_id = $this->profile->id;
            $h->premium_id = $this->premium->id;
            $h->contacts = $this->premium->contacts;
            $h->price = $this->premium->price;
            $h->save(false);

            $this->profile->purse->addTransaction(Transaction::factory(
                Transaction::OUTBOUND,
                $this->premium->price,
                new PremiumHistoryPartner(['id' => $h->id]),
                "Покупка {$this->premium->contacts} контактов для общения"
            ), true, false);

            $this->profile->premium_contacts = $this->profile->premium_contacts + $this->premium->contacts;
            $this->profile->save(false);

            $trans->commit();
        }
        catch (\Exception $e) {
            $trans->rollBack();
            throw $e;
        }

        return true;
    }
}
