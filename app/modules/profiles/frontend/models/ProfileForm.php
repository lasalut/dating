<?php

namespace modules\profiles\frontend\models;

use libphonenumber\PhoneNumberFormat;
use marketingsolutions\phonenumbers\PhoneNumber;
use modules\profiles\common\models\Profile;
use Yii;

class ProfileForm extends Profile
{
    public $password;
    /**
     * @var string user password repeat
     */
    public $passwordCompare;

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['password', 'string'],
            ['passwordCompare', 'string'],
            ['passwordCompare', 'compare', 'compareAttribute' => 'password'],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'password' => 'Новый пароль',
            'passwordCompare' => 'Повтор пароля',
        ]);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (!empty($this->password)) {
                $this->passhash = Yii::$app->security->generatePasswordHash($this->password);
            }

            return true;
        }
        return false;
    }
}
