<?php

use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \modules\profiles\common\models\Profile $profile
 * @var \modules\profiles\common\models\Profile $model
 * @var string $source
 */

$js = <<<JS
	jQuery(document).ready(function() {
		jQuery('#complain-reasons').click(function() {
			if ($(this).val() == 'other') {
				$(this).remove();
				$('#complain-reason').show().focus().attr('required', 'required');
			}
		});
		
		jQuery('#complain-form').submit(function(e) {
			e.preventDefault();
			jQuery.ajax({
           		type: "POST",
           		url: '/profiles/profile/complain',
           		data: jQuery("#complain-form").serialize(), // serializes the form's elements.
           		success: function(data) {
           			jQuery('#complain-form').html('<b>' + data + '</b>');
           		}
         	});
		});
	});
JS;
$this->registerJs($js);
?>

<a href="#complainForm" id="complain-btn" class="btn btn-default" data-toggle="modal" role="button">
	Пожаловаться
</a>

<div id="complainForm" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content" style="max-width:500px">
			<button type="button" class="close" data-dismiss="modal">×</button>

			<div class="box-wrap auto-center" id="auth-block">
				<div class="box">
					<div class="box-header" style="margin-left:-130px">
						Оставить жалобу
					</div>
					<div class="box-content">
						<p>Если Вы считаете поведение участника недопустимым, не соответствующим правилам портала,
							то Вы можете оставить жалобу. Мы прочитаем заявку и примем надлежащие меры.</p>
						<hr/>
						<form id="complain-form">
							<div class="form-row">
								<label class="control-label" for="complain-reasons">Укажите причину</label>
								<select class="form-control full" id="complain-reasons" name="reasons"
										required="required">
									<option value="Неадекватное поведение" selected="">Неадекватное поведение</option>
									<option value="Порнография">Порнография</option>
									<option value="Не интересно">Не интересно</option>
									<option value="Нецензурные предложения">Нецензурные предложения</option>
									<option value="other">Другое</option>
								</select>
								<input type="text" placeholder="причина..." name="reason" style="display:none"
									   id="complain-reason" class="full"/>
							</div>
							<input type="hidden" name="model_id" value="<?= $model->id ?>"/>
							<input type="hidden" name="source" value="<?= $source ?>"/>
							<div class="form-row form-submit">
								<input type="submit" class="btn btn-primary" name="submit-send" value="Отправить"/>
							</div>
							<ul class="errors" id="login-errors"></ul>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>