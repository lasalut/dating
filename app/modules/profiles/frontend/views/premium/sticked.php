<?php

use modules\profiles\common\models\Broadcast;
use modules\profiles\common\models\Premium;
use modules\profiles\common\models\Profile;
use modules\profiles\frontend\models\BroadcastForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var Profile $profile
 * @var \modules\profiles\frontend\models\IncognitoForm $model
 */

$title = 'Закрепление анкеты в ТОП – будьте выше всех';
$this->title = $title;

\modules\profiles\frontend\assets\CallsAsset::register($this);

?>
<h1 class="main center"><?= $title ?></h1>

<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<p>После закрепления в ТОП:</p>

		<p>Вы всегда будете находиться по позициям выше всех;</p>

		<p>Ваша анкета будет отображаться в пределах города не зависимо от критериев поиска;</p>

		<?php if ($profile->hasPremiumSticked()): ?>
			<p>Услуга доступна до: <?= (new \DateTime($profile->premium_sticked_to))->format('d.m.Y H:i') ?></p>
		<?php endif; ?>

		<hr/>
		<div class="sticked-form">
            <?php $form = ActiveForm::begin(['method' => 'POST']); ?>
			<div class="form-row">
                <?= $form->field($model, 'premium_id', [
                    'errorOptions' => [
                        'encode' => false,
                        'class' => 'help-block'
                    ]
                ])
                    ->dropDownList(Premium::stickedOptions(), ['class' => 'full'])
					->label(false)
                ?>
			</div>

			<div class="form-row center">
                <?= Html::submitButton('Приобрести', ['class' => 'btn btn-primary']) ?>
			</div>
            <?php ActiveForm::end(); ?>
		</div>
	</div>
</div>

