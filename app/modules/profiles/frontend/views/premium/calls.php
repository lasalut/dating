<?php

use modules\profiles\common\models\Broadcast;
use modules\profiles\common\models\Premium;
use modules\profiles\common\models\Profile;
use modules\profiles\frontend\models\BroadcastForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var Profile $profile
 * @var \modules\profiles\frontend\models\CallsForm $model
 */

$title = 'Звонки';
$this->title = $title;

\modules\profiles\frontend\assets\CallsAsset::register($this);

?>
<h1 class="main center"><?= $title ?></h1>

<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<p>Вы можете звонить другим пользователям прямо с сайта.</p>
		<p>Осталось минут: <?= $profile->premium_minutes ?></p>
		<hr/>
		<div class="calls-form">
            <?php $form = ActiveForm::begin(['method' => 'POST']); ?>
			<div class="form-row">
                <?= $form->field($model, 'premium_id', [
                    'errorOptions' => [
                        'encode' => false,
                        'class' => 'help-block'
                    ]
                ])
                    ->dropDownList(Premium::callsMinutesOptions(), ['class' => 'full'])
					->label(false)
                ?>
			</div>

			<div class="form-row center">
                <?= Html::submitButton('Приобрести', ['class' => 'btn btn-primary']) ?>
			</div>
            <?php ActiveForm::end(); ?>
		</div>
	</div>
</div>

