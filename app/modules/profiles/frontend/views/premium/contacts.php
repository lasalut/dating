<?php

use modules\profiles\common\models\Broadcast;
use modules\profiles\common\models\Premium;
use modules\profiles\common\models\Profile;
use modules\profiles\frontend\models\BroadcastForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var Profile $profile
 * @var \modules\profiles\frontend\models\ContactsForm $model
 */

$title = 'Подняться в топ';
$this->title = $title;

\modules\profiles\frontend\assets\CallsAsset::register($this);

?>
<h1 class="main center"><?= $title ?></h1>

<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<p>Вы можете начинать общаться с девушками.</p>

		<p>Осталось новых контактов: <?= $profile->premium_contacts ?></p>

		<hr/>

		<div class="contacts-form">
            <?php $form = ActiveForm::begin(['method' => 'POST']); ?>
			<div class="form-row">
                <?= $form->field($model, 'premium_id', [
                    'errorOptions' => [
                        'encode' => false,
                        'class' => 'help-block'
                    ]
                ])
                    ->dropDownList(Premium::contactsOptions(), ['class' => 'full'])
					->label(false)
                ?>
			</div>

			<div class="form-row center">
                <?= Html::submitButton('Приобрести', ['class' => 'btn btn-primary']) ?>
			</div>
            <?php ActiveForm::end(); ?>
		</div>
	</div>
</div>

