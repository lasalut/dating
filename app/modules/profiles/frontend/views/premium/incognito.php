<?php

use modules\profiles\common\models\Broadcast;
use modules\profiles\common\models\Premium;
use modules\profiles\common\models\Profile;
use modules\profiles\frontend\models\BroadcastForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var Profile $profile
 * @var \modules\profiles\frontend\models\IncognitoForm $model
 */

$title = 'Услуга "Инкогнито"';
$this->title = $title;

\modules\profiles\frontend\assets\CallsAsset::register($this);

?>
<h1 class="main center"><?= $title ?></h1>

<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<p>Если вы по каким-либо причинам хотите скрыть свое пребывание на сайте,
			то услуга "Инкогнито" позволит это сделать.</p>

		<p>Открывайте и скрывайте присутствие на сайте по желанию.</p>

		<?php if ($profile->hasPremiumIncognito()): ?>
			<p>Услуга доступна до: <?= (new \DateTime($profile->premium_incognito))->format('d.m.Y H:i') ?></p>
		<?php endif; ?>

		<hr/>
		<div class="calls-form">
            <?php $form = ActiveForm::begin(['method' => 'POST']); ?>
			<div class="form-row">
                <?= $form->field($model, 'premium_id', [
                    'errorOptions' => [
                        'encode' => false,
                        'class' => 'help-block'
                    ]
                ])
                    ->dropDownList(Premium::incognitoOptions(), ['class' => 'full'])
					->label(false)
                ?>
			</div>

			<div class="form-row center">
                <?= Html::submitButton('Приобрести', ['class' => 'btn btn-primary']) ?>
			</div>
            <?php ActiveForm::end(); ?>
		</div>
	</div>
</div>

