<?php

use modules\profiles\common\models\Profile;
use modules\profiles\frontend\controllers\VisitsController;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var Profile $profile
 * @var Profile $model
 * @var Profile $modelPrev
 * @var Profile $modelNext
 * @var array $photos
 * @var array likedAvatars
 */

$title = 'Играть в Salut';
$this->title = $title;

\modules\profiles\frontend\assets\AnyAsset::register($this);
?>

<h1 class="main center"><?= $title ?></h1>

<div class="row any">
	<div class="col-md-1"></div>
	<div class="col-md-10">
		<table class="profile">
			<tr>
				<td class="avatar">
					<div class="avatar-wrap">
						<a href="/profile/<?= $model->id ?>">
							<img src="<?= $model->avatar_path ?>"/>
						</a>
						<div class="avatar-buttons">
							<a href="/any?type=prev"><img src="/images/small/any-left.png"/></a><a
								href="/any?type=next"><img src="/images/small/any-right.png"/></a>
						</div>
					</div>
				</td>
				<td class="profile-td">
					<!-- ЦЕЛИ ЗНАКОМСТВА -->
                    <?php if (!empty($model->intension)): ?>
						<h4 class="serif">Цели знакомства</h4>
						<div class="intensions">
							<ul class="list">
                                <?php foreach (Profile::getIntensionOptions() as $intension => $title): ?>
                                    <?php if (in_array($intension, $model->my_intensions)): ?>
										<li>
											<span><?= $title ?></span>
										</li>
                                    <?php endif; ?>
                                <?php endforeach; ?>
							</ul>
						</div>
                    <?php endif; ?>

					<!-- О СЕБЕ -->
                    <?php if (!empty($model->social_about)): ?>
						<h4 class="serif">О себе</h4>
						<div class="profile-about">
                            <?= \yii\helpers\Html::encode($model->social_about) ?>
						</div>
                    <?php endif; ?>

					<?php if ($profile): ?>
					<div class="profile-controls">
						<button class="btn btn-primary">Да</button>
						<button class="btn btn-primary">Возможно</button>
						<button class="btn btn-primary">Нет</button>
					</div>
					<?php endif; ?>
				</td>
			</tr>
		</table>

		<div class="row">
			<div class="col-md-4 profile-info">
				<div class="pull-right online-text ua" data-ua="<?= $model->id ?>">
                    <?= $model->online ?>
					<img src="/images/heart_beige.jpg"/>
				</div>
				<h3 class="profile-name serif"><?= $model->name ?></h3>
				<div class="profile-age">
                    <?= $model->years ?>, <?= $model->zodiacal_sign ?>.
                    <?php if ($model->city): ?>
                        <?= $model->city->title ?>
                    <?php endif; ?>
				</div>

                <?php if ($model->subway): ?>
					<div class="metro-block">
						<img src="/images/metro.jpg"/> <?= $model->subway->title ?>
					</div>
                <?php endif; ?>
			</div>
			<div class="col-md-8">
				<div class="row row-models">
                    <?php if (!empty($modelPrev)): ?>
						<div class="col-md-3">
							<a href="/any?type=prev">
								<img src="<?= $modelPrev->avatar_url ?>"/>
							</a>
						</div>
                    <?php endif; ?>
					<div class="col-md-3">
						<img class="current" src="<?= $model->avatar_url ?>"/>
					</div>
                    <?php if (!empty($modelNext)): ?>
						<div class="col-md-3">
							<a href="/any?type=prev">
								<img src="<?= $modelNext->avatar_url ?>"/>
							</a>
						</div>
                    <?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-1"></div>
</div>