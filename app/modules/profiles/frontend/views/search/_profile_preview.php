<?php

use modules\profiles\common\models\Profile;
use modules\profiles\common\models\ProfileLike;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var Profile $profile */
/** @var Profile $model */
/** @var array $photos */
/** @var array $likedAvatars */
/** @var string $at */

$key = $model->id . '';
?>

<div class="row profile-preview">
	<div class="col-md-6 col-avatar">
		<a class="preview-avatar ua" data-ua="<?= $model->id ?>" href="<?= Url::to(['/profile/' . $model->id]) ?>">
			<div class="preview-info">
				<img class="preview-image" src="<?= $model->avatar_path ?>">

				<div class="preview-hover">
					<div class="preview-hover-top clearfix">
                        <?php if (!empty($photos[$key])): ?>
							<span class="pull-left">
							<i class="fa fa-image"></i> <?= $photos[$key] ?>
						</span>
                        <?php endif; ?>
						<span class="pull-right">
						<?= $model->online ?>
					</span>
					</div>
                    <?php if ($profile): ?>
						<table class="preview-hover-bottom">
							<tr>
								<td style="padding: 0 4px 0 8px;">
									<a style="display:none" href="<?= Url::to(['/web', 'with' => $model->id]) ?>">
										<img src="/images/icons/w_phone.png"/>
									</a>
								</td>
								<td style="padding: 0 6px 0 6px;">
									<a href="#" data-href="<?= Url::to([
                                        '/profiles/profile/like',
                                        'id' => $model->id,
                                        'type' => ProfileLike::TYPE_AVATAR,
                                    ]) ?>"
									   class="preview-like <?= in_array($model->id, $likedAvatars) ? 'filled' : 'empty' ?>">
										<img class="preview-like-empty" src="/images/icons/w_heart.png"/>
										<img class="preview-like-filled" src="/images/icons/w_heart_filled.png"/>
									</a>
								</td>
								<td style="padding: 0 8px 0 4px;">
									<a href="<?= Url::to(['/chat', 'with' => $model->id]) ?>">
										<img src="/images/icons/w_comment.png"/>
									</a>
								</td>
							</tr>
						</table>
                    <?php endif; ?>
				</div>
			</div>
		</a>
	</div>
	<div class="col-md-6 col-info">
		<div class="preview-name">
			<a href="<?= Url::to(['/profile/' . $model->id]) ?>">
                <?= Html::encode($model->name) ?>, <?= $model->age ?>
			</a>
		</div>
		<div class="preview-city">
            <?php if ($city = $model->city): ?>
                <?= $city->title ?>, <?= $city->country->title ?>
            <?php endif ?>
		</div>
		<div class="online-text">
            <?= empty($at) ? $model->online : $at ?>
		</div>
	</div>
</div>