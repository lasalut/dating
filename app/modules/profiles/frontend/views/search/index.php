<?php

use modules\profiles\common\models\Profile;
use modules\profiles\frontend\assets\SearchAsset;
use modules\profiles\frontend\controllers\SearchController;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\widgets\ListView;

/**
 * @var \yii\web\View $this
 * @var Profile $profile
 * @var Profile[] $models
 * @var ActiveDataProvider $dataProvider
 * @var array $photos
 * @var array $likedAvatars
 */

SearchAsset::register($this);

$s = Yii::$app->request->get('s', SearchController::SECTION_ALL);
?>

<?php if (Yii::$app->request->get('t') == 'm' && $profile == null): ?>
	<h2>Просмотр мужских анкет доступен лишь после регистрации на сайте</h2>
<?php else: ?>
	<div class="search-found clearfix">
		<h1 class="pull-left">Найдено: <?= count($models) ?></h1>
		<div class="pull-right">
			<div class="btn-group">
				<a href="<?= Url::to(['/search',
                    't' => Yii::$app->request->get('t', 'f'),
                    'c' => Yii::$app->request->get('c', ''),
                    'from' => Yii::$app->request->get('from', SearchController::FROM),
                    'to' => Yii::$app->request->get('to', SearchController::TO),
                    's' => SearchController::SECTION_ALL,
                ]) ?>" class="btn btn-default <?= $s == SearchController::SECTION_ALL ? 'active' : '' ?>">Все</a>

				<a href="<?= Url::to(['/search',
                    't' => Yii::$app->request->get('t', 'f'),
                    'c' => Yii::$app->request->get('c', ''),
                    'from' => Yii::$app->request->get('from', SearchController::FROM),
                    'to' => Yii::$app->request->get('to', SearchController::TO),
                    's' => SearchController::SECTION_CLOSE
                ]) ?>" class="btn btn-default <?= $s == SearchController::SECTION_CLOSE ? 'active' : '' ?>">Рядом</a>

				<a href="<?= Url::to(['/search',
                    't' => Yii::$app->request->get('t', 'f'),
                    'c' => Yii::$app->request->get('c', ''),
                    'from' => Yii::$app->request->get('from', SearchController::FROM),
                    'to' => Yii::$app->request->get('to', SearchController::TO),
                    's' => SearchController::SECTION_TOP
                ]) ?>" class="btn btn-default <?= $s == SearchController::SECTION_TOP ? 'active' : '' ?>">Топ</a>

				<a href="<?= Url::to(['/search',
                    't' => Yii::$app->request->get('t', 'f'),
                    'c' => Yii::$app->request->get('c', ''),
                    'from' => Yii::$app->request->get('from', SearchController::FROM),
                    'to' => Yii::$app->request->get('to', SearchController::TO),
                    's' => SearchController::SECTION_ONLINE
                ]) ?>" class="btn btn-default <?= $s == SearchController::SECTION_ONLINE ? 'active' : '' ?>">Онлайн</a>
			</div>
		</div>
	</div>
	<div class="row row-5">
        <?php if (!empty($models)): ?>
            <?php foreach ($models as $model): ?>
				<?= $this->renderFile('@modules/profiles/frontend/views/search/_profile.php',
					compact('profile', 'model', 'photos', 'likedAvatars')) ?>
            <?php endforeach; ?>
        <?php endif; ?>
	</div>
<?php endif; ?>