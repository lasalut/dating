<?php

/**
 * @var \yii\web\View $this
 * @var \modules\profiles\common\models\Profile $profile
 * @var \modules\profiles\common\models\Profile $model
 */

use modules\profiles\common\models\Profile;
use modules\profiles\frontend\assets\SettingsProfileAsset;

$this->title = 'Анкета была удалена';

/** @var Profile $profile */
$profile = \Yii::$app->user->isGuest ? null : \Yii::$app->user->identity;
?>

<h1 class="main center"><?= $this->title ?></h1>

<?php if ($profile && $profile->id == $model->id): ?>
	<div class="center">
		<a href="/profiles/profile/restore" class="btn btn-default">Восстановить анкету</a>
	</div>
<?php endif; ?>
