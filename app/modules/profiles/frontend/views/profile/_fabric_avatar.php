<?php
/** @var \yii\web\View $view */
/** @var \modules\profiles\common\models\Profile $profile */

\modules\profiles\frontend\assets\FabricAsset::register($view);
?>

<span data-toggle="modal" data-target="#modal-fabric" class="span-fabric <?= empty($profile->avatar) ?: 'visible' ?>">
	<a href="#" type="button" id="btn-fabric" data-tooltip="tooltip" title="добавить маску">
		<img src="/images/masks/mask-2.png"/>
	</a>
</span>

<div id="modal-fabric" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Выберите одну из масок</h4>
				<div class="faces">
					<i class="mask-1"></i>
					<i class="mask-2"></i>
					<i class="mask-3"></i>
					<i class="mask-4"></i>
					<i class="mask-5"></i>
				</div>
			</div>
			<div class="modal-body">
				<canvas id="c" width="370" height="370"></canvas>
			</div>
			<div class="modal-footer">
				<img class="loadersmall" src="/images/ajax-loader.gif"/>
				<button id="fabric-save" class="btn btn-primary">Сохранить</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Отменить</button>
			</div>
		</div>
	</div>
</div>