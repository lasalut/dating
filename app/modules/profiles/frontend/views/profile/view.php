<?php

/**
 * @var \yii\web\View $this
 * @var Profile $model
 * @var Profile $profile
 * @var Photo[] $photos
 */

use modules\photo\common\models\Photo;
use modules\profiles\common\models\Profile;
use modules\profiles\common\models\ProfileList;
use modules\profiles\frontend\assets\ViewProfileAsset;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\MaskedInput;

ViewProfileAsset::register($this);

$my = ($profile && $profile->id == $model->id) ? true : false;
$this->title = $my ? 'Моя анкета' : 'Анкета участника ' . $model->name;
$showNudes = $profile && $profile->hasPremiumNudes();

$hasNudes = false;
for ($i = 0; $i < count($photos); $i++) {
    if ($photos[$i]->nudes) {
        $hasNudes = true;
    }
}
?>

<?php if ($my): ?>
    <?= $this->render('_edit_avatar', ['profile' => $model, 'view' => $this]) ?>
<?php endif; ?>

	<div class="block-main-box">
		<table class="block-main">
			<tr>
				<td class="profile-main-avatar">
					<div class="avatar-block">
                        <?php if ($my): ?>
							<label class="label" data-toggle="tooltip" title="сменить фотографию">
								<img src="<?= $model->avatar_path ?>" id="avatar">
								<input type="file" class="sr-only" id="input" name="image" accept="image/*">
							</label>
                        <?php else: ?>
							<img src="<?= $model->avatar_path ?>">
                        <?php endif; ?>
					</div>

                    <?php if ($profile && !$my): ?>
						<div class="profile-lists">
							<a href="<?= Url::to(['/chat', 'with' => $model->id]) ?>"
							   class="btn btn-primary link-chat">
								Написать
							</a>
							<a href="#" data-href="<?= Url::to(['/web', 'with' => $model->id]) ?>" data-toggle="tooltip"
							   title="Звонок" class="btn btn-primary btn-web link-video">
								<span class="fa fa-phone"></span>
							</a>

                            <?php if ($profile->hasFavorite($model->id)): ?>
								<a class="btn link-favorite has" title="Убрать из избранных" data-toggle="tooltip"
								   href="<?= Url::to(['/profiles/profile/list', 'id' => $model->id, 'type' => ProfileList::TYPE_FAVORITE]) ?>">
									<i class="fa fa-star"></i></a>
                            <?php else: ?>
								<a class="btn link-favorite" title="Добавить в избранные" data-toggle="tooltip"
								   href="<?= Url::to(['/profiles/profile/list', 'id' => $model->id, 'type' => ProfileList::TYPE_FAVORITE]) ?>">
									<i class="fa fa-star-o"></i></a>
                            <?php endif; ?>

							<a href="<?= Url::to(['/chat/chat/winked', 'to' => $model->id]) ?>"
							   title='Помашите в знак приветствия'
							   class="btn link-winked" data-toggle="tooltip">
								<img src="/images/wave.png"/>
							</a>

                            <?= $this->renderFile('@modules/profiles/frontend/views/complain.php', [
                                'profile' => $profile,
                                'model' => $model,
                                'source' => \modules\profiles\common\models\ProfileComplaint::SOURCE_VIEW
                            ]) ?>

                            <?php if ($profile->hasBlacklist($model->id)): ?>
								<a class="btn link-blacklist has" title="Убрать из игнора" data-toggle="tooltip"
								   href="<?= Url::to(['/profiles/profile/list', 'id' => $model->id, 'type' => ProfileList::TYPE_BLACKLIST]) ?>">
									<i class="fa fa-ban"></i></a>
                            <?php else: ?>
								<a class="btn link-blacklist" title="Добавить в игнор" data-toggle="tooltip"
								   href="<?= Url::to(['/profiles/profile/list', 'id' => $model->id, 'type' => ProfileList::TYPE_BLACKLIST]) ?>">
									<i class="fa fa-ban"></i></a>
                            <?php endif; ?>
						</div>
                    <?php endif; ?>
				</td>
				<td class="profile-main-info">
					<div class="pull-right online-text ua" data-ua="<?= $model->id ?>">
                        <?= $model->online ?>
						<img src="/images/heart_beige.jpg"/>
					</div>
					<h3 class="profile-name serif"><?= $model->name ?></h3>
					<div class="profile-years">
                        <?= $model->years ?>, <?= $model->zodiacal_sign ?>.
                        <?php if ($model->city): ?>
                            <?= $model->city->title ?>
                        <?php endif; ?>
					</div>

                    <?php if ($model->subway): ?>
						<div class="metro-block">
							<img src="/images/metro.jpg"/> <?= $model->subway->title ?>
						</div>
                    <?php endif; ?>

                    <?php if (!empty($model->my_intensions)): ?>
						<h4 class="serif">Цели знакомства</h4>
                        <?php if (!empty($model->intension)): ?>
							<div class="intensions">
								<ul class="list">
                                    <?php foreach (Profile::getIntensionOptions() as $intension => $title): ?>
                                        <?php if (in_array($intension, $model->my_intensions)): ?>
											<li>
												<span><?= $title ?></span>
											</li>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
								</ul>
							</div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if ($my): ?>
						<div class="edit-btn">
							<a href="<?= Url::to(['/edit']) ?>" class="btn btn-primary">
								Редактировать профиль
							</a>
						</div>
                    <?php endif; ?>
				</td>

				<td class="profile-right <?= $my ? 'my' : '' ?>">
                    <?php if ($my): ?>
                        <?php if (false === $profile->isActivated()): ?>
							<!-- Неполная регистрация -->
							<div class="serif profile-right-title">Анкета не активирована</div>
							<div id="modal-registered">
								Поздравляем, Вы зарегистрированы!
								Для того, что бы можно было воспользоваться услугами сайта осталось:
								<ul>
									<li>
										<i class="fa fa-check-circle"></i>
										Ознакомились с условиями
										<a href="/terms-of-use" target="_blank">пользовательского&nbsp;соглашения</a>
									</li>
									<li>
                                        <?php if (empty($profile->avatar)): ?>
											<i class="fa fa-ban"></i>
											Пожалуйста, <label for="input">загрузите аватар</label>
                                        <?php else: ?>
                                            <?php if ($profile->avatar_approved == false): ?>
												<i class="fa fa-ban"></i>
												Аватар загружен, но еще не был подтвержден администратором
                                            <?php else: ?>
												<i class="fa fa-check-circle"></i>
												Аватар загружен и подтвержден
                                            <?php endif; ?>
                                        <?php endif; ?>
									</li>
									<li>
                                        <?php if ($profile->email_confirmed == false): ?>
											<i class="fa fa-ban" id="phone_indicator"></i>
											Вам на почту <b><?= $profile->email ?></b>
											отправлено письмо с подтверждением.
											После подтверждения e-mail, Вы сможете заполнить анкету и
											воспользоваться услугами сайта.
											<div class="line">
												Если вы не получили письмо в течении 30 секунд, проверьте папку
												<b>спам</b>.
											</div>
											<div class="line">
												<a href="/profiles/auth/resend-email" id="resend_email">
													Продублировать письмо с активацией
												</a>
											</div>
											<div class="line msg-text" id="resend_msg"></div>
                                        <?php else: ?>
											<i class="fa fa-check-circle"></i>E-mail адрес подтвержден
                                        <?php endif; ?>
									</li>

									<li>
                                        <?php if ($profile->phone_confirmed == false): ?>
											<i class="fa fa-ban"></i>
											<a href="#" id="resend_sms">Подтвердите телефон</a>

											<div class="line-phone">
                                                <?= MaskedInput::widget([
                                                    'name' => 'phone',
                                                    'id' => 'phone_number',
                                                    'mask' => '+7 999 999-99-99',
                                                ]); ?>
												<a class="btn btn-default" id="phone_send">Отправить</a>
                                                <?= MaskedInput::widget([
                                                    'name' => 'phone',
                                                    'id' => 'phone_code',
                                                    'mask' => '9999',
                                                ]); ?>
												<a class="btn btn-default" id="phone_confirm">Подтвердить</a>
											</div>
											<div class="line msg-text" id="resend_sms_msg"></div>

											<div class="line">
												Чтобы сделать доступ к учетной записи безопасным, добавьте свой номер
												мобильного телефона, на который мы вышлем бесплатное СМС с кодом.
											</div>
											<div class="line">
												Данная информация необходима для защиты учетной записи. Мы ни кому не
												разглашаем
												Ваши данные, Ваш номер не будет доступен другим пользователям.
												Вся процедура бесплатна.
											</div>
											<div class="line msg-text" id="msg_phone"></div>
                                        <?php else: ?>
											<i class="fa fa-check-circle"></i>Номер <?= $profile->phone_mobile ?> подтвержден
                                        <?php endif; ?>
									</li>
								</ul>
							</div>
                        <?php else: ?>
							<!-- Что случилось за сегодня -->
                            <?php
                            $visits = $profile->findTodayVisits();
                            // $favorites = $profile->findTodayFavorites();
                            $likes = $profile->findTodayLikes();
                            $countPhotos = Profile::countPhotos();
                            ?>
							<div class="serif profile-right-title">Активность за сегодня</div>

                            <?php if (!empty($visits)): ?>
								<div class="profile-right-header">ПОСМОТРЕЛИ</div>
								<div class="row row-avatars">
                                    <?php foreach ($visits as $visit): ?>
                                        <?= $this->render('_profile_mini', [
                                            'model' => $visit['profile'],
                                            'profile' => $profile,
                                            'countPhotos' => $countPhotos,
                                            'class' => 'col-md-3 col-avatars'
                                        ]) ?>
                                    <?php endforeach; ?>
								</div>
                            <?php endif; ?>

                            <?php if (!empty($likes)): ?>
								<div class="profile-right-header">ВЫ ПОНРАВИЛИСЬ</div>
								<div class="row row-avatars">
                                    <?php foreach ($likes as $like): ?>
                                        <?= $this->render('_profile_mini', [
                                            'model' => $like['profile'],
                                            'profile' => $profile,
                                            'countPhotos' => $countPhotos,
                                            'class' => 'col-md-3 col-avatars',
                                            'heart_favorite' => true,
                                        ]) ?>
                                    <?php endforeach; ?>
								</div>
                            <?php endif; ?>
                        <?php endif; ?>

                    <?php elseif ($profile): ?>
						<!-- ЧАТ -->
                    <?php endif; ?>
				</td>
			</tr>
		</table>
	</div>

	<div class="row">
		<!-- О СЕБЕ -->
        <?php if (!empty($model->social_about)): ?>
			<div class="col-md-6">
				<div class="info-block info-block-about">
					<h4 class="serif">О себе
                        <?php if ($my): ?>
							<a href="/edit#about"><i class="fa fa-pencil"></i></a>
                        <?php endif; ?>
					</h4>
					<div class="profile-about">
                        <?= \yii\helpers\Html::encode($model->social_about) ?>
					</div>
				</div>
			</div>
        <?php endif; ?>

		<!-- ВНЕШНОСТЬ -->
        <?php if (!empty($model->my_height)
            || !empty($model->my_weight)
            || !empty($model->my_type)
            || !empty($model->my_constitution)
            || !empty($model->my_eyes)
            || !empty($model->my_hair)
            || !empty($model->my_bust)
        ): ?>
			<div class="col-md-6">
				<div class="info-block">
					<h4 class="serif">Внешность
                        <?php if ($my): ?>
							<a href="/edit#looks"><i class="fa fa-pencil"></i></a>
                        <?php endif; ?>
					</h4>
					<div class="row">
						<div class="col-md-6">
                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    [
                                        'label' => 'Рост',
                                        'value' => $model->my_height . ' см',
                                        'visible' => $model->my_height > 0,
                                    ],
                                    [
                                        'label' => 'Вес',
                                        'value' => $model->my_weight . ' кг',
                                        'visible' => $model->my_weight > 0,
                                    ],
                                    [
                                        'label' => 'Внешность',
                                        'value' => $model->my_type_label,
                                        'visible' => $model->my_type,
                                    ],
                                ],
                            ]) ?>
						</div>
						<div class="col-md-6">
                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    [
                                        'label' => 'Телосложение',
                                        'value' => $model->my_constitution_label,
                                        'visible' => $model->my_constitution,
                                    ],
                                    [
                                        'label' => 'Цвет глаз',
                                        'value' => $model->my_eyes_label,
                                        'visible' => $model->my_eyes,
                                    ],
                                    [
                                        'label' => 'Цвет волос',
                                        'value' => $model->my_hair_label,
                                        'visible' => $model->my_hair,
                                    ],
                                    [
                                        'label' => 'Бюст',
                                        'value' => $model->my_bust . ' размер',
                                        'visible' => $model->my_bust && $model->isFemale(),
                                    ],
                                ],
                            ]) ?>
						</div>
					</div>
				</div>
			</div>
        <?php endif; ?>

		<!-- СЕКС -->
        <?php if (!empty($model->sex_freq)
            || !empty($model->sex_role)
            || !empty($model->sex_types)
        ): ?>
			<div class="col-md-6">
				<div class="info-block">
					<h4 class="serif">Секс
                        <?php if ($my): ?>
							<a href="/edit#sex"><i class="fa fa-pencil"></i></a>
                        <?php endif; ?>
					</h4>
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            [
                                'label' => 'Частота',
                                'value' => $model->sex_freq_label,
                                'visible' => $model->sex_freq,
                            ],
                            [
                                'label' => 'Роль',
                                'value' => $model->sex_role_label,
                                'visible' => $model->sex_role,
                            ],
                            [
                                'label' => 'Предпочтения',
                                'value' => $model->sex_types_label,
                                'visible' => $model->sex_types,
                            ],
                        ],
                    ]) ?>
				</div>
			</div>
        <?php endif; ?>

		<!-- СОЦИУМ -->
        <?php if (!empty($model->social_education)
            || !empty($model->social_relationship)
            || !empty($model->social_kids)
            || !empty($model->social_finance)
        ): ?>
			<div class="col-md-6">
				<div class="info-block">
					<h4 class="serif">Социум
                        <?php if ($my): ?>
							<a href="/edit#social"><i class="fa fa-pencil"></i></a>
                        <?php endif; ?>
					</h4>
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            [
                                'label' => 'Образование',
                                'value' => $model->social_education_label,
                                'visible' => $model->social_education,
                            ],
                            [
                                'label' => 'Отношения',
                                'value' => $model->social_relationship_label,
                                'visible' => $model->social_relationship,
                            ],
                            [
                                'label' => 'Дети',
                                'value' => $model->social_kids_label,
                                'visible' => $model->social_kids,
                            ],
                            [
                                'label' => 'Материальное положение',
                                'value' => $model->social_finance_label,
                                'visible' => $model->social_finance,
                            ],
                        ],
                    ]) ?>
				</div>
			</div>
        <?php endif; ?>

		<!-- ИНТЕРЕСЫ -->
        <?php if (!empty($model->social_interests)): ?>
			<div class="col-md-6">
				<div class="info-block">
					<h4 class="serif">Интересы
                        <?php if ($my): ?>
							<a href="/edit#interests"><i class="fa fa-pencil"></i></a>
                        <?php endif; ?>
					</h4>
					<div class="profile-about">
                        <?= \yii\helpers\Html::encode($model->social_interests) ?>
					</div>
				</div>
			</div>
        <?php endif; ?>
	</div>

	<!-- ФОТОГАЛЛЕРЕЯ -->
<?php if (!empty($photos)): ?>
    <?php $this->beginBlock('after-content'); ?>
	<div class="container">
		<div class="h1 center">Фотографии</div>

        <?php if ($hasNudes && $showNudes == false): ?>
			<div class="show-nudes pre-info">
				Просмотр фотографий 18+ доступен лишь <a href="/finance" class="underlined">Премиум Plus</a> аккаунтам,
				либо при покупке отдельно этой функции в разделе <a href="/finance" class="underlined">Финансы</a>
			</div>
        <?php endif; ?>

		<div class="row row-5 photogallery">
            <?php foreach ($photos as $photo): ?>
				<div class="col-md-5ths col-xs-6 preview photo">
					<a class="fancybox-thumb" rel="fancybox-thumb"
					   href="<?= $photo->nudes ? ($showNudes ? $photo->url : $photo->blur) : $photo->url ?>"
					   target="_blank"
                        <?= $photo->nudes ? 'title="18+"' : '' ?>>
						<img
							src="<?= $photo->nudes ? ($showNudes ? $photo->crop : $photo->blur_crop) : $photo->crop ?>"/>
					</a>
				</div>
            <?php endforeach; ?>
		</div>

		<div class="photo-buttons center">
            <?php if ($my): ?>
				<a href="/photo/albums" class="btn btn-default beige-border" id="photo-add">Добавить</a>
            <?php else: ?>
				<a href="/search" class="btn btn-primary">Больше <?= $model->isMale() ? 'мужчин' : 'девушек' ?></a>
            <?php endif; ?>
		</div>
	</div>
    <?php $this->registerCss('#footer { background: white !important; }'); ?>
    <?php $this->endBlock(); ?>
<?php endif; ?>