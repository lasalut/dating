<?php

/**
 * @var \yii\web\View $this
 * @var \modules\profiles\common\models\Profile $model
 */

$this->title = 'Учетная запись заблокирована';
?>

<div class="row">
	<div class="col-md-2"></div>
	<div class="col-md-8">
		<h1 class="main center"><?= $this->title ?></h1>

        <?php if (!empty($model->blocked_reason)): ?>
			<div class="center">
				<h3>Причина блокировки: <?= $model->blocked_reason ?></h3>
			</div>
        <?php endif; ?>
	</div>
	<div class="col-md-2"></div>
</div>