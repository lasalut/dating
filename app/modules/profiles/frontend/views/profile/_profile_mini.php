<?php
/** @var Profile $profile */
/** @var Profile $model */
/** @var array $countPhotos */
/** @var boolean $heart_favorite */
/** @var string $class */

use modules\profiles\common\models\Profile;

$key = $model->id . '';
?>

<div class="<?= $class ?>">
	<a href="/profile/<?= $model->id ?>">
		<div class="preview-info">
			<img class="preview-image" src="<?= $model->avatar_path ?>">

			<div class="preview-hover mini">
				<div class="preview-hover-top">
					<div><?= $model->name ?></div>
					<div><?= $model->years ?></div>
					<?php if ($city = $model->city): ?>
						<div class="priview-hover-city">г. <?= $city->title ?></div>
					<?php endif; ?>
				</div>
				<div class="preview-hover-bottom">
                    <?php if (!empty($countPhotos[$key])): ?>
						<i class="fa fa-image"></i> <?= $countPhotos[$key] ?>
                    <?php endif; ?>
				</div>
			</div>

			<?php if (!empty($heart_favorite)): ?>
				<img class="heart-favorite" src="/images/heart_favorite.jpg"/>
			<?php endif; ?>
		</div>
	</a>
</div>