<?php
/** @var \yii\web\View $view */
/** @var \modules\profiles\common\models\Profile $profile */
\modules\profiles\frontend\assets\EditAvatarAsset::register($view);
?>
<div class="upload-box">
	<div class="alert alert-avatar" role="alert"></div>
	<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="img-container">
						<img id="image" src="">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" id="rotate-left"
							title="повернуть на левый бок">
						<i class="fa fa-mail-reply"></i>
					</button>
					<button type="button" class="btn btn-default" id="rotate-right"
							title="повернуть на правый бок">
						<i class="fa fa-mail-forward"></i>
					</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
					<button type="button" class="btn btn-primary" id="crop">Сохранить</button>
				</div>
			</div>
		</div>
	</div>
</div>