<?php

/**
 * @var \yii\web\View $this
 * @var \modules\profiles\frontend\models\ProfileForm $model
 */

use modules\profiles\frontend\assets\SettingsProfileAsset;

$this->title = 'Настройки';

SettingsProfileAsset::register($this);
?>

<h1 class="main center">Настройки</h1>

<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-6 checkboxes">
        <?php $form = \yii\bootstrap\ActiveForm::begin([
            'method' => 'POST',
            'options' => ['enctype' => 'multipart/form-data']
        ]) ?>
		<div class="row first-row">
			<div class="col-md-6">
				<div class="block-title serif">
					<span>Личные данные</span>
				</div>
                <?= $form->field($model, 'email')->input('email') ?>
                <?= $form->field($model, 'phone_mobile_local')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '+7 999 999-99-99',
                ]) ?>
			</div>
			<div class="col-md-6">
				<div class="block-title serif">
					<span>Смена пароля</span>
				</div>
                <?= $form->field($model, 'password')->input('password') ?>
                <?= $form->field($model, 'passwordCompare')->input('password') ?>
			</div>
		</div>

		<div class="block-title serif">
			<span>Премиум Plus</span>
		</div>
        <?php if ($model->hasPremiumIncognito()): ?>
            <?= $form->field($model, 'premium_incognito_on')->checkbox() ?>
		<?php else: ?>
            <?= $form->field($model, 'premium_incognito_on')->checkbox()->staticControl() ?>
        <?php endif; ?>

		<div class="block-title serif">
			<span>Уведомления</span>
		</div>
        <?= $form->field($model, 'notify_sound')->checkbox() ?>
        <?= $form->field($model, 'notify_email_messages')->checkbox() ?>
        <?= $form->field($model, 'notify_email_visits')->checkbox() ?>
        <?= $form->field($model, 'notify_webpush')->checkbox(['id' => 'notify_webpush']) ?>

		<div class="btn-save-last">
			<button type="submit" class="btn btn-primary btn-lg">Сохранить</button>
		</div>
        <?php \yii\bootstrap\ActiveForm::end() ?>

		<div class="block-title serif">
			<span>Удаление анкеты</span>
		</div>
		<div class="clearfix">
            <?php if ($model->removed): ?>
				<a href="/profiles/profile/restore" class="btn btn-default">Восстановить</a>
            <?php else: ?>
				<a href="/profiles/profile/remove" class="btn btn-default">Удалить</a>
            <?php endif; ?>
		</div>
	</div>
	<div class="col-md-3"></div>
</div>