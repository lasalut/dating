<?php

/**
 * @var \yii\web\View $this
 * @var \modules\profiles\common\models\Profile $model
 */

use modules\profiles\common\models\Profile;
use modules\profiles\common\models\Subway;
use modules\profiles\frontend\assets\EditProfileAsset;
use vova07\select2\Widget;

$this->title = 'Моя анкета';
$birthday = new \DateTime($model->birthday_on);
$day = intval($birthday->format('d'));
$month = intval($birthday->format('m'));
$year = intval($birthday->format('Y'));

EditProfileAsset::register($this);
?>

<?= $this->render('_edit_avatar', ['profile' => $model, 'view' => $this]) ?>

<?php $form = \yii\bootstrap\ActiveForm::begin([
    'method' => 'POST',
    'options' => ['enctype' => 'multipart/form-data']
]) ?>

	<div class="block-main-box">
		<table class="block-main">
			<tr>
				<td class="profile-main-avatar">
					<div class="avatar-block">
						<label class="label" data-toggle="tooltip" title="сменить фотографию">
							<img src="<?= $model->avatar_path ?>" id="avatar">
							<input type="file" class="sr-only" id="input" name="image" accept="image/*">
						</label>
					</div>
				</td>
				<td class="profile-main-info">
					<div class="block-title serif"><span>Основная информация</span></div>
					<div class="profile-edit-years">
                        <?= $model->years ?>, <?= $model->zodiacal_sign ?>
						<div class="pull-right">
                            <?= $this->render('_fabric_avatar', ['profile' => $model, 'view' => $this]) ?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-left">
                            <?= $form->field($model, 'name')->textInput(['class' => 'full']) ?>
						</div>
						<div class="col-md-6 col-right">
                            <?= $form->field($model, 'email')->input('email', ['class' => 'full']) ?>
						</div>
					</div>
                    <?= $form->field($model, 'city_local')->textInput(['value' => $model->city_string, 'class' => 'full']) ?>
                    <?= $form->field($model, 'subway_id')->dropDownList(Subway::getOptions(), ['prompt' => 'не указано', 'class' => 'full']) ?>
				</td>

				<td class="profile-right">
					<div class="block-title serif"><span>Цели знакомства</span></div>
					<ul class="checkboxes intensions">
                        <?php foreach (Profile::getIntensionOptions() as $intension => $title): ?>
							<li>
                                <?php if (in_array($intension, $model->my_intensions)): ?>
									<input type="checkbox" name="Profile[intensions][]" value="<?= $intension ?>"
										   checked/>
                                <?php else: ?>
									<input type="checkbox" name="Profile[intensions][]" value="<?= $intension ?>"/>
                                <?php endif; ?>
                                <?= $title ?>
							</li>
                        <?php endforeach; ?>
					</ul>
					<div class="save-btn">
						<button type="submit" class="btn btn-primary">
							Сохранить
						</button>
					</div>
				</td>
			</tr>
		</table>
	</div>

<?= $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-md-6">
			<div class="block-title serif"><span>О себе</span></div>
            <?= $form->field($model, 'social_about')
                ->textarea(['rows' => 8, 'placeholder' => $model->isMale() ? 'Расскажите о себе пару слов, чтобы девушка быстро поняла как с вами лучше общаться' : 'Милые девушки расскажите о себе, чтобы собеседнику было легко начать беседу и понять, как вас завоевать'])
                ->label('Короткий рассказ о себе') ?>
            <?= $form->field($model, 'social_interests')
				->textarea(['rows' => 8, 'placeholder' => $model->isMale() ? 'Расскажите о ваших увлечениях и пристрастиях, чтобы собеседнику было проще найти с вами общий язык' : 'Здесь расскажите о ваших увлечениях, как вы любите проводите время, что вызывает у вас восторг и делает вас счастливой. Поделитесь частичкой своей жизни: пусть она будет красочной и яркой, чтобы мужчина очаровался вами с первых слов'])
				->label('Интересы') ?>
		</div>

		<div class="col-md-6">
			<div class="block-title serif"><span>Внешность</span></div>
			<div class="row">
                <?php if ($model->isFemale()): ?>
					<div class="col-md-4">
                        <?= $form->field($model, 'my_height')->dropDownList(Profile::getHeightOptions(), ['prompt' => 'не указано', 'class' => 'full']) ?>
					</div>
					<div class="col-md-4">
                        <?= $form->field($model, 'my_weight')->dropDownList(Profile::getWeightOptions(), ['prompt' => 'не указано', 'class' => 'full']) ?>
					</div>
					<div class="col-md-4">
                        <?= $form->field($model, 'my_bust')->radioList(Profile::getBustOptions()) ?>
					</div>
                <?php else: ?>
					<div class="col-md-6">
                        <?= $form->field($model, 'my_height')->dropDownList(Profile::getHeightOptions(), ['prompt' => 'не указано', 'class' => 'full']) ?>
					</div>
					<div class="col-md-6">
                        <?= $form->field($model, 'my_weight')->dropDownList(Profile::getWeightOptions(), ['prompt' => 'не указано', 'class' => 'full']) ?>
					</div>
                <?php endif; ?>
			</div>

			<div class="row">
				<div class="col-md-6">
                    <?= $form->field($model, 'my_hair')->radioList(Profile::getHairOptions()) ?>
				</div>
				<div class="col-md-6">
                    <?= $form->field($model, 'my_eyes')->radioList(Profile::getEyesOptions()) ?>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
                    <?= $form->field($model, 'my_constitution')->radioList(Profile::getMyConstitutionOptions()) ?>
				</div>
				<div class="col-md-6">
                    <?= $form->field($model, 'my_type')->radioList(Profile::getMyTypeOptions()) ?>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="block-title serif"><span>Секс</span></div>
			<div class="row">
				<div class="col-md-6">
                    <?= $form->field($model, 'sex_freq')->radioList(Profile::getSexFreqOptions()) ?>
                    <?= $form->field($model, 'sex_role')->radioList(Profile::getSexRoleOptions()) ?>
				</div>
				<div class="col-md-6">
					<label class="control-label">Предпочтения</label>
					<ul class="checkboxes">
                        <?php foreach (Profile::getSexTypesOptions() as $option => $title): ?>
							<li>
                                <?php if (in_array($option, $model->my_sex_types)): ?>
									<input type="checkbox" name="Profile[sex_types_local][]" value="<?= $option ?>"
										   checked/>
                                <?php else: ?>
									<input type="checkbox" name="Profile[sex_types_local][]" value="<?= $option ?>"/>
                                <?php endif; ?>
								<span><?= $title ?></span>
							</li>
                        <?php endforeach; ?>
					</ul>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="block-title serif"><span>Социум</span></div>
			<div class="row">
				<div class="col-md-6" style="padding-right:0">
                    <?= $form->field($model, 'social_education')->radioList(Profile::getEducationOptions()) ?>
                    <?= $form->field($model, 'social_finance')->radioList(Profile::getFinanceOptions()) ?>
				</div>
				<div class="col-md-6">
                    <?= $form->field($model, 'social_relationship')->radioList(Profile::getRelationshipOptions()) ?>
                    <?= $form->field($model, 'social_kids')->radioList(Profile::getKidsOptions()) ?>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="block-title serif"><span>ПОИСК</span></div>
			<label class="control-label">Предпочитаемый возраст</label>
			<ul class="checkboxes">
                <?php foreach (Profile::getLookingYearsOptions() as $option => $title): ?>
					<li>
                        <?php if (in_array($option, $model->my_looking_years)): ?>
							<input type="checkbox" name="Profile[looking_years_local][]" value="<?= $option ?>"
								   checked/>
                        <?php else: ?>
							<input type="checkbox" name="Profile[looking_years_local][]" value="<?= $option ?>"/>
                        <?php endif; ?>
						<span><?= $title ?></span>
					</li>
                <?php endforeach; ?>
			</ul>
		</div>
	</div>

	<div class="btn-save-last">
		<button type="submit" class="btn btn-primary btn-lg">
			Сохранить
		</button>
	</div>
<?php \yii\bootstrap\ActiveForm::end() ?>