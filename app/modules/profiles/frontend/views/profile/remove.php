<?php

/**
 * @var \yii\web\View $this
 * @var \modules\profiles\common\models\Profile $profile
 */

use modules\profiles\frontend\assets\SettingsProfileAsset;

$this->title = 'Удаление анкеты';

SettingsProfileAsset::register($this);
?>

<h1 class="main center"><?= $this->title ?></h1>

<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-6 checkboxes">
        <?php $form = \yii\bootstrap\ActiveForm::begin([
            'method' => 'POST',
            'options' => ['enctype' => 'multipart/form-data']
        ]) ?>

        <?= $form->field($profile, 'removed_comment')
            ->label('Пожалуйста, укажите причину удаления профиля')
            ->textarea() ?>

		<p class="removal">
			Вы сможете восстановить анкету после ее удаления, при этом Ваш баланс будет восстановлен,
			но все подключенные платные услуги будут анулированны. Данные вашей анкеты хранятся в з
			ашифрованном виде и подлежат полному удалению через 180 дней.</p>

		<div class="btn-save-last center">
			<button type="submit" class="btn btn-primary btn-lg">Удалить</button>
		</div>
        <?php \yii\bootstrap\ActiveForm::end() ?>
	</div>
	<div class="col-md-3"></div>
</div>