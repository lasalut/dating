<?php

use modules\profiles\common\models\Profile;
use marketingsolutions\finance\models\Transaction;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel modules\profiles\frontend\models\BonusesTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Баллы и подарка';
$this->params['breadcrumbs'][] = 'Личный кабинет';
$this->params['breadcrumbs'][] = $this->title;

/** @var Profile $profile */
$profile = \Yii::$app->user->identity;

$css = <<<CSS
	section {
		display: none;
	}
	.table {
		margin-top: 20px;
	}
CSS;
$this->registerCss($css);
?>

<div class="row">
	<div class="col-md-4 text-center">
		<img src="<?= $profile->avatar_path ?>" alt="" class="profile-avatar">
	</div>
	<div class="col-md-8">
		<h2>Личный кабинет</h2>
		<ul class="lk-menu">
			<li id="personal-data" class="lk-menu-item">
				<a href="/profiles/profile/index">Личные данные</a>
			</li>
			<li id="my-history" class="lk-menu-item active">
				<a href="/profiles/bonuses-transactions/index">Баллы и подарки</a>
			</li>
		</ul>
		<div class="clearfix"></div>

		<div class="transaction-index">
			<?= GridView::widget([
				'tableOptions' => ['class' => 'table table-striped'],
				'dataProvider' => $dataProvider,
				'columns' => [
					[
						'attribute' => 'type',
						'format' => 'raw',
						'value' => function (Transaction $data) {
							if ($data->type == Transaction::INCOMING) {
								return Html::tag('span', 'Входящая', ['class' => 'label label-success']);
							}

							return Html::tag('span', 'Исходящая', ['class' => 'label label-danger']);
						}
					],
					'created_at:datetime',
					[
						'attribute' => 'amount',
						'format' => 'raw',
						'value' => function (Transaction $data) {
							return Yii::$app->formatter->asDecimal($data->amount, 0);
						}
					],
					'title',
				],
			]); ?>
		</div>
	</div>
</div>

