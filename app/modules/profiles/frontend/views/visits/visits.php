<?php

use modules\profiles\common\models\Profile;
use modules\profiles\frontend\assets\VisitsAsset;
use modules\profiles\frontend\controllers\VisitsController;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\widgets\ListView;

/**
 * @var \yii\web\View $this
 * @var Profile $profile
 * @var array $visits
 */

$title = 'Мои гости';
$this->title = $title;

VisitsAsset::register($this);

$today = (new \DateTime())->format('d.m.Y');
$s = Yii::$app->request->get('s', VisitsController::SECTION_ALL);

$photos = Profile::countPhotos();
$likedAvatars = $profile ? $profile->findLikedAvatars() : null;
?>

	<div class="content-header clearfix">
		<h1 class="pull-left"><?= $title ?></h1>
		<div class="pull-right">
			<div class="btn-group">
				<a href="<?= Url::to(['/visits', 's' => VisitsController::SECTION_ALL]) ?>"
				   class="btn btn-default <?= $s == VisitsController::SECTION_ALL ? 'active' : '' ?>">Все</a>
				<a href="<?= Url::to(['/visits', 's' => VisitsController::SECTION_TODAY]) ?>"
				   class="btn btn-default <?= $s == VisitsController::SECTION_TODAY ? 'active' : '' ?>">Сегодня</a>
				<a href="<?= Url::to(['/visits', 's' => VisitsController::SECTION_VISITS]) ?>"
				   class="btn btn-default <?= $s == VisitsController::SECTION_VISITS ? 'active' : '' ?>">Гости</a>
				<a href="<?= Url::to(['/visits', 's' => VisitsController::SECTION_LIKES]) ?>"
				   class="btn btn-default <?= $s == VisitsController::SECTION_LIKES ? 'active' : '' ?>">Симпатии</a>
			</div>
		</div>
	</div>

<?php if (empty($visits)): ?>
	<h4>Событий не найдено</h4>
<?php else: ?>
	<div class="row row-5">
		<?php
        foreach ($visits as $v) {
            /** @var Profile $model */
            $model = $v['profile'];
            $at = $today == $v['created'] ? 'Сегодня' : $v['created'];
            $at .= ' в ' . (new \DateTime($v['created_at']))->format('H:i');
            echo $this->renderFile('@modules/profiles/frontend/views/search/_profile.php',
                compact('profile', 'model', 'photos', 'likedAvatars', 'at'));
		}
		?>
	</div>
<?php endif; ?>