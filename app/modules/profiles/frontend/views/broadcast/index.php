<?php

use modules\profiles\common\models\Broadcast;
use modules\profiles\common\models\Premium;
use modules\profiles\common\models\Profile;
use modules\profiles\frontend\models\BroadcastForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var Profile $profile
 * @var Premium[] $premiums
 * @var BroadcastForm $broadcast
 * @var Profile[] $myContacts
 * @var Broadcast[] $broadcasts
 * @var array $likedAvatars
 * @var array $photos
 */

$title = 'Рассылка';
$this->title = $title;

\modules\profiles\frontend\assets\BroadcastAsset::register($this);

$placeholder = $profile->isMale()
	? 'Пример: Милые девушки! Ищу спутницу для поездки в Лондон в эти выходные. Знание английского языка будет плюсом.'
	: 'Я ласковая, нежная ищу приятную компанию на вечер. Люблю театры и гулять. Парни Моложе 28 просьба не беспокоить.';

?>
<h1 class="main center"><?= $title ?></h1>

<div class="row">
	<div class="col-md-2"></div>
	<div class="col-md-8" id="broadcast-wrap">
		<div class="btn-group mobile-title">
			<a href="#" class="btn btn-default active" id="tab-broadcasts"><?= $title ?></a>
			<a href="#" class="btn btn-default" id="tab-add">+ добавить</a>
		</div>

		<div class="broadcast-form">
            <?php if ($profile->isMale()): ?>
				<p>Просто напиши что ты хочешь от девушки сегодня и сейчас, и нажми кнопку отправить. Твое сообщение
					увидят сразу все девушки на сайте, так что прояви фантазию!</p>
            <?php else: ?>
				<p>Просто напиши что ты хочешь от мужчины сегодня и сейчас, и нажми кнопку отправить. Твое сообщение
					увидят сразу все мужчины на сайте, так что прояви фантазию!</p>
            <?php endif; ?>

            <?php $form = ActiveForm::begin([
                'action' => '/broadcast',
                'method' => 'POST',
                'options' => ['id' => 'formBroadcast']
            ]); ?>
			<div class="row form-row">
				<div class="col-md-6">
                    <?= $form->field($broadcast, 'hours')
                        ->dropDownList(Premium::broadcastHourOptions(), ['class' => 'full'])
                        ->label(false) ?>
				</div>
				<div class="col-md-6">
                    <?= $form->field($broadcast, 'city_local')
                        ->textInput([
                        	'class' => 'full',
							'placeholder' => 'Город',
							'id' => 'broadcast-city',
							'data-toggle' => 'tooltip',
							'title' => 'Если Вы хотите, что бы рассылка была видна лишь в определенном городе',
						])
                        ->label(false) ?>
				</div>
			</div>
			<div class="row form-row">
				<div class="col-md-12">
                    <?= $form->field($broadcast, 'comment')
                        ->textarea(['rows' => 6, 'placeholder' => $placeholder])
                        ->label(false)
                    ?>
				</div>
			</div>

			<!--КОНТАКТЫ МОИ-->
            <?php if (!empty($myContacts)): ?>
				<div class="row form-row">
					<div class="col-md-6 col-xs-6 col-contacts">
						<div class="contacts-controls row">
							<div class="col-md-6 contacts-title">Контакты</div>
							<div class="col-md-6 to-right">
								<a href="#" id="btn-contacts-add" class="link-restore">Выбрать всех</a>
							</div>
						</div>
                        <?php for ($i = 0; $i < count($myContacts); $i++): ?>
							<div class="contact" data-id="<?= $myContacts[$i]->id ?>">
								<img src="<?= $myContacts[$i]->avatar_url ?>"/>
								<span class="contact-name">
							<?= $myContacts[$i]->name ?>, <?= $myContacts[$i]->age ?>
						</span>
							</div>
                        <?php endfor; ?>
					</div>
					<div class="col-md-6 col-xs-6 col-excluded">
						<div class="contacts-controls row">
							<div class="col-md-6 contacts-title">Заблокировать</div>
							<div class="col-md-6 to-right">
								<a href="#" id="btn-contacts-exclude" class="link-restore pull-right">Убрать всех</a>
							</div>
						</div>
						<div class="excluded-info">
							Выбранные контакты не увидят объявление
						</div>
					</div>
				</div>
                <?= $form->field($broadcast, 'excluded')->hiddenInput(['id' => 'excluded_ids'])->label(false) ?>
            <?php endif; ?>

			<div class="form-row center">
                <?= Html::submitButton('Опубликовать', ['class' => 'btn btn-primary']) ?>
			</div>
            <?php ActiveForm::end(); ?>
		</div>

        <?php if (!empty($broadcasts)): ?>
			<div class="broadcasts">
                <?php foreach ($broadcasts as $b): ?>
					<div class="broadcast row">
						<div class="col-md-5">
                            <?= $this->renderFile('@modules/profiles/frontend/views/search/_profile_preview.php', [
                                'likedAvatars' => $likedAvatars,
                                'photos' => $photos,
                                'profile' => $profile,
                                'model' => $b->profile,
                            ]) ?>
						</div>
						<div class="col-md-7">
							<div class="broadcast-comment">
                                <?= Html::encode($b->comment) ?>
							</div>
						</div>

						<div class="broadcast-date">
                            <?= (new \DateTime($b->created_at))->format('d.m.Y H:i') ?>
						</div>
					</div>
                <?php endforeach; ?>
			</div>
        <?php endif; ?>
	</div>
	<div class="col-md-2"></div>
</div>

