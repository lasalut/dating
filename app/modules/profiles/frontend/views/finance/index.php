<?php

use modules\profiles\common\models\Premium;
use modules\profiles\common\models\Profile;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var Profile $profile
 */

$title = 'Мой кошелек';
$this->title = $title;

\modules\profiles\frontend\assets\FinanceAsset::register($this);
?>
<h1 class="main center"><?= $title ?></h1>

<style>
	li.anketa, li.anketa * {
		background: #8e93c0;
		color: white !important;
	}
	#finance-mobile .to-right {
		float: right;
		margin-right: 2px;
		padding: 3px 10px;
		background: #c89c6b;
		color: white;
		vertical-align: top;
		margin-top: -3px;
	}
	#finance-mobile .purse-balance {
		color: #d94b53;
		font-weight: bold;
		font-size: 20px;
	}
	#finance-mobile .purse-balance img {
		margin-right: 10px;
	}
	#finance-mobile a img {
		max-width:  100%;
		max-height: 50px;
	}
	#finance-mobile ul {
		margin:     0 0 50px;
		padding:    0;
		background: white;
	}
	#finance-mobile li {
		overflow:      hidden;
		border-bottom: 1px solid #e2e2e2;
		padding:       0 12px;
	}
	#finance-mobile li.active {
		background: #8e93c0;
	}
	#finance-mobile li.with-avatar {
		padding-left:   30px;
		padding-bottom: 6px;
		background:     #f7f7f7;
		border-top:     1px solid #e2e2e2;
	}
	#finance-mobile li a {
		display:     block;
		padding:     15px 0 15px;
		color:       #333;
		font-size:   18px;
		line-height: 19px;
	}
	#finance-mobile li.active a {
		color: white;
	}
	#finance-mobile li a, #mobile-menu li a:hover, #mobile-menu li a:active, #mobile-menu li a:focus {
		text-decoration: none;
	}
	#finance-mobile .fa {
		width:        40px;
		text-align:   center;
		color:        #c69c6d;
		float:        left;
		font-size:    21px;
		margin-right: 5px;
	}
	#finance-mobile li.active .fa {
		color: white;
	}
	@media (max-width: 700px) {
		.container {
			margin:  0;
			padding: 0;
		}
	}
</style>

<div id="finance-mobile">
	<ul>
		<li class="anketa center">
			<a href="/top">
				Анкета. Место: 1
			</a>
		</li>
		<li>
			<a href="/balance">
				<span class="purse-balance">
					<img src="/images/fin/wallet.png"><?= $profile->purse->balance ?> р.</span>
				<span class="to-right">
					Пополнить
				</span>
			</a>
		</li>
        <?php if ($profile->isMale()): ?>
		<li>
			<a href="#" class="premium-btn">
				Премиум
				<span class="to-right">
					Купить
				</span>
			</a>
		</li>
		<?php endif; ?>
		<li>
			<a href="/top">
				Поднятие в ТОП
				<span class="to-right">
					Купить
				</span>
			</a>
		</li>
		<li>
			<a href="/calls">
				Минуты
				<span class="to-right">
					Купить
				</span>
			</a>
		</li>
        <?php if ($profile->isMale()): ?>
		<li>
			<a href="/contacts">
				Контакты
				<span class="to-right">
					Купить
				</span>
			</a>
		</li>
		<?php endif; ?>

		<li>
			<a href="/online">
				Кто онлайн
				<span class="to-right">
					Купить
				</span>
			</a>
		</li>

		<li>
			<a href="/incognito">
				Скрыть статус онлайн
				<span class="to-right">
					Купить
				</span>
			</a>
		</li>

		<li>
			<a href="/sticked">
				Закрепление в ТОП
				<span class="to-right">
					Купить
				</span>
			</a>
		</li>
	</ul>
</div>

<div class="row finance" id="finance-desktop">
	<div class="col-md-6">
		<div class="f-box">
			<div class="f-image"><img src="/images/fin/fin-rub.png"/></div>
			<div class="f-title">Баланс: <span class="my-balance"><?= $profile->purse->balance ?></span>
				<i class="fa fa-rub"></i></div>
			<div class="f-descr">
				Пополняйте баланс заранее, чтобы в любой момент можно было воспользоваться любой из
				предоставленных функций. Пополнить баланс можно картой любого банка, а также через Qiwi,
				Яндекс.Деньги или со счета мобильного телефона.
			</div>
			<div class="f-buttons">
				<input id="checkout-amount" type="text" class="form-control" autocomplete="off" placeholder="сумма">
				<a href="#" class="btn btn-primary" id="checkout-btn">Пополнить</a>
			</div>
		</div>
	</div>

    <?php if ($profile->isMale()): ?>
		<div class="col-md-6">
			<div class="f-box">
				<div class="f-image"><img src="/images/fin/fin-diamond.png"/></div>
                <?php if ($profile->isPremium()): ?>
					<div class="f-title">Премиум:
						<span class="premium-days"><?= $profile->getPremiumDays() ?></span>
					</div>
					<div class="f-descr">Премиум позволяет использовать любые дополнительные функции сайта</div>
					<div class="f-buttons">
						<a href="#" class="btn btn-primary premium-btn">Продлить</a>
					</div>
                <?php else: ?>
					<div class="f-title">Премиум: нет</div>
					<div class="f-descr">Премиум позволяет использовать любые дополнительные функции сайта</div>
					<div class="f-buttons">
						<a href="#" class="btn btn-primary premium-btn">Приобрести</a>
					</div>
                <?php endif; ?>
			</div>
		</div>
    <?php endif; ?>

	<div class="col-md-6">
		<div class="f-box">
			<div class="f-image"><img src="/images/fin/fin-top.png"/></div>
			<div class="f-title">Рассылка</div>
			<div class="f-descr">
				Рассылка – это очень удобный способ сообщить о каком-то событии большому количеству людей и получить
				отклик, не прилагая усилий. Длительность действия рассылки от 4 до 8 часов.
				Пример рассылки: Милые девушки ищу спутницу для поездки в Лондон в эти выходные. Знание
				английского языка будет плюсом.
			</div>
			<div class="f-buttons">
				<a href="/broadcast" class="btn btn-primary">Разместить</a>
			</div>
		</div>
	</div>

	<div class="col-md-6">
		<div class="f-box">
			<div class="f-image"><img src="/images/fin/fin-top.png"/></div>
			<div class="f-title">Подняться в топ</div>
			<div class="f-descr">
				Ваша анкета будет размещаться выше обычных пользователей, но ниже анкет пользователей,
				которые воспользовались функцией «Закрепление в ТОП».
			</div>
			<div class="f-buttons">
				<a href="/top" class="btn btn-primary top-btn">Приобрести</a>
			</div>
		</div>
	</div>

	<div class="col-md-6">
		<div class="f-box">
			<div class="f-image"><img src="/images/fin/fin-calls.png"/></div>
			<div class="f-title">Осталось минут: <?= $profile->premium_minutes ?></div>
			<div class="f-descr">Вы можете звонить другим пользователям прямо с сайта</div>
			<div class="f-buttons">
				<a href="/calls" class="btn btn-primary">Подключить звонки</a>
			</div>
		</div>
	</div>

    <?php if ($profile->isMale()): ?>
		<div class="col-md-6">
			<div class="f-box">
				<div class="f-image"><img src="/images/fin/fin-other.png"/></div>
				<div class="f-title">Осталось контактов: <?= $profile->premium_contacts ?></div>
				<div class="f-descr">Вы можете начинать общаться с девушками</div>
				<div class="f-buttons">
					<a href="/contacts" class="btn btn-primary">Приобрести</a>
				</div>
			</div>
		</div>
    <?php endif; ?>

	<div class="col-md-6">
		<div class="f-box">
			<div class="f-image"><img src="/images/fin/fin-diamond.png"/></div>
			<div class="f-title">Функция «Кто онлайн»</div>
			<div class="f-descr">Вы можете узнать кто сейчас на сайте и готов к общению.<br/>
				Это позволит отсечь анкеты тех девушек, кто давно не заходил на сайт или находится в
				оффлайн режиме.
			</div>
			<div class="f-buttons">
				<a href="/online" class="btn btn-primary">Приобрести</a>
			</div>
		</div>
	</div>

	<div class="col-md-6">
		<div class="f-box">
			<div class="f-image"><img src="/images/fin/fin-diamond.png"/></div>
			<div class="f-title">Услуга «Инкогнито»</div>
			<div class="f-descr">Вы можете узнать кто сейчас на сайте и готов к общению.<br/>
				Это позволит отсечь анкеты тех <?= $profile->isMale() ? 'девушек' : 'мужчин' ?>,
				кто давно не заходил на сайт или находится в оффлайн режиме.
			</div>
			<div class="f-buttons">
				<a href="/incognito" class="btn btn-primary">Приобрести</a>
			</div>
		</div>
	</div>

</div>