<?php

use modules\profiles\common\models\Premium;
use modules\profiles\common\models\Profile;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var Profile $profile
 */

$title = 'Мой кошелек';
$this->title = $title;

\modules\profiles\frontend\assets\FinanceAsset::register($this);
?>
<h1 class="main center"><?= $title ?></h1>

<div class="row finance">
	<div class="col-md-8 col-md-offset-2">
		<div class="f-box">
			<div class="f-image"><img src="/images/fin/fin-rub.png"/></div>
			<div class="f-title">Баланс: <span class="my-balance"><?= $profile->purse->balance ?></span>
				<i class="fa fa-rub"></i></div>
			<div class="f-descr">
				Пополняйте баланс заранее, чтобы в любой момент можно было воспользоваться любой из
				предоставленных функций. Пополнить баланс можно картой любого банка, а также через Qiwi,
				Яндекс.Деньги или со счета мобильного телефона.
			</div>
			<div class="f-buttons">
				<input id="checkout-amount" type="text" class="form-control" autocomplete="off" placeholder="сумма">
				<a href="#" class="btn btn-primary" id="checkout-btn">Пополнить</a>
			</div>
		</div>
	</div>
</div>