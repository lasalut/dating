<?php

namespace modules\profiles\frontend\listeners;

use modules\profiles\frontend\widgets\ProfileDashboard;
use modules\profiles\frontend\widgets\ProfileTopBar;
use modules\profiles\frontend\widgets\ProfileTopMenu;
use ms\loyalty\contracts\widgets\WidgetsCollectionInterface;
use yii\base\Event;


/**
 * Class InterfaceWidgets
 */
class InterfaceWidgets
{
    public static function whenInitCollection(Event $event)
    {
        /** @var WidgetsCollectionInterface $dashboard */
        $dashboard = $event->sender;

        $dashboard->addWidget(WidgetsCollectionInterface::GROUP_DASHBOARD_PROFILE, ProfileDashboard::class);
        $dashboard->addWidget(WidgetsCollectionInterface::GROUP_TOP_BAR_ITEMS, ProfileTopBar::class);
        $dashboard->addWidget(WidgetsCollectionInterface::GROUP_MAIN_MENU_ITEMS, ProfileTopMenu::class);
    }
}