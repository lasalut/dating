<?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \modules\profiles\common\models\Profile $profile
 * @var \yii\mail\BaseMessage $message instance of newly created mail message
 */

$url = getenv('FRONTEND_WEB') . "/profiles/auth/password?hash={$profile->hash}&profile_id={$profile->id}";
?>

<p>Здравствуйте, <?= Html::encode($profile->name) ?>!</p>

<p>Для восстановления пароля воспользуйтесь следующей ссылкой:<br/>
	<a href="<?= $url ?>"><?= $url ?></a>
</p>

<p>Если вдруг Вы нашли это письмо в папке СПАМ - обязательно перенесите письмо во входящие,
	это необходимо чтобы получать и не терять нужные письма от нас.</p>

<p>Если же Вы не проходили регистрацию на <?= getenv('FRONTEND_WEB') ?> , то просто проигнорируйте это письмо.</p>

