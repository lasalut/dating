<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var string $pw
 * @var \modules\profiles\common\models\Profile $profile
 * @var \yii\mail\BaseMessage $message instance of newly created mail message
 */

$created = (new \DateTime($profile->created_at))->format('Y-m-d_H:i:s');
$url = getenv('FRONTEND_WEB') . "/profiles/auth/confirm-email?hash={$profile->hash}&created={$created}";
?>

<p>Здравствуйте, <?= Html::encode($profile->name) ?>!</p>

<p>Вы успешно зарегистрировались на сайте La-Salut.</p>

<p>Логин:<br/><?= $profile->email ?></p>

<p>Пароль:<br/><?= $pw ?></p>

<p>Для подтверждения Вашего аккаунта воспользуйтесь следующей ссылкой:<br/>
	<a href="<?= $url ?>"><?= $url ?></a>
</p>

<p>Если вдруг Вы нашли это письмо в папке СПАМ - обязательно перенесите письмо во входящие,
	это необходимо чтобы получать и не терять нужные письма от нас.</p>

<p>Если же Вы не проходили регистрацию на <?= getenv('FRONTEND_WEB') ?> , то просто проигнорируйте это письмо.</p>

