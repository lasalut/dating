<?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \modules\profiles\common\models\Profile $profile
 * @var \yii\mail\BaseMessage $message instance of newly created mail message
 */

$url = getenv('FRONTEND_WEB') . '/feedback';
?>

<p>Здравствуйте, <?= Html::encode($profile->name) ?>!</p>

<p>Ваша учетная запись была разблокирована.</p>

<p>Если Вы хотите связаться с администрацией портала, перейдите по ссылке:<br/>
	<a href="<?= $url ?>"><?= $url ?></a>
</p>

