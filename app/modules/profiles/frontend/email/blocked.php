<?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \modules\profiles\common\models\Profile $profile
 * @var \yii\mail\BaseMessage $message instance of newly created mail message
 */

$url = getenv('FRONTEND_WEB') . '/feedback';
?>

<p>Здравствуйте, <?= Html::encode($profile->name) ?>!</p>

<p>Администратором портала была заблокирована Ваша учетная запись.
	<br/>Дата блокировки: <?= (new \DateTime($profile->blocked_at))->format('d.m.Y H:i') ?>.
</p>

<?php if (!empty($profile->blocked_reason)): ?>
<p>Причина блокировки:<br/>
	<b><?= $profile->blocked_reason ?></b>
</p>
<?php endif; ?>

<p>Если Вы хотите связаться с администрацией портала, перейдите по ссылке:<br/>
	<a href="<?= $url ?>"><?= $url ?></a>
</p>

