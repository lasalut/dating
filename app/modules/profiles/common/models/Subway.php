<?php

namespace modules\profiles\common\models;

use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_subways".
 *
 * @property integer $id
 * @property string $title
 * @property string $city_id
 * @property string $color
 * @property string $line
 * @property integer $position
 *
 * @property City $city
 */
class Subway extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%subways}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Станция Метро';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Метро';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'string', 'max' => 255],
            ['city_id', 'string', 'max' => 255],
            ['color', 'string', 'max' => 255],
            ['line', 'string', 'max' => 255],
            ['position', 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Метро',
            'city_id' => 'Город',
            'color' => 'Цвет',
            'line' => 'Линия',
            'position' => 'Позиция',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public static function getOptions()
    {
        return self::find()->indexBy('id')->select('title, id')->orderBy(['title' => SORT_ASC])->column();
    }
}
