<?php

namespace modules\profiles\common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_profile_lists".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property integer $with_id
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 */
class ProfileList extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    const TYPE_FAVORITE = 'favorite';
    const TYPE_BLACKLIST = 'blacklist';

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profile_lists}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Profile List';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Profile Lists';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['profile_id', 'integer'],
            ['with_id', 'integer'],
            ['type', 'string', 'max' => 255],
            ['created_at', 'safe'],
            ['updated_at', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Profile ID',
            'with_id' => 'With ID',
            'type' => 'Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
