<?php

namespace modules\profiles\common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_premiums".
 *
 * @property integer $id
 * @property string $code
 * @property string $title
 * @property string $package
 * @property integer $hours
 * @property integer $days
 * @property integer $price
 * @property string $created_at
 * @property string $updated_at
 *
 * @property integer $contacts
 * @property integer $minutes
 * @property integer $broadcasts
 * @property integer $days_incognito
 * @property integer $days_online
 * @property integer $days_nudes
 */
class Premium extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    const CODE_STARTER = 'starter';
    const CODE_PREMIUM1 = 'premium-1';
    const CODE_PREMIUM2 = 'premium-2';
    const CODE_PREMIUM3 = 'premium-3';
    const CODE_PLUS1 = 'plus-1';
    const CODE_PLUS2 = 'plus-2';
    const CODE_PLUS3 = 'plus-3';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%premiums}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Премиум';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Премиум';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['code', 'string', 'max' => 255],
            ['title', 'string', 'max' => 255],
            ['package', 'string', 'max' => 255],
            ['hours', 'integer'],
            ['days', 'integer'],
            ['price', 'integer'],
            ['created_at', 'safe'],
            ['updated_at', 'safe'],
            ['title', 'required'],
            ['price', 'required'],
            ['days', 'required'],
            [['broadcasts', 'contacts', 'minutes', 'days_incognito', 'days_online', 'days_nudes'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Код',
            'title' => 'Название',
            'package' => 'Пакет',
            'days' => 'Дни премиум доступа',
            'hours' => 'Часов рассылки',
            'price' => 'Стоимость',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
            'broadcasts' => 'Рассылки',
            'contacts' => 'Контакты для общения',
            'minutes' => 'Минуты видео-звонка',
            'days_incognito' => 'Режим «Инкогнито», дней',
            'days_online' => 'Функция «Кто онлайн?», дней',
            'days_nudes' => 'Возможность просмотра 18+, дней',
        ];
    }

    public function hoursText()
    {
        if ($this->hours <= 4) {
            return $this->hours . ' часа';
        }

        return $this->hours . ' часов';
    }

    public static function broadcastHourOptions()
    {
        /** @var Premium[] $premiums */
        $premiums = Premium::find()->where(['like', 'code', 'broadcast-'])
            ->orderBy(['hours' => SORT_ASC])
            ->all();

        $options = [];

        /** @var Profile $profile */
        if ($profile = Yii::$app->user->identity) {
            if ($profile->premium_broadcasts > 0) {
                $options['0'] = 'Использовать рассылку. Осталось: ' . $profile->premium_broadcasts;
            }
        }

        if (!empty($premiums)) {
            foreach ($premiums as $premium) {
                $key = $premium->hours;
                $options[$key] = 'Купить на ' . $premium->hoursText() . ' за ' . $premium->price . ' руб.';
            }
        }

        return $options;
    }

    public static function callsMinutesOptions()
    {
        /** @var Premium[] $premiums */
        $premiums = Premium::find()->where(['like', 'code', 'calls-'])
            ->orderBy(['price' => SORT_ASC])
            ->all();

        $options = [];

        if (!empty($premiums)) {
            foreach ($premiums as $premium) {
                $key = $premium->id;
                $options[$key] = $premium->minutes . ' минут за ' . $premium->price . ' руб.';
            }
        }

        return $options;
    }

    public static function incognitoOptions()
    {
        /** @var Premium[] $premiums */
        $premiums = Premium::find()->where(['like', 'code', 'incognito-'])
            ->orderBy(['price' => SORT_ASC])
            ->all();

        $options = [];

        if (!empty($premiums)) {
            foreach ($premiums as $premium) {
                $key = $premium->id;

                $options[$key] = self::daysText($premium->days_incognito) . ' за ' . $premium->price . ' руб.';
            }
        }

        return $options;
    }

    public static function onlineOptions()
    {
        /** @var Premium[] $premiums */
        $premiums = Premium::find()->where(['like', 'code', 'online-'])
            ->orderBy(['price' => SORT_ASC])
            ->all();

        $options = [];

        if (!empty($premiums)) {
            foreach ($premiums as $premium) {
                $key = $premium->id;
                $options[$key] = self::daysText($premium->days_online) . ' за ' . $premium->price . ' руб.';
            }
        }

        return $options;
    }

    public static function topOptions()
    {
        /** @var Premium[] $premiums */
        $premiums = Premium::find()->where(['like', 'code', 'top-'])
            ->orderBy(['price' => SORT_ASC])
            ->all();

        $options = [];

        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;

        if (!empty($premiums)) {
            foreach ($premiums as $premium) {
                $key = $premium->id;

                if ($premium->code == 'top-0') {
                    if ($profile->premium_ups > 0) {
                        $options[$key] = 'Использовать поднятие. Осталось: ' . $profile->premium_ups;
                    }
                }
                else {
                    $options[$key] = 'Купить за ' . $premium->price . ' руб.';
                }
            }
        }

        return $options;
    }

    public static function contactsOptions()
    {
        /** @var Premium[] $premiums */
        $premiums = Premium::find()->where(['like', 'code', 'contacts-'])
            ->orderBy(['price' => SORT_ASC])
            ->all();

        $options = [];

        if (!empty($premiums)) {
            foreach ($premiums as $premium) {
                $key = $premium->id;
                $options[$key] = $premium->contacts . ' контактов за ' . $premium->price . ' руб.';
            }
        }

        return $options;
    }

    public static function stickedOptions()
    {
        /** @var Premium[] $premiums */
        $premiums = Premium::find()->where(['like', 'code', 'sticked-'])
            ->orderBy(['price' => SORT_ASC])
            ->all();

        $options = [];

        if (!empty($premiums)) {
            foreach ($premiums as $premium) {
                $key = $premium->id;
                $options[$key] = 'На ' . $premium->daysText($premium->days) . ' за ' . $premium->price . ' руб.';
            }
        }

        return $options;
    }

    public static function daysText($days)
    {
        $days = intval($days);

        if ($days % 10 == 1 && $days != 11) {
            return $days . ' день';
        }
        elseif ($days % 10 >= 2 && $days % 10 <= 5 && ($days < 10 || $days > 20)) {
            return $days . ' дня';
        }
        else {
            return $days . ' дней';
        }
    }
}
