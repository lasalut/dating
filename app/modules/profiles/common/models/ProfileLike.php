<?php

namespace modules\profiles\common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_profile_likes".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property integer $with_id
 * @property integer $photo_id
 * @property integer $comment_id
 * @property boolean $seen
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 */
class ProfileLike extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    const TYPE_AVATAR = 'avatar';
    const TYPE_PHOTO = 'photo';
    const TYPE_COMMENT = 'comment';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profile_likes}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Profile Like';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Profile Likes';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['profile_id', 'integer'],
            ['with_id', 'integer'],
            ['photo_id', 'integer'],
            ['comment_id', 'integer'],
            ['type', 'string', 'max' => 255],
            ['seen', 'safe'],
            ['created_at', 'safe'],
            ['updated_at', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Profile ID',
            'with_id' => 'With ID',
            'photo_id' => 'Photo ID',
            'comment_id' => 'Comment ID',
            'type' => 'Type',
            'seen' => 'Seen',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function fields()
    {
        /** @var Profile $userFrom */
        $userFrom = Profile::findOne($this->profile_id);
        /** @var Profile $userTo */
        $userTo = Profile::findOne($this->with_id);

        return [
            'id',
            'type',
            'seen',
            'photo_id',
            'comment_id',
            'userFromId' => function (ProfileLike $model) use ($userFrom, $userTo) {
                return $userFrom->id;
            },
            'userFromName' => function (ProfileLike $model) use ($userFrom, $userTo) {
                return $userFrom->name;
            },
            'userFromAvatar' => function (ProfileLike $model) use ($userFrom, $userTo) {
                return $userFrom->avatar_path;
            },
            'userToId' => function (ProfileLike $model) use ($userFrom, $userTo) {
                return $userTo->id;
            },
            'userToName' => function (ProfileLike $model) use ($userFrom, $userTo) {
                return $userTo->name;
            },
            'userToAvatar' => function (ProfileLike $model) use ($userFrom, $userTo) {
                return $userTo->avatar_path;
            },
            'created' => 'created_at',
        ];
    }

    public static function countByProfile(Profile $profile)
    {
        $counter = self::find()->where(['seen' => false, 'with_id' => $profile->id])->count();

        return intval($counter);
    }
}
