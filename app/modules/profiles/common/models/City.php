<?php

namespace modules\profiles\common\models;

use Yii;
use yii\db\Query;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_cities".
 *
 * @property integer $id
 * @property string $title
 * @property integer $region_id
 * @property integer $country_id
 *
 * @property Region $region
 * @property Country $country
 * @property Profile[] $profiles
 */
class City extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cities}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'City';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'string', 'max' => 255],
            ['region_id', 'integer'],
            ['country_id', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Город',
            'region_id' => 'Регион',
            'country_id' => 'Страна',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles()
    {
        return $this->hasMany(Profile::className(), ['city_id' => 'id']);
    }

    public static function getOptions()
    {
        $raw = (new Query)
            ->select(['c.id', 'city' => 'c.title', 'region' => 'r.title'])
            ->from(['c' => City::tableName()])
            ->leftJoin(['r' => Region::tableName()], 'r.id = c.region_id')
            ->orderBy(['c.title' => SORT_ASC])
            ->all();

        $options = [];

        foreach ($raw as $r) {
            $key = $r['id'];
            $options[$key] = $r['city'] . ' (' . $r['region'] . ')';
        }

        return $options;
    }
}
