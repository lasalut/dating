<?php

namespace modules\profiles\common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_broadcasts".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property integer $city_id
 * @property string $comment
 * @property string $excluded
 * @property integer $hours
 * @property string $expires_at
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Profile $profile
 */
class Broadcast extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%broadcasts}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Рассылка';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Рассылки';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['profile_id', 'integer'],
            ['city_id', 'integer'],
            ['comment', 'string'],
            ['excluded', 'string'],
            ['hours', 'integer'],
            ['expires_at', 'safe'],
            ['created_at', 'safe'],
            ['updated_at', 'safe'],

            ['profile_id', 'required'],
            ['comment', 'required'],
            ['hours', 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Участник ID',
            'city_id' => 'Город',
            'comment' => 'Комментарий',
            'excluded' => 'Исключены',
            'hours' => 'Часы',
            'expires_at' => 'Заканчивается',
            'created_at' => 'Создана',
            'updated_at' => 'Обновлена',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['id' => 'profile_id']);
    }
}
