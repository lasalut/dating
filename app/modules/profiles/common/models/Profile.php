<?php

namespace modules\profiles\common\models;

use libphonenumber\PhoneNumberFormat;
use marketingsolutions\phonenumbers\PhoneNumber;
use modules\chat\common\models\ChatMessage;
use modules\photo\common\models\Album;
use modules\photo\common\models\Photo;
use ms\loyalty\contracts\identities\IdentityRoleInterface;
use ms\loyalty\contracts\prizes\PrizeRecipientInterface;
use ms\loyalty\contracts\profiles\HasEmail;
use ms\loyalty\contracts\profiles\HasEmailInterface;
use ms\loyalty\contracts\profiles\HasPhoneMobile;
use ms\loyalty\contracts\profiles\HasPhoneMobileInterface;
use ms\loyalty\contracts\profiles\ProfileInterface;
use ms\loyalty\contracts\profiles\UserName;
use ms\loyalty\contracts\profiles\UserNameInterface;
use marketingsolutions\datetime\DateTimeBehavior;
use marketingsolutions\finance\models\Purse;
use marketingsolutions\finance\models\PurseInterface;
use marketingsolutions\finance\models\PurseOwnerInterface;
use marketingsolutions\finance\models\PurseOwnerTrait;
use marketingsolutions\phonenumbers\PhoneNumberBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\FileHelper;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_profiles".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone_mobile
 * @property string $email
 * @property string $passhash
 * @property string $birthday_on
 * @property string $created_at
 * @property string $updated_at
 * @property integer $city_id
 * @property integer $subway_id
 *
 * @property string $avatar
 * @property boolean $avatar_sent
 * @property boolean $avatar_approved
 * @property string $avatar_approved_at
 * @property integer $avatar_approved_by
 * @property boolean $avatar_banned
 * @property string $hash
 *
 * @property string $intension
 * @property string $looking_for
 * @property string $looking_years
 *
 * @property string $blocked_at
 * @property string $blocked_reason
 * @property boolean $blocked
 *
 * @property string $my_gender
 * @property integer $my_height
 * @property integer $my_weight
 * @property string $my_bust
 * @property string $my_hair
 * @property string $my_eyes
 * @property string $my_type
 * @property string $my_constitution
 *
 * @property string $my_hair_label
 * @property string $my_eyes_label
 * @property string $my_type_label
 * @property string $my_constitution_label
 *
 * @property string $sex_orientation
 * @property string $sex_freq
 * @property string $sex_role
 * @property string $sex_types
 *
 * @property string $sex_orientation_label
 * @property string $sex_freq_label
 * @property string $sex_role_label
 * @property string $sex_types_label
 *
 * @property string $social_education
 * @property string $social_finance
 * @property string $social_relationship
 * @property string $social_kids
 * @property string $social_interests
 * @property string $social_about
 *
 * @property string $social_education_label
 * @property string $social_finance_label
 * @property string $social_relationship_label
 * @property string $social_kids_label
 *
 * @property string $logged_at
 * @property string $online_at
 *
 * @property boolean $email_confirmed
 * @property boolean $email_subscribed
 * @property boolean $phone_confirmed
 * @property string $phone_code
 * @property boolean $is_mobile
 *
 * @property boolean $notify_sound
 * @property boolean $notify_email_messages
 * @property boolean $notify_email_visits
 * @property boolean $notify_webpush
 * @property string $webpush_endpoint
 * @property string $webpush_publicKey
 * @property string $webpush_authToken
 *
 * @property string $phone_mobile_local
 * @property string $birthday_on_local
 * @property string $years
 * @property string $age
 * @property array $my_intensions
 * @property array $my_looking_for
 * @property array $my_looking_years
 * @property array $my_sex_types
 * @property string $city_string
 *
 * @property string $phone_confirmed_at
 * @property string $email_confirmed_at
 * @property string $phone_sent_at
 * @property string $email_sent_at
 * @property boolean $removed
 * @property string $removed_comment
 *
 * @property string $premium_date
 * @property integer $premium_minutes
 * @property integer $premium_contacts
 * @property integer $premium_ups
 * @property integer $premium_broadcasts
 * @property string $premium_broadcast
 * @property string $premium_incognito
 * @property string $premium_online
 * @property string $premium_nudes
 * @property boolean $premium_incognito_on
 * @property integer $premium_priority
 * @property string $premium_sticked_to
 *
 * @property string $zodiacal_sign
 * @property City $city
 * @property Subway $subway
 * @property string $avatar_path
 * @property string $avatar_url
 * @property string $online
 */
class Profile extends \yz\db\ActiveRecord implements IdentityInterface, \ms\loyalty\contracts\identities\IdentityInterface, ModelInfoInterface,
                                                     HasEmailInterface, HasPhoneMobileInterface, UserNameInterface, IdentityRoleInterface,
                                                     PurseOwnerInterface, PrizeRecipientInterface, ProfileInterface
{
    use HasEmail, HasPhoneMobile, UserName, PurseOwnerTrait;

    public $avatar_local = null;
    public $city_local = null;
    public $intensions = null;
    public $looking_for_local = null;
    public $looking_years_local = null;
    public $sex_types_local = null;
    public $emailPhone = null;

    const SCENARIO_REGISTER = 'register';
    const SCENARIO_EDIT = 'frontend';

    const EVENT_AFTER_BLOCK = 'afterBlock';
    const EVENT_AFTER_UNBLOCK = 'afterUnblock';

    const GENDER_MALE = 'male';
    const GENDER_FEMALE = 'female';

    const I_SPONSOR = 'sponsor';
    const I_RELATIONSHIP = 'relationship';
    const I_EVENING = 'evening';
    const I_TRAVEL = 'travel';
    const I_MEETING = 'meeting';
    const I_SWINGER = 'swinger';

    const LOOKING_18_20 = '18-20';
    const LOOKING_21_25 = '21-25';
    const LOOKING_26_30 = '26-30';
    const LOOKING_31_35 = '31-35';
    const LOOKING_36_40 = '36-40';
    const LOOKING_41_50 = '41-50';
    const LOOKING_61_80 = '61-80';

    const EYES_GREEN = 'green';
    const EYES_BLUE = 'blue';
    const EYES_GRAY = 'gray';
    const EYES_BROWN = 'brown';

    const TYPE_EURO = 'euro';
    const TYPE_ASIAN = 'asian';
    const TYPE_KAVKAZ = 'kavkaz';
    const TYPE_INDIAN = 'indian';
    const TYPE_NIGER = 'niger';
    const TYPE_SPANISH = 'spanish';
    const TYPE_VOSTOK = 'vostok';
    const TYPE_AMERICAN = 'american';
    const TYPE_MIXED = 'mixed';
    const TYPE_OTHER = 'other';

    const C_SKIN = 'skin';
    const C_NORMAL = 'normal';
    const C_SPORTIVE = 'sportive';
    const C_MUSCULE = 'muscule';
    const C_DENSE = 'dense';
    const C_FAT = 'fat';

    const E_MIDDLE = 'middle';
    const E_MIDDLE_SPECIAL = 'middle_special';
    const E_HALF_BACHELOR = 'half_bachelor';
    const E_BACHELOR = 'bachelor';
    const E_MAGISTER = 'magister';
    const E_DOCTOR = 'doctor';

    const R_NONE = 'none';
    const R_NOT_SERIOUS = 'not_serious';
    const R_IN = 'in';
    const R_MARRIED = 'married';

    const KIDS_NONE = 'none';
    const KIDS_DESIRE = 'resire';
    const KIDS_HAVE_TOGETHER = 'have_together';
    const KIDS_HAVE_APART = 'have_apart';

    const BUST_1 = '1';
    const BUST_2 = '2';
    const BUST_3 = '3';
    const BUST_4 = '4';
    const BUST_5 = '5';

    const HAIR_BLOND = 'blond';
    const HAIR_LIGHT_BROWN = 'light brown';
    const HAIR_DARK_BROWN = 'light brown';
    const HAIR_RED = 'red';
    const HAIR_DARK = 'dark';
    const HAIR_GRAY = 'gray';

    const SEX_FREQ_EVERY_DAY = 'every_day';
    const SEX_FREQ_TWICE = 'twice';
    const SEX_REQ_ONE_A_WEEK = 'once_a_week';

    const SEX_ROLE_ACTIVE = 'active';
    const SEX_ROLE_PASSIVE = 'passive';

    const SEX_TYPE_ORAL = 'oral';
    const SEX_TYPE_ANAL = 'anal';
    const SEX_TYPE_CLASSIC = 'classic';
    const SEX_TYPE_GROUP = 'group';
    const SEX_TYPE_BDSM = 'bdsm';
    const SEX_TYPE_ELECTRO = 'electro';
    const SEX_TYPE_EXPEPIMENT = 'experiment';
    const SEX_TYPE_SWINGER = 'swinger';

    const SEX_ORIENT_HOMO = 'homo';
    const SEX_ORIENT_HETERO = 'hetero';
    const SEX_ORIENT_BOTH = 'both';

    const FINANCE_UNSTABLE = 'unstable';
    const FINANCE_LOW = 'low';
    const FINANCE_MIDDLE = 'middle';
    const FINANCE_HIGH = 'high';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profiles}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Профиль участника';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Профили участников';
    }

    /**
     * Returns purse's owner by owner's id
     *
     * @param int $id
     * @return $this
     */
    public static function findPurseOwnerById($id)
    {
        return static::findOne($id);
    }

    protected static function purseOwnerType()
    {
        return self::class;
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            'phonenumber' => [
                'class' => PhoneNumberBehavior::className(),
                'attributes' => [
                    'phone_mobile_local' => 'phone_mobile',
                ],
                'defaultRegion' => 'RU',
            ],
            'datetime' => [
                'class' => DateTimeBehavior::className(),
                'performValidation' => false,
                'attributes' => [
                    'birthday_on',
                ]
            ],
        ];
    }

    public function checkBirthday()
    {
        $day = intval(\Yii::$app->request->post('day', 0));
        $month = intval(\Yii::$app->request->post('month', 0));
        $year = intval(\Yii::$app->request->post('year', 0));

        if (empty($day) || empty($month) || empty($year)) {
            $this->addError('name', 'Укажите дату рождения');
        }
        else {
            $this->birthday_on = (new \DateTime("$day.$month.$year"))->format('Y-m-d');
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            ['name', 'string', 'max' => 30],
            ['email', 'string', 'max' => 64],
            ['email', 'unique'],
            ['phone_mobile', 'string', 'max' => 16],
            ['phone_mobile', 'unique'],
            ['passhash', 'string'],
            ['birthday_on', 'string'],
            ['birthday_on_local', 'date', 'format' => 'dd.mm.yyyy'],
            ['city_id', 'integer'],
            ['subway_id', 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            ['removed', 'safe'],
            ['removed_comment', 'string'],

            ['premium_date', 'safe'],
            ['premium_sticked_to', 'safe'],
            [['premium_minutes', 'premium_contacts', 'premium_ups', 'premium_broadcasts', 'premium_priority'], 'integer'],
            [['premium_nudes', 'premium_incognito', 'premium_online', 'premium_incognito_on', 'premium_broadcast'], 'safe'],

            ['avatar', 'string'],
            ['avatar_local', 'file', 'extensions' => ['png', 'jpg', 'jpeg', 'gif'], 'maxSize' => 1024 * 1024 * 15],
            ['avatar_sent', 'safe'],
            ['avatar_banned', 'safe'],
            ['avatar_approved', 'safe'],
            ['avatar_approved_by', 'integer'],
            ['avatar_approved_at', 'string'],
            ['hash', 'string'],

            ['blocked', 'safe'],
            ['blocked_at', 'string'],
            ['blocked_reason', 'string'],

            ['my_gender', 'string'],
            ['my_height', 'integer'],
            ['my_weight', 'integer'],
            ['my_bust', 'string'],
            ['my_hair', 'string'],
            ['my_eyes', 'string'],
            ['my_type', 'string'],
            ['my_constitution', 'string'],

            ['sex_orientation', 'string'],
            ['sex_freq', 'string'],
            ['sex_role', 'string'],
            ['sex_types', 'string'],

            ['social_education', 'string'],
            ['social_finance', 'string'],
            ['social_relationship', 'string'],
            ['social_kids', 'string'],
            ['social_interests', 'string'],
            ['social_about', 'string'],

            ['logged_at', 'string'],
            ['online_at', 'string'],

            ['email_confirmed', 'safe'],
            ['email_subscribed', 'safe'],
            ['phone_confirmed', 'safe'],
            ['phone_code', 'string', 'max' => 4],

            ['intension', 'string', 'max' => 255],
            ['looking_for', 'string', 'max' => 255],
            ['looking_years', 'string', 'max' => 255],
            ['is_mobile', 'safe'],
            ['emailPhone', 'safe'],

            [['phone_confirmed_at', 'email_confirmed_at', 'phone_sent_at', 'email_sent_at',], 'safe'],
            [['intensions', 'looking_for_local', 'looking_years_local', 'sex_types_local', 'city_local'], 'safe'],
            [['notify_sound', 'notify_email_messages', 'notify_email_visits', 'notify_webpush'], 'safe'],
            [['webpush_endpoint', 'webpush_publicKey', 'webpush_authToken'], 'safe'],

            ['phone_mobile_local', 'uniquePhone'],

            [['name', 'my_gender'], 'required', 'on' => self::SCENARIO_REGISTER],
            ['emailPhone', 'checkEmailPhone', 'on' => self::SCENARIO_REGISTER],
            ['name', 'checkBirthday', 'on' => self::SCENARIO_REGISTER],

            [['name'], 'required', 'on' => self::SCENARIO_EDIT],
            ['city_local', 'checkCity', 'on' => self::SCENARIO_EDIT],
        ];
    }

    public function checkEmailPhone()
    {
        $emailPhone = trim($this->emailPhone);

        if (empty($emailPhone)) {
            $this->addError('emailPhone', 'Необходимо указать E-mail или номер телефона');
        }

        if (strpos($emailPhone, '@') !== false) {
            if (filter_var($emailPhone, FILTER_VALIDATE_EMAIL)) {
                if (Profile::findOne(['email' => $emailPhone])) {
                    $this->addError('emailPhone', 'Участник с данным E-mail уже зарегистрирован');
                }
                else {
                    $this->email = $emailPhone;
                }
            }
            else {
                $this->addError('emailPhone', 'E-mail указан в неверном формате');
            }
        }
        else {
            if (PhoneNumber::validate($emailPhone, 'RU') == false) {
                $this->addError('emailPhone', 'Номер телефона указан в неверном формате');
            }
            else {
                $phone = PhoneNumber::format($emailPhone, PhoneNumberFormat::E164, 'RU');
                if (Profile::findOne(['phone_mobile' => $phone])) {
                    $this->addError('emailPhone', 'Участник с данным номером телефона уже зарегистрирован');
                }
                else {
                    $this->phone_mobile = $phone;
                }
            }
        }
    }

    public function uniquePhone()
    {
        if (!empty($this->phone_mobile_local)) {
            $phone = PhoneNumber::format($this->phone_mobile_local, PhoneNumberFormat::E164, 'RU');
            if ($profile = Profile::findOne(['phone_mobile' => $phone])) {
                if (!empty($this->id) && $this->id != $profile->id) {
                    $this->addError('phone_mobile_local', 'Номер телефона уже занят');
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'phone_mobile' => 'Номер телефона',
            'phone_mobile_local' => 'Номер телефона',
            'email' => 'Эл. почта',
            'passhash' => 'Пароль',
            'birthday_on' => 'Дата рождения',
            'birthday_on_local' => 'Дата рождения',
            'city_id' => 'Город',
            'city_local' => 'Город',
            'subway_id' => 'Метро',
            'created_at' => 'Зарегистрирован',
            'updated_at' => 'Дата изменения',
            'removed' => 'Анкета удалена участником',
            'removed_comment' => 'Комментарий удаления',

            'premium_date' => 'Премиум до',
            'premium_minutes' => 'Премиум минуты',
            'premium_contacts' => 'Премиум контакты',
            'premium_ups' => 'Премиум поднятия',
            'premium_broadcasts' => 'Премиум рассылки',
            'premium_broadcast' => 'Премиум рассылка до',
            'premium_nudes' => 'Премиум фото 18+ до',
            'premium_online' => 'Премиум "Кто онлайн" до',
            'premium_incognito' => 'Премиум "Инкогнито" до',
            'premium_incognito_on' => 'Режим "Инкогнито" включен',
            'premium_priority' => 'Приоритет анкеты, поднятие в топ',
            'premium_sticked_to' => 'Премиум закреплен в топ',

            'avatar' => 'Аватар',
            'avatar_local' => 'Аватар',
            'avatar_sent' => 'Уведомление аватара',
            'avatar_banned' => 'Аватар отклонен',
            'avatar_approved' => 'Аватар подтвержден',
            'avatar_approved_by' => 'Аватар подтвержден админом',
            'avatar_approved_at' => 'Аватар подтвержден, дата',
            'hash' => 'Хэш',

            'intension' => 'Цели знакомства',
            'looking_for' => 'Я ищу',
            'looking_from' => 'Начиная с возраста',
            'looking_to' => 'До возраста',

            'blocked_at' => 'Дата блокировки',
            'blocked_reason' => 'Причина блокировки',
            'blocked' => 'Заблокирован',

            'my_gender' => 'Пол',
            'my_height' => 'Рост',
            'my_weight' => 'Вес',
            'my_bust' => 'Бюст',
            'my_hair' => 'Цвет волос',
            'my_eyes' => 'Цвет глаз',
            'my_type' => 'Внешность',
            'my_constitution' => 'Телосложение',

            'sex_orientation' => 'Ориентация',
            'sex_freq' => 'Частота',
            'sex_role' => 'Роль',
            'sex_types' => 'Предпочтения',

            'social_education' => 'Образование',
            'social_finance' => 'Материальное положение',
            'social_relationship' => 'Отношения',
            'social_kids' => 'Дети',
            'social_interests' => 'Интересы',
            'social_about' => 'О себе',

            'logged_at' => 'Время входа',
            'online_at' => 'Время онлайна',
            'is_mobile' => 'С мобильного устройства',

            'email_confirmed' => 'Почта подтверждена',
            'email_subscribed' => 'Подписан на уведомления по почте',
            'phone_confirmed' => 'Телефон подтвержден',
            'phone_code' => 'Код',

            'notify_sound' => 'Проигрывать звук при поступлении новых сообщений',
            'notify_email_messages' => 'Уведомления на почту: личные сообщения',
            'notify_email_visits' => 'Уведомления на почту: просмотры анкеты',
            'notify_webpush' => 'Браузерные оповещения',
        ];
    }

    public function beforeDelete()
    {
        if ($purse = $this->purse) {
            $purse->delete();
        }
        if ($avatar_path = $this->avatar_path) {
            $path = Yii::getAlias("@data/photos/" . $avatar_path);
            @unlink($path);
        }
        Photo::deleteAll(['profile_id' => $this->id]);
        Album::deleteAll(['profile_id' => $this->id]);
        ProfileVisit::deleteAll(['profile_id' => $this->id]);
        ProfileList::deleteAll(['profile_id' => $this->id]);
        ProfileLike::deleteAll(['profile_id' => $this->id]);
        PremiumHistory::deleteAll(['profile_id' => $this->id]);

        return parent::beforeDelete();
    }

    public function lookingForMale()
    {
        return $this->isMale() ? false : true;
    }

    public function beforeSave($insert)
    {
        if (!empty($this->intensions)) {
            $this->intension = implode(';', $this->intensions);
        }
        elseif ($this->scenario == self::SCENARIO_EDIT) {
            $this->intension = null;
        }

        if (!empty($this->looking_for_local)) {
            $this->looking_for = implode(';', $this->looking_for_local);
        }
        elseif ($this->scenario == self::SCENARIO_EDIT) {
            $this->looking_for = null;
        }

        if (!empty($this->looking_years_local)) {
            $this->looking_years = implode(';', $this->looking_years_local);
        }
        elseif ($this->scenario == self::SCENARIO_EDIT) {
            $this->looking_years = null;
        }

        if (!empty($this->sex_types_local)) {
            $this->sex_types = implode(';', $this->sex_types_local);
        }
        elseif ($this->scenario == self::SCENARIO_EDIT) {
            $this->sex_types = null;
        }

        if (empty($this->city_local) && $this->scenario == self::SCENARIO_EDIT) {
            $this->city_id = null;
        }

        if ($this->subway_id == "") {
            $this->subway_id = null;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return string
     */
    public function getIdentityRole()
    {
        return 'profile';
    }

    /**
     * Returns id of the recipient
     *
     * @return integer
     */
    public function getRecipientId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Returns purse for the recipient, that should contain enough money
     *
     * @return PurseInterface
     */
    public function getRecipientPurse()
    {
        return $this->purse;
    }

    protected function addPremium()
    {
        $premium = Premium::findOne(['code' => 'starter']);
        $premiumDate = new \DateTime("+ {$premium->days} days");
        $premiumDateStr = $premiumDate->format('Y-m-d H:i:s');

        $h = new PremiumHistory();
        $h->profile_id = $this->id;
        $h->premium_id = $premium->id;
        $h->price = $premium->price;
        $h->days = $premium->days;
        $h->expires_at = $premiumDateStr;
        $h->save(false);

        $this->premium_date = $premiumDateStr;
        $this->premium_contacts = 15;
        $this->premium_minutes = 10;
        $this->save(false);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $this->createPurse();
            $this->setHash();
            $this->addPremium();
        }

        if (!$insert && $this->isAttributeChanged('name')) {
            $this->updatePurse();
        }

        $this->upload(['avatar']);

        parent::afterSave($insert, $changedAttributes);
    }

    protected function upload(array $fields)
    {
        foreach ($fields as $field) {
            $file = UploadedFile::getInstance($this, $field . '_local');

            if ($file instanceof UploadedFile && $file->hasError == false) {
                $uniqid = uniqid();
                $name = "profile_{$this->id}_avatar_{$uniqid}.{$file->extension}";
                $dir = Yii::getAlias("@data/photos");
                FileHelper::createDirectory($dir);
                $path = $dir . DIRECTORY_SEPARATOR . $name;
                $file->saveAs($path);

                $cropName = "profile_{$this->id}_avatar_crop_{$uniqid}.{$file->extension}";
                $cropPath = $dir . DIRECTORY_SEPARATOR . $cropName;
                $im = new \Imagick($path);
                $im->cropThumbnailImage(370, 370);
                $im->writeImage($cropPath);

                if (!empty($this->$field)) {
                    $oldFile = Yii::getAlias("@data/photos/{$this->$field}");
                    @unlink($oldFile);
                }
                @unlink($path);

                $this->$field = $cropName;
                $this->updateAttributes([$field]);

                if (getenv('YII_ENV') == 'dev') {
                    $frontendDir = Yii::getAlias("@frontendWebroot/data/photos");
                    FileHelper::copyDirectory($dir, $frontendDir);
                }

                if ($field == 'avatar') {
                    $this->updateAttributes(['avatar_sent' => false]);
                }
            }
        }
    }

    protected function createPurse()
    {
        Purse::create(self::class, $this->id, strtr('Счет пользователя #{id} ({name})', [
            '{id}' => $this->id,
            '{name}' => $this->name,
        ]));
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     *
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     *
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     *
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return null;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     *
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return false;
    }

    protected function updatePurse()
    {
        $this->purse->updateAttributes([
            'title' => strtr('Счет пользователя #{id} ({name})', [
                '{id}' => $this->id,
                '{name}' => $this->name,
            ]),
        ]);
    }

    public function afterDelete()
    {
        Purse::remove(self::class, $this->id);

        parent::afterDelete();
    }

    /**
     * @return integer
     */
    public function getProfileId()
    {
        return $this->getPrimaryKey();
    }

    public function getProfile()
    {
        return $this;
    }

    /**
     * Block profile from be able to login
     *
     * @param string $reason
     */
    public function block($reason = null)
    {
        $this->updateAttributes([
            'blocked' => true,
            'blocked_at' => (new \DateTime('now'))->format('Y-m-d H:i:s'),
            'blocked_reason' => $reason,
        ]);

        try {
            Yii::$app->mailer->compose('@modules/profiles/frontend/email/blocked', [
                'profile' => $this,
            ])->setSubject('Ваша учетная запись заблокирована')->setTo($this->email)->send();
        }
        catch (\Exception $e) {
            throw $e;
        }

        $this->trigger(self::EVENT_AFTER_BLOCK);
    }

    /**
     * Unblock profile from be able to login
     */
    public function unblock()
    {
        $this->updateAttributes([
            'blocked' => false,
            'blocked_at' => null,
            'blocked_reason' => null,
        ]);

        try {
            Yii::$app->mailer->compose('@modules/profiles/frontend/email/unblocked', [
                'profile' => $this,
            ])->setSubject('Ваша учетная запись заблокирована')->setTo($this->email)->send();
        }
        catch (\Exception $e) {
            throw $e;
        }

        $this->trigger(self::EVENT_AFTER_UNBLOCK);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubway()
    {
        return $this->hasOne(Subway::className(), ['id' => 'subway_id']);
    }

    public function isMale()
    {
        return $this->my_gender == self::GENDER_MALE;
    }

    public function isFemale()
    {
        return $this->my_gender == self::GENDER_FEMALE;
    }

    public function getAvatar_path()
    {
        return empty($this->avatar)
            ? ($this->isMale() ? '/images/icon_male.jpg' : '/images/icon_female.jpg')
            : '/data/photos/' . $this->avatar;
    }

    public function getAvatar_url()
    {
        return getenv('FRONTEND_WEB') . $this->avatar_path;
    }

    public static function dayOptions()
    {
        $options = [];
        for ($i = 1; $i <= 31; $i++) {
            $options[$i] = $i;
        }
        return $options;
    }

    public static function yearOptions()
    {
        $options = [];
        for ($i = 2000; $i >= 1939; $i--) {
            $options[$i] = $i;
        }
        return $options;
    }

    public static function monthOptions()
    {
        $options = [
            '1' => 'января',
            '2' => 'февраля',
            '3' => 'марта',
            '4' => 'апреля',
            '5' => 'мая',
            '6' => 'июня',
            '7' => 'июля',
            '8' => 'авгуса',
            '9' => 'сентября',
            '10' => 'октября',
            '11' => 'ноября',
            '12' => 'декабря',
        ];

        return $options;
    }

    public function updateLoggedAt($detectMobile = false)
    {
        $now = (new \DateTime())->format('Y-m-d H:i:s');

        if ($detectMobile) {
            $detect = new \Mobile_Detect;
            $this->updateAttributes(['logged_at' => $now, 'is_mobile' => $detect->isMobile()]);
        }
        else {
            $this->updateAttributes(['logged_at' => $now]);
        }
    }

    public function updateOnlineAt($detectMobile = false)
    {
        $now = (new \DateTime())->format('Y-m-d H:i:s');

        if ($detectMobile) {
            $detect = new \Mobile_Detect;
            $this->updateAttributes(['online_at' => $now, 'is_mobile' => $detect->isMobile()]);
        }
        else {
            $this->updateAttributes(['online_at' => $now]);
        }
    }

    public function updateEmailConfirmedAt()
    {
        $now = (new \DateTime())->format('Y-m-d H:i:s');
        $this->updateAttributes(['email_confirmed_at' => $now]);
    }

    public function updatePhoneConfirmedAt()
    {
        $now = (new \DateTime())->format('Y-m-d H:i:s');
        $this->updateAttributes(['phone_confirmed_at' => $now]);
    }

    public function updateEmailSentAt()
    {
        $now = (new \DateTime())->format('Y-m-d H:i:s');
        $this->updateAttributes(['email_sent_at' => $now]);
    }

    public function updatePhoneSentAt()
    {
        $now = (new \DateTime())->format('Y-m-d H:i:s');
        $this->updateAttributes(['phone_sent_at' => $now]);
    }

    public function setHash()
    {
        $hash = md5($this->id . '_hash');
        $this->updateAttributes(['hash' => $hash]);
    }

    public function updatePhoneCode()
    {
        $code = self::generateRandomNumber(4);
        $this->updateAttributes(['phone_code' => $code]);
    }

    public function setRandomPassword()
    {
        $pw = self::generateRandomPassword(6);
        $this->updateAttributes(['passhash' => Yii::$app->security->generatePasswordHash($pw)]);
        return $pw;
    }

    public static function generateRandomNumber($length, $prefix = '')
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $prefix . $randomString;
    }

    public static function generateRandomPassword($length, $prefix = '')
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $prefix . $randomString;
    }

    public function getFull_name()
    {
        return $this->name;
    }

    public function getYears()
    {
        $now = new \DateTime('now');
        $birthday = new \DateTime($this->birthday_on);
        $diff = $birthday->diff($now);

        $years = intval($diff->y);

        if ($years % 10 == 1) {
            return $years . ' год';
        }
        elseif ($years % 10 == 2 || $years % 10 == 3 || $years % 10 == 4) {
            return $years . ' года';
        }
        else {
            return $years . ' лет';
        }
    }

    public function getAge()
    {
        $now = new \DateTime('now');
        $birthday = new \DateTime($this->birthday_on);
        $diff = $birthday->diff($now);

        return intval($diff->y);
    }

    public function checkCity()
    {
        if (!empty($this->city_local)) {
            $titles = explode(',', $this->city_local);
            $city = trim($titles[0]);
            $region = null;
            $country = null;

            if (isset($titles[2])) {
                $region = trim($titles[1]);
                $country = trim($titles[2]);
            }

            $query = (new Query())
                ->select('c.id')
                ->from(['c' => City::tableName()])
                ->leftJoin(['r' => Region::tableName()], 'r.id = c.region_id')
                ->leftJoin(['co' => Country::tableName()], 'co.id = c.country_id')
                ->where("c.title LIKE :cityName", [':cityName' => $city . '%'])
                ->limit(1);

            if ($country) {
                $query->andWhere("co.title LIKE :countryName", [':countryName' => $country . '%']);
            }

            $result = $query->one();

            if (empty($result)) {
                $this->addError('city_local', 'Город не найден');
            }
            else {
                $this->city_id = $result['id'];
            }
        }
    }

    public static function getIntensionOptions()
    {
        return [
            self::I_SPONSOR => 'Ищу спонсора',
            self::I_RELATIONSHIP => 'Серьезные отношения',
            self::I_EVENING => 'Провести вечер',
            self::I_TRAVEL => 'Путешествие',
            self::I_MEETING => 'Периодические встречи',
            self::I_SWINGER => 'Свингерство',
        ];
    }

    public function getMy_intensions()
    {
        return empty($this->intension) ? [] : explode(';', $this->intension);
    }

    public function getMy_looking_for()
    {
        return empty($this->looking_for) ? [] : explode(';', $this->looking_for);
    }

    public function getMy_looking_years()
    {
        return empty($this->looking_years) ? [] : explode(';', $this->looking_years);
    }

    public function getMy_sex_types()
    {
        return empty($this->sex_types) ? [] : explode(';', $this->sex_types);
    }

    public function getCity_string()
    {
        $city = City::findOne($this->city_id);

        if (!$city) {
            return '';
        }

        $string = $city->title;

        if ($region = $city->region) {
            $string .= ', ' . $region->title;
        }

        if ($country = $city->country) {
            $string .= ', ' . $country->title;
        }

        return $string;
    }

    public static function getLookingYearsOptions()
    {
        return [
            self::LOOKING_18_20 => self::LOOKING_18_20,
            self::LOOKING_21_25 => self::LOOKING_21_25,
            self::LOOKING_26_30 => self::LOOKING_26_30,
            self::LOOKING_31_35 => self::LOOKING_31_35,
            self::LOOKING_36_40 => self::LOOKING_36_40,
            self::LOOKING_41_50 => self::LOOKING_41_50,
            self::LOOKING_61_80 => self::LOOKING_61_80,
        ];
    }

    public static function getLookingForOptions()
    {
        return [
            self::GENDER_FEMALE => 'девушку',
            self::GENDER_MALE => 'мужчину',
        ];
    }

    public static function getHeightOptions()
    {
        $options = [];
        for ($i = 150; $i <= 220; $i++) {
            $options[$i] = $i . ' см';
        }
        return $options;
    }

    public static function getWeightOptions()
    {
        $options = [];
        for ($i = 40; $i <= 160; $i++) {
            $options[$i] = $i . ' кг';
        }
        return $options;
    }

    public static function getEyesOptions()
    {
        return [
            self::EYES_BROWN => 'Карие',
            self::EYES_BLUE => 'Голубые',
            self::EYES_GREEN => 'Зеленые',
            self::EYES_GRAY => 'Cерые',
            null => 'нет ответа',
        ];
    }

    public static function getMyTypeOptions()
    {
        return [
            self::TYPE_EURO => 'Европейская',
            self::TYPE_ASIAN => 'Азиатская',
            self::TYPE_KAVKAZ => 'Кавказская',
            self::TYPE_INDIAN => 'Индийская',
            self::TYPE_NIGER => 'Темнокожий',
            self::TYPE_SPANISH => 'Испанская',
            self::TYPE_VOSTOK => 'Ближневосточная',
            self::TYPE_AMERICAN => 'Коренной американец',
            self::TYPE_MIXED => 'Смешанная',
            self::TYPE_OTHER => 'Другая',
            null => 'нет ответа',
        ];
    }

    public static function getMyConstitutionOptions()
    {
        return [
            self::C_SKIN => 'Худощавое',
            self::C_NORMAL => 'Обычное',
            self::C_SPORTIVE => 'Спортивное',
            self::C_MUSCULE => 'Мускулистое',
            self::C_DENSE => 'Плотное',
            self::C_FAT => 'Полное',
            null => 'нет ответа',
        ];
    }

    public static function getFinanceOptions()
    {
        return [
            self::FINANCE_UNSTABLE => 'Непостоянные заработки',
            self::FINANCE_LOW => 'Постоянный небольшой доход',
            self::FINANCE_MIDDLE => 'Стабильный средний доход',
            self::FINANCE_HIGH => 'Хорошо зарабатываю/обеспечен',
            null => 'нет ответа',
        ];
    }

    public static function getEducationOptions()
    {
        return [
            self::E_MIDDLE => 'Среднее',
            self::E_MIDDLE_SPECIAL => 'Среднее специальное',
            self::E_HALF_BACHELOR => 'Неполное высшее',
            self::E_BACHELOR => 'Высшее',
            self::E_MAGISTER => 'Два или более высших',
            self::E_DOCTOR => 'Ученая степень',
            null => 'нет ответа',
        ];
    }

    public static function getRelationshipOptions()
    {
        return [
            self::R_NONE => 'Нет',
            self::R_NOT_SERIOUS => 'Ничего серьезного',
            self::R_IN => 'Есть отношения',
            self::R_MARRIED => 'В браке',
            null => 'нет ответа',
        ];
    }

    public static function getKidsOptions()
    {
        return [
            self::KIDS_NONE => 'Нет',
            self::KIDS_DESIRE => 'Нет, но хотелось бы',
            self::KIDS_HAVE_TOGETHER => 'Есть, живём вместе',
            self::KIDS_HAVE_APART => 'Есть, живём порознь',
            null => 'нет ответа',
        ];
    }

    public static function getBustOptions()
    {
        return [
            self::BUST_1 => '1 размер',
            self::BUST_2 => '2 размер',
            self::BUST_3 => '3 размер',
            self::BUST_4 => '4 размер',
            self::BUST_5 => '5 размер',
            null => 'нет ответа',
        ];
    }

    public static function getHairOptions()
    {
        return [
            self::HAIR_BLOND => 'Блонд',
            self::HAIR_LIGHT_BROWN => 'Русый',
            self::HAIR_DARK_BROWN => 'Шатен',
            self::HAIR_RED => 'Рыжий',
            self::HAIR_DARK => 'Брюнет',
            self::HAIR_GRAY => 'Серый',
            null => 'нет ответа',
        ];
    }

    public static function getSexFreqOptions()
    {
        return [
            self::SEX_FREQ_EVERY_DAY => 'Каждый день',
            self::SEX_FREQ_TWICE => 'Несколько раз в неделю',
            self::SEX_REQ_ONE_A_WEEK => 'Раз в неделю и реже',
            null => 'нет ответа',
        ];
    }

    public static function getSexRoleOptions()
    {
        return [
            self::SEX_ROLE_ACTIVE => 'Актив',
            self::SEX_ROLE_PASSIVE => 'Пассив',
            null => 'нет ответа',
        ];
    }

    public static function getSexTypesOptions()
    {
        return [
            self::SEX_TYPE_ORAL => 'Оральный',
            self::SEX_TYPE_ANAL => 'Анальный',
            self::SEX_TYPE_CLASSIC => 'Классический',
            self::SEX_TYPE_GROUP => 'Групповой',
            self::SEX_TYPE_ELECTRO => 'Электросекс',
            self::SEX_TYPE_EXPEPIMENT => 'Эксперименты',
            self::SEX_TYPE_BDSM => 'БДСМ',
            self::SEX_TYPE_SWINGER => 'Свингерство',
        ];
    }

    public static function getSexOrientOptions()
    {
        return [
            self::SEX_ORIENT_HOMO => 'homo',
            self::SEX_ORIENT_HETERO => 'hetero',
            self::SEX_ORIENT_BOTH => 'both',
        ];
    }

    public function minMaxYears()
    {
        $min = $max = 0;

        foreach ($this->my_looking_years as $group) {
            $years = explode('-', $group);
            if (!empty($years)) {
                foreach ($years as $year) {
                    if ($min === 0) {
                        $min = $year;
                        $max = $year;
                    }

                    if ($year < $min) {
                        $min = $year;
                    }
                    if ($year > $max) {
                        $max = $year;
                    }
                }
            }
        }

        return $min . ' - ' . $max;
    }

    public function getZodiacal_sign()
    {
        $bd = new \DateTime($this->birthday_on);
        $month = $bd->format('m');
        $day = $bd->format('d');

        $signs = array("Козерог", "Водолей", "Рыбы", "Овен", "Телец", "Близнецы", "Рак", "Лев", "Девы", "Весы", "Скорпион", "Стрелец");
        $signsstart = array(1 => 21, 2 => 20, 3 => 20, 4 => 20, 5 => 20, 6 => 20, 7 => 21, 8 => 22, 9 => 23, 10 => 23, 11 => 23, 12 => 23);
        return $day < $signsstart[$month + 1] ? $signs[$month - 1] : $signs[$month % 12];
    }

    public function getMy_hair_label()
    {
        $options = self::getHairOptions();

        if (empty($this->my_hair) || !isset($options[$this->my_hair])) {
            return null;
        }

        return $options[$this->my_hair];
    }

    public function getMy_eyes_label()
    {
        $options = self::getEyesOptions();

        if (empty($this->my_eyes) || !isset($options[$this->my_eyes])) {
            return null;
        }

        return $options[$this->my_eyes];
    }

    public function getMy_type_label()
    {
        $options = self::getMyTypeOptions();

        if (empty($this->my_type) || !isset($options[$this->my_type])) {
            return null;
        }

        return $options[$this->my_type];
    }

    public function getMy_constitution_label()
    {
        $options = self::getMyConstitutionOptions();

        if (empty($this->my_constitution) || !isset($options[$this->my_constitution])) {
            return null;
        }

        return $options[$this->my_constitution];
    }

    public function getSocial_education_label()
    {
        $options = self::getEducationOptions();

        if (empty($this->social_education) || !isset($options[$this->social_education])) {
            return null;
        }

        return $options[$this->social_education];
    }

    public function getSocial_relationship_label()
    {
        $options = self::getRelationshipOptions();

        if (empty($this->social_relationship) || !isset($options[$this->social_relationship])) {
            return null;
        }

        return $options[$this->social_relationship];
    }

    public function getSocial_kids_label()
    {
        $options = self::getKidsOptions();

        if (empty($this->social_kids) || !isset($options[$this->social_kids])) {
            return null;
        }

        return $options[$this->social_kids];
    }

    public function getSocial_finance_label()
    {
        $options = self::getFinanceOptions();

        if (empty($this->social_finance) || !isset($options[$this->social_finance])) {
            return null;
        }

        return $options[$this->social_finance];
    }

    public function getSex_orientation_label()
    {
        $options = self::getSexOrientOptions();

        if (empty($this->sex_orientation) || !isset($options[$this->sex_orientation])) {
            return null;
        }

        return $options[$this->sex_orientation];
    }

    public function getSex_freq_label()
    {
        $options = self::getSexFreqOptions();

        if (empty($this->sex_freq) || !isset($options[$this->sex_freq])) {
            return null;
        }

        return $options[$this->sex_freq];
    }

    public function getSex_role_label()
    {
        $options = self::getSexRoleOptions();

        if (empty($this->sex_role) || !isset($options[$this->sex_role])) {
            return null;
        }

        return $options[$this->sex_role];
    }

    public function getSex_types_label()
    {
        $options = self::getSexTypesOptions();

        if (empty($this->sex_types)) {
            return null;
        }
        $types = explode(';', $this->sex_types);
        $labels = [];
        foreach ($types as $type) {
            if (isset($options[$type])) {
                $labels[] = $options[$type];
            }
        }

        return implode(', ', $labels);
    }

    private function checkPremiumDate($attr)
    {
        if (empty($this->$attr)) {
            return false;
        }

        $now = new \DateTime('now');
        $premium = new \DateTime($this->$attr);

        return $premium >= $now;
    }

    /**
     * @return bool
     */
    public function isPremium()
    {
        return $this->checkPremiumDate('premium_date');
    }

    /**
     * @return bool
     */
    public function hasPremiumBroadcast()
    {
        return $this->checkPremiumDate('premium_broadcast');
    }

    /**
     * @return bool
     */
    public function hasPremiumOnline()
    {
        return $this->checkPremiumDate('premium_online');
    }

    /**
     * @return bool
     */
    public function hasPremiumNudes()
    {
        return $this->checkPremiumDate('premium_nudes');
    }

    /**
     * @return bool
     */
    public function hasPremiumIncognito()
    {
        return $this->checkPremiumDate('premium_incognito');
    }

    /**
     * @return bool
     */
    public function hasPremiumSticked()
    {
        return $this->checkPremiumDate('premium_sticked_to');
    }

    public function isIncognito()
    {
        return $this->premium_incognito_on && $this->hasPremiumIncognito();
    }

    public function getPremium_minutes()
    {
        return empty($this->premium_minutes) ? 0 : $this->premium_minutes;
    }

    public function getPremiumDays()
    {
        if (empty($this->premium_date)) {
            return 0;
        }

        $now = new \DateTime('now');
        $premium = new \DateTime($this->premium_date);
        $diff = $now->diff($premium);
        $diffDays = $diff->days;

        if ($diffDays == 0) {
            if ($diff->h == 0) {
                return $diff->m . ' м.';
            }
            else {
                return $diff->h . ' ч.';
            }
        }

        if ($diffDays == 1 || ($diffDays != 11 && $diffDays % 10 == 1)) {
            return $diffDays . ' день';
        }
        elseif (($diffDays > 4 && $diffDays < 21) || ($diffDays % 10 > 4 && $diff % 10 < 10)) {
            return $diffDays . ' дней';
        }
        else {
            return $diffDays . ' дня';
        }
    }

    /**
     * @return bool
     */
    public function isActivated()
    {
        if ($this->removed) {
            return false;
        }
        if ($this->isPremium()) {
            return true;
        }

        return $this->email_confirmed && $this->phone_confirmed && $this->avatar_approved;
    }

    public static function activatedSql($alias = null)
    {
        $now = (new \DateTime('now'))->format('Y-m-d H:i:s');

        return empty($alias)
            ? "(removed=0 AND (premium_date >= '$now' OR (email_confirmed=1 AND phone_confirmed=1 AND avatar_approved=1)))"
            : "($alias.removed=0 AND ($alias.premium_date >= '$now' OR ($alias.email_confirmed=1 AND $alias.phone_confirmed=1 AND $alias.avatar_approved=1)))";
    }

    public function refreshHash()
    {
        $this->updateAttributes(['hash' => uniqid()]);
    }

    public static function getOnlineIds()
    {
        $online = new \DateTime('-5 minutes');
        $online = $online->format('Y-m-d H:i:s');

        $ids = Profile::find()->select('id')
            ->where('online_at IS NOT NULL')
            ->andWhere('online_at > :online', compact('online'))
            ->column();

        return $ids;
    }

    public function hasFavorite($with_id)
    {
        $cond = ['profile_id' => $this->id, 'with_id' => $with_id, 'type' => ProfileList::TYPE_FAVORITE];

        return ProfileList::findOne($cond) ? true : false;
    }

    public function hasBlacklist($with_id)
    {
        $cond = ['profile_id' => $this->id, 'with_id' => $with_id, 'type' => ProfileList::TYPE_BLACKLIST];

        return ProfileList::findOne($cond) ? true : false;
    }

    public function getOnline()
    {
        if (empty($this->online_at)) {
            return null;
        }

        $online = new \DateTime($this->online_at);
        $now = new \DateTime();

        if ($online > $now) {
            return null; # даты онлайн из будущего быть не может
        }

        $interval = $online->diff($now, true);
        $str = $this->my_gender == self::GENDER_FEMALE ? 'была ' : 'был ';

        if (empty($interval->d) && empty($interval->h) && $interval->i < 5) {
            return 'online';
        }
        elseif (empty($interval->d) && empty($interval->h) && $interval->i < 60) {
            if ($interval->i % 10 == 1) {
                $str .= $interval->i . ' минуту назад';
            }
            elseif ($interval->i % 10 > 1 && $interval->i % 10 < 5) {
                $str .= $interval->i . ' минуты назад';
            }
            else {
                $str .= $interval->i . ' минут назад';
            }
        }
        elseif (empty($interval->d) && $interval->h < 24) {
            if ($interval->h % 10 == 1) {
                $str .= $interval->h . ' час назад';
            }
            elseif ($interval->h % 10 > 1 && $interval->h < 5) {
                $str .= $interval->h . ' часа назад';
            }
            else {
                $str .= $interval->h . ' часов назад';
            }
        }
        elseif ($interval->d < 30) {
            if ($interval->d % 10 == 1) {
                $str .= $interval->d . ' день назад';
            }
            elseif ($interval->d > 1 && $interval->d < 5) {
                $str .= $interval->d . ' дня назад';
            }
            else {
                $str .= $interval->d . ' дней назад';
            }
        }
        else {
            $str .= 'более месяца назад';
        }

        if ($this->is_mobile) {
            $str .= ' <i class="fa fa-mobile"></i>';
        }

        return $str;
    }

    public static function countPhotos()
    {
        $raw = (new Query())
            ->select("profile.id, COUNT(photo.id) AS photos")
            ->from(['profile' => Profile::tableName()])
            ->leftJoin(['photo' => Photo::tableName()], 'photo.profile_id = profile.id AND photo.approved = 1')
            ->groupBy('profile.id')
            ->all();

        $results = [];
        foreach ($raw as $r) {
            $key = $r['id'];
            $results[$key] = intval($r['photos']);
        }

        return $results;
    }

    public function findLikedAvatars()
    {
        return ProfileLike::find()
            ->select('with_id')
            ->where(['profile_id' => $this->id, 'type' => ProfileLike::TYPE_AVATAR])
            ->groupBy('with_id')
            ->column();
    }

    public function findTodayVisits()
    {
        return $this->findToday(ProfileVisit::tableName());
    }

    public function findTodayFavorites()
    {
        return $this->findToday(ProfileList::tableName());
    }

    public function findTodayLikes()
    {
        return $this->findToday(ProfileLike::tableName());
    }

    private function findToday($tableName, $max = 4)
    {
        $grouped = [];
        $profileIds = [];
        $now = (new \DateTime('now'))->format('Y-m-d 00:00:00');

        $visits = (new Query())
            ->select('v.*')
            ->from(['v' => $tableName])
            ->innerJoin(['p' => Profile::tableName()], 'p.id = v.profile_id')
            ->where(['v.with_id' => $this->id])
            ->andWhere(Profile::activatedSql('p'))
            ->andWhere(['>=', 'v.created_at', $now])
            ->orderBy(['v.created_at' => SORT_DESC])
            ->all();

        if (!empty($visits)) {
            foreach ($visits as &$v) {
                $created = (new \DateTime($v['created_at']))->format('d.m.Y');
                $key = $created . '_' . $v['profile_id'];
                if (!isset($grouped[$key])) {
                    $profileIds[] = $v['profile_id'];
                    $v['created'] = $created;
                    $grouped[$key] = $v;
                }
            }
        }

        # когда ничего не нашлось
        if (empty($grouped) || empty($profileIds)) {
            return [];
        }

        /** @var Profile[] $profiles */
        $profiles = Profile::find()->where(['id' => $profileIds])->all();
        $profilesByKey = [];
        foreach ($profiles as $profile) {
            $key = $profile->id;
            $profilesByKey[$key] = $profile;
        }

        foreach ($grouped as $key => &$v) {
            $v['profile'] = isset($profilesByKey[$v['profile_id']]) ? $profilesByKey[$v['profile_id']] : null;
        }

        return array_slice(array_values($grouped), 0, $max);
    }

    /**
     * @return Profile[]
     */
    public function findMyContacts()
    {
        $ids = [];

        $chatIds = ChatMessage::find()->select('from_id', 'to_id')
            ->where(['from_id' => $this->id])
            ->orWhere(['to_id' => $this->id])
            ->all();

        for ($i = 0; $i < count($chatIds); $i++) {
            $key = $this->id == $chatIds[$i]['from_id'] ? $chatIds[$i]['to_id'] : $chatIds[$i]['from_id'];
            $key = $key . '';
            $ids[$key] = true;
        }

        $listIds = ProfileList::find()->select('profile_id', 'with_id')
            ->where(['profile_id' => $this->id])
            ->orWhere(['with_id' => $this->id])
            ->all();

        for ($i = 0; $i < count($listIds); $i++) {
            $key = $this->id == $listIds[$i]['profile_id'] ? $listIds[$i]['with_id'] : $listIds[$i]['profile_id'];
            $key = $key . '';
            $ids[$key] = true;
        }

        $likesIds = ProfileLike::find()->select('profile_id', 'with_id')
            ->where(['profile_id' => $this->id])
            ->orWhere(['with_id' => $this->id])
            ->all();

        for ($i = 0; $i < count($likesIds); $i++) {
            $key = $this->id == $likesIds[$i]['profile_id'] ? $likesIds[$i]['with_id'] : $likesIds[$i]['profile_id'];
            $key = $key . '';
            $ids[$key] = true;
        }

        return Profile::find()
            ->where(['id' => array_keys($ids)])
            ->andWhere(self::activatedSql())
            ->all();
    }
}
