<?php

namespace modules\profiles\common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_profile_complaints".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property integer $with_id
 * @property string $source
 * @property string $comment
 * @property string $created_at
 * @property string $updated_at
 */
class ProfileComplaint extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    const SOURCE_WEBRTC = 'webrtc';
    const SOURCE_VIEW = 'view';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profile_complaints}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Жалоба участника';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Жалобы участников';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['profile_id', 'integer'],
            ['with_id', 'integer'],
            ['source', 'string', 'max' => 255],
            ['comment', 'string'],
            ['created_at', 'safe'],
            ['updated_at', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Profile ID',
            'with_id' => 'With ID',
            'source' => 'Источник',
            'comment' => 'Комментарий',
            'created_at' => 'Создана',
            'updated_at' => 'Обновлена',
        ];
    }

    public static function getSourceOptions()
    {
        return [
            self::SOURCE_WEBRTC => 'Видео-конференция',
            self::SOURCE_VIEW => 'Профиль участника',
        ];
    }
}
