<?php

namespace modules\profiles\common\models;

use modules\profiles\frontend\controllers\VisitsController;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Query;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_profile_visits".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property integer $with_id
 * @property integer $is_mobile
 * @property string $created_at
 * @property string $updated_at
 */
class ProfileVisit extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profile_visits}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Profile Visit';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Profile Visits';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['profile_id', 'string', 'max' => 255],
            ['with_id', 'integer'],
            ['is_mobile', 'integer'],
            ['created_at', 'safe'],
            ['updated_at', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Profile ID',
            'with_id' => 'With ID',
            'is_mobile' => 'Is Mobile',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function add($profile_id, $with_id)
    {
        $detect = new \Mobile_Detect;

        $visit = new ProfileVisit();
        $visit->profile_id = $profile_id;
        $visit->with_id = $with_id;
        $visit->is_mobile = $detect->isMobile();
        $visit->save(false);
    }

    public static function findByProfile(Profile $profile, $section)
    {
        $grouped = [];
        $profileIds = [];

        # Визиты гостей
        if (in_array($section, [VisitsController::SECTION_ALL, VisitsController::SECTION_TODAY, VisitsController::SECTION_VISITS])) {
            $query_visits = (new Query())
                ->select('v.*')
                ->from(['v' => ProfileVisit::tableName()])
                ->innerJoin(['p' => Profile::tableName()], 'p.id = v.profile_id')
                ->where(['v.with_id' => $profile->id])
                ->andWhere(['p.avatar_approved' => true])
                ->orderBy(['v.created_at' => SORT_DESC]);

            if ($section == VisitsController::SECTION_TODAY) {
                $now = (new \DateTime('now'))->format('Y-m-d 00:00:00');
                $query_visits->andWhere(['>=', 'v.created_at', $now]);
            }

            $visits = $query_visits->all();

            if (!empty($visits)) {
                foreach ($visits as &$v) {
                    $created = (new \DateTime($v['created_at']))->format('d.m.Y');
                    $key = $created . '_' . $v['profile_id'];
                    if (!isset($grouped[$key])) {
                        $profileIds[] = $v['profile_id'];
                        $v['created'] = $created;
                        $grouped[$key] = $v;
                    }
                }
            }
        }

        # Лайки
        if (in_array($section, [VisitsController::SECTION_ALL, VisitsController::SECTION_TODAY, VisitsController::SECTION_LIKES])) {
            $query_likes = (new Query())
                ->select('l.*')
                ->from(['l' => ProfileLike::tableName()])
                ->innerJoin(['p' => Profile::tableName()], 'p.id = l.profile_id')
                ->where(['l.with_id' => $profile->id])
                ->andWhere(['p.avatar_approved' => true])
                ->orderBy(['l.created_at' => SORT_DESC]);

            if ($section == VisitsController::SECTION_TODAY) {
                $now = (new \DateTime('now'))->format('Y-m-d 00:00:00');
                $query_likes->andWhere(['>=', 'l.created_at', $now]);
            }

            $likes = $query_likes->all();

            if (!empty($likes)) {
                foreach ($likes as &$like) {
                    $created = (new \DateTime($like['created_at']))->format('d.m.Y');
                    $key = $created . '_' . $like['profile_id'];

                    if (!isset($grouped[$key])) {
                        $profileIds[] = $like['profile_id'];
                        $like['created'] = $created;
                        $grouped[$key] = $like;
                    }
                    else {
                        $createdVisit = new \DateTime($grouped[$key]['created_at']);
                        $createdLike = new \DateTime($like['created_at']);
                        if ($createdLike > $createdVisit) {
                            $grouped[$key]['created'] = $created;
                            $grouped[$key]['created_at'] = $like['created_at'];
                        }
                    }
                }
            }
        }

        # когда ничего не нашлось
        if (empty($grouped) || empty($profileIds)) {
            return [];
        }

        /** @var Profile[] $profiles */
        $profiles = Profile::find()->where(['id' => $profileIds])->all();
        $profilesByKey = [];
        foreach ($profiles as $profile) {
            $key = $profile->id;
            $profilesByKey[$key] = $profile;
        }

        foreach ($grouped as $key => &$v) {
            $v['profile'] = isset($profilesByKey[$v['profile_id']]) ? $profilesByKey[$v['profile_id']] : null;
        }

        return array_values($grouped);
    }

    public static function countByProfile(Profile $profile)
    {
        $grouped = [];
        $visits = (new Query())
            ->select('v.created_at, p.id userId, p.name userName, p.avatar userAvatar, p.my_gender userGender')
            ->from(['v' => ProfileVisit::tableName()])
            ->innerJoin(['p' => Profile::tableName()], 'p.id = v.profile_id')
            ->where(['v.with_id' => $profile->id])
            ->andWhere(['v.seen' => false])
            ->andWhere(['p.avatar_approved' => true])
            ->orderBy(['v.created_at' => SORT_DESC])
            ->all();

        foreach ($visits as &$v) {
            $created = (new \DateTime($v['created_at']))->format('d.m.Y');
            $key = $created . '_' . $v['userId'];
            if (!isset($grouped[$key])) {
                $v['created'] = $created;
                $grouped[$key] = $v;
            }
        }

        return count($grouped);
    }
}
