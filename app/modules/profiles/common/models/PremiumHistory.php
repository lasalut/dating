<?php

namespace modules\profiles\common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_premium_history".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property string $premium_id
 * @property integer $hours
 * @property integer $days
 * @property integer $price
 * @property string $expires_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $contacts
 * @property integer $minutes
 * @property integer $broadcasts
 * @property integer $days_incognito
 * @property integer $days_online
 * @property integer $days_nudes
 */
class PremiumHistory extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%premium_history}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Премиум история';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Премиум история';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['profile_id', 'integer'],
            ['premium_id', 'string', 'max' => 255],
            ['hours', 'integer'],
            ['days', 'integer'],
            ['price', 'integer'],
            [['contacts', 'minutes', 'broadcasts'], 'integer'],
            [['days_incognito', 'days_online', 'days_nudes'], 'integer'],
            ['expires_at', 'safe'],
            ['created_at', 'safe'],
            ['updated_at', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Участник ID',
            'premium_id' => 'Премиум',
            'hours' => 'Часы',
            'days' => 'Дни',
            'price' => 'Стоимость',
            'expires_at' => 'Дата окончания',
            'created_at' => 'Дата начала',
            'updated_at' => 'Обновления дата',
            'broadcasts' => 'Рассылки',
            'contacts' => 'Контакты для общения',
            'minutes' => 'Минуты видео-звонка',
            'days_incognito' => 'Режим «Инкогнито», часов',
            'days_online' => 'Функция «Кто онлайн?», часов',
            'days_nudes' => 'Возможность просмотра 18+, часов',
        ];
    }
}
