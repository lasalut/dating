<?php

namespace modules\profiles\common\finance;

use marketingsolutions\finance\models\TransactionPartnerInterface;
use modules\profiles\common\models\Premium;
use modules\profiles\common\models\PremiumHistory;
use modules\profiles\common\models\Profile;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class PremiumHistoryPartner extends Object implements TransactionPartnerInterface
{
	public $id;
	protected static $_titles = [];

	/**
	 * Returns partner for some transaction by partner's id
	 * @param int $id
	 * @return $this
	 */
	public static function findById($id)
	{
		return \Yii::createObject([
			'class' => self::className(),
			'id' => $id,
		]);
	}

	/**
	 * Returns id of the partner. Could NULL if partner does not support id
	 * @return int|string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Returns title for partner
	 * @return string
	 */
	public function getTitleForTransaction()
	{
		if (!array_key_exists($this->id, self::$_titles)) {
			self::$_titles[$this->id] = ArrayHelper::getValue(PremiumHistory::findOne($this->id), 'id', 'Неизвестно');
		}
		return self::$_titles[$this->id];
	}

	/**
	 * Returns type of the partner
	 * @return string
	 */
	public function getTypeForTransaction()
	{
		return 'Зачисление на счет';
	}
}