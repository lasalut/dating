<?php

use yii\db\Migration;

/**
 * Class m180509_180823_profiles_add_premium_priority_column
 */
class m180509_180823_profiles_add_premium_priority_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profiles}}', 'premium_priority', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'premium_priority');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180509_180823_profiles_add_premium_priority_column cannot be reverted.\n";

        return false;
    }
    */
}
