<?php

use yii\db\Migration;

/**
 * Class m180510_164247_profiles_add_premium_sticked_to_column
 */
class m180510_164247_profiles_add_premium_sticked_to_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profiles}}', 'premium_sticked_to', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'premium_sticked_to');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180510_164247_profiles_add_premium_sticked_to_column cannot be reverted.\n";

        return false;
    }
    */
}
