<?php

use yii\db\Migration;

/**
 * Class m180408_155215_premiums_add_extra_columns
 */
class m180408_155215_premiums_add_extra_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%premiums}}', 'contacts', $this->integer());
        $this->addColumn('{{%premiums}}', 'minutes', $this->integer());
        $this->addColumn('{{%premiums}}', 'broadcasts', $this->integer());
        $this->addColumn('{{%premiums}}', 'days_incognito', $this->integer());
        $this->addColumn('{{%premiums}}', 'days_online', $this->integer());
        $this->addColumn('{{%premiums}}', 'days_nudes', $this->integer());

        $this->addColumn('{{%premium_history}}', 'contacts', $this->integer());
        $this->addColumn('{{%premium_history}}', 'minutes', $this->integer());
        $this->addColumn('{{%premium_history}}', 'broadcasts', $this->integer());
        $this->addColumn('{{%premium_history}}', 'days_incognito', $this->integer());
        $this->addColumn('{{%premium_history}}', 'days_online', $this->integer());
        $this->addColumn('{{%premium_history}}', 'days_nudes', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%premiums}}', 'contacts');
        $this->dropColumn('{{%premiums}}', 'minutes');
        $this->dropColumn('{{%premiums}}', 'broadcasts');
        $this->dropColumn('{{%premiums}}', 'days_incognito');
        $this->dropColumn('{{%premiums}}', 'days_online');
        $this->dropColumn('{{%premiums}}', 'days_nudes');

        $this->dropColumn('{{%premium_history}}', 'contacts');
        $this->dropColumn('{{%premium_history}}', 'minutes');
        $this->dropColumn('{{%premium_history}}', 'broadcasts');
        $this->dropColumn('{{%premium_history}}', 'days_incognito');
        $this->dropColumn('{{%premium_history}}', 'days_online');
        $this->dropColumn('{{%premium_history}}', 'days_nudes');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180408_155215_premiums_add_extra_columns cannot be reverted.\n";

        return false;
    }
    */
}
