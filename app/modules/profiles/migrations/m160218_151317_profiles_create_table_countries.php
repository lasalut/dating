<?php

use yii\db\Migration;

class m160218_151317_profiles_create_table_countries extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%countries}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'shortTitle' => $this->string(),
        ], $tableOptions);
    }

    public function down()
    {
       $this->dropTable('{{%countries}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
