<?php

use yii\db\Migration;

/**
 * Class m180317_193815_profiles_create_likes_table
 */
class m180317_193815_profiles_create_likes_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%profile_likes}}', [
            'id' => $this->primaryKey(),
            'profile_id' => $this->integer(),
            'with_id' => $this->integer(),
            'photo_id' => $this->integer(),
            'comment_id' => $this->integer(),
            'seen' => $this->boolean()->defaultValue(false),
            'type' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);

        $this->createIndex('fk_profile_id', '{{%profile_likes}}', 'profile_id');
        $this->createIndex('fk_with_id', '{{%profile_likes}}', 'with_id');
        $this->createIndex('fk_photo_id', '{{%profile_likes}}', 'photo_id');
        $this->createIndex('fk_comment_id', '{{%profile_likes}}', 'comment_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%profile_likes}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180317_193815_profiles_create_likes_table cannot be reverted.\n";

        return false;
    }
    */
}
