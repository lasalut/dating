<?php

use yii\db\Migration;

/**
 * Class m180416_194827_profiles_create_broadcasts_table
 */
class m180416_194827_profiles_create_broadcasts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%broadcasts}}', [
            'id' => $this->primaryKey(),
            'profile_id' => $this->integer(),
            'city_id' => $this->integer(),
            'comment' => $this->text(),
            'excluded' => $this->text(),
            'hours' => $this->integer(),
            'expires_at' => $this->dateTime(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);
        $this->createIndex('profile_id', '{{%broadcasts}}', 'profile_id');
        $this->createIndex('city_id', '{{%broadcasts}}', 'city_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%broadcasts}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180416_194827_profiles_create_broadcasts_table cannot be reverted.\n";

        return false;
    }
    */
}
