<?php

use yii\db\Migration;

/**
 * Class m180225_110455_profiles_add_confirmed_columns
 */
class m180225_110455_profiles_add_confirmed_columns extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%profiles}}', 'email_confirmed_at', $this->dateTime());
        $this->addColumn('{{%profiles}}', 'email_sent_at', $this->dateTime());
        $this->addColumn('{{%profiles}}', 'phone_confirmed_at', $this->dateTime());
        $this->addColumn('{{%profiles}}', 'phone_sent_at', $this->dateTime());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'email_confirmed_at');
        $this->dropColumn('{{%profiles}}', 'email_sent_at');
        $this->dropColumn('{{%profiles}}', 'phone_confirmed_at');
        $this->dropColumn('{{%profiles}}', 'phone_sent_at');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180225_110455_profiles_add_confirmed_columns cannot be reverted.\n";

        return false;
    }
    */
}
