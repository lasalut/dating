<?php

use yii\db\Migration;

class m160218_151315_profiles_create_table_cities extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%cities}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'region_id' => $this->integer(),
            'country_id' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('fk_region_id', '{{%cities}}', 'region_id');
        $this->createIndex('fk_country_id', '{{%cities}}', 'country_id');
    }

    public function down()
    {
       $this->dropTable('{{%cities}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
