<?php

use yii\db\Migration;

/**
 * Class m180325_120425_profiles_add_removed_columns
 */
class m180325_120425_profiles_add_removed_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profiles}}', 'removed', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%profiles}}', 'removed_comment', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'removed');
        $this->dropColumn('{{%profiles}}', 'removed_comment');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180325_120425_profiles_add_removed_columns cannot be reverted.\n";

        return false;
    }
    */
}
