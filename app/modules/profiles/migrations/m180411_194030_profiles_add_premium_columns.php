<?php

use yii\db\Migration;

class m180411_194030_profiles_add_premium_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profiles}}', 'premium_online', $this->dateTime());
        $this->addColumn('{{%profiles}}', 'premium_nudes', $this->dateTime());
        $this->addColumn('{{%profiles}}', 'premium_incognito', $this->dateTime());
        $this->addColumn('{{%profiles}}', 'premium_incognito_on', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'premium_online');
        $this->dropColumn('{{%profiles}}', 'premium_nudes');
        $this->dropColumn('{{%profiles}}', 'premium_incognito');
        $this->dropColumn('{{%profiles}}', 'premium_incognito_on');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180402_194029_profiles_add_premium_columns cannot be reverted.\n";

        return false;
    }
    */
}
