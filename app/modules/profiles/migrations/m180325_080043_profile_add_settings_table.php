<?php

use yii\db\Migration;

/**
 * Class m180325_080043_profile_add_settings_table
 */
class m180325_080043_profile_add_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profiles}}', 'notify_sound', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%profiles}}', 'notify_email_messages', $this->boolean()->defaultValue(true));
        $this->addColumn('{{%profiles}}', 'notify_email_visits', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%profiles}}', 'notify_webpush', $this->boolean()->defaultValue(false));

        $this->addColumn('{{%profiles}}', 'webpush_endpoint', $this->string(500));
        $this->addColumn('{{%profiles}}', 'webpush_publicKey', $this->string());
        $this->addColumn('{{%profiles}}', 'webpush_authToken', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'notify_sound');
        $this->dropColumn('{{%profiles}}', 'notify_email_messages');
        $this->dropColumn('{{%profiles}}', 'notify_email_visits');
        $this->dropColumn('{{%profiles}}', 'notify_webpush');

        $this->dropColumn('{{%profiles}}', 'webpush_endpoint');
        $this->dropColumn('{{%profiles}}', 'webpush_publicKey');
        $this->dropColumn('{{%profiles}}', 'webpush_authToken');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180325_080043_profile_add_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
