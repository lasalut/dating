<?php

use yii\db\Migration;

class m180127_193644_profiles_add_is_mobile_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%profiles}}', 'is_mobile', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'is_mobile');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180127_193643_profiles_extend_avatar_hash_columns cannot be reverted.\n";

        return false;
    }
    */
}
