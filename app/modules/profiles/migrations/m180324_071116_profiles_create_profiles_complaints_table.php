<?php

use yii\db\Migration;

/**
 * Class m180324_071116_profiles_create_profiles_complaints_table
 */
class m180324_071116_profiles_create_profiles_complaints_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%profile_complaints}}', [
            'id' => $this->primaryKey(),
            'profile_id' => $this->integer(),
            'with_id' => $this->integer(),
            'source' => $this->string(),
            'comment' => $this->text(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);
        $this->createIndex('profile_id', '{{%profile_complaints}}', 'profile_id');
        $this->createIndex('with_id', '{{%profile_complaints}}', 'profile_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%profile_complaints}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180324_071116_profiles_create_profiles_complaints_table cannot be reverted.\n";

        return false;
    }
    */
}
