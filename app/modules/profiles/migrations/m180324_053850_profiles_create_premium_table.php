<?php

use yii\db\Migration;

/**
 * Class m180324_053850_profiles_create_premium_table
 */
class m180324_053850_profiles_create_premium_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%premiums}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string(),
            'title' => $this->string(),
            'package' => $this->string(),
            'hours' => $this->integer(),
            'days' => $this->integer(),
            'price' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%premiums}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180324_053850_profiles_create_premium_table cannot be reverted.\n";

        return false;
    }
    */
}
