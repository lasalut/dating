<?php

use yii\db\Migration;


class m180211_075921_profiles_create_visits_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%profile_visits}}', [
            'id' => $this->primaryKey(),
            'profile_id' => $this->integer(),
            'with_id' => $this->integer(),
            'is_mobile' => $this->boolean()->defaultValue(false),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);

        $this->createIndex('fk_profile_id', '{{%profile_visits}}', 'profile_id');
        $this->createIndex('fk_with_id', '{{%profile_visits}}', 'with_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%profile_visits}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180211_075920_profiles_create_lists_table cannot be reverted.\n";

        return false;
    }
    */
}
