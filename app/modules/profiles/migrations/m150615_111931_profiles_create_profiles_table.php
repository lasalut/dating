<?php

use yii\db\Schema;
use yii\db\Migration;

class m150615_111931_profiles_create_profiles_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%profiles}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(32),
            'phone_mobile' => $this->string(16)->unique(),
            'email' => $this->string(64)->unique(),
            'passhash' => $this->string(),
            'birthday_on' => $this->date(),
            'city_id' => $this->integer(),
            'subway_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'avatar' => $this->string(),

			'blocked' => $this->boolean()->defaultValue(false),
            'blocked_at' => $this->dateTime(),
			'blocked_reason' => $this->string(),

            'my_gender' => $this->string(6),
            'my_height' => $this->integer(),
            'my_weight' => $this->integer(),
            'my_bust' => $this->string(),
            'my_hair' => $this->string(),
            'my_eyes' => $this->string(),
            'my_type' => $this->string(),
            'my_constitution' => $this->string(),

            'sex_orientation' => $this->string(),
            'sex_freq' => $this->string(),
            'sex_role' => $this->string(),
            'sex_types' => $this->string(),

            'social_education' => $this->string(),
            'social_finance' => $this->string(),
            'social_relationship' => $this->string(),
            'social_kids' => $this->string(),
            'social_interests' => $this->text(),
            'social_about' => $this->text(),

            'logged_at' => $this->dateTime(),
            'online_at' => $this->dateTime(),

            'email_confirmed' => $this->boolean()->defaultValue(false),
            'email_subscribed' => $this->boolean()->defaultValue(true),
            'phone_confirmed' => $this->boolean()->defaultValue(false),
            'phone_code' => $this->string(4),

            'intension' => $this->string(),
            'looking_for' => $this->string(),
            'looking_years' => $this->string(),
        ], $tableOptions);

        $this->createIndex('fk_city_id', '{{%profiles}}', 'city_id');
        $this->createIndex('fk_subway_id', '{{%profiles}}', 'subway_id');
    }

    public function down()
    {
        $this->dropTable('{{%profiles}}');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
