<?php

use yii\db\Migration;

/**
 * Class m180324_053909_profiles_create_premium_history_table
 */
class m180324_053909_profiles_create_premium_history_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%premium_history}}', [
            'id' => $this->primaryKey(),
            'profile_id' => $this->integer(),
            'premium_id' => $this->integer(),
            'hours' => $this->integer(),
            'days' => $this->integer(),
            'price' => $this->integer(),
            'expires_at' => $this->dateTime(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);
        $this->createIndex('profile_id', '{{%premium_history}}', 'profile_id');
        $this->createIndex('premium_id', '{{%premium_history}}', 'premium_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%premium_history}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180324_053909_profiles_create_premium_history_table cannot be reverted.\n";

        return false;
    }
    */
}
