<?php

use yii\db\Migration;

class m160218_151318_profiles_create_table_subways extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%subways}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'city_id' => $this->string(),
            'color' => $this->string(),
            'line' => $this->string(),
            'position' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
       $this->dropTable('{{%subways}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
