<?php

use yii\db\Migration;

/**
 * Class m180127_193643_profiles_extend_avatar_hash_columns
 */
class m180127_193643_profiles_extend_avatar_hash_columns extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%profiles}}', 'avatar_sent', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%profiles}}', 'avatar_approved', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%profiles}}', 'avatar_approved_at', $this->dateTime());
        $this->addColumn('{{%profiles}}', 'avatar_approved_by', $this->integer());
        $this->addColumn('{{%profiles}}', 'avatar_banned', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%profiles}}', 'hash', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'avatar_sent');
        $this->dropColumn('{{%profiles}}', 'avatar_approved');
        $this->dropColumn('{{%profiles}}', 'avatar_approved_at');
        $this->dropColumn('{{%profiles}}', 'avatar_approved_by');
        $this->dropColumn('{{%profiles}}', 'avatar_banned');
        $this->dropColumn('{{%profiles}}', 'hash');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180127_193643_profiles_extend_avatar_hash_columns cannot be reverted.\n";

        return false;
    }
    */
}
