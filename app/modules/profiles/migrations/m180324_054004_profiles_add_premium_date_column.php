<?php

use yii\db\Migration;

/**
 * Class m180324_054004_profiles_add_premium_date_column
 */
class m180324_054004_profiles_add_premium_date_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%profiles}}', 'premium_date', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'premium_date');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180324_054004_profiles_add_premium_date_column cannot be reverted.\n";

        return false;
    }
    */
}
