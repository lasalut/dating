<?php

use yii\db\Migration;

/**
 * Class m180402_194030_profiles_add_premium_columns
 */
class m180402_194030_profiles_add_premium_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        @$this->dropColumn('{{%profiles}}', 'premium_minutes');
        @$this->dropColumn('{{%profiles}}', 'premium_contacts');

        $this->addColumn('{{%profiles}}', 'premium_minutes', $this->integer()->defaultValue(0));
        $this->addColumn('{{%profiles}}', 'premium_contacts', $this->integer()->defaultValue(0));

        $this->addColumn('{{%profiles}}', 'premium_ups', $this->integer()->defaultValue(0));
        $this->addColumn('{{%profiles}}', 'premium_broadcasts', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'premium_minutes');
        $this->dropColumn('{{%profiles}}', 'premium_contacts');
        $this->dropColumn('{{%profiles}}', 'premium_ups');
        $this->dropColumn('{{%profiles}}', 'premium_broadcasts');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180402_194029_profiles_add_premium_columns cannot be reverted.\n";

        return false;
    }
    */
}
