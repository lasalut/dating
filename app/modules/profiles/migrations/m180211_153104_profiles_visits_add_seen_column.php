<?php

use yii\db\Migration;

/**
 * Class m180211_153104_profiles_visits_add_seen_column
 */
class m180211_153104_profiles_visits_add_seen_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile_visits}}', 'seen', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profile_visits}}', 'seen');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180211_153104_profiles_visits_add_seen_column cannot be reverted.\n";

        return false;
    }
    */
}
