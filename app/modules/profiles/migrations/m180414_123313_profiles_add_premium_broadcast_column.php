<?php

use yii\db\Migration;

/**
 * Class m180414_123313_profiles_add_premium_broadcast_column
 */
class m180414_123313_profiles_add_premium_broadcast_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profiles}}', 'premium_broadcast', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'premium_broadcast');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180414_123313_profiles_add_premium_broadcast_column cannot be reverted.\n";

        return false;
    }
    */
}
