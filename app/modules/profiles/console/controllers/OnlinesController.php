<?php

namespace modules\profiles\console\controllers;

use modules\chat\console\models\ZmqService;
use modules\profiles\common\models\Profile;
use console\base\Controller;
use yii\mutex\FileMutex;

class OnlinesController extends Controller
{
    public function actionIndex()
    {
        $mutex = new FileMutex();
        if ($mutex->acquire(__METHOD__) === false) {
            $this->stdout("Another instance is running...\n");
            return self::EXIT_CODE_NORMAL;
        }

        while (true) {
            $ids = Profile::getOnlineIds();
            ZmqService::publish(ZmqService::SUBSCRIBER_ALL, ZmqService::TYPE_ONLINES, ['ids' => $ids]);
            sleep(20);
        }

        $mutex->release(__METHOD__);
        return self::EXIT_CODE_NORMAL;
    }

    public function actionStop()
    {
        system("pkill -f profiles/onlines");
    }
}