<?php

namespace modules\profiles\console\controllers;

use modules\profiles\common\models\Bonus;
use modules\profiles\common\models\Profile;
use modules\profiles\common\models\Subway;
use Yii;
use console\base\Controller;

class ImportController extends Controller
{
    public function actionMetro()
    {
        $string = file_get_contents(__DIR__ . '/data/metro.json');
        $data = json_decode($string, true);

        foreach ($data as $d) {
            $line = $d['line'];

            for ($i = 0; $i < count($d['stations']); $i++) {
                $station = $d['stations'][$i];
                $subway = new Subway();
                $subway->title = $station;
                $subway->line = $line;
                $subway->position = $i + 1;
                $subway->save();
            }
        }
    }

    public function actionGirls()
    {
        $girl = new Profile();
        $girl->my_gender = Profile::GENDER_FEMALE;
        $girl->looking_for = Profile::GENDER_MALE;
        $girl->looking_years = '21-25;25-30';
        $girl->email = 'g1@mail.ru';
        $girl->intension = 'friendship;chatting;family;sport';
        $girl->email_subscribed = 1;
        $girl->passhash = '$2y$13$Bh4C8WyQgP02UyqusxrBtuTcEtcJsqfKBTcugt4Yizp2m9SAhccPG';
        $girl->avatar = '1.jpg';
        $girl->name = 'Екатерина';
        $girl->birthday_on = '1997-02-05';
        $girl->city_id = 33;
        $girl->save();

        $girl = new Profile();
        $girl->my_gender = Profile::GENDER_FEMALE;
        $girl->looking_for = Profile::GENDER_MALE;
        $girl->looking_years = '21-25;25-30';
        $girl->email = 'g2@mail.ru';
        $girl->intension = 'friendship;chatting;family;sport';
        $girl->email_subscribed = 1;
        $girl->passhash = '$2y$13$Bh4C8WyQgP02UyqusxrBtuTcEtcJsqfKBTcugt4Yizp2m9SAhccPG';
        $girl->avatar = '2.jpg';
        $girl->name = 'Анна';
        $girl->birthday_on = '1988-04-11';
        $girl->city_id = 1;
        $girl->save();

        $girl = new Profile();
        $girl->my_gender = Profile::GENDER_FEMALE;
        $girl->looking_for = Profile::GENDER_MALE;
        $girl->looking_years = '21-25;25-30';
        $girl->email = 'g3@mail.ru';
        $girl->intension = 'friendship;chatting;family;sport';
        $girl->email_subscribed = 1;
        $girl->passhash = '$2y$13$Bh4C8WyQgP02UyqusxrBtuTcEtcJsqfKBTcugt4Yizp2m9SAhccPG';
        $girl->avatar = '3.jpg';
        $girl->name = 'Елена';
        $girl->birthday_on = '1990-06-14';
        $girl->city_id = 39;
        $girl->save();

        $girl = new Profile();
        $girl->my_gender = Profile::GENDER_FEMALE;
        $girl->looking_for = Profile::GENDER_MALE;
        $girl->looking_years = '21-25;25-30';
        $girl->email = 'g4@mail.ru';
        $girl->intension = 'friendship;chatting;family;sport';
        $girl->email_subscribed = 1;
        $girl->passhash = '$2y$13$Bh4C8WyQgP02UyqusxrBtuTcEtcJsqfKBTcugt4Yizp2m9SAhccPG';
        $girl->avatar = '4.jpg';
        $girl->name = 'Кристина';
        $girl->birthday_on = '1992-07-18';
        $girl->city_id = 1;
        $girl->save();

        $girl = new Profile();
        $girl->my_gender = Profile::GENDER_FEMALE;
        $girl->looking_for = Profile::GENDER_MALE;
        $girl->looking_years = '18-20';
        $girl->email = 'g5@mail.ru';
        $girl->intension = 'friendship;chatting;family;sport';
        $girl->email_subscribed = 1;
        $girl->passhash = '$2y$13$Bh4C8WyQgP02UyqusxrBtuTcEtcJsqfKBTcugt4Yizp2m9SAhccPG';
        $girl->avatar = '5.jpg';
        $girl->name = 'Александра';
        $girl->birthday_on = '1994-09-22';
        $girl->city_id = 115;
        $girl->save();
    }
}