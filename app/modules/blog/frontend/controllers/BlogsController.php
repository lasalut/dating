<?php

namespace modules\blog\frontend\controllers;

use modules\blog\common\models\Blog;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class BlogsController extends Controller
{
    public function actionIndex()
    {
        /** @var Blog[] $blogs */
        $blogs = Blog::find()
            ->where(['enabled' => true])
            ->orderBy(['dated_at' => SORT_DESC, 'id' => SORT_DESC])
            ->all();

        return $this->render('index', compact('blogs'));
    }

    public function actionArticle($url)
    {
        /** @var Blog $blog */
        $blog = Blog::findOne(['url' => $url, 'enabled' => true]);

        if ($blog === null) {
            throw new NotFoundHttpException();
        }

        return $this->render('article', compact('blog'));
    }
}