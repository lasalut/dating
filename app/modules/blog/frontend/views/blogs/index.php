<?php

/**
 * @var \yii\web\View $this
 * @var \modules\blog\common\models\Blog[] $blogs
 */

$this->params['title'] = 'Блог';
\modules\blog\frontend\assets\BlogAsset::register($this);
?>

<h1 class="main">Блог</h1>

<?php for ($i = 0; $i < count($blogs); $i++): ?>
    <?php
    $rest = $i % 4;
    if ($rest === 0 || $rest == 3) {
        $class = 8;
    }
    else {
        $class = 4;
    }
    ?>

    <?php if ($i % 2 == 0): ?>
		<div class="row blogs">
    <?php endif; ?>

	<div class="blog col-md-<?= $class ?>">
		<div class="blog-image">
			<a href="/blog/<?= $blogs[$i]->url ?>">
				<img class="image" src="<?= $class == 4 ? $blogs[$i]->crop_url : $blogs[$i]->middle_url ?>"/>
			</a>
		</div>
		<div class="blog-date">
            <?= $blogs[$i]->date ?>
		</div>
		<div class="blog-title serif">
			<a href="/blog/<?= $blogs[$i]->url ?>" class="no-underline">
                <?= $blogs[$i]->title ?>
			</a>
		</div>
		<div class="blog-description">
            <?= $blogs[$i]->description ?>
		</div>
	</div>

    <?php if ($i % 2 == 1 || $i == count($blogs) - 1): ?>
		</div>
    <?php endif; ?>

<?php endfor; ?>

