<?php

/**
 * @var \yii\web\View $this
 * @var \modules\blog\common\models\Blog $blog
 */

$this->params['title'] = $blog->title;
\modules\blog\frontend\assets\BlogAsset::register($this);
?>

<style>
	.article {
		font-size: 15px;
		margin-bottom: 40px;
	}
	.article-image img {
		width: 100%;
	}
	.article > div {
		margin-bottom: 20px;
	}
	.article > div.article-date {
		margin-bottom: 10px;
		color:         #bbb;
	}
	.article-title h1 {
		display:       inline-block;
		font-size:     30px;
		border-bottom: 2px solid #c69c6d;
		margin: 0;
		color:         #c69c6d;
	}
	.article-content {

	}
</style>

<div class="article">
	<div class="article-image">
		<img src="<?= $blog->photo_url ?>"/>
	</div>
	<div class="article-date">
        <?= $blog->date ?>
	</div>
	<div class="article-title">
		<h1><?= $blog->title ?></h1>
	</div>
	<div class="article-content">
		<?= $blog->content ?>
	</div>
</div>

<div class="center">
	<a class="btn btn-default beige-border" href="/blogs">
		Ко всем статьям
	</a>
</div>

