<?php

namespace modules\blog\frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class BlogAsset extends AssetBundle
{
    public $sourcePath = '@modules/blog/frontend/assets';

    public $css = [
        'css/blog.css'
    ];
    public $js = [
    ];

    public $depends = [
        'frontend\assets\AppAsset',
    ];
}