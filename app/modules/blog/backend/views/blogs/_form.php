<?php

use dosamigos\ckeditor\CKEditor;
use modules\blog\common\models\Blog;
use yii\helpers\Html;
use yii\widgets\MaskedInput;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use yz\admin\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var modules\blog\common\models\Blog $model
 * @var yz\admin\widgets\ActiveForm $form
 */
?>

<?php $box = FormBox::begin(['cssClass' => 'blog-form box-primary', 'title' => '']) ?>
<?php $form = ActiveForm::begin(); ?>

<?php $box->beginBody() ?>

<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'category')->dropDownList(Blog::getCategoryOptions(), ['prompt' => 'выберите...']) ?>
<?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>
<?= $form->field($model, 'content')->widget(CKEditor::className(), [
    'options' => ['rows' => 6],
    'preset' => 'full'
]) ?>

<?= $form->field($model, 'enabled')->checkbox() ?>
<?= $form->field($model, 'dated_at_local')->widget(MaskedInput::className(), ['mask' => '99.99.9999']) ?>

<?= $form->field($model, 'photo')->fileInput() ?>
<?= $this->render('_image', ['model' => $model, 'field' => 'photo_name']); ?>
<?= $this->render('_image', ['model' => $model, 'field' => 'middle_name']); ?>
<?= $this->render('_image', ['model' => $model, 'field' => 'crop_name']); ?>
<?= $this->render('_image', ['model' => $model, 'field' => 'square_name']); ?>

<?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'seo_description')->textarea(['rows' => 2]) ?>
<?= $form->field($model, 'seo_keywords')->textarea(['rows' => 2]) ?>
<?= $form->field($model, 'url')->staticControl() ?>
<?= $form->field($model, 'created_at')->staticControl() ?>
<?= $form->field($model, 'updated_at')->staticControl() ?>

<?php $box->endBody() ?>

<?php $box->actions([
    AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
    AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
    AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
]) ?>
<?php ActiveForm::end(); ?>

<?php FormBox::end() ?>
