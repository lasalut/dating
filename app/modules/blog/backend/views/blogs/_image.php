<?php if (!empty($model->$field)): ?>
    <div class="row" style="margin-bottom:20px;">
        <div class="col-sm-2"></div>
        <?php $getVal = time();?>
        <div class="col-sm-8">
            <a href="<?= Yii::getAlias('@backendWeb/data/blog/' . $model->$field) ?>" target="_blank" class="btn btn-default">
                <?php if (strpos($model->$field, '.pdf') !== false): ?>
                    PDF-документ
                <?php else: ?>
                    <img src="<?= Yii::getAlias('@backendWeb/data/blog/' . $model->$field  ."?". $getVal) ?>"
                         style="max-width:200px; max-height:100px;"/>
                <?php endif; ?>
            </a>
        </div>
    </div>
<?php endif; ?>