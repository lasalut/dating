<?php

namespace modules\blog\backend\controllers;

use Yii;
use modules\blog\common\models\Blog;
use modules\blog\backend\models\BlogSearch;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\models\User;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;

/**
 * BlogsController implements the CRUD actions for Blog model.
 */
class BlogsController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function ($params) {
                    /** @var BlogSearch $searchModel */
                    return Yii::createObject(BlogSearch::className());
                },
                'dataProvider' => function ($params, BlogSearch $searchModel) {
                    $dataProvider = $searchModel->search($params);
                    return $dataProvider;
                },
            ]
        ]);
    }

    /**
     * Lists all Blog models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var BlogSearch $searchModel */
        $searchModel = Yii::createObject(BlogSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(BlogSearch $searchModel)
    {
        return [
            'id',
            [
                'label' => 'Фотография',
                'format' => 'raw',
                'value' => function (Blog $model) {
                    return empty($model->square_url)
                        ? Html::img($model->middle_url, ['style' => 'max-height:200px; max-width:200px;'])
                        : Html::img($model->square_url, ['style' => 'max-height:200px; max-width:200px;']);
                }
            ],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'contentOptions' => ['style' => 'width:20%'],
            ],
            [
                'attribute' => 'category',
                'contentOptions' => ['style' => 'width:140px'],
                'titles' => Blog::getCategoryOptions(),
                'filter' => Blog::getCategoryOptions(),
            ],
            [
                'attribute' => 'description',
                'format' => 'raw',
                'contentOptions' => ['style' => 'width:40%'],
            ],
            [
                'attribute' => 'url',
                'format' => 'raw',
                'contentOptions' => ['style' => 'width:15%'],
            ],
            'enabled:boolean',
            'dated_at:date',
        ];
    }

    /**
     * Creates a new Blog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Blog;
        /** @var User $admin */
        $admin = \Yii::$app->user->identity;
        $model->admin_id = $admin->id;
        $model->enabled = true;
        $model->dated_at = (new \DateTime())->format('Y-m-d');

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
            return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Blog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
            return $this->getCreateUpdateResponse($model);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Blog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array) $id;

        foreach ($id as $id_) {
            $this->findModel($id_)->delete();
        }

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Blog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Blog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Blog::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
