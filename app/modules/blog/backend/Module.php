<?php

namespace modules\blog\backend;

use yz\icons\Icons;

/**
 * Class Module
 */
class Module extends \modules\blog\common\Module
{
    public function getAdminMenu()
    {
        return [
            [
                'route' => ['/blog/blogs/index'],
                'label' => 'Статьи',
                'icon' => Icons::o('newspaper-o'),
            ],
        ];
    }
}