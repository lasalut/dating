<?php

namespace modules\blog\common\models;

use marketingsolutions\datetime\DateTimeBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_blogs".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property integer $admin_id
 * @property string $category
 * @property string $url
 * @property boolean $enabled
 * @property string $crop_name
 * @property string $middle_name
 * @property string $photo_name
 * @property string $square_name
 * @property string $dated_at
 * @property string $created_at
 * @property string $updated_at
 *
 * @property string $photo_url
 * @property string $crop_url
 * @property string $middle_url
 * @property string $square_url
 * @property string $date
 */
class Blog extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    const CATEGORY_BLOG = 'blog';
    const CATEGORY_INDEX = 'index';

    public $photo;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blogs}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Статья';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Статьи';
    }

    public static function getCategoryOptions()
    {
        return [
            self::CATEGORY_INDEX => 'На главную',
            self::CATEGORY_BLOG => 'Блог',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            'datetime' => [
                'class' => DateTimeBehavior::className(),
                'attributes' => [
                    'dated_at' => [
                        'targetAttribute' => 'dated_at_local',
                        'originalFormat' => ['date', 'yyyy-MM-dd'],
                        'targetFormat' => ['date', 'dd.MM.yyyy'],
                    ],
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'string', 'max' => 500],
            ['description', 'string'],
            ['content', 'string'],
            ['seo_title', 'string', 'max' => 500],
            ['seo_description', 'string'],
            ['seo_keywords', 'string'],
            ['admin_id', 'integer'],
            ['category', 'string', 'max' => 255],
            ['enabled', 'safe'],
            ['crop_name', 'string', 'max' => 255],
            ['middle_name', 'string', 'max' => 255],
            ['photo_name', 'string', 'max' => 255],
            ['square_name', 'string', 'max' => 255],
            ['dated_at', 'string'],
            ['url', 'string'],
            ['dated_at_local', 'string'],
            ['created_at', 'string'],
            ['updated_at', 'string'],

            ['title', 'required'],
            ['category', 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description' => 'Анонс',
            'content' => 'Содержимое',
            'seo_title' => 'Seo Title',
            'seo_description' => 'Seo Description',
            'seo_keywords' => 'Seo Keywords',
            'admin_id' => 'Admin ID',
            'category' => 'Категория',
            'url' => 'Адрес URL',
            'enabled' => 'Активна',
            'crop_name' => 'Crop Name',
            'middle_name' => 'Middle Name',
            'photo_name' => 'Photo Name',
            'square_name' => 'Square Name',
            'dated_at' => 'Дата публикации',
            'dated_at_local' => 'Дата публикации',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'photo' => 'Фотография',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->dated_at == '1970-01-01') {
            $this->updateAttributes(['dated_at' => null]);
        }
        if ($insert || !empty($changedAttributes['title'])) {
            $this->updateAttributes(['url' => $this->translit($this->title)]);
        }

        $this->upload();
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        $dir = Yii::getAlias('@data/blog');

        if ($this->photo_name) {
            @unlink($dir . DIRECTORY_SEPARATOR . $this->photo_name);
        }
        if ($this->middle_name) {
            @unlink($dir . DIRECTORY_SEPARATOR . $this->middle_name);
        }
        if ($this->crop_name) {
            @unlink($dir . DIRECTORY_SEPARATOR . $this->crop_name);
        }
        if ($this->square_name) {
            @unlink($dir . DIRECTORY_SEPARATOR . $this->square_name);
        }

        return true;
    }

    public function getPhoto_url()
    {
        return getenv('FRONTEND_WEB') . '/data/blog/' . $this->photo_name;
    }

    public function getCrop_url()
    {
        return getenv('FRONTEND_WEB') . '/data/blog/' . $this->crop_name;
    }

    public function getMiddle_url()
    {
        return getenv('FRONTEND_WEB') . '/data/blog/' . $this->middle_name;
    }

    public function getSquare_url()
    {
        return getenv('FRONTEND_WEB') . '/data/blog/' . $this->square_name;
    }

    public function getDate()
    {
        $months = explode(' ', '0 января февраля марта апреля мая июня июля августа сентября октября ноября декабря');
        $dated = new \DateTime($this->dated_at);
        $month = $dated->format('m');
        $month = intval($month);

        return $dated->format('d') . ' ' . $months[$month];
    }

    private function upload()
    {
        $dir = Yii::getAlias('@data/blog');
        FileHelper::createDirectory($dir, $mode = 0775, $recursive = true);

        $file = UploadedFile::getInstance($this, 'photo');
        $unique = uniqid();

        if ($file instanceof UploadedFile) {
            $name = "{$this->id}_photo_{$unique}.{$file->extension}";
            $path = $dir . DIRECTORY_SEPARATOR . $name;
            $file->saveAs($path, false);

            $cropName = "{$this->id}_crop_{$unique}.{$file->extension}";
            $cropPath = $dir . DIRECTORY_SEPARATOR . $cropName;
            $im = new \Imagick($path);
            $im->cropThumbnailImage(370, 350);
            $im->writeImage($cropPath);

            $middleName = "{$this->id}_middle_{$unique}.{$file->extension}";
            $middlePath = $dir . DIRECTORY_SEPARATOR . $middleName;
            $im = new \Imagick($path);
            $im->cropThumbnailImage(770, 445);
            $im->writeImage($middlePath);

            $squareName = "{$this->id}_square_{$unique}.{$file->extension}";
            $squarePath = $dir . DIRECTORY_SEPARATOR . $squareName;
            $im = new \Imagick($path);
            $im->cropThumbnailImage(500, 500);
            $im->writeImage($squarePath);

            $im = new \Imagick($path);
            $im->cropThumbnailImage(1170, 465);
            $im->writeImage($path);

            if ($this->photo_name) {
                @unlink($dir . DIRECTORY_SEPARATOR . $this->photo_name);
            }
            if ($this->middle_name) {
                @unlink($dir . DIRECTORY_SEPARATOR . $this->middle_name);
            }
            if ($this->crop_name) {
                @unlink($dir . DIRECTORY_SEPARATOR . $this->crop_name);
            }
            if ($this->square_name) {
                @unlink($dir . DIRECTORY_SEPARATOR . $this->square_name);
            }

            $this->photo_name = $name;
            $this->middle_name = $middleName;
            $this->crop_name = $cropName;
            $this->square_name = $squareName;

            $this->updateAttributes(['photo_name', 'middle_name', 'crop_name', 'square_name']);
        }

        if (getenv('YII_ENV') == 'dev') {
            $frontendDir = Yii::getAlias("@frontendWebroot/data/blog");
            FileHelper::copyDirectory($dir, $frontendDir);
            $backendDir = Yii::getAlias("@backendWebroot/data/blog");
            FileHelper::copyDirectory($dir, $backendDir);
        }
    }

    private function translit($text)
    {
        $pat = array('/&[a-z]+;/', '/<sup>(.*?)<\/sup>/i', '/<sub>(.*?)<\/sub>/i');
        $rep = array('', '$1', '$1');
        $text = preg_replace($pat, $rep, $text);
        $text = mb_strtolower($text, 'utf-8');

        // Русский алфавит
        $rus_alphabet = array(
            'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й',
            'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф',
            'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я',
            'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й',
            'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф',
            'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я',
            ' ', '.', '(', ')', ',', '/', '?', ':',
        );

        // Английская транслитерация
        $rus_alphabet_translit = array(
            'A', 'B', 'V', 'G', 'D', 'E', 'IO', 'ZH', 'Z', 'I', 'Y',
            'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F',
            'H', 'TS', 'CH', 'SH', 'SCH', '', 'Y', '', 'E', 'YU', 'IA',
            'a', 'b', 'v', 'g', 'd', 'e', 'io', 'zh', 'z', 'i', 'y',
            'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f',
            'h', 'ts', 'ch', 'sh', 'sch', '', 'y', '', 'e', 'yu', 'ia',
            '-', '-', '-', '-', '-', '-', '-', '-'
        );

        $text = str_replace($rus_alphabet, $rus_alphabet_translit, $text);
        $text = str_replace('--', '-', $text);
        $text = trim($text, '-');
        $text = strtolower($text);

        return $text;
    }
}
