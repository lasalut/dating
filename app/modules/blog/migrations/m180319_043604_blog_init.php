<?php

use yii\db\Migration;

/**
 * Class m180319_043604_blog_init
 */
class m180319_043604_blog_init extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%blogs}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(500),
            'description' => $this->text(),
            'content' => $this->text(),
            'url' => $this->string(),
            'seo_title' => $this->string(500),
            'seo_description' => $this->text(),
            'seo_keywords' => $this->text(),
            'admin_id' => $this->integer(),
            'category' => $this->string(),
            'enabled' => $this->boolean()->defaultValue(true),
            'crop_name' => $this->string(),
            'middle_name' => $this->string(),
            'photo_name' => $this->string(),
            'dated_at' => $this->date(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%blogs}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180319_043604_blog_init cannot be reverted.\n";

        return false;
    }
    */
}
