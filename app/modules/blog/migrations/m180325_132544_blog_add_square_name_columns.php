<?php

use yii\db\Migration;

/**
 * Class m180325_132544_blog_add_square_name_columns
 */
class m180325_132544_blog_add_square_name_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%blogs}}', 'square_name', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%blogs}}', 'square_name');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180325_132544_blog_add_square_name_columns cannot be reverted.\n";

        return false;
    }
    */
}
