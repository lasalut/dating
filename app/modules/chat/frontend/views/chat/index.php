<?php

/**
 * @var \yii\web\View $this
 * @var \modules\profiles\common\models\Profile $profile
 * @var string $conversations
 * @var string $with
 * @var \modules\profiles\common\models\Profile $withUser
 */

use modules\chat\frontend\assets\ChatAsset;

$this->title = 'Чат';
ChatAsset::register($this);

$withName = !empty($withUser) ? $withUser->name : '';
$withAvatar = !empty($withUser) ? $withUser->avatar_path : '';
$ws = getenv('WS');

$js = <<<JS
	const userId = '{$profile->id}';
	const userName = '{$profile->name}';
	const userAvatar = '{$profile->avatar_path}';
	const withId = '{$with}';
	const withName = '{$withName}';
	const withAvatar = '{$withAvatar}';
	const conversations = '{$conversations}';
	const ws = '{$ws}';
JS;
$this->registerJs($js, \yii\web\View::POS_HEAD);
?>

<h1 class="main center">Мои сообщения</h1>

<div class="row <?= $with ? 'with' : 'without' ?>" id="chat">
	<div class="mobile-title mobile-dialogs">Мои сообщения</div>
	<div class="mobile-title mobile-messages" v-on:click="showDialogs()">
		<i class="fa fa-arrow-left"></i> Все диалоги
	</div>

	<div class="col-md-4" id="chat-users" v-show="conversations.length > 0">
		<div id="chat-search">
			<input type="text" placeholder="Поиск" v-model="search" autocomplete="off"/>
		</div>
		<ul>
			<li v-for="conv in conversations" class="chat-with container"
				v-on:click="setChat(conv)" v-cloak
				v-bind:class="{'active': active_user == conv.w.id}"
				v-show="!search.length || conv.w.name.toLowerCase().indexOf(search.trim().toLowerCase()) !== -1">
				<div class="row chat-with-row">
					<div class="col-md-2 chat-with-avatar ua" v-bind:data-ua="conv.w.id">
						<a v-bind:href="'/profile/' + conv.w.id" v-on:click="" target="_blank">
							<img :src="conv.w.avatar"/>
						</a>
					</div>
					<div class="col-md-10 chat-with-info">
						<div class="chat-with-title">
							<span class="user-name">{{conv.w.name}}, {{conv.w.age}}</span>
							<span class="pull-right lastmsg-time" v-if="conv.lastMessage">
								{{conv.lastMessage.created | dated }}
							</span>
						</div>
						<div v-if="conv.lastMessage">
							<span class="lastmsg-message" v-if="conv.lastMessage.userFromId == my_id">
								<span v-if="conv.lastMessage.winked == '1'">
									Вы помахали {{conv.lastMessage.userToName}}!
								</span>
								<span v-else>
									<span class="lastmsg-who">Вы:</span>
									{{ conv.lastMessage.message | truncate(30) }}
								</span>
							</span>
							<span class="lastmsg-message" v-else>
								<span v-if="conv.lastMessage.winked == '1'">
									машет вам!
								</span>
								<span v-else>
									{{ conv.lastMessage.message | truncate(34) }}
								</span>

							</span>
							<span class="badge alert-info pull-right" v-if="conv.newMessages > 0">
								{{conv.newMessages}}
							</span>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>

	<div class="col-md-8 col-messages-box">
		<div class="scrollbar-inner" id="messages-box">
			<div class="messages" v-for="conv in conversations" v-show="active_user == conv.w.id" v-cloak
				 v-bind:id="'messages-' + conv.w.id">
				<ul>
					<li class="container" v-if="showWink(conv)">
						<div class="row message wink" v-on:click="postMessage(true)">
							<div class="col-md-1 wink-img">
								<img src="/images/wave.png">
							</div>
							<div class="col-md-11">"Salute" вместо тысячи слов и неуклюжих приветствий. Отправь
								собеседнику "Salute", чтобы начать диалог и избежать стеснений
							</div>
						</div>
					</li>
					<li v-for="message in conv.messages" class="container">
						<div class="row message" v-bind:class="{'readed': message.readed == '1'}">
							<div class="col-md-2 message-avatar">
								<img :src="message.userFromId == my_id ? my_avatar : conv.w.avatar"
									 v-on:click="scrollChat()"/>
							</div>
							<div class="col-md-10">
								<div class="message-header">
									<a v-bind:href="'/profile/' + message.userFromId"
									   class="message-name" target="_blank">
										{{message.userFromName}}
									</a>
									<span class="message-time">{{message.created | msg-time }}</span>
								</div>
								<div class="message-message">
									{{ message.winked == '1'
									? (message.userFromId == my_id
									? 'Вы помахали ' + message.userToName + '!'
									: 'машет вам!')
									: message.message }}
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div class="message-sendbox" v-show="active_user">
			<input v-model="post_message" autocomplete="off"
				   v-on:keyup.enter="postMessage()" placeholder="Введите сообщение"
				   class="post-message form-control" id="post-message"/>
			<i class="post-btn fa fa-send" v-on:click="postMessage()"></i>
		</div>
	</div>
</div>
