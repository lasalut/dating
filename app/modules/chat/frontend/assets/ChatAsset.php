<?php

namespace modules\chat\frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class ChatAsset extends AssetBundle
{
    public $sourcePath = '@modules/chat/frontend/assets';

    public $css = [
        'css/chat.css'
    ];
    public $js = [
        'js/chat.js',
    ];

    public $depends = [
        'frontend\assets\AppAsset',
        'frontend\assets\VueJsAsset',
        'frontend\assets\NicescrollAsset',
    ];

    public function registerAssetFiles($view)
    {
        $view->registerJs("var connOnlines = true;", View::POS_HEAD, __CLASS__);
        return parent::registerAssetFiles($view);
    }
}