$(document).ready(function() {
	var $mb = $("#messages-box");
	var $cu = $("#chat-users");
	const maxHeight = 500;

	const resizeDiv = function(object) {
		var height = $(window).height() - $('#header').height() - 100;
		if (height > maxHeight) {
			height = maxHeight;
		}
		object.height(height);
	};

	$(window).bind("resize", function() {
		resizeDiv($mb);
		resizeDiv($cu);
	});

	$mb.niceScroll({cursorcolor: "#bac3d8", cursorwidth: "8px"});
	$cu.niceScroll({cursorcolor: "#bac3d8", cursorwidth: "8px"});

	setTimeout(function() {
		resizeDiv($mb);
		resizeDiv($cu);
	}, 50);
});

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

const monthNames = ["янв", "фев", "мар", "апр", "мая", "июня", "июля", "авг", "сен", "окт", "ноя", "дек"];

Vue.filter('truncate', function(text, stop, clamp) {
	return text && text.length ? text.slice(0, stop) + (stop < text.length ? clamp || '...' : '') : '';
});

Vue.filter('dated', function(dateTime) {
	const date = new Date(dateTime);
	var inputDate = new Date(dateTime);
	var today = new Date();
	var yesterday = new Date();
	yesterday.setDate(yesterday.getDate() - 1);

	// call setHours to take the time out of the comparison
	if (inputDate.setHours(0, 0, 0, 0) == today.setHours(0, 0, 0, 0)) {
		return date.getHours() + ':' + (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
	}
	else if (date < today.setHours(0, 0, 0, 0) && date > yesterday.setHours(0, 0, 0, 0)) {
		return 'вчера';
	}
	else {
		return date.getUTCDate() + ' ' + monthNames[date.getUTCMonth()];
	}
});

Vue.filter('msg-time', function(dateTime) {
	const date = new Date(dateTime);

	return date.getHours() + ':' + (date.getMinutes() < 10 ? '0' : '') + date.getMinutes()
		+ ', ' + date.getUTCDate() + ' ' + monthNames[date.getUTCMonth()];
});

Array.prototype.move = function(old_index, new_index) {
	if (new_index >= this.length) {
		var k = new_index - this.length;
		while ((k--) + 1) {
			this.push(undefined);
		}
	}
	this.splice(new_index, 0, this.splice(old_index, 1)[0]);
	return this; // for testing purposes
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
var menuCounterInterval = null;
var oldTitle = document.title;

var vm = new Vue({
	el:       "#chat",
	data:     {
		conversations: JSON.parse(conversations),
		post_message:  null,
		search:  '',
		active_user:   withId,
		my_id:         userId,
		my_name:       userName,
		my_avatar:     userAvatar,
		conn:          null,
		online_ids:    []
	},
	computed: {
		search_conversations: function() {
			setTimeout(function() {
				return vm.conversations.filter(function(conv) {
					return true;
					if (!vm.chat_search || vm.chat_search.length == 0) {
						return true;
					}
					var name_search = vm.chat_search.toLowerCase();
					var name = conv.w.name.toLowerCase();
					return name.indexOf(name_search) !== -1;
				});
			}, 1000);
		}
	},
	mounted:  function() {
		if (withId.length) {
			// ждем инициализации VM и сокета соединения, проверяем каждые 100мс
			const myInterval = setInterval(function() {
				if (!vm.conn) {
					return;
				}
				// прокручиваем чат сообщений вниз
				vm.scrollChatMessages(50);
				// сбрасываем интервал, когда сокетное соединение установлено
				clearInterval(myInterval);
				// если диалог пуст, то создаем его
				if (vm.conversations.length == 0) {
					vm.beginChat(withId, withName, withAvatar);
				}

				// сбрасываем счетчик непрочитанных сообщений в локальном скрипте
				if (vm.conversations.length) {
					var hasChat = false;
					for (var i = 0; i < vm.conversations.length; i++) {
						if (vm.conversations[i].w.id == withId) {
							// vm.readedChat(vm.conversations[i]);
							hasChat = true;
							break;
						}
					}
					if (hasChat === false) {
						vm.beginChat(withId, withName, withAvatar);
					}
				}
				// сбрасываем счетчик непрочитанных сообщений пушем через сокет в базу данных
				vm.publishChatReaded(withId);
			}, 100);
		}
	},
	methods:  {
		clearInput:         function() {
			vm.post_message = null;
		},
		focusInput:         function(delay) {
			delay = delay || null;
			delay ? setTimeout(function() { $('#post-message').focus(); }, delay) : $('#post-message').focus();
		},
		readedChat:         function(conv, resetNewMessages, setChat) {
			setTimeout(function() {
				resetNewMessages = resetNewMessages || true;
				setChat = setChat || false;

				for (var i = 0; i < conv.messages.length; i++) {
					if (setChat) {
						if (conv.messages[i].userToId == userId) {
							conv.messages[i].readed = '1';
						}
					}
					else {
						conv.messages[i].readed = '1';
					}
				}
				if (resetNewMessages) {
					conv.newMessages = 0;
				}
			}, 1000);
		},
		scrollChatMessages: function(delay) {
			delay = delay || null;
			delay ? setTimeout(function() { $('#messages-box').scrollTop(1E10); }, delay) : $('#messages-box').scrollTop(1E10);
			vm.focusInput();
		},
		scrollChatUsers:    function(delay) {
			delay = delay || null;
			delay ? setTimeout(function() { $('#chat-users').scrollTop(0); }, delay) : $('#chat-users').scrollTop(0);
		},
		onEnterClick:       function(conv) {
			vm.postMessage(conv);
		},
		reinitNicescroll:   function() {
			var $mb = $("#messages-box");
			$mb.getNiceScroll().remove();
			$mb.niceScroll({cursorcolor: "#bac3d8", cursorwidth: "8px"});
		},
		rearrangeChatUsers: function(top_user_id) {
			var i;
			for (i = 0; i < vm.conversations.length; i++) {
				if (vm.conversations[i].w.id == top_user_id) {
					break;
				}
			}
			if (i > 0) {
				vm.conversations.move(i, 0);
			}
		},
		showWink:           function(conv) {
			var show = true;
			for (var i = 0; i < conv.messages.length; i++) {
				if (conv.messages[i].userFromId == userId) {
					show = false;
				}
			}
			return show;
		},
		postMessage:        function(winked) {
			winked = winked || null;
			var conv = vm.getConv();
			var data = {'message': $('#post-message').val(), 'to': vm.active_user, 'winked': winked};

			$.ajax({
				type:     "POST",
				url:      '/chat/chat/message',
				data:     data,
				dataType: 'json',
				success:  function(response) {
					conv.messages.push(response.message);
					vm.clearInput();
					vm.reinitNicescroll();
					vm.scrollChatMessages(50);
					vm.rearrangeChatUsers(vm.active_user);
					conv.lastMessage = response.message;
				},
				error:    function(response) {
					var data = response.responseJSON;
					if (data.type == 'premium') {
						$('#premiumForm').modal('show');
					}
					else {
						$.notify(data.error, {className: 'info', autoHideDelay: 7000});
					}
				}
			});
		},
		updateURL:          function() {
			// если поддерживается история браузера, то при переключении чата мы меняем и URL-адрес
			if (history.pushState) {
				var baseUrl = window.location.protocol + "//" + window.location.host;
				var newurl = this.active_user ? baseUrl + '/chat?with=' + vm.active_user : baseUrl + '/chat';

				window.history.pushState({path: newurl}, '', newurl);
			}
		},
		getConv:            function() {
			for (var i = 0; i < vm.conversations.length; i++) {
				if (vm.conversations[i].w.id == vm.active_user) {
					return vm.conversations[i];
				}
			}
			return null;
		},
		setChat:            function(conv) {
			vm.active_user = conv.w.id;
			vm.updateURL();
			vm.focusInput(50);
			vm.reinitNicescroll();
			vm.scrollChatMessages(50);
			// выставляем прочитанными все сообщения в этом чате, затем публикуем на сервер о том, что прочитали
			vm.subMessageCounter(conv.newMessages);
			vm.readedChat(conv, true, true);
			vm.publishChatReaded(conv.w.id);
			$('#chat').removeClass('without').addClass('with');
		},
		beginChat:          function(user_id, user_name, user_avatar) {
			const w = {'id': user_id, 'name': user_name, 'avatar': user_avatar};
			vm.conversations.unshift({'w': w, 'messages': [], 'lastMessage': null, 'newMessages': 0});
			vm.active_user = user_id;
			vm.updateURL();
			vm.focusInput(50);
		},
		publishChatReaded:  function(with_user_id) {
			var data = {'type': 'readed', 'withId': with_user_id, 'userId': userId};
			vm.conn.publish('dating.chat', data);
		},
		addMessageCounter:  function() {
			var $messageCounter = $('#messages-counter');
			var msgCounter = $messageCounter.html().length
				? parseInt($messageCounter.html())
				: 0;
			var newCounter = msgCounter + 1;
			$messageCounter.html(newCounter);
			$messageCounter.show();

			var newTitle;
			var i = 0;

			if (newCounter == 1) {
				newTitle = '1 новое сообщение';
			}
			else if (newCounter > 1 && newCounter < 5) {
				newTitle = newCounter + ' новых сообщения';
			}
			else {
				newTitle = newCounter + ' новых сообщений';
			}

			menuCounterInterval = setInterval(function() {
				i++;
				document.title = i % 2 ? newTitle : oldTitle;
			}, 1500);
		},
		subMessageCounter:  function(counter) {
			counter = counter || 1;
			var $messageCounter = $('#messages-counter');
			var msgCounter = $messageCounter.html().length
				? parseInt($messageCounter.html())
				: 0;
			if (msgCounter - counter <= 0) {
				$messageCounter.hide();
				$messageCounter.html('');
				if (menuCounterInterval) {
					clearInterval(menuCounterInterval);
					document.title = oldTitle;
				}
			}
			else {
				$messageCounter.html(msgCounter - counter);
			}
		},
		showDialogs: function() {
			$('#chat').removeClass('with').addClass('without');
			this.active_user = null;
			this.updateURL();
		}
	}
});

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

var conn = new ab.Session(ws + ':8080',
	function() {
		vm.conn = conn;
		conn.subscribe('all', function(subscriber, data) {
			if (data.type === 'online') {
				$('ua-' + data.object.userId).addClass('ua-online');
			}
		});

		conn.subscribe(userId, function(subscriber, data) {
			// приняли сигнал, что чат прочитан другим участником
			if (data.type === 'readed') {
				var obj = data.object;
				for (var i = 0; i < vm.conversations.length; i++) {
					if (vm.conversations[i].w.id == obj.withUserId) {
						vm.readedChat(vm.conversations[i], false);
					}
				}
			}

			// приняли входящее сообщение от другого участника
			if (data.type === 'message') {
				const message = data.object;
				const userFromId = message.userFromId;
				var hasConversation = false;

				// диалог найден - добавляем в чат, выставляем последнее сообщение в нем
				if (vm.conversations.length) {
					for (var i = 0; i < vm.conversations.length; i++) {
						if (vm.conversations[i].w.id == userFromId) {
							vm.conversations[i]['messages'].push(message);
							vm.conversations[i].lastMessage = message;

							// если уже открыт чат с этим участником
							if (vm.active_user == userFromId) {
								// обновляем прокрутку чата, закрываем счетчик новых сообщений
								vm.reinitNicescroll();
								vm.scrollChatMessages(50);
								vm.readedChat(vm.conversations[i]);
								vm.publishChatReaded(userFromId);
							}
							else {
								vm.addMessageCounter();
								vm.conversations[i].newMessages = parseInt(vm.conversations[i].newMessages) + 1;
							}
							hasConversation = true;
							break;
						}
					}
				}

				// когда диалог не найден, необходимо создать его структуру
				if (!hasConversation) {
					var w = {'id': userFromId, 'name': message.userFromName, 'avatar': message.userFromAvatar};
					var conv = {'w': w, 'messages': [message], 'lastMessage': message, 'newMessages': 1};
					vm.conversations.unshift(conv);
					vm.reinitNicescroll();
					vm.scrollChatMessages(50);
					vm.addMessageCounter();
				}

				vm.rearrangeChatUsers(userFromId);
			}
		});
	},
	function() {
		console.warn('WebSocket connection closed');
	},
	{'skipSubprotocolCheck': true}
);