<?php

namespace modules\chat\console\models;

use ZMQ;
use ZMQContext;
use ZMQSocket;

class ZmqService
{
    const TYPE_MESSAGE = 'message';
    const TYPE_ONLINE = 'online'; # онлайн одного участника
    const TYPE_ONLINES = 'onlines'; # онлайн всех участников
    const TYPE_WEBRTC = 'webrtc'; # позвал в комнату общения
    const TYPE_READED = 'readed'; # прочитал сообщение
    const TYPE_VISIT = 'visit'; # заходил на страницу
    const TYPE_LIKE = 'like'; # лайкнул

    const SUBSCRIBER_ALL = 'all';

    /**
     * @param string $subscriber
     * @param string $type
     * @param array $object
     * @param integer $port
     *
     * @return ZMQSocket
     */
    public static function publish($subscriber, $type, array $object = [], $port = 5555)
    {
        $context = new ZMQContext();
        $socket = $context->getSocket(ZMQ::SOCKET_PUSH, 'sf4');
        $socket->connect("tcp://localhost:" . $port);

        $data = [
            'subscriber' => $subscriber,
            'type' => $type,
            'object' => $object
        ];

        if (getenv('YII_ENV') == 'dev') {
            \Yii::info('... ZmqService on tcp://localhost:' . $port . ' :: '
                . json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
        }

        return $socket->send(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
    }
}