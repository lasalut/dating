<?php

namespace modules\chat\console\models;

use modules\chat\common\models\ChatCounter;
use modules\chat\common\models\ChatMessage;
use modules\profiles\common\models\Profile;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampServerInterface;

class Chat implements WampServerInterface
{
    /**
     * A lookup of all the topics clients have subscribed to
     */
    protected $subscribedTopics = array();

    public function onSubscribe(ConnectionInterface $conn, $message)
    {
        $this->subscribedTopics[$message->getId()] = $message;
    }

    /**
     * @param string JSON'ified string we'll receive from ZeroMQ
     */
    public function onEvent($entry)
    {
        $entryData = json_decode($entry, true);

        if (!isset($entryData['subscriber']) || !array_key_exists($entryData['subscriber'], $this->subscribedTopics)) {
            return;
        }

        $message = $this->subscribedTopics[$entryData['subscriber']];

        // re-send the data to all the clients subscribed to that category
        $message->broadcast($entryData);
    }

    /* The rest of our methods were as they were, omitted from docs to save space */

    public function onUnSubscribe(ConnectionInterface $conn, $message)
    {
    }

    public function onOpen(ConnectionInterface $conn)
    {
    }

    public function onClose(ConnectionInterface $conn)
    {
    }

    public function onCall(ConnectionInterface $conn, $id, $topic, array $params)
    {
        // In this application if clients send data it's because the user hacked around in console
        $conn->callError($id, $topic, 'You are not allowed to make calls')->close();
    }

    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible)
    {
        if (!is_array($event) || empty($event['type'])) {
            return;
        }

        if ($event['type'] == 'readed' && !empty($event['userId']) && !empty($event['withId'])) {
            $userId = $event['userId']; # это тот, который из своего браузера послал событие прочитанного сообщения
            $withId = $event['withId']; # с ним он ведет беседу
            ChatCounter::updateAll(['counter' => 0], ['to_id' => $userId, 'from_id' => $withId]); # сброс счетчика прочитанных
            ZmqService::publish($withId, ZmqService::TYPE_READED, ['withUserId' => $userId]); # отправить уведомление тому, с кем беседа
            ChatMessage::updateAll(['readed' => true], ['from_id' => $withId, 'to_id' => $userId]); # обновить флаг прочитанных сообщений в базе данных в беседе
        }
        elseif ($event['type'] == 'online' && !empty($event['userId'])) {
            /** @var Profile $profile */
            if ($profile = Profile::findOne($event['userId'])) {
                $profile->updateOnlineAt();
            }
        }

        // In this application if clients send data it's because the user hacked around in console
        //$conn->close();
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
    }
}