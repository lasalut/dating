<?php

namespace modules\chat\console\controllers;

use console\base\Controller;
use modules\chat\console\models\Chat;
use yii\mutex\FileMutex;

class StopController extends Controller
{
    public function actionIndex()
    {
        system("pkill -f chat/run");
        
        echo '... chat stopped' . PHP_EOL;
    }
}