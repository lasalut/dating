<?php

namespace modules\chat\console\controllers;

use console\base\Controller;
use modules\chat\console\models\Chat;
use yii\mutex\FileMutex;

class RunController extends Controller
{
    public function actionIndex()
    {
        $mutex = new FileMutex();
        if ($mutex->acquire(__METHOD__) === false) {
            $this->stdout("Another instance is running...\n");
            return self::EXIT_CODE_NORMAL;
        }

        $this->runChat();

        $mutex->release(__METHOD__);
        return self::EXIT_CODE_NORMAL;
    }

    private function runChat()
    {
        $loop = \React\EventLoop\Factory::create();
        $chat = new Chat();

        // Listen for the web server to make a ZeroMQ push after an ajax request
        $context = new \React\ZMQ\Context($loop);
        /** @var \React\ZMQ\SocketWrapper $pull */
        $pull = $context->getSocket(\ZMQ::SOCKET_PULL);
        $pull->bind('tcp://127.0.0.1:5555'); // Binding to 127.0.0.1 means the only client that can connect is itself
        $pull->on('message', array($chat, 'onEvent'));

        // Set up our WebSocket server for clients wanting real-time updates
        $webSock = new \React\Socket\Server('0.0.0.0:8080', $loop); // Binding to 0.0.0.0 means remotes can connect
        $webServer = new \Ratchet\Server\IoServer(
            new \Ratchet\Http\HttpServer(
                new \Ratchet\WebSocket\WsServer(
                    new \Ratchet\Wamp\WampServer(
                        $chat
                    )
                )
            ),
            $webSock
        );

        $loop->run();

        echo '=> chat started' . PHP_EOL;
    }
}