<?php

use yii\db\Migration;

/**
 * Class m180113_105900_chat_init
 */
class m180113_105900_chat_init extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8mb4 DEFAULT COLLATE utf8mb4_unicode_ci';
        }

        # сообщения чата
        $this->createTable('{{%chat_messages}}', [
            'id' => $this->primaryKey(),
            'message' => $this->text(),
            'from_id' => $this->integer(),
            'to_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'model' => $this->string(10)->defaultValue('profile'),
            'removed' => $this->boolean()->defaultValue(false),
        ], $tableOptions);

        $this->createIndex('ix_from_id', '{{%chat_messages}}', 'from_id');
        $this->createIndex('ix_to_id', '{{%chat_messages}}', 'to_id');

        # счетчики новых сообщений
        $this->createTable('{{%chat_counters}}', [
            'id' => $this->primaryKey(),
            'counter' => $this->integer()->defaultValue(0),
            'from_id' => $this->integer(),
            'to_id' => $this->integer(),
            'model' => $this->string(10)->defaultValue('profile'),
        ], $tableOptions);

        $this->createIndex('ix_from_id', '{{%chat_counters}}', 'from_id');
        $this->createIndex('ix_to_id', '{{%chat_counters}}', 'to_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%chat_messages}}');
        $this->dropTable('{{%chat_counters}}');
    }
}
