<?php

use yii\db\Migration;

/**
 * m180120_082229_chat_message_add_winked_column
 */
class m180120_082229_chat_message_add_winked_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%chat_messages}}', 'winked', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%chat_messages}}', 'winked');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180120_082228_chat_message_add_readed_column cannot be reverted.\n";

        return false;
    }
    */
}
