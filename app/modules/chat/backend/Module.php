<?php

namespace modules\chat\backend;

use yz\icons\Icons;

/**
 * Class Module
 */
class Module extends \modules\chat\common\Module
{
    public function getAdminMenu()
    {
        return [
            [
                'route' => ['/chat/chat-messages/index'],
                'label' => 'Сообщения',
                'icon' => Icons::o('comments'),
            ],
        ];
    }

}