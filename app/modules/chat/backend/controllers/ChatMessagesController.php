<?php

namespace modules\chat\backend\controllers;

use modules\profiles\common\models\Profile;
use Yii;
use modules\chat\common\models\ChatMessage;
use modules\chat\backend\models\ChatMessageSearch;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yz\admin\actions\ExportAction;
use yz\admin\widgets\ActiveForm;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\admin\contracts\AccessControlInterface;

/**
 * ChatMessagesController implements the CRUD actions for ChatMessage model.
 */
class ChatMessagesController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessControl' => $this->accessControlBehavior(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::className(),
                'searchModel' => function ($params) {
                    /** @var ChatMessageSearch $searchModel */
                    return Yii::createObject(ChatMessageSearch::className());
                },
                'dataProvider' => function ($params, ChatMessageSearch $searchModel) {
                    $dataProvider = $searchModel->search($params);
                    return $dataProvider;
                },
            ]
        ]);
    }

    /**
     * Lists all ChatMessage models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var ChatMessageSearch $searchModel */
        $searchModel = Yii::createObject(ChatMessageSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(ChatMessageSearch $searchModel)
    {
        return [
            'id',
            [
                'attribute' => 'message',
                'format' => 'raw',
                'contentOptions' => ['style' => 'width:30%'],
            ],
            [
                'attribute' => 'from__name',
                'format' => 'raw',
                'contentOptions' => ['style' => 'width:20%'],
                'value' => function (ChatMessage $model) {
                    return $this->renderFile('@modules/profiles/backend/views/_profile.php', [
                        'profile_id' => $model->from_id,
                    ]);
                }
            ],
            [
                'attribute' => 'to__name',
                'format' => 'raw',
                'contentOptions' => ['style' => 'width:20%'],
                'value' => function (ChatMessage $model) {
                    return $this->renderFile('@modules/profiles/backend/views/_profile.php', [
                        'profile_id' => $model->to_id,
                    ]);
                }
            ],
            'created_at:datetime',
            'winked:boolean',
            'readed:boolean',
        ];
    }

    /**
     * Creates a new ChatMessage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ChatMessage;

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
            return $this->getCreateUpdateResponse($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ChatMessage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
            return $this->getCreateUpdateResponse($model);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ChatMessage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array) $id;

        foreach ($id as $id_) {
            $this->findModel($id_)->delete();
        }

        \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the ChatMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return ChatMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ChatMessage::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
