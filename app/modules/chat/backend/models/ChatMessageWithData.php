<?php

namespace modules\chat\backend\models;

use modules\chat\common\models\ChatMessage;
use modules\profiles\common\models\City;
use modules\profiles\common\models\Profile;
use yii\db\ActiveQuery;
use yz\admin\search\WithExtraColumns;

class ChatMessageWithData extends ChatMessage
{
    use WithExtraColumns;

    protected static function extraColumns()
    {
        return [
            'from__name',
            'to__name',
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), static::extraColumns());
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'from__name' => 'Отправил',
            'to__name' => 'Получил',
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public static function find()
    {
        return parent::find()
            ->select(static::selectWithExtraColumns(['msg.*']))
            ->from(['msg' => self::tableName()])
            ->leftJoin(['from' => Profile::tableName()], 'from.id = msg.from_id')
            ->leftJoin(['to' => Profile::tableName()], 'to.id = msg.to_id')
            ;
    }
}