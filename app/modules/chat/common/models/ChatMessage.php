<?php

namespace modules\chat\common\models;

use modules\profiles\common\models\Profile;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Query;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_chat_messages".
 *
 * @property integer $id
 * @property string $message
 * @property integer $from_id
 * @property integer $to_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $model
 * @property integer $removed
 * @property boolean $readed
 * @property boolean $winked
 */
class ChatMessage extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%chat_messages}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Сообщение чата';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Сообщения чата';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['message', 'string'],
            ['from_id', 'integer'],
            ['to_id', 'integer'],
            ['created_at', 'safe'],
            ['updated_at', 'safe'],
            ['model', 'string', 'max' => 10],
            ['removed', 'safe'],
            ['readed', 'safe'],
            ['winked', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message' => 'Сообщение',
            'from_id' => 'От',
            'to_id' => 'Кому',
            'created_at' => 'Отправлено',
            'updated_at' => 'Обновлено',
            'model' => 'Модель',
            'removed' => 'Удалено',
            'readed' => 'Прочитано',
            'winked' => 'Подмигнул',
        ];
    }

    public function fields()
    {
        /** @var Profile $userFrom */
        $userFrom = Profile::findOne($this->from_id);
        /** @var Profile $userTo */
        $userTo = Profile::findOne($this->to_id);

        return [
            'id',
            'message',
            'readed',
            'winked',
            'userFromId' => function (ChatMessage $model) use ($userFrom, $userTo) {
                return $userFrom->id;
            },
            'userFromName' => function (ChatMessage $model) use ($userFrom, $userTo) {
                return $userFrom->name;
            },
            'userFromAvatar' => function (ChatMessage $model) use ($userFrom, $userTo) {
                return $userFrom->avatar_path;
            },
            'userFromAge' => function (ChatMessage $model) use ($userFrom, $userTo) {
                $now = new \DateTime('now');
                $birthday = new \DateTime($userFrom->birthday_on);
                $diff = $birthday->diff($now);
                return intval($diff->y);
            },
            'userFromGender' => function (ChatMessage $model) use ($userFrom, $userTo) {
                return $userFrom->my_gender;
            },
            'userToId' => function (ChatMessage $model) use ($userFrom, $userTo) {
                return $userTo->id;
            },
            'userToName' => function (ChatMessage $model) use ($userFrom, $userTo) {
                return $userTo->name;
            },
            'userToAvatar' => function (ChatMessage $model) use ($userFrom, $userTo) {
                return $userTo->avatar_path;
            },
            'userToAge' => function (ChatMessage $model) use ($userFrom, $userTo) {
                $now = new \DateTime('now');
                $birthday = new \DateTime($userTo->birthday_on);
                $diff = $birthday->diff($now);
                return intval($diff->y);
            },
            'userToGender' => function (ChatMessage $model) use ($userFrom, $userTo) {
                return $userTo->my_gender;
            },
            'created' => 'created_at',
        ];
    }

    public static function findConversations($profile_id)
    {
        $conversations = [];

        # находим счетчик новых сообщений переписки
        $counters = ChatCounter::find()->where(['to_id' => $profile_id])
            ->indexBy('from_id')->select('counter, from_id')->column();

        # находим все сообщения переписки для участника
        $rows = (new Query())
            ->select('m.message, m.created_at as created, m.id, m.readed, m.winked,
              userFrom.id userFromId, userFrom.name userFromName, userFrom.avatar userFromAvatar, 
              userFrom.my_gender userFromGender, userFrom.birthday_on userFromBirthday,
              userTo.id userToId, userTo.name userToName, userTo.avatar userToAvatar,
              userTo.my_gender userToGender, userTo.birthday_on userToBirthday
              ')
            ->from(['m' => ChatMessage::tableName()])
            ->innerJoin(['userFrom' => Profile::tableName()], 'userFrom.id = m.from_id')
            ->innerJoin(['userTo' => Profile::tableName()], 'userTo.id = m.to_id')
            ->where('userFrom.id = :userId OR userTo.id = :userId', ['userId' => $profile_id])
            ->orderBy(['m.created_at' => SORT_DESC])
            ->all();

        foreach ($rows as $row) {
            $key = ($row['userToId'] == $profile_id) ? $row['userFromId'] : $row['userToId'];
            if (!isset($conversations[$key])) {
                if ($row['userToId'] == $profile_id) {
                    $now = new \DateTime('now');
                    $birthday = new \DateTime($row['userFromBirthday']);
                    $diff = $birthday->diff($now);
                    $age = intval($diff->y);
                    $with = [
                        'id' => $row['userFromId'],
                        'name' => $row['userFromName'],
                        'age' => $age,
                        'gender' => $row['userFromGender'],
                        'avatar' => empty($row['userFromAvatar'])
                            ? ($row['userFromGender'] == 'male' ? '/images/icon_male.jpg' : '/images/icon_female.jpg')
                            : '/data/photos/' . $row['userFromAvatar']
                    ];
                }
                else {
                    $now = new \DateTime('now');
                    $birthday = new \DateTime($row['userToBirthday']);
                    $diff = $birthday->diff($now);
                    $age = intval($diff->y);
                    $with = [
                        'id' => $row['userToId'],
                        'name' => $row['userToName'],
                        'age' => $age,
                        'gender' => $row['userToGender'],
                        'avatar' => empty($row['userToAvatar'])
                            ? ($row['userToGender'] == 'male' ? '/images/icon_male.jpg' : '/images/icon_female.jpg')
                            : '/data/photos/' . $row['userToAvatar']
                    ];
                }

                $conversations[$key] = [
                    'w' => $with,
                    'lastMessage' => null,
                    'messages' => [],
                    'newMessages' => 0,
                ];
            }
            $conversations[$key]['messages'][] = $row;
        }

        foreach ($conversations as $userId => &$data) {
            $data['lastMessage'] = $data['messages'][0];
            $data['messages'] = array_reverse($data['messages']);
        }

        # находим счетчик новых сообщений переписки
        if (!empty($counters)) {
            foreach ($counters as $userFromId => $counter) {
                if (isset($conversations[$userFromId])) {
                    $conversations[$userFromId]['newMessages'] = $counter;
                }
            }
        }

        return array_values($conversations);
    }

    public static function countByProfile(Profile $profile)
    {
        $count = self::find()->where(['readed' => false, 'to_id' => $profile->id])->count();

        return intval($count);
    }
}
