<?php

namespace modules\chat\common\models;

use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_chat_counters".
 *
 * @property integer $id
 * @property integer $counter
 * @property integer $from_id
 * @property integer $to_id
 * @property string $model
 */
class ChatCounter extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%chat_counters}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Chat Counter';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Chat Counters';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['counter', 'integer'],
            ['from_id', 'integer'],
            ['to_id', 'integer'],
            ['model', 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'counter' => 'Counter',
            'from_id' => 'From ID',
            'to_id' => 'To ID',
            'model' => 'Model',
        ];
    }

    public static function countMessage(ChatMessage $msg)
    {
        $chatCounter = ChatCounter::findOne(['from_id' => $msg->from_id, 'to_id' => $msg->to_id]);

        if ($chatCounter == null) {
            $chatCounter = new ChatCounter();
            $chatCounter->from_id = $msg->from_id;
            $chatCounter->to_id = $msg->to_id;
            $chatCounter->save(false);
        }

        $chatCounter->updateAttributes(['counter' => $chatCounter->counter + 1]);
    }
}
