<?php

namespace modules\webrtc\common;


/**
 * Class Module
 */
class Module extends \yz\Module
{
    public function getName()
    {
        return 'WebRTC';
    }
}