<?php

namespace modules\webrtc\frontend\controllers;

use modules\chat\console\models\ZmqService;
use modules\profiles\common\models\Profile;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class RoomController extends Controller
{
    public function behaviors()
    {
        return [
            'accessControl' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                    ]
                ]
            ]
        ];
    }

    public function actionInvite($with)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->statusCode = 200;

        /** @var Profile $model */
        $model = Profile::findOne($with);
        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;

        if (empty($model) || empty($profile)) {
            throw new NotFoundHttpException();
        }
        $ids = [$profile->id, $model->id];
        sort($ids);
        $webrtcRoom = implode('+', $ids);

        ZmqService::publish($with, ZmqService::TYPE_WEBRTC, [
            'userId' => $profile->id,
            'userName' => $profile->name,
            'userAvatar' => $profile->avatar_url,
            'userYears' => $profile->years,
            'webrtcRoom' => $webrtcRoom,
        ]);

        return 'OK';
    }

    public function actionIndex($with)
    {
        /** @var Profile $model */
        $model = Profile::findOne($with);
        /** @var Profile $profile */
        $profile = Yii::$app->user->identity;

        if (empty($model) || empty($profile)) {
            throw new NotFoundHttpException();
        }

        $ids = [$profile->id, $model->id];
        sort($ids);
        $webrtcRoom = implode('+', $ids);
        $webrtcUrl = getenv('YII_ENV') == 'dev'
            ? 'http://localhost:8888/'
            : trim(getenv('FRONTEND_WEB'), '/') . ':8888/';

        ZmqService::publish($with, ZmqService::TYPE_WEBRTC, [
            'userId' => $profile->id,
            'userName' => $profile->name,
            'userAvatar' => $profile->avatar_url,
            'userYears' => $profile->years,
            'webrtcRoom' => $webrtcRoom,
        ]);

        return $this->render('index', compact('profile', 'model', 'webrtcRoom', 'webrtcUrl'));
    }
}