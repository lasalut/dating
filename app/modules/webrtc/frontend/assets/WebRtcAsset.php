<?php

namespace modules\webrtc\frontend\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class WebRtcAsset extends AssetBundle
{
    public $sourcePath = '@modules/webrtc/frontend/assets';

    public $css = [
        'css/webrtc.css'
    ];
    public $js = [
        'js/simple-web-rtc-v2.js',
        'js/webrtc.js',
    ];

    public $depends = [
        JqueryAsset::class,
    ];
}