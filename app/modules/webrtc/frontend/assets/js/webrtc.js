$(document).ready(function() {
	$('a.link-invite').click(function (e) {
		e.preventDefault();
		$.getJSON(this.href);
		return false;
	});

	var remotes = document.getElementById('remotes');

	var webrtc = new SimpleWebRTC({
		// the id/element dom element that will hold "our" video
		localVideoEl:     'localVideo',
		// the id/element dom element that will hold remote videos
		remoteVideosEl:   '',
		// immediately ask for camera access
		autoRequestMedia: true,
		url:              webrtcUrl
	});

// we have to wait until it's ready
	webrtc.on('readyToCall', function() {
		// you can name it anything
		webrtc.joinRoom(webrtcRoom);
	});

// a peer video has been added
	webrtc.on('videoAdded', function(video, peer) {
		console.log('video added', peer);
		if (remotes) {
			var container = document.createElement('div');
			container.className = 'videoContainer';
			container.id = 'container_' + webrtc.getDomId(peer);
			container.appendChild(video);

			// suppress contextmenu
			video.oncontextmenu = function() { return false; };
			$('.videoContainer.waiting').hide();
			remotes.appendChild(container);
		}
	});

// a peer video was removed
	webrtc.on('videoRemoved', function(video, peer) {
		console.log('video removed ', peer);
		var remotes = document.getElementById('remotes');
		var el = document.getElementById(peer ? 'container_' + webrtc.getDomId(peer) : 'localScreenContainer');
		if (remotes && el) {
			remotes.removeChild(el);
			$('.videoContainer.waiting').show();
		}
	});

	// mute button
	$('#mute').click(function(e) {
		e.preventDefault();
		var $this = $(this);
		if ($this.find('i').hasClass('fa-volume-up')) {
			$this.find('i').addClass('fa-volume-off').removeClass('fa-volume-up');
			webrtc.mute();
		}
		else {
			$this.find('i').addClass('fa-volume-up').removeClass('fa-volume-off');
			webrtc.unmute();
		}
	});

	// webcam button
	$('#webcam').click(function(e) {
		e.preventDefault();
		var $this = $(this);
		if ($this.find('i').hasClass('fa-eye')) {
			$this.find('i').addClass('fa-eye-slash').removeClass('fa-eye');
			webrtc.pauseVideo();
		}
		else {
			$this.find('i').addClass('fa-eye').removeClass('fa-eye-slash');
			webrtc.resumeVideo();
		}
	});
});