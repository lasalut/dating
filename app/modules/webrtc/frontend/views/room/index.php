<?php

/**
 * @var \yii\web\View $this
 * @var \modules\profiles\common\models\Profile $profile
 * @var \modules\profiles\common\models\Profile $model
 * @var string $webrtcRoom
 * @var string $webrtcUrl
 */

use modules\profiles\common\models\ProfileList;
use modules\webrtc\frontend\assets\WebRtcAsset;
use yii\helpers\Url;

$this->title = 'Видео-конференция';
WebRtcAsset::register($this);

$js = <<<JS
	const webrtcRoom = '{$webrtcRoom}';
	const webrtcUrl = '{$webrtcUrl}';
JS;
$this->registerJs($js, \yii\web\View::POS_HEAD);
?>

<div class="row">
	<div class="col-md-6" id="profile-block">
		<a class="avatar-block" href="<?= Url::to(['/profile/' . $model->id]) ?>">
			<img src="<?= $model->avatar_path ?>">
		</a>
		<h3 class="profile-name ua" data-ua="<?= $model->id ?>"><?= $model->name ?></h3>
		<div class="profile-years">
            <?= $model->years ?>, <?= $model->zodiacal_sign ?>.
            <?php if ($model->city): ?>
                <?= $model->city->country ? $model->city->country->title . ', ' : '' ?>
                <?= $model->city->title ?>
            <?php endif; ?>
		</div>
		<div class="online-text"><?= $model->online ?></div>
		<div class="profile-lists">
			<a href="<?= Url::to(['/chat', 'with' => $model->id]) ?>" class="btn btn-primary">
				Написать
			</a>

            <?php if ($profile->hasFavorite($model->id)): ?>
				<a class="btn link-favorite has" title="Убрать из избранных" data-toggle="tooltip"
				   href="<?= Url::to(['/profiles/profile/list', 'id' => $model->id, 'type' => ProfileList::TYPE_FAVORITE]) ?>">
					<i class="fa fa-star"></i></a>
            <?php else: ?>
				<a class="btn link-favorite" title="Добавить в избранные" data-toggle="tooltip"
				   href="<?= Url::to(['/profiles/profile/list', 'id' => $model->id, 'type' => ProfileList::TYPE_FAVORITE]) ?>">
					<i class="fa fa-star-o"></i></a>
            <?php endif; ?>
            <?php if ($profile->hasBlacklist($model->id)): ?>
				<a class="btn link-blacklist has" title="Убрать из игнора" data-toggle="tooltip"
				   href="<?= Url::to(['/profiles/profile/list', 'id' => $model->id, 'type' => ProfileList::TYPE_BLACKLIST]) ?>">
					<i class="fa fa-ban"></i></a>
            <?php else: ?>
				<a class="btn link-blacklist" title="Добавить в игнор" data-toggle="tooltip"
				   href="<?= Url::to(['/profiles/profile/list', 'id' => $model->id, 'type' => ProfileList::TYPE_BLACKLIST]) ?>">
					<i class="fa fa-ban"></i></a>
            <?php endif; ?>

			<div>
				<a href="<?= Url::to(['/webrtc/room/invite', 'with' => $model->id]) ?>" data-toggle="tooltip"
				   class="btn btn-primary link-invite" title="Пригласить в эту комнату">
					Пригласить
				</a>

                <?= $this->renderFile('@modules/profiles/frontend/views/complain.php', [
                	'profile' => $profile,
					'model' => $model,
					'source' => \modules\profiles\common\models\ProfileComplaint::SOURCE_WEBRTC
				]) ?>
			</div>
		</div>
	</div>
	<div class="col-md-6" id="remotes-block">
		<div id="remotes">
			<div class="videoContainer waiting" style="background:beige">
				В ожидании подключения собеседника...
			</div>
		</div>

		<div class="row web-local">
			<div class="col-md-6">
				<div class="webrtc-controls pull-right">
					<a href="#" id="mute" class="btn btn-default" title="Звук">
						<i class="fa fa-volume-up"></i>
					</a>
					<br/>
					<a href="#" id="webcam" class="btn btn-default" title="Видео">
						<i class="fa fa-eye"></i>
					</a>
				</div>
			</div>
			<div class="col-md-6">
				<video id="localVideo"></video>
			</div>
		</div>

	</div>
</div>
