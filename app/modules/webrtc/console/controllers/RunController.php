<?php

namespace modules\webrtc\console\controllers;

use console\base\Controller;
use Yii;

class RunController extends Controller
{
    public function actionIndex()
    {
        $domain = trim(getenv('FRONTEND_WEB'), '/');
        $url = $domain .':8888/socket.io/';

        $handle = curl_init($url);
        curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);

        if ($httpCode == 400) {
            echo 'Running webrts: server.js is already running' . PHP_EOL;
        }
        else {
            $path = Yii::getAlias("@modules/webrtc/signalmaster/server.js");
            $env = getenv('YII_ENV') == 'dev' ? 'development' : 'production';
            $out = getenv('OS_WINDOWS') == 'true' ? system("node $path") :system("NODE_ENV=$env node $path");
            echo 'Running webrts: ' . $out . PHP_EOL;
        }

        // NODE_ENV=development node /vagrant/app/modules/webrtc/signalmaster/server.js

        curl_close($handle);
    }
}