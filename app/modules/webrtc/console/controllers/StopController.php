<?php

namespace modules\webrtc\console\controllers;

use console\base\Controller;

class StopController extends Controller
{
    public function actionIndex()
    {
        system("pkill -f webrtc/signalmaster");
        system("pkill -f webrtc/run");
    }
}