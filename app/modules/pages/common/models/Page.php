<?php

namespace modules\pages\common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_pages".
 *
 * @property integer $id
 * @property string $url
 * @property string $title
 * @property string $content
 * @property string $seo_title
 * @property string $seo_description
 * @property string $css
 * @property string $created_at
 * @property string $updated_at
 */
class Page extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pages}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Страница сайта';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Страницы сайта';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['url', 'required'],
            ['url', 'string', 'max' => 255],
            [['url'], 'unique'],
            ['title', 'required'],
            ['title', 'string'],
            ['content', 'required'],
            ['content', 'string'],
            ['seo_title', 'string'],
            ['seo_description', 'string'],
            ['css', 'string'],
            ['created_at', 'safe'],
            ['updated_at', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'URL адрес страницы',
            'title' => 'Заголовок страницы',
            'content' => 'Контент',
            'seo_title' => 'СЕО-заголовок',
            'seo_description' => 'СЕО-описание',
            'css' => 'Стили CSS',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }
}
