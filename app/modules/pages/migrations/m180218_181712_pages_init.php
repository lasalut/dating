<?php

use yii\db\Migration;

/**
 * Class m180218_181712_pages_init
 */
class m180218_181712_pages_init extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%pages}}', [
            'id' => $this->primaryKey(),
            'url' => $this->string()->unique()->notNull(),
            'title' => $this->string(500)->notNull(),
            'content' => $this->text()->notNull(),
            'seo_title' => $this->string(500),
            'seo_description' => $this->string(500),
            'css' => $this->text(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%pages}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180218_181712_pages_init cannot be reverted.\n";

        return false;
    }
    */
}
