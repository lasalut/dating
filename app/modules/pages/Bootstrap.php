<?php

namespace modules\pages;
use modules\pages\common\models\Page;
use yii\base\Application;
use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        $pages = Page::find()->select('url')->column();

        if (!empty($pages)) {
            foreach ($pages as $url) {
                $app->getUrlManager()->addRules([$url => 'pages/pages/index'], false);
            }
        }
    }
}