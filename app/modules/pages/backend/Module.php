<?php

namespace modules\pages\backend;

use yz\icons\Icons;

/**
 * Class Module
 */
class Module extends \modules\pages\common\Module
{
    public function getAdminMenu()
    {
        return [
            [
                'label' => 'Страницы сайта',
                'icon' => Icons::o('sticky-note'),
                'route' => ['/pages/pages/index'],
            ],
        ];
    }

}