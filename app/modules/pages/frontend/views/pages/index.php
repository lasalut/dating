<?php

/**
 * @var \yii\web\View $this
 * @var \modules\pages\common\models\Page $page
 */


$this->title = $page->title;
?>

<h1><?= $this->title ?></h1>

<div class="page">
    <?= $page->content ?>
</div>