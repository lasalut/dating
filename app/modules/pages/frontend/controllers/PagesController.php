<?php

namespace modules\pages\frontend\controllers;

use modules\chat\console\models\ZmqService;
use modules\pages\common\models\Page;
use modules\profiles\common\models\Profile;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class PagesController extends Controller
{
    public function actionIndex()
    {
        $url = \Yii::$app->request->pathInfo;
        /** @var Page $page */
        $page = Page::findOne(['url' => $url]);

        if ($page === null) {
            throw new NotFoundHttpException();
        }

        return $this->render('index', compact('page'));
    }
}