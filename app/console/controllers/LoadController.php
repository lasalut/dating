<?php
namespace console\controllers;

use modules\profiles\common\models\City;
use modules\profiles\common\models\Dealer;
use modules\profiles\common\models\Profile;
use modules\profiles\common\models\Region;
use ms\loyalty\feedback\common\models\Message;
use Yii;
use yii\console\Controller;

class LoadController extends Controller
{
	public function actionFeedback()
	{
		$raw = \Yii::$app->db->createCommand("
			SELECT * FROM feedbackmessage ORDER BY TS ASC
		")->queryAll();

		foreach ($raw as $i => $r) {
			$ts = (new \DateTime($r['TS']))->format('Y-m-d H:i:s');
			if (strpos($ts, '2017-01-19') !== false) {
				$ts = null;
			}
			$i++;

			$m = new Message();
			$m->id = $i;
			$m->name = $r['UserName'];
			$m->email = $r['Email'];
			$m->phone_mobile_local = $r['Phone'];
			$m->content = str_replace('<br/>', '', $r['Text']);
			$m->dealer = $r['Distributor'];
			$m->city = $r['City'];
			$m->department = $r['DepartmentId'] == '1' ? 'Технической поддержке' : 'GYPROC';
			$m->is_processed = true;
			$m->is_old = true;

			$m->save(false);
			$m->refresh();

			if ($profile = Profile::findOne(['phone_mobile' => $m->phone_mobile])) {
				$m->updateAttributes([
					'user_id' => $profile->id,
					'created_at' => $ts,
					'processed_at' => $ts,
					'updated_at' => $ts,
				]);
			}
		}
	}

	public function actionDealers()
	{
		$models = \Yii::$app->db->createCommand("
			SELECT *
			FROM distributorinfo
		")->queryAll();

		foreach ($models as $model) {
			$dealer = new Dealer();
			$dealer->name = $model['Name'];
			$dealer->save();
		}
	}

	public function actionProfiles()
	{
		$profiles = \Yii::$app->db->createCommand("
			SELECT u.*
			FROM userprofile u
		")->queryAll();
		$total = count($profiles);

		foreach ($profiles as $i => $profile) {
			$i++;
			echo "... $i / $total" . PHP_EOL;
			$region = $this->findRegion($profile['Region']);
			$city = $this->findCity($profile['City'], $region);
			$mediaId = $profile['MediaId'];
			$userId = $profile['UserId'];

			$p = new Profile();
			$p->id = $i;
			$p->first_name = $profile['FirstName'];
			$p->last_name = $profile['LastName'];
			$p->middle_name = $profile['MiddleName'];
			$p->phone_mobile_local = $profile['Phone'];
			$p->birthday_on = empty($profile['BirthDate'])
				? null
				: (new \DateTime($profile['BirthDate']))->format('Y-m-d');
			$p->is_activated = $profile['IsActivated'] ? true : false;
			$p->email = $profile['Email'];
			$p->created_at = empty($profile['CreatedDate'])
				? null
				: (new \DateTime($profile['CreatedDate']))->format('Y-m-d H:i:s');
			$p->is_old = true;
			$p->old_id = $userId;
			$p->gender = $profile['Sex'] == '1' ? Profile::GENDER_MALE : Profile::GENDER_FEMALE;
			$p->city_id = $city->id;
			$p->invited = true;
			$p->invited_at = empty($profile['InviteDate'])
				? null
				: (new \DateTime($profile['InviteDate']))->format('Y-m-d H:i:s');
			$p->delivery_info = empty($profile['DeliveryInfo']) ? null : $profile['DeliveryInfo'];

			# Notifications
			$p->notify = $profile['NotificationTypeId'] == '1'
				? Profile::NOTIFY_SMS_EMAIL
				: ($profile['NotificationTypeId'] == '2' ? Profile::NOTIFY_SMS : Profile::NOTIFY_EMAIL);

			# Avatar
			if (!empty($mediaId)) {
				$avatar = \Yii::$app->db->createCommand("
					SELECT * FROM media WHERE MediaId = {$mediaId}
				")->queryOne();
				if ($avatar) {
					$p->avatar = $avatar['Path'];
				}
			}

			# Documents
			$documents = \Yii::$app->db->createCommand("
				SELECT m.*, ud.DocumentTypeId
				FROM media m
				INNER JOIN userdocument ud ON ud.MediaId = m.MediaId
				WHERE ud.UserId = $userId
			")->queryAll();

			if (!empty($documents)) {
				foreach ($documents as $document) {
					if ($document['DocumentTypeId'] == '1') {
						$p->document_passport = $document['Path'];
					}
					elseif ($document['DocumentTypeId'] == '2') {
						$p->document_passport2 = $document['Path'];
					}
					elseif ($document['DocumentTypeId'] == '3') {
						$p->document_confirm = $document['Path'];
					}
				}
			}

			$p->save(false);
			$p->refresh();
			$p->updateAttributes(['login_code' => $profile['UserName']]);
		}

		$profileOne = Profile::findOne(['login_code' => '791049578225']);
		$profileOne->updateAttributes(['login_code' => 'ASK1NSP']);
	}

	/**
	 * @param $name
	 * @param Region $region
	 * @return City|static
	 */
	private function findCity($name, $region)
	{
		$city = City::findOne(['name' => $name]);

		if ($city == null) {
			$city = new City();
			$city->name = $name;
			$city->region_id = $region ? $region->id : null;
			$city->save();
			$city->refresh();
		}

		return $city;
	}

	/**
	 * @param $name
	 * @return Region|null
	 */
	private function findRegion($name)
	{
		if (empty($name)) {
			return null;
		}

		$region = Region::findOne(['name' => $name]);

		if ($region == null) {
			$region = new Region();
			$region->name = $name;
			$region->save();
			$region->refresh();
		}

		return $region;
	}

	public function actionIndex()
	{
		$region = new Region();
		$region->name = 'Московская область';
		$region->save();
		$region->refresh();

		$city = new City();
		$city->region_id = $region->id;
		$city->name = 'Сергиев Посад';
		$city->save();
		$city->refresh();

		$dealer = new Dealer();
		$dealer->name = 'Дистрибьютор тестовый';
		$dealer->city_id = $city->id;
		$dealer->save();
		$dealer->refresh();
	}

	public function actionTest()
	{
		\Yii::$app->mailer->compose()
			->setFrom('gyproc.corp@yandex.ru')
			->setTo('7binary@bk.ru')
			->setSubject('Message subject')
			->setTextBody('Plain text content')
			->setHtmlBody('<b>HTML content</b>')
			->send();
	}
}