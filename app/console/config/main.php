<?php

return [
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'console\controllers',
    'controllerMap' => [
        'migrate' => [
            'class' => dmstr\console\controllers\MigrateController::class,
            'migrationTable' => '{{%migrations}}',
            'migrationPath' => '@migrations',
        ],
    ],
    'components' => [
        'schedule' => [
            'class' => \marketingsolutions\scheduling\Schedule::class,
        ],
    ],
    'modules' => [
        'mailing' => [
            'class' => \yz\admin\mailer\console\Module::class,
        ],
		'sms' => [
			'class' => modules\sms\console\Module::class,
		],
		'profiles' => [
			'class' => modules\profiles\console\Module::class,
		],
        'chat' => [
            'class' => modules\chat\console\Module::class,
        ],
        'photo' => [
            'class' => modules\photo\console\Module::class,
        ],
        'webrtc' => [
            'class' => modules\webrtc\console\Module::class,
        ],
    ],
    'params' => [
        'yii.migrations' => [
            '@modules/profiles/migrations',
            '@modules/chat/migrations',
            '@modules/photo/migrations',
			'@modules/sms/migrations',
            '@modules/webrtc/migrations',
            '@modules/pages/migrations',
            '@modules/blog/migrations',
        ],
    ],
];
