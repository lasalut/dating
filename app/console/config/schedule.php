<?php
/**
 * @var \marketingsolutions\scheduling\Schedule $schedule
 */

/** E-mail & SMS */
$schedule->command('mailing/mails/send');

/** Photos notify */
$schedule->command('photo/notify')->everyFiveMinutes();
