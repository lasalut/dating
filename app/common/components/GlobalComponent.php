<?php

namespace common\components;

use frontend\controllers\DashboardController;
use modules\chat\console\models\ZmqService;
use ms\loyalty\prizes\payments\frontend\controllers\PaymentsController;
use frontend\controllers\SiteController;
use ms\loyalty\catalog\frontend\controllers\CatalogController;
use ms\loyalty\catalog\frontend\controllers\OrdersController;
use ms\loyalty\ozon\frontend\controllers\CatalogController as OzonCatalogController;
use modules\profiles\frontend\controllers\BonusesTransactionsController;
use modules\profiles\frontend\controllers\ProfileController;
use ms\loyalty\taxes\frontend\controllers\AccountController;
use ms\loyalty\ozon\frontend\controllers\ProductController;
use ms\loyalty\feedback\frontend\controllers\MessagesController;
use modules\profiles\common\models\Profile;
use ms\loyalty\identity\phonesEmails\common\models\Identity;

class GlobalComponent extends \yii\base\Component
{
    public function init()
    {
        /** @var Profile $profile */
        if ($profile = \Yii::$app->user->identity) {
            $profile->updateOnlineAt(true);

            if ($profile->blocked) {
                \Yii::$app->user->logout();
                \Yii::$app->response->redirect(['/profiles/profile/blocked', 'id' => $profile->id]);
                \Yii::$app->end();
            }
        }

        parent::init();
    }
}