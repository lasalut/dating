<?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \yii\mail\BaseMessage $content
 */
$font = "font-size: 13px; color: #333; font-family: Arial, Helvetica, sans-serif; background: white;";
?>
<?php $this->beginPage() ?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>"/>
		<title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
	</head>
	<body style="background: #c69c6d">

	<table style="background: #c69c6d; width: 100%; padding:40px 0;">
		<tr>
			<td>
				<table style="background:white; max-width:550px; margin: 0 auto; <?= $font ?>">
					<tr>
						<td style="padding:10px; border-bottom: 1px solid #c69c6d; text-align:center; <?= $font ?>">
							<img src="https://la-salut.ru/images/logo.jpg"/>
						</td>
					</tr>
					<tr>
						<td style="padding:20px 30px; <?= $font ?>"><?= $content ?></td>
					</tr>
					<tr>
						<td style="padding:20px 30px; border-top: 1px solid #c69c6d; <?= $font ?>">
							С уважением, <br/>
							Команда сайта LA-SALUT.RU
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

    <?php $this->beginBody() ?>
    <?php $this->endBody() ?>
	</body>
	</html>
<?php $this->endPage() ?>