<?php
namespace common\utils;

use modules\profiles\common\models\Premium;
use modules\profiles\common\models\PremiumHistory;
use modules\profiles\common\models\Profile;
use Yii;
use modules\sms\common\models\SmsLog;


class Notifier
{
    public static function profileRegistered(Profile $profile, $pw)
    {
        /** @var Premium $premiumStarter */
        if ($premiumStarter = Premium::findOne(['code' => Premium::CODE_STARTER])) {
            $expires = new \DateTime("+ {$premiumStarter->days} days");
            $expires = $expires->format('Y-m-d H:i:s');

            $h = new PremiumHistory();
            $h->profile_id = $profile->id;
            $h->premium_id = $premiumStarter->id;
            $h->price = $premiumStarter->price;
            $h->days = $premiumStarter->days;
            $h->hours = $premiumStarter->hours;
            $h->expires_at = $expires;
            $h->save(false);

            $profile->updateAttributes(['premium_date' => $expires]);
        }

        try {
            \Yii::$app->mailer->compose('@modules/profiles/frontend/email/registered', [
                'pw' => $pw,
                'profile' => $profile,
            ])->setSubject('Добро пожаловать на La-Salut')->setTo($profile->email)->send();
        }
        catch (\Exception $e) {
        }
    }

}