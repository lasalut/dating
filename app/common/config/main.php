<?php

return [
    'id' => 'yz2-app-standard',
    'language' => 'ru',
    'timeZone' => 'Europe/Moscow',
    'sourceLanguage' => 'en-US',
    'extensions' => require(YZ_VENDOR_DIR . '/yiisoft/extensions.php'),
    'vendorPath' => YZ_VENDOR_DIR,
    'bootstrap' => [
        'log',
    ],
    'components' => [
        'GlobalComponent' => ['class' => 'common\components\GlobalComponent'],
        'formatter' => [
            'dateFormat' => 'php:d.m.Y',
            'defaultTimeZone' => 'Europe/Moscow',
        ],
        'db' => [
            'class' => yii\db\Connection::class,
            'charset' => 'utf8',
            'dsn' => getenv('DB_DSN'),
            'username' => getenv('DB_USER'),
            'password' => getenv('DB_PASSWORD'),
            'tablePrefix' => getenv('DB_TABLE_PREFIX'),
            'attributes' => [PDO::MYSQL_ATTR_INIT_COMMAND => "SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));"]
        ],
        'i18n' => [
            'translations' => [
                'loyalty' => [
                    'class' => yii\i18n\PhpMessageSource::class,
                    'basePath' => '@common/messages',
                    'sourceLanguage' => 'ru-RU',
                ],
                'common' => [
                    'class' => yii\i18n\PhpMessageSource::class,
                    'basePath' => '@common/messages',
                    'sourceLanguage' => 'en-US',
                ],
                'frontend' => [
                    'class' => yii\i18n\PhpMessageSource::class,
                    'basePath' => '@frontend/messages',
                    'sourceLanguage' => 'en-US',
                ],
                'backend' => [
                    'class' => yii\i18n\PhpMessageSource::class,
                    'basePath' => '@backend/messages',
                    'sourceLanguage' => 'en-US',
                ],
                'console' => [
                    'class' => yii\i18n\PhpMessageSource::class,
                    'basePath' => '@console/messages',
                    'sourceLanguage' => 'en-US',
                ],
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => yii\log\FileTarget::class,
                    'levels' => ['error', 'warning', 'trace'],
                ],
            ],
        ],
        'mailer' => [
            'class' => \yii\swiftmailer\Mailer::class,
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'messageConfig' => [
                'from' => ['info@la-salut.ru' => 'La-Salut'],
            ],
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => 'info@la-salut.ru',
                'password' => 'afyeubopujsazpmo',
                'port' => 465,
                'encryption' => 'ssl',
            ],
        ],
        'sms' => [
            'class' => \marketingsolutions\sms\Sms::class,
            'services' => [
                'service' => [
                    'class' => \marketingsolutions\sms\services\Smsc::class,
                    'login' => getenv('SMSC_LOGIN'),
                    'password' => getenv('SMSC_PASSWORD'),
                    'from' => getenv('SMS_FROM'),
                ],
            ]
        ],
    ],
    'modules' => [
        'blog' => [
            'class' => modules\blog\common\Module::class,
        ],
        'pages' => [
            'class' => modules\pages\common\Module::class,
        ],
        'profiles' => [
            'class' => \modules\profiles\common\Module::class,
            'enableFreeRegistration' => true,
        ],
        'chat' => [
            'class' => modules\chat\common\Module::class,
        ],
        'photo' => [
            'class' => modules\photo\common\Module::class,
        ],
        'mailing' => [
            'class' => \yz\admin\mailer\common\Module::class,
            'mailLists' => [
                \modules\profiles\common\mailing\ProfileMailingList::class,
            ]
        ],
        'webrtc' => [
            'class' => modules\webrtc\common\Module::class,
        ],
        'sms' => [
            'class' => modules\sms\common\Module::class,
        ],
        'feedback' => [
            'class' => ms\loyalty\feedback\common\Module::class,
        ],
        'files-attachments' => [
            'class' => \ms\files\attachments\common\Module::class,
        ],
        'api' => [
            'class' => \ms\loyalty\api\common\Module::class,
            'authType' => \ms\loyalty\api\common\Module::AUTH_TOKEN,
            'hashSecret' => getenv('API_HASH_SECRET'),
            'tokenLiveMinutes' => getenv('API_TOKEN_LIVE_MINUTES'),
        ],
    ],
    'params' => [
        'adminEmails' => ['7binary@gmail.com'],
    ],
];
