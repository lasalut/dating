<?php

return [
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'components' => [
		'session' => [
			'name' => 'PHPBACKSESSID',
		],
        'request' => [
            'cookieValidationKey' => getenv('BACKEND_COOKIE_VALIDATION_KEY'),
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'admin/main/index',
                'login' => 'admin/main/login',
                'logout' => 'admin/main/logout',
                'profile' => 'admin/profile/index',
            ],
        ],
        'user' => [
            'identityClass' => '\yz\admin\models\User',
            'enableAutoLogin' => false,
            'loginUrl' => ['admin/main/login'],
            'on afterLogin' => ['\yz\admin\models\User', 'onAfterLoginHandler'],
        ],
        'authManager' => [
            'class' => \yz\admin\components\AuthManager::class,
        ],
        'errorHandler' => [
            'errorAction' => 'admin/main/error',
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => \yz\admin\Module::class,
            'allowLoginViaToken' => false,
        ],
        'filemanager' => [
            'class' => \yz\admin\elfinder\Module::class,
            'roots' => [
                [
                    'baseUrl' => '@frontendWeb',
                    'basePath' => '@frontendWebroot',
                    'path' => 'media/uploads',
                    'name' => 'Файлы на сайте',
                ]
            ]
        ],
        'mailing' => [
            'class' => \yz\admin\mailer\backend\Module::class,
        ],
        'manual' => [
            'class' => \ms\loyalty\bonuses\manual\backend\Module::class,
        ],
        'profiles' => [
            'class' => \modules\profiles\backend\Module::class,
        ],
        'pages' => [
            'class' => \modules\pages\backend\Module::class,
        ],
        'chat' => [
            'class' => \modules\chat\backend\Module::class,
        ],
        'photo' => [
            'class' => modules\photo\backend\Module::class,
        ],
		'sms' => [
			'class' => modules\sms\backend\Module::class,
		],
        'blog' => [
            'class' => modules\blog\backend\Module::class,
        ],
        'feedback' => [
            'class' => \ms\loyalty\feedback\backend\Module::class,
        ],
        'files-attachments' => [
            'class' => \ms\files\attachments\backend\Module::class,
        ],
        'api' => [
            'class' => \ms\loyalty\api\backend\Module::class,
        ],
    ],
    'params' => [

    ],
];
