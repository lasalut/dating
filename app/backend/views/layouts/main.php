<?php
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var string $content
 */

/**
 * By default this layout simply renders layout
 * of the admin module. But if you need some customizations
 * you can overwrite this file
 */
$this->registerCss('.main-header .logo { display:none } .extra-logo { max-height: 50px; vertical-align: top; margin-top: 5px; margin-left: 65px; }')
?>

<?php $this->beginBlock('header-extra-logo') ?>
	<img class="extra-logo" src="<?= getenv('FRONTEND_WEB') ?>/images/logo.png"/>
    <!-- Client logo here -->
<?php $this->endBlock() ?>
<?php $this->beginContent('@ms/loyalty/theme/backend/views/layouts/main.php'); ?>
<?= $content ?>
<?php $this->endContent() ?>
