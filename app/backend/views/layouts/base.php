<?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var string $content
 */

/**
 * By default this layout simply renders layout
 * of the admin module. But if you need some customizations
 * you can overwrite this file
 */

$css = <<<CSS
		body {
			background: url(/images/bg.jpg) no-repeat top left !important;
		}
		.b-login {
			position:   absolute;
			right:      45px;
			top:        40px;
			background: rgba(0, 0, 0, .3);
		}
		.b-login *, .b-login .btn:hover, .b-login .btn:focus, .b-login .btn:active {
			background-color: transparent !important;
			border-color:     white !important;
			color:            white !important;
		}
		.form-group.has-success .form-control {
			border: none !important;
		}
CSS;
$this->registerCss($css);

$this->beginContent('@ms/loyalty/theme/backend/views/layouts/base.php');
echo $content;
$this->endContent();
?>