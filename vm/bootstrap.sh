#!/usr/bin/env bash

# Custom params
ip=$(echo "$1")
frontend_port=$(echo "$2")
backend_port=$(echo "$3")
generated_key="something-rand0m"

apt-get update
if ! [ -L /var/www ]; then
    rm -rf /var/www
    ln -fs /vagrant /var/www
fi

# Correct Moscow date
rm -rf /etc/localtime
ln -s /usr/share/zoneinfo/Europe/Moscow /etc/localtime

# Install nginx
apt-get install -y nginx htop

# Install mysql
debconf-set-selections <<< "mysql-server mysql-server/root_password password root"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password root"
apt-get install -y mysql-server

# Install php-fpm
add-apt-repository -y ppa:ondrej/php && sudo apt-get update
apt-get install -y zip unzip imagemagick
apt-get install -y \
    php7.1-cli php7.1-fpm php7.1-mysql php7.1-curl php-memcached php7.1-dev \
    php7.1-mcrypt php7.1-sqlite3 php7.1-mbstring php7.1-gd php7.1-intl php7.1-xml \
    php7.1-mbstring php7.1-soap php7.1-gmp php7.1-zip php-zmq php-imagick

# In case of reinstall
# add-apt-repository -y ppa:ondrej/php && sudo apt-get update
# apt-get remove php*
# apt-get install -y php7.1-cli php7.1-fpm php7.1-mysql php7.1-curl php7.1-memcached php7.1-dev php7.1-mcrypt php7.1-sqlite3 php7.1-mbstring php7.1-gd php7.1-intl php7.1-xml php7.1-mbstring php7.1-soap php7.1-zip

apt-cache search php7.1

# php.ini
sed -i.bak 's/^;cgi.fix_pathinfo.*$/cgi.fix_pathinfo = 1/g' /etc/php/7.1/fpm/php.ini

service php7.1-fpm restart

# Creating database
cd /vagrant
mysql -u root -proot -e "create database if not exists yz2app";

# Configuring nginx
rm /etc/nginx/sites-enabled/default
cp vm/configs/nginx.conf /etc/nginx/sites-available/yz2app
ln -s /etc/nginx/sites-available/yz2app /etc/nginx/sites-enabled/yz2app

sed -i "s/<ip>/$ip/" /etc/nginx/sites-available/yz2app
sed -i "s/<frontend_port>/$frontend_port/" /etc/nginx/sites-available/yz2app
sed -i "s/<backend_port>/$backend_port/" /etc/nginx/sites-available/yz2app

# Setup environment
cp .env.dist .env
sed -i "s/<ip>/$ip/" .env
sed -i "s/<frontend_port>/$frontend_port/" .env
sed -i "s/<backend_port>/$backend_port/" .env
sed -i "s/<generated_key>/$generated_key/" .env

# Restart servers
service nginx restart
service php7.1-fpm restart
service mysql restart

# Configure application
cd /vagrant

# Setup project
php yii app/setup --interactive=0
php yii migrate --interactive=0
php yii admin-users/create-default-admin