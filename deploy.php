<?php
namespace Deployer;
/**
 * Deploying recipe for deployer utility
 *
 * NOTE: you should add the following permission to the sudo file (with visudo command)
 *
 * user_name ALL=(ALL) NOPASSWD: /usr/bin/setfacl
 */

require 'recipe/common.php';

serverList('deploy/servers.yml');
set('ssh_type', 'native');
set('ssh_multiplexing', true);

// Do not use sudo to setup writable folders.
// You should setup access to setfacl via sudoers file on the server
set('writable_use_sudo', false);

set('repository', 'git@bitbucket.org:7binary/dating.git');

set('shared_dirs', [
    'data',
    'app/frontend/runtime',
    'app/backend/runtime',
    'app/common/runtime',
    'app/console/runtime',
    'app/backend/web/media',
    'app/frontend/web/media',
]);

set('shared_files', [
    '.env',
]);

set('writable_dirs', [
    'data',
    'app/frontend/runtime',
    'app/backend/runtime',
    'app/common/runtime',
    'app/console/runtime',
    'app/backend/web/media',
    'app/frontend/web/media',
    'app/backend/web/assets',
    'app/frontend/web/assets',
]);

set('copy_dirs', [
//    'app/vendor',
]);

// Temp, while deployer does not have this feature in it's release
task('deploy:copy_dirs', function () {
    $dirs = get('copy_dirs');
    foreach ($dirs as $dir) {
        //Delete directory if exists
        run("if [ -d $(echo {{release_path}}/$dir) ]; then rm -rf {{release_path}}/$dir; fi");
        //Copy directory
        run("if [ -d $(echo {{deploy_path}}/current/$dir) ]; then cp -rpf {{deploy_path}}/current/$dir {{release_path}}/$dir; fi");
    }
})->desc('Copy directories');

/**
 * Migrate database
 */
task('database:migrate', function () {
    run('php {{release_path}}/yii migrate/up --interactive=0');
})->desc('Migrate database');

/**
 * Saving version of the application
 */
task('save_version', function () {
    run ('cd {{release_path}} && git rev-parse --short HEAD > .version');
})->desc('Saving version');

/**
 * Links from data to web
 */
task('deploy:links', function () {
	run('ln -sf {{deploy_path}}/shared/data/ {{release_path}}/app/backend/web/');
	run('ln -sf {{deploy_path}}/shared/data/ {{release_path}}/app/frontend/web/');
})->desc('Set links for data directory');

task('prepare', [
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:copy_dirs',
    'deploy:vendors',
	'deploy:links',
    'deploy:symlink',
    'cleanup'
])->desc('Prepare library, using first time deploy');

task('deploy', [
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:copy_dirs',
    'deploy:vendors',
    'database:migrate',
	'deploy:links',
    'save_version',
    'deploy:symlink',
    'cleanup'
])->desc('Deploy');

after('deploy', 'success');


